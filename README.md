# demos

#### 项目介绍
各种功能测试

1. ##  demo-dynamic-datasource 
	1. springboot mybatis 动态数据源
	2. 自定义注解自动切换
	3. 动态注册数据源
	*** 事务问题没有解决 ***
	
1. ## demo-mongo-file
	1. springboot spring-data-mongodb 
	2. mongodb 管理文件
	
1. ## demo-security
	1. 单应用security应用，自定义权限管理
	2. 主要类 ： SecurityInterceptorFilter,AuthenticationManager,AuthenticationProvider.....
	
2. ## quartz-demo 定时任务

3. ## redis-expired-demo redis到期提醒
    > 目前有一个问题,就是使用@Cacheable 注解时,除了需要缓存的结果,还会自动保存的一个 注解的value值+'~key'的这么一个缓存,当这个缓存到期 时,由于用的不是同一种Serializer,所以就会报错
    
4. ## security-cas-demo 
	security + cas 客户端 

5. ## cas-overlay-template-5.1 
	单点登陆服务器

6. ## cas-cilent-demo cas 
	单点登陆客户端 这个有bug ,不能单点登出，如果配合security 就可以
	
7. ## chat 
	springboot websocket + vue 

7. ## design-pattern
    这玩意讲课挺有用

7. ## rabbitMQ
	rabbitMQ demo 包括单独应用 与spring 集成

7. ## zookeeper
	一个监听的例子,没有深入研究,主要是为了学习 kafka打一些基础,明白zookeeper 的简单原理
	
8. ## kafka-producer-demo
    kafka的一些最粗浅的demo 学习kafka需要知道它大量的可配置项,通过长时间,大量的观察配置项更改后的效果才能明白如果使用kafka ,没有实战想学习kafka 会动力不足.加油吧

9. ## bean-copy-and-asm
	asm的一个小demo,不完善,主要是对java深层原理不理解,还包含一个复制对象的功能,功能通过,但是代码写的不完美,递归过程比较乱,没有处理数组
	
10. ## openjdk10-demo  
	函数式编程与Stream ,Reactor
	
16.  ## test-util

      一些测试的东西

17. ## spring-boot-feature
    
    1. 文件上传
	2. 自定义表单
	3. 自定义注解
	4. 自动分页插件
	5. rabbitmq
    
18.  hibernate-practice

     《hibernate 实战练习》
     
19. spring-jpa-practice

     spring-data-jpa 练习
