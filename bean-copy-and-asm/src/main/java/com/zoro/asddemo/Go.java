package com.zoro.asddemo;


import com.zoro.asddemo.democlass.Cat;
import com.zoro.asddemo.democlass.Dog;
import com.zoro.asddemo.democlass.Fish;
import com.zoro.asddemo.democlass.GoodBoy;
import com.zoro.asddemo.democlass.Student;
import com.zoro.asddemo.util.BeanCopyUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author zhaoxw
 * Create at 2018/8/29 11:52
 * @version 1.0
 */
public class Go {

    public static void main(String[] args) throws InvocationTargetException,
            IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InstantiationException {

        /*
        读取类的内容,不包含方法体,注解
         */
        /*ClassVisitor printer = new PrintClass();
        ClassReader reader = new ClassReader("com.zoro.asddemo.democlass.Student");
        reader.accept(printer, 0);*/

        /*
        生成类 接口
         */
        /*new GeneratClass().letsGo();*/

        /*
        修改类
         */
/*
        new TransformClass().letsGo();
*/

//        Method[] methods = Student.class.getMethods();
//        Field[] fields = Student.class.getFields();
//        for (int i = 0; i < fields.length; i++) {
//            System.out.println(fields[i].getName());
//            System.out.println(fields[i].getType().getName());
//        }
//        Student student = new Student();
//        Method friends = Student.class.getMethod("getFriends");
//        Method girls = Student.class.getMethod("getGrils");
//        Method hh = Student.class.getMethod("getHaHHH");
//        Method dog = Student.class.getMethod("getDao");
//        Type  fType = friends.getGenericReturnType();
//        System.out.println(fType.getTypeName());
//        Class fClass = Class.forName(fType.getTypeName());
//        Type  gType = girls.getGenericReturnType();
//        Class c = girls.getReturnType();
//        System.out.println(gType.getTypeName());
//        System.out.println(c.getComponentType().getTypeName());

//        System.out.println(Student.class);

//        Type hType = hh.getGenericReturnType();
//        System.out.println(hType.getTypeName());

//        Type dType = dog.getGenericReturnType();
//        Class gClass = Class.forName(gType.getTypeName());

        Student student = new Student();
        student.setAge(19);
        student.setName("小明");
        student.setSex("男");
        List<String> friends = new ArrayList<>();
        friends.add("张三");
        friends.add("sdsdf");
        student.setFriends(friends);


        List<Cat> cats = new ArrayList<>();
        cats.add(new Cat("maomao"));
        student.setCats(cats);

        List<List<Cat>> catss = new ArrayList<>();
        catss.add(cats);
        catss.add(cats);
        student.setCatss(catss);

        List<List<List<Cat>>> haha = new ArrayList<>();
        haha.add(catss);
        haha.add(catss);
        haha.add(catss);
        student.setCatsss(haha);


        List<List<String>> girls = new ArrayList<>();
        girls.add(friends);
        girls.add(friends);
        girls.add(friends);
        student.setGirlFriends(girls);
        List<Dog<String>> dogs = new ArrayList<>();
        dogs.add(new Dog<String>("wangwang"));
        dogs.add(new Dog<String>("gougou"));

        List<List<Dog<String>>> dogss = new ArrayList<>();
        dogss.add(dogs);
        dogss.add(dogs);
        student.setDogss(dogss);

        student.setDog(new Dog<Integer>("uyyuyuyuyuyuyuyu"));
        student.setBirth("1990--06-04");

        GoodBoy goodBoy = new GoodBoy();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 50; i++) {

            BeanCopyUtil.copy(student, goodBoy, true);
        }

        System.err.println(System.currentTimeMillis() - start);

      /*  long start2 = System.currentTimeMillis();
        for (int i = 0; i < 50; i++) {

            BeanCopyUtil.copy(student, goodBoy, false);
        }

        System.err.println(System.currentTimeMillis() - start2);
*/

//        String test = "java.util.List<java.util.List<java.lang.String>>";
//        String pattern = "<([\\w.]+)>";
//        Pattern p = Pattern.compile(pattern);
//        Matcher matcher = p.matcher(test);
//
//        test = test.replaceAll(pattern, "<>");
//
//
//
//        if (matcher.find()) {
//            for (int i = 0; i < matcher.groupCount(); i++) {
//                System.out.println(matcher.group(i));
//            }
//        }

    }

}

