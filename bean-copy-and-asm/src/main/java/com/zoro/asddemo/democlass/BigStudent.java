package com.zoro.asddemo.democlass;

/**
 * @author zhaoxw
 * Create at 2018/8/31 13:45
 * @version 1.0
 */
public class BigStudent {
    private String name;
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
