package com.zoro.asddemo.democlass;

/**
 * @author zhaoxw
 * Create at 2018/9/1 16:52
 * @version 1.0
 */
public class Cat {
    public String name;

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Cat{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
