package com.zoro.asddemo.democlass;

/**
 * @author zhaoxw
 * Create at 2018/8/31 17:58
 * @version 1.0
 */
public class Dog<T> {

    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Dog{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
