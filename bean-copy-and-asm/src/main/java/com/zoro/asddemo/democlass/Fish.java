package com.zoro.asddemo.democlass;

/**
 * @author zhaoxw
 * Create at 2018/9/3 9:39
 * @version 1.0
 */
public class Fish {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
