package com.zoro.asddemo.democlass;

/**
 * @author zhaoxw
 * Create at 2018/9/3 14:18
 * @version 1.0
 */
public class Pig<T> {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
