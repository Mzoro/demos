package com.zoro.asddemo.democlass;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/29 14:23
 * @version 1.0
 */
public class Student {

    private String name;
    private String sex;
    private int age;
    private List<String> friends;
    private List<List<String>> girlFriends;
    private List<Cat> cats;
    private List<List<List<Cat>>> catsss;
    private List<List<Cat>> catss;
    private List<List<Dog<String>>> dogss;
    private Dog<Integer> dog;
    private String birth;

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public Dog<Integer> getDog() {
        return dog;
    }

    public void setDog(Dog<Integer> dog) {
        this.dog = dog;
    }

    public List<List<Dog<String>>> getDogss() {
        return dogss;
    }

    public void setDogss(List<List<Dog<String>>> dogss) {
        this.dogss = dogss;
    }

    public List<List<String>> getGirlFriends() {
        return girlFriends;
    }

    public void setGirlFriends(List<List<String>> girlFriends) {
        this.girlFriends = girlFriends;
    }

    public void sayHi() {
        System.out.println("hi");
    }
    //
    public void sayHaHa() {
        System.out.println("haha");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public List<List<String>> g3etHaHHH() {
        return null;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }


    public List<List<List<Cat>>> getCatsss() {
        return catsss;
    }

    public void setCatsss(List<List<List<Cat>>> catsss) {
        this.catsss = catsss;
    }

    public List<Cat> getCats() {
        return cats;
    }

    public void setCats(List<Cat> cats) {
        this.cats = cats;
    }

    public List<List<Cat>> getCatss() {
        return catss;
    }

    public void setCatss(List<List<Cat>> catss) {
        this.catss = catss;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append("cats=").append(cats);
        sb.append(", catsss=").append(catsss);
        sb.append(", catss=").append(catss);
        sb.append('}');
        return sb.toString();
    }
}
