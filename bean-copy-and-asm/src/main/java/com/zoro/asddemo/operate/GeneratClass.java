package com.zoro.asddemo.operate;

import com.zoro.asddemo.MyClassLoader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.ACC_ABSTRACT;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;

/**
 * @author zhaoxw
 * Create at 2018/8/30 10:30
 * @version 1.0
 */
public class GeneratClass {

    public void letsGo() throws IOException {
        ClassWriter cw = new ClassWriter(0);
        cw.visit(V1_5, ACC_PUBLIC + ACC_ABSTRACT + ACC_INTERFACE, "com/zoro/asddemo/AsmHello", null,
                "java/lang/Object", null);

        cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC,
                "name",
                Type.getDescriptor(String.class), null, "yudi").visitEnd();
        cw.visitMethod
                (ACC_PUBLIC + ACC_ABSTRACT, "sayHello",
                        "(" + Type.getDescriptor(String.class) + ")" + Type.getDescriptor(String.class),
                        null, null).visitEnd();
        cw.visitEnd();
        byte[] b = cw.toByteArray();
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(b));
        BufferedReader br = new BufferedReader(isr);
        String line;
        line = br.readLine();
        while (line != null) {
            System.out.println(line);
            line = br.readLine();
        }

        Class hello = MyClassLoader.getLoader().defineClass("com.zoro.asddemo.AsmHello", b);

        System.out.println(hello.getName());
    }
}
