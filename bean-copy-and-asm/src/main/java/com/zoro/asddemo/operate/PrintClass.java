package com.zoro.asddemo.operate;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.Arrays;

/**
 * @author zhaoxw
 * Create at 2018/8/29 18:10
 * @version 1.0
 */
public class PrintClass extends ClassVisitor {
    public PrintClass() {
        super(Opcodes.ASM6);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        System.out.println("/*****************  visit() ********************/");
        System.out.println("version:" + version);
        System.out.println("access:" + access);
        System.out.println("name:" + name);
        System.out.println("signature:" + signature);
        System.out.println("superName:" + superName);
        System.out.println("interfaces:" + Arrays.toString(interfaces));
//        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public void visitSource(String source, String debug) {
        System.out.println("/*****************  visitSource() ********************/");
        System.out.println("source:" + source);
        System.out.println("debug:" + debug);
//        super.visitSource(source, debug);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[]
            exceptions) {
        System.out.println("/*****************  visitMethod() ********************/");
        System.out.println("access:" + access);
        System.out.println("name:" + name);
        System.out.println("descriptor:" + descriptor);
        System.out.println("signature:" + signature);
        System.out.println("exceptions:" + Arrays.toString(exceptions));
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        System.out.println("/*****************  visitField() ********************/");
        System.out.println("access:" + access);
        System.out.println("name:" + name);
        System.out.println("descriptor:" + descriptor);
        System.out.println("signature:" + signature);
        return super.visitField(access, name, descriptor, signature, value);
    }
}
