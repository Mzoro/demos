package com.zoro.asddemo.operate;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.ASM6;

/**
 * @author zhaoxw
 * Create at 2018/8/30 13:36
 * @version 1.0
 */
public class RemoveMethodAdaptor extends ClassVisitor {


    public RemoveMethodAdaptor(ClassVisitor classVisitor) {
        super(ASM6, classVisitor);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[]
            exceptions) {
        if ("sayHaHa".equals(name)) {
            return null;
        }
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }
}
