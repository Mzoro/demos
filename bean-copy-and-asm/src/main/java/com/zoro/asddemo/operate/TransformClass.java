package com.zoro.asddemo.operate;

import com.zoro.asddemo.MyClassLoader;
import com.zoro.asddemo.democlass.Student;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author zhaoxw
 * Create at 2018/8/30 13:21
 * @version 1.0
 */
public class TransformClass {

    public void letsGo() throws IOException, InterruptedException {

        ClassReader reader = new ClassReader("com.zoro.asddemo.democlass.Student");
        ClassWriter writer = new ClassWriter(0);
        ClassVisitor visitor = new RemoveMethodAdaptor(writer);
        reader.accept(visitor, 0);
        byte[] b = writer.toByteArray();

        //在一个新的classLoad中去取得类对象,如果new 的话,还是当前classLoad中的类对象
        Class c = MyClassLoader.getLoader().defineClass("com.zoro.asddemo.democlass.Student", b);
        for (Method m : c.getMethods()) {
            System.out.println(m.getName());
        }

        Thread.sleep(100);


        try {
            c.getMethod("sayHaHa").invoke(c.newInstance());
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException
                e) {
            e.printStackTrace();
        }

        new Student().sayHaHa();
    }
}
