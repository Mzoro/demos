package com.zoro.asddemo.util;

import net.sf.cglib.beans.BeanCopier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/8/31 13:02
 * @version 1.0
 */
public class BeanCopyUtil {

	private static final String GETTER_PREFIX = "get";
	private static final String SETTER_PREFIX = "set";
	private static final String BASE_PACKAGE = "com.zoro";

	private static final Logger LOGGER = LoggerFactory.getLogger(BeanCopyUtil.class);

	public static void copy(Object source, Object target) throws InvocationTargetException, IllegalAccessException,
			ClassNotFoundException, InstantiationException {
		copy(source, target, false);
	}

	public static void copy(Object source, Object target, boolean deep) throws InvocationTargetException,
			IllegalAccessException, ClassNotFoundException, InstantiationException {

		LOGGER.debug("BeanCopyUtil for {}", BASE_PACKAGE);

		if (source == null) {
			LOGGER.warn("source can not be NULL!");
//            throw new NullSourceException();
			return;
		}
		if (target == null) {
			LOGGER.error("target can not be NULL!");
//            throw new NullTargetException();
			return;
		}

		if (!deep) {
			BeanCopier.create(source.getClass(), target.getClass(), Boolean.parseBoolean(null)).copy(source, target,
					null);
		} else {
			Class sourceClass = source.getClass();
			List<Method> sourceMethods = getAllGetter(sourceClass);
			Class targetClass = target.getClass();
			Map<String, Method> targetMethods = getAllSetter(targetClass);
			for (Method m : sourceMethods) {
				Class returClass = m.getReturnType();
				String name = m.getName().replaceFirst(GETTER_PREFIX, "");
				Method setMethod = targetMethods.get(name);

				if (setMethod == null) {
					if (m.getDeclaringClass().getName().startsWith(BASE_PACKAGE)) {
						LOGGER.error("there is no set method for property called {}!", name);
//                        throw new NoSetMethodException();
					}
					continue;
				}
				if (isBasicType(returClass)) {
					//基本类型,简单赋值
					copyBasicProperty(m, source, setMethod, target);
				} else if (Collection.class.isAssignableFrom(returClass)) {
					copyCollectionPropertyValue(m, source, setMethod, target);
				} else if (returClass.isArray()) {
					//TODO 数组
				} else {
					//其他全部按照 自定义类型处理
					Object getValue = m.invoke(source);
					Object setParam = setMethod.getParameterTypes()[0].newInstance();
					if (returClass.getName().startsWith(BASE_PACKAGE)) {
						BeanCopyUtil.copy(getValue, setParam, true);
						setMethod.invoke(target, setParam);
					} else {
						try {

							setMethod.invoke(target, getValue);
						} catch (IllegalArgumentException e) {
							LOGGER.warn("get method return type is not same with set method parameter type");
						}
					}
				}
			}
		}

	}


	/**
	 * 是否是基本类型 java 中的基本类型 + void + String,或者Collection的泛型是基本类型
	 *
	 * @param clazz class
	 * @return true 是基本类型
	 * @see Boolean#TYPE
	 * @see Character#TYPE
	 * @see Byte#TYPE
	 * @see Short#TYPE
	 * @see Integer#TYPE
	 * @see Long#TYPE
	 * @see Float#TYPE
	 * @see Double#TYPE
	 * @see Void#TYPE
	 */
	private static boolean isBasicType(Class clazz) {
		return Boolean.class.isAssignableFrom(clazz) || Character.class.isAssignableFrom(clazz) ||
				Byte.class.isAssignableFrom(clazz) || Short.class.isAssignableFrom(clazz) ||
				Integer.class.isAssignableFrom(clazz) || Long.class.isAssignableFrom(clazz) ||
				Float.class.isAssignableFrom(clazz) || Double.class.isAssignableFrom(clazz) ||
				Void.class.isAssignableFrom(clazz) || clazz.isPrimitive() || String.class.isAssignableFrom(clazz);
	}

	/**
	 * 获取指定前缀的方法
	 *
	 * @param clazz 类对象
	 * @return 指定前缀的方法
	 */
	private static List<Method> getMethods(final Class clazz, final String prefix) {
		Method[] methods = clazz.getMethods();
		List<Method> getMethods = new ArrayList<>(16);
		for (Method m : methods) {
			if (m.getName().startsWith(prefix) && m.getName().length() > prefix.length()) {
				getMethods.add(m);
			}
		}
		return getMethods;
	}

	/**
	 * 获取所有 get方法
	 *
	 * @param clazz 类对象
	 * @return 所有get方法
	 */
	private static List<Method> getAllGetter(Class clazz) {
		return getMethods(clazz, GETTER_PREFIX);
	}

	/**
	 * 获取所有set方法,并以方法名去掉 set前缀为key,Method对象为value 保存在一个Map中
	 *
	 * @param clazz clazz 类对象
	 * @return 所有set方法
	 */
	private static Map<String, Method> getAllSetter(Class clazz) {
		List<Method> methods = getMethods(clazz, SETTER_PREFIX);

		Map<String, Method> methodMap = new HashMap<>();
		for (Method m : methods) {
			String name = m.getName();
			name = name.replaceFirst(SETTER_PREFIX, "");
			methodMap.put(name, m);
		}
		return methodMap;
	}

	/**
	 * 基本类型属性值复制
	 *
	 * @param getMethod source 的 get方法
	 * @param source    被复制的对象
	 * @param setMethod target 的 set方法
	 * @param target    复制后的对象
	 * @throws InvocationTargetException 调用方法异常
	 * @throws IllegalAccessException    调用方法异常
	 */
	private static void copyBasicProperty(Method getMethod, Object source, Method setMethod, Object target) throws
			InvocationTargetException, IllegalAccessException {
		try {
			Object value = getMethod.invoke(source);
			setMethod.invoke(target, value);
		} catch (IllegalAccessException | InvocationTargetException e) {
			LOGGER.error("invoke method {} or {} error!", getMethod.getName(), setMethod.getName());
			throw e;
		} catch (IllegalArgumentException e) {
			LOGGER.warn("get method return type is not same with set method parameter type:{}", getMethod.getName());
		}
	}


	/**
	 * @param getMethod source 的 get方法
	 * @param source    被复制的对象
	 * @param setMethod target 的 set方法
	 * @param target    复制后的对象onTargetException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 */
	private static void copyCollectionPropertyValue(Method getMethod, Object source, Method setMethod, Object target)
			throws InvocationTargetException, IllegalAccessException, ClassNotFoundException, InstantiationException {

		Object value = getMethod.invoke(source);
		if (value == null) {
			setMethod.invoke(target, value);
			return;
		}
		Type returnType = getMethod.getGenericReturnType();
		//默认set方法只有一个参数,否则会带来太多错误
		Type paramType = setMethod.getGenericParameterTypes()[0];
		List tempList = new ArrayList();
		if (returnType instanceof ParameterizedType && paramType instanceof ParameterizedType) {
			try {
				collectionResolve((ParameterizedType) returnType, (ParameterizedType) paramType, (Collection)
						value, setMethod, target, value, tempList);
				setMethod.invoke(target, tempList);
			} catch (TypeInconsistencyException | UnnecessaryException e) {
				LOGGER.debug(e.getMessage());
			}
		} else {
			copyBasicProperty(getMethod, source, setMethod, target);
		}
	}

	/**
	 * @param getType   get方法返回值类型
	 * @param setType   set方法参数类型
	 * @param value     本次处理的value
	 * @param setMethod set方法对象
	 * @param target    要复制的对象
	 * @param initValue get方法返回值
	 * @param temp      temp 就是复制的collection
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 * @throws TypeInconsistencyException
	 */
	private static void collectionResolve(ParameterizedType getType, ParameterizedType setType, Collection
			value, Method setMethod, Object target, Object initValue, Collection temp) throws IllegalAccessException,
			InstantiationException,
			InvocationTargetException,
			ClassNotFoundException, TypeInconsistencyException, UnnecessaryException {

		if (value == null) {
			return;
		}

		Type returnType = getType.getActualTypeArguments()[0];
		Type paramType = setType.getActualTypeArguments()[0];

		// set 与 get 的泛型依然是一个有泛型的
		if (returnType instanceof ParameterizedType && paramType instanceof ParameterizedType) {
			ParameterizedType returnTypeImpl = (ParameterizedType) returnType;
			ParameterizedType paramTypeImpl = (ParameterizedType) paramType;

			if (returnTypeImpl.getRawType() instanceof Class) {
				// set 与 get 的泛型的泛型依然是一个 Collection
				if (Collection.class.isAssignableFrom((Class) returnTypeImpl.getRawType())) {
					for (Object v : value) {
						List tempList = new ArrayList();
						collectionResolve(returnTypeImpl, paramTypeImpl, (Collection) v, setMethod, target,
								initValue, tempList);
						temp.add(tempList);
					}

				} else if (isBasicType((Class) returnTypeImpl.getRawType())) {
					// 有泛型 ,但不都是collection ,如果是 BasicType
					setMethod.invoke(target, initValue);
					throw new UnnecessaryException();
				} else {
					// 有泛型 ,但不都是collection ,如果不是 BasicType
					List tempList = new ArrayList();
					copyCustomerGanericCollection(returnType, paramType, value, setMethod, target, initValue, tempList);
					temp.add(tempList);
				}
			} else {
				LOGGER.error(" unknown type object! check {} method .", getType.toString());
				throw new RuntimeException("unknown type object!");
			}
		} else if (isBasicType((Class) returnType)) {
			//set 与 get 只要有一个没有泛型
			setMethod.invoke(target, initValue);
			throw new UnnecessaryException();
		} else {
			//TODO 还差数组没有处理
			// 自定义类型 Collection
			List tempList = new ArrayList();
			copyCustomerGanericCollection(returnType, paramType, value, setMethod, target, initValue, tempList);
			temp.addAll(tempList);
		}
	}

	/**
	 * 复制一个 get 方法返回值是下面这种形式的 List<User>
	 * get 方法的参数是下面这种形式的 List<People>
	 * <p>
	 * 为个方法是用在递归的情况,如果返回值是 List<List<User>> 这个方法会递归调用,这时 参数 <code>value</code>指的是本次需要复制的值 initValue指的是get方法最初始的值
	 *
	 * @param returnType get 方法返回值类型
	 * @param paramType  set 方法参数类型
	 * @param value      集合
	 * @param setMethod  set方法
	 * @param target     最外层的目标
	 * @param initValue  最开始的值
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 * @throws TypeInconsistencyException
	 */
	private static void copyCustomerGanericCollection(final Type returnType, final Type paramType, Collection value,
													  Method setMethod, Object target, Object initValue, Collection
															  tmpCollection) throws
			IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException,
			TypeInconsistencyException {


		Class returnClass;
		Class paramClass;
		if (returnType instanceof ParameterizedType) {
			returnClass = (Class) ((ParameterizedType) returnType).getRawType();
			paramClass = (Class) ((ParameterizedType) paramType).getRawType();
		} else if (returnType instanceof Class) {
			returnClass = (Class) returnType;
			paramClass = (Class) paramType;

		} else {

			LOGGER.error(" unknown type object! check {} method .", returnType.toString());
			throw new RuntimeException("unknown type object!");
		}
//        List targetCollection = new ArrayList();
		if (returnClass.getName().startsWith(BASE_PACKAGE) && paramClass.getName().startsWith
				(BASE_PACKAGE)) {
			LOGGER.debug("collection generic type is custom Type: {}", returnClass.getName());
			List list = new ArrayList();
			for (Object o : value) {
				Object targetObj = paramClass.newInstance();
				BeanCopyUtil.copy(o, targetObj, true);
				list.add(targetObj);
			}
			tmpCollection.addAll(list);
//            setMethod.invoke(target, targetCollection);
		} else {
			LOGGER.warn("SET-collection OR GET-collection generic is not belong {}", BASE_PACKAGE);
			setMethod.invoke(target, initValue);
			throw new TypeInconsistencyException();
		}
	}
}

