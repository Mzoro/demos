package com.zoro.asddemo.util;

/**
 * @author zhaoxw
 * Create at 2018/8/31 18:54
 * @version 1.0
 */
public class NoSetMethodException extends RuntimeException {
    public NoSetMethodException(){
        super("Can not find set Method!");
    }
}
