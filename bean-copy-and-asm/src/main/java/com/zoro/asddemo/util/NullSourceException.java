package com.zoro.asddemo.util;

/**
 * @author zhaoxw
 * Create at 2018/8/31 15:47
 * @version 1.0
 */
public class NullSourceException extends RuntimeException {

    public NullSourceException() {
        super("source can not be NULL");
    }
}
