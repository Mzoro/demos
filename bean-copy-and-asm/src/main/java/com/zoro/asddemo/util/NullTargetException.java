package com.zoro.asddemo.util;

/**
 * @author zhaoxw
 * Create at 2018/8/31 14:50
 * @version 1.0
 */
public class NullTargetException extends RuntimeException {

    public NullTargetException() {
        super("target can not be NULL");
    }
}
