package com.zoro.asddemo.util;

/**
 * @author zhaoxw
 * Create at 2018/9/3 10:17
 * @version 1.0
 */
public class TypeInconsistencyException extends Exception {

    public TypeInconsistencyException(){
        super("set method's return Type is not same with get method's parameter Type");
    }
}
