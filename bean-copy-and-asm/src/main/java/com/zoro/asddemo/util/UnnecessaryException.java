package com.zoro.asddemo.util;

/**
 * 这个Exception 不是说出现了哪些错误,只是为了方便直接跳出递归
 *
 * @author zhaoxw
 * Create at 2018/9/3 14:01
 * @version 1.0
 */
public class UnnecessaryException extends Exception {
    public UnnecessaryException(){
        super("This Collection Not Need Recursive !!");
    }
}
