package com.zoro.cascilentdemo.casconfigs;

/**
 * @author zoro
 * create in 2018/7/6
 */
public class CasConfig {

    public static final String CAS_SERVER_LOGIN_PATH="http://localhost:8443/cas/login" ;
    public static final String CAS_SERVER_PATH="http://localhost:8443/cas" ;
    public static final String SERVER_NAME="localhost" ;
}
