package com.zoro.cascilentdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zoro
 * create in 2018/7/6
 */
@RestController
public class TestController {

    @Autowired
    private Environment environment;

    @RequestMapping("/hello")
    public String hello() {
        return "hello-" + environment.getProperty("server.port");
    }
}
