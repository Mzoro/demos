package com.zoro.cas.server.security;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 19:09
 * @version: 1.0
 */
public class CustomPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] rpd = digest.digest(rawPassword.toString().getBytes());
            result = new String(Base64.encodeBase64(rpd));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        //应该看一下return false 会怎么样
        String rpd = encode(rawPassword);
        return rpd.equals(encodedPassword);
    }

}
