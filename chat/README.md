# demos

#### 一个 利用websocket 创建的聊天项目

##### spring websocket 使用不是很方便

	1. java方面,websocket 可以接收几类信息,常用的是TextMessage,也就是文本类型的,想要扩展不太方便
	2. websocket 连接状态不稳定,需要定时心跳
	3. js方面,他提供了4个事件,open,message,error,close; 两个方法,send,close
		send方法没有回调,不会知道消息发送是否成功,只能自己仿造,虽然有 监听error ,但是不能区分是哪个消息成功

#### TODO

	1. 发送心跳时,消息不显示在聊天窗口
	2. 接收消息后分发到指定的session中
	3. sessionId的获取
	4. sendMessage方法还有问题,这个方法除了消息内容应该多传必修参数,1.msgId,2.type (心跳消息-heart,还是用户自己要发给别人的消息-self);
	5. dispatch方法还没有写好,NEW-MESSAGE还没有处理