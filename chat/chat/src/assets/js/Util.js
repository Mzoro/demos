const util = {
  cloneObject: function (object) {

    if (this.isPrimaryType(object)) {
      return object
    } else {
      if (object instanceof Object) {
        let cloneObject = {};
        for (let o in object) {
          cloneObject[o] = this.cloneObject(object[o]);
        }
        return cloneObject;
      }
      if (object instanceof Array) {
        let copy = [];
        let len = object.length;
        for (let i = 0; i < len; i++) {
          copy[i] = this.cloneObject(object[i]);
        }
        return copy;
      }

      throw new Error("Unable to copy obj! Its type isn't supported.");
    }

  },
  isPrimaryType: function (object) {
    if (typeof object === 'undefined') {
      return true;
    }
    if (typeof object === 'boolean') {
      return true;
    }

    if (typeof object === 'number') {
      return true;
    }

    if (typeof object === 'string') {
      return true;
    }
    if (typeof object === 'object') {
      if (object == null) {
        return true;
      }
    }

    return false;
  },
  S4: function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  },
  guid: function () {
    return (this.S4() + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4());
  },
  //时间处理
  timeUpdate:function(time) {
    var date = new Date(time);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y+M+D+h+m+s;
  }
};

export default util;
export const guid = util.guid;
export const cloneObject = util.cloneObject;
export const timeUpdate = util.timeUpdate;




