function sessionConst() {
  const everyone = "everyone";
  return {
    everyone: everyone
  }
}

/**
 * 消息类型 , 这个类型应该在调用 sendMessage() 之前就放入消息对象中
 * @returns {{self: string, other: string, heart: string}}
 */
function typeConst() {
  //自己发送的
  const self = "self";
  //别人发来的
  const other = "other";
  //用来发送心跳,不在窗口显示
  const heart = "heart";

  return {
    self: self,
    other: other,
    heart: heart
  }
}

/**
 * 发送状态
 * @returns {{sending: number, ok: number, fail: number}}
 */
function sendStatu() {
  //发送中
  const sending = 1;
  //已经发送
  const ok = 2;
  //发送失败
  const fail = 3;

  return {
    sending: sending,
    ok: ok,
    fail: fail
  }

}

export default sessionConsts = sessionConst();
export let sessionConsts = sessionConst()
export let typeConsts = typeConst();
export let sendStatus = sendStatu();

