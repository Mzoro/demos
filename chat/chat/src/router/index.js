import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import ChatRoom from '@/components/chatroom/ChatRoom'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/index',
      name: 'index',
      component: Index
    }, {
      path: '/',
      redirect: {
        name: "index"
      }
    }, {
      path: '/chatroom',
      component: ChatRoom
    }
  ]
})
