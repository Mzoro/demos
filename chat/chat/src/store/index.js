import Vue from 'vue'
import vuex from 'vuex'
import user from "./user"
import socket from "./socket"

Vue.use(vuex);

export default new vuex.Store({
  modules: {
    user: user,
    socket:socket
  }
});
