export default {
  namespaced: true,
  state: {
    socket: null,
  },
  getters: {
    getSocket(state) {
      return state.socket
    },
    getSocketStatus(state) {
      if (!state.socket) {
        return false;
      }
      return state.socket.readyState == 1;

    }
  },
  mutations: {
    setSocket(state, payload) {
      state.socket = payload;
    },
    disconnect(state) {
      state.socket.close()
    }
  }
}
