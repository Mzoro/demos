export default {
  /*
  这是为了增加命名空间,为了不重名,但是这样做在调用是需要加上模块名,如果在需要调用其他modul的方法时需要多两个参数
  使用方法 $store.commit('user/setUser',{});
   */
  namespaced: true,
  state: {
    account: "",
    username: ""
  },
  mutations: {
    /*
       mutations 是用来更改 state的值的 方法  $store.commit('setUser',{});
       当不同文件的mutations 中包含同名的mutations时会全部调用一次,
       也可以 这样调用 $store.state.user.account = "";但是这样多了代码会过多,
       而且在面向对象时 会出现改失误修改对象内部值的问题,所有state中最好保存基本类型的值*/
    setUser(state, payload) {
      state.account = payload.account;
      state.username = payload.username;
    }
  },
  actions: {
    /*
    action就向一个适配器,可以调用多个mutation
    action 可以有异步操作,但是mutations不行
    使用方法 $store.dispatch('setUser',{})"
     */
    setUser(context, payload) {
      context.commit('setUser', payload);
    }
  },

  /*
    使用方法:$store.getters.not_show
   */
  getters: {
    getUser(state) {
      return {
        account: state.account,
        username: state.username
      }
    }
  }
}
