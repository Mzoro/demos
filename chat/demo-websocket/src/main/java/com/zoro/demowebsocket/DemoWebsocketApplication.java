package com.zoro.demowebsocket;

import com.zoro.demowebsocket.filters.LoginFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.Set;

/**
 * 启动类
 */
@SpringBootApplication
@MapperScan("com.zoro.demowebsocket.dao")
public class DemoWebsocketApplication {

    /**
     * 添加过滤器
     *
     * @return filters
     */
    @Bean
    public FilterRegistrationBean myFilter() {

        Set<String> ignore = new HashSet<>(16);
        ignore.add("/");
        ignore.add("/login.html");
        ignore.add("/socket/login");
        ignore.add("/favicon.ico");

        Set<String> ignorePattern = new HashSet<>(16);
        ignorePattern.add("^/js/.*");

        FilterRegistrationBean myFilter = new FilterRegistrationBean();
        myFilter.addUrlPatterns("/*");
        myFilter.setFilter(new LoginFilter(ignore,ignorePattern));
        return myFilter;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoWebsocketApplication.class, args);
    }
}
