package com.zoro.demowebsocket.consts;

/**
 * @author zhaoxw
 * Create at 2018/7/16 18:21
 * @version 1.0
 */
public class SessionAttName {
    /**
     * 当前登陆人
     */
    public static final String CURRENT_USER = "CURRENT_USER";
}
