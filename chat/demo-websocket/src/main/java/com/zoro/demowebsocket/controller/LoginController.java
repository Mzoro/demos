package com.zoro.demowebsocket.controller;

import com.zoro.demowebsocket.consts.SessionAttName;
import com.zoro.demowebsocket.entity.MessageCode;
import com.zoro.demowebsocket.entity.ResponseMessage;
import com.zoro.demowebsocket.entity.SysUser;
import com.zoro.demowebsocket.service.SysUserService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 登陆控制器
 *
 * @author zhaoxw
 * Create at 2018/7/16 17:46
 * @version 1.0
 */

@RestController
public class LoginController {

    @Resource
    private SysUserService sysUserService;

    @RequestMapping("/socket/login")
    public ResponseMessage login(HttpServletRequest request, @RequestParam("account") String account, @RequestParam
            ("password") String password) {
        SysUser sysUser = sysUserService.getByAccount(account);

        if (sysUser == null) {
            return new ResponseMessage(MessageCode.NOT_LOGIN);
        }

        if (password == null) {
            password = "";
        }

        if (!password.equals(sysUser.getPassword())) {
            return new ResponseMessage(MessageCode.PASSWORD_WRONG);
        }
        //不在session保存密码
        sysUser.setPassword("");
        request.getSession().setAttribute(SessionAttName.CURRENT_USER, sysUser);
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setData(sysUser);

        return responseMessage;
    }
}
