package com.zoro.demowebsocket.dao;

import com.zoro.demowebsocket.entity.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhaoxw
 * Create at 2018/7/16 17:56
 * @version 1.0
 */
public interface SysUserDao {

    /**
     * 通过 account 查找用户
     * @param account 账号
     * @return 查找结果
     */
    public SysUser getByAccount(@Param("account") String account);
}
