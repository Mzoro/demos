package com.zoro.demowebsocket.entity;

import java.util.Arrays;
import java.util.List;

/**
 * 消息类
 * @author zhaoxw
 * Create at 2018/7/18 19:18
 * @version 1.0
 */
public class ChatContent {

    /**
     * 消息内容
     */
    private String msg;

    /**
     * 消息的唯一识别,客户端产生就可以
     */
    private String msgId;

    /**
     * 这个某个会话,类似qq,可能同时与多人会话,这个是用来区分是哪个会话的;这个id应该是由会话的发起者发起会话时产生
     * TODO 需要写一个controller生产uuid
     */
    private String sessionId;

    /**
     * 需要发给谁
     */
    private List<String> accounts;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChatContent{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", msgId='").append(msgId).append('\'');
        sb.append(", sessionId='").append(sessionId).append('\'');
        sb.append(", accounts=").append(accounts);
        sb.append('}');
        return sb.toString();
    }
}
