package com.zoro.demowebsocket.entity;

/**
 * 返回给客户端的消息主体
 *
 * @author zhaoxw
 * Create at 2018/7/19 9:01
 * @version 1.0
 */
public class ChatResponse<T> {


    /**
     * 客户端会话id
     */
    private String sessionId;

    /**
     * 返回消息的类型:
     */
    private ChatResponseType type;

    /**
     * 消息id
     */
    private String msgId;

    /**
     * 需要返回的内容
     */
    private T msg;

    /**
     * 发消息的人
     */
    private SysUser sysUser;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public ChatResponseType getType() {
        return type;
    }

    public void setType(ChatResponseType type) {
        this.type = type;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChatResponse{");
        sb.append("sessionId='").append(sessionId).append('\'');
        sb.append(", type=").append(type);
        sb.append(", msgId='").append(msgId).append('\'');
        sb.append(", msg=").append(msg);
        sb.append(", sysUser=").append(sysUser);
        sb.append('}');
        return sb.toString();
    }

    public enum ChatResponseType {
        /**
         * 消息接收成功反馈
         */
        RECEIVED,
        /**
         * 新消息
         */
        NEW_MESSAGE,
        /**
         * 消息发送失败,因为没有回调,想要知识发送失败,并返回给消息发送人比较难
         */
        SEND_ERROR,
        /**
         * 返回用户列表
         */
        USER_COLLECTION,
        /**
         * 掉线通知
         */
        OUTLINE;

    }
}
