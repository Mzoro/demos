package com.zoro.demowebsocket.entity;

/**
 * 常用返回消息
 *
 * @author zhaoxw
 * Create at 2018/7/16 18:04
 * @version 1.0
 */
public enum MessageCode {

    /**
     * 操作成功
     */
    SECCUSSE(0, "操作成功"),
    /**
     * 用户名不存在
     */
    NOT_LOGIN(1, "用户名不存在"),
    /**
     * 密码错误
     */
    PASSWORD_WRONG(2, "密码错误");

    MessageCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
