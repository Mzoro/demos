package com.zoro.demowebsocket.entity;

/**
 * 消息返回对象
 *
 * @author zhaoxw
 * Create at 2018/7/16 17:47
 * @version 1.0
 */
public class ResponseMessage {
    /**
     * 消息内容
     */
    private String msg;

    /**
     * 代码
     */
    private int code;

    /**
     * 需要返回的数据
     */
    private Object data;

    public ResponseMessage(MessageCode messageCode) {
        this.code = messageCode.getCode();
        this.msg = messageCode.getMessage();
    }

    public ResponseMessage() {
        this.code = MessageCode.SECCUSSE.getCode();
        this.msg = MessageCode.SECCUSSE.getMessage();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResponseMessage{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", code=").append(code);
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }
}
