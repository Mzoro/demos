package com.zoro.demowebsocket.entity;

/**
 * 用户对象
 *
 * @author zhaoxw
 * Create at 2018/7/16 17:10
 * @version 1.0
 */
public class SysUser {

    /**
     * 登陆账号
     */
    private String account;

    /**
     * 用户名字
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SysUser{");
        sb.append("account='").append(account).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
