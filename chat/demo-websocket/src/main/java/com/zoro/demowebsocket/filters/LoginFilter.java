package com.zoro.demowebsocket.filters;

import com.zoro.demowebsocket.consts.SessionAttName;
import com.zoro.demowebsocket.entity.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * 非法访问拦截
 *
 * @author zhaoxw
 * Create at 2018/7/16 17:20
 * @version 1.0
 */
public class LoginFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginFilter.class);

    private final Set<String> ignoreUri;
    private final Set<String> ignorePattern;

    public LoginFilter(Set<String> ignoreUri, Set<String> ignorePattern) {
        this.ignorePattern = ignorePattern;
        this.ignoreUri = ignoreUri;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();

        if (isIgnore(uri)) {
            chain.doFilter(request, response);
        } else {
            SysUser sysUser = (SysUser) req.getSession().getAttribute(SessionAttName.CURRENT_USER);
            if (sysUser == null) {
                res.sendRedirect("/");
            } else {
                chain.doFilter(request, response);
            }
        }

    }

    @Override
    public void destroy() {

    }

    /**
     * 判断是否忽略
     *
     * @param uri 请求的uri;
     * @return true or false;
     */
    private boolean isIgnore(String uri) {
        if (ignoreUri.contains(uri)) {
            return true;
        }

        for (String p : ignorePattern) {
            if (Pattern.matches(p, uri)) {
                return true;
            }
        }

        return false;
    }
}
