package com.zoro.demowebsocket.service;

import com.zoro.demowebsocket.dao.SysUserDao;
import com.zoro.demowebsocket.entity.SysUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * SysUser相关业务处理类
 *
 * @author zhaoxw
 * Create at 2018/7/16 17:57
 * @version 1.0
 */
@Service
public class SysUserService {

    @Resource
    private SysUserDao dao;

    /**
     * 通过account 查找用户
     *
     * @param account 账号
     * @return 查找结果
     */
    public SysUser getByAccount(String account) {
        return dao.getByAccount(account);
    }
}
