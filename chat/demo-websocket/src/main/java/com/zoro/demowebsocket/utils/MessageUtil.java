package com.zoro.demowebsocket.utils;

import com.zoro.demowebsocket.entity.ChatContent;
import com.zoro.demowebsocket.entity.ChatResponse;

/**
 * @author zhaoxw
 * Create at 2018/7/19 9:13
 * @version 1.0
 */
public class MessageUtil {

    /**
     * 将 收到 的消息的sessionId与MsgId 复制到返回消息的内容中
     *
     * @param chatResponse 返回消息体
     * @param chatContent  收到的消息体
     */
    public static void copySessionIdAndMsgId(ChatResponse chatResponse, ChatContent chatContent) {
        chatResponse.setMsgId(chatContent.getMsgId());
        chatResponse.setSessionId(chatContent.getSessionId());
    }
}
