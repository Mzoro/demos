package com.zoro.demowebsocket.websocket;

import com.alibaba.fastjson.JSONObject;
import com.zoro.demowebsocket.consts.SessionAttName;
import com.zoro.demowebsocket.entity.ChatContent;
import com.zoro.demowebsocket.entity.ChatResponse;
import com.zoro.demowebsocket.entity.SysUser;
import com.zoro.demowebsocket.utils.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 消息处理器
 *
 * @author zhaoxw
 * Create at 2018/7/16 16:52
 * @version 1.0
 */
public class MessageHandler extends TextWebSocketHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);

    private static final ConcurrentMap<String, WebSocketSession> SESSIONS = new ConcurrentHashMap();
    private static final ConcurrentMap<String, SysUser> USERS = new ConcurrentHashMap();

    /**
     * 只要 客户端创建或发送消息就会调用这个方法,那么还需要Interceptor吗?
     * Interceptor 可以接收一个ServerServletRequest ,他可以得到Http session,从而得到登陆人
     * 客户端每次发消息都会产生一个新的session 来保存失效时间从当前这次发送消息来计算
     *
     * @param session session与Http session不一样
     * @throws Exception e
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        Map<String, Object> attr = session.getAttributes();
        SysUser sysUser = (SysUser) attr.get(SessionAttName.CURRENT_USER);
        LOGGER.info(sysUser.getUsername() + ":连接成功");

        ChatResponse<Collection> chatResponse = new ChatResponse();
        chatResponse.setSessionId("everyone");
        chatResponse.setType(ChatResponse.ChatResponseType.USER_COLLECTION);
        chatResponse.setMsg(USERS.values());
        session.sendMessage(new TextMessage(JSONObject.toJSONString(chatResponse)));

        SESSIONS.put(sysUser.getAccount(), session);
        USERS.put(sysUser.getAccount(), sysUser);

    }

    /**
     * 接收消息的处理类
     *
     * @param session socket session
     * @param message 消息主体
     * @throws Exception e
     */

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        LOGGER.info("收到消息" + message.getPayload().toString());
        ChatContent chatContent = JSONObject.parseObject(message.getPayload(), ChatContent.class);
        ChatResponse<String> chatResponse = new ChatResponse();
        chatResponse.setType(ChatResponse.ChatResponseType.RECEIVED);
        MessageUtil.copySessionIdAndMsgId(chatResponse, chatContent);
        //返回发送成功的消息
        session.sendMessage(new TextMessage(JSONObject.toJSON(chatResponse).toString()));

        //下面是需要将这条消息发给指定的人
        Collection<WebSocketSession> s = this.getSessionByAccounts(chatContent.getAccounts());
        chatResponse.setType(ChatResponse.ChatResponseType.NEW_MESSAGE);

        SysUser sysUser = (SysUser) session.getAttributes().get(SessionAttName.CURRENT_USER);
        chatResponse.setSysUser(sysUser);
        this.sendToUsers(s, new TextMessage(JSONObject.toJSONString(chatResponse)));
    }

    /**
     * 连接断开,如果客户端发送了 reason,那么这里可以得到
     *
     * @param session socket session
     * @param status  status
     * @throws Exception e
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        SysUser sysUser = (SysUser) session.getAttributes().get(SessionAttName.CURRENT_USER);
        SESSIONS.remove(sysUser.getAccount());
        USERS.remove(sysUser.getAccount());
        LOGGER.info(sysUser.getUsername() + "掉线,原因是:" + status.getReason());

    }

    /**
     * 错误处理
     *
     * @param session   socket session
     * @param exception e
     * @throws Exception e
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        LOGGER.info("transportError" + exception.getMessage());
        ChatResponse chatResponse = new ChatResponse();
        chatResponse.setType(ChatResponse.ChatResponseType.OUTLINE);
        session.sendMessage(new TextMessage(JSONObject.toJSONString(chatResponse)));
    }

    /**
     * 获取当前在线user
     *
     * @return 当前在线user
     */
    public Collection<SysUser> getOnlineUsers() {
        return USERS.values();
    }

    /**
     * 发送消息
     *
     * @param sessions    发送消息的session
     * @param textMessage 消息
     * @throws IOException e
     */
    private void sendToUsers(Collection<WebSocketSession> sessions, TextMessage textMessage) throws IOException {
        for (WebSocketSession session : sessions) {
            session.sendMessage(textMessage);
        }
    }

    /**
     * 通过account 取得 当前在线的session
     *
     * @param accounts 用户
     * @return sessions
     */
    private Collection<WebSocketSession> getSessionByAccounts(Collection<String> accounts) {
        Set<WebSocketSession> sessions = new HashSet<>();
        for (String account : accounts) {
            WebSocketSession session = SESSIONS.get(account);
            if (session != null && session.isOpen()) {
                sessions.add(SESSIONS.get(account));
            } else {
                //这里会有并发问题
                SESSIONS.remove(account);
                USERS.remove(account);
            }
        }
        return sessions;
    }


}
