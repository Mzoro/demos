package com.zoro.demowebsocket.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * webSocket配置
 *
 * @author zhaoxw
 * Create at 2018/7/16 16:30
 * @version 1.0
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(textWebSocketHandler(), "/socket/startChat").setAllowedOrigins("*").addInterceptors(new
                WebSocketInterceptor());
    }

    @Bean
    public TextWebSocketHandler textWebSocketHandler() {
        return new MessageHandler();
    }

    /**
     * ServletServerContainerFactoryBean可以添加对WebSocket的一些配置
     * @return
     */
//    @Bean
//    public ServletServerContainerFactoryBean createWebSocketContainer() {
//        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
//        container.setMaxTextMessageBufferSize(8192*4);
//        container.setMaxBinaryMessageBufferSize(8192*4);
//        return container;
//    }
}
