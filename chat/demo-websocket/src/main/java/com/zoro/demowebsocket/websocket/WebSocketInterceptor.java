package com.zoro.demowebsocket.websocket;

import com.zoro.demowebsocket.consts.SessionAttName;
import com.zoro.demowebsocket.entity.SysUser;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * socket拦截,在Handler调用 之前
 *
 * @author zhaoxw
 * Create at 2018/7/16 16:53
 * @version 1.0
 */
public class WebSocketInterceptor implements HandshakeInterceptor {

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler
            wsHandler, Map<String, Object> attributes) throws Exception {
        HttpSession session = getSession(request);
        if (session != null) {
            SysUser sysUser = (SysUser) session.getAttribute(SessionAttName.CURRENT_USER);
            if (sysUser != null) {
                //将当前登陆的用户保存在attributes中,
                //这个attributes是 WebSocketHandler类中所方法中 WebSocketSession参数的getAttributes方法的返回值
                attributes.put(SessionAttName.CURRENT_USER, sysUser);
            }
        }
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                               Exception exception) {
    }

    private HttpSession getSession(ServerHttpRequest request) {
        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest serverRequest = (ServletServerHttpRequest) request;
            return serverRequest.getServletRequest().getSession(false);
        }
        return null;
    }
}
