## 这种方案的特点
1. 同一个事务同是不能切换数据源的； 如果用了 ＠Transactional 直到方法结束都不会再调用 determineTargetDataSource()方法，所以数据源的key再怎么换也没有用
2. 因为1中的特点，多数据源切换带来的问题就是不能回滚
3. 在一个方法内调用同一个类的另一个方法 不会触发aop ,因为spring 注入的都是代理对象，也只有代理对象能触发AOP，而 this 指向的是真实对象；
比较简单的处理方式是向对象中注入一个本类的对象

```
@Service
public class TestService {

    @Autoware
    public TestService testService;
}
```