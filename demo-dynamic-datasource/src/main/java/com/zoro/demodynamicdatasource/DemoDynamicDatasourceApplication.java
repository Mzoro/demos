package com.zoro.demodynamicdatasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author zhaoxw
 */
@SpringBootApplication
@MapperScan("com.zoro.demodynamicdatasource.dao")
public class DemoDynamicDatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoDynamicDatasourceApplication.class, args);
    }
}
