package com.zoro.demodynamicdatasource.controller;

import com.zoro.demodynamicdatasource.entity.DataSourceInfo;
import com.zoro.demodynamicdatasource.service.DataSourceService;
import com.zoro.demodynamicdatasource.service.SysUserService;
import com.zoro.helloservice.entity.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author: zhaoxw
 * Create at: 2018/6/26 10:24
 * @version: 1.0
 */
@RestController
public class SysUserController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private DataSourceService dataSourceService;

    @RequestMapping("/hello")
    public SysUser hello(@RequestBody SysUser sysUser) {
        SysUser sysUser1 = sysUserService.getById(sysUser.getId());
        return sysUser1;
    }

    @RequestMapping("/getDataSourceInfo")
    public DataSourceInfo getDataSourceInfo(@RequestBody DataSourceInfo dataSourceInfo) {
        DataSourceInfo dataSourceInfo1 = dataSourceService.getDataSourceInfo(dataSourceInfo);
        SysUser sysUser1 = sysUserService.getById(1L);
        return dataSourceService.getDataSourceInfo(dataSourceInfo);
    }
}
