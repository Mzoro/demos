package com.zoro.demodynamicdatasource.customconfigs;


import com.zoro.demodynamicdatasource.dynamicdatasource.DataSourceCreator;
import com.zoro.demodynamicdatasource.dynamicdatasource.DataSourceDbService;
import com.zoro.demodynamicdatasource.dynamicdatasource.DynamicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * Description:
 *
 * @author zhaoxw
 * Create at: 2018/6/25 16:08
 * @version 1.0
 */
@Configuration
@PropertySource("classpath:configs/db.properties")
public class DataSourceConfig {

    /**
     * 注入主数据源，就是动态数据源；
     * 这个类中的DataSourceDbService属性不用set进去 ，可以使用注解直接注入；
     *
     * @return 主数据源
     * @throws IOException 找不到配置文件
     */
    @Bean
    public DataSource dataSource() throws IOException {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        DataSource dataSource = DataSourceCreator.createDataSource();
        dynamicDataSource.setMasterDataSource(dataSource);
        return dynamicDataSource;
    }

    /**
     * 注入一个去数据库数据数据源的类
     *
     * @return DataSourceDbService
     * @throws IOException 找不到配置文件
     */
    @Bean
    public DataSourceDbService dataSourceDbService() throws IOException {
        DataSourceDbService dataSourceDbService = new DataSourceDbService();
        dataSourceDbService.setDataSource(DataSourceCreator.createDataSource());
        return dataSourceDbService;
    }

}
