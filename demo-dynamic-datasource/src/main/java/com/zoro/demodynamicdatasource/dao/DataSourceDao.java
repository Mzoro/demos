package com.zoro.demodynamicdatasource.dao;

import com.zoro.demodynamicdatasource.entity.DataSourceInfo;
import org.apache.ibatis.annotations.Param;

/**
 * Description:
 *
 * @author: zhaoxw
 * Create at: 2018/6/26 11:01
 * @version: 1.0
 */
public interface DataSourceDao {

    /**
     * 通过数据源名字查询数据源信息
     *
     * @param dataSourceName 数据源信息
     * @return 数据源
     */
    DataSourceInfo getDataSourceByName(@Param("dataSourceName") String dataSourceName);

}
