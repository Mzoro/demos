package com.zoro.demodynamicdatasource.dao;

import com.zoro.helloservice.entity.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * Description:
 *
 * @author: zhaoxw
 * Create at: 2018/6/26 10:18
 * @version: 1.0
 */
public interface SysUserDao {

    /**
     * 通过Id查询数据
     *
     * @param id 用户id
     * @return 查询结果
     */
    SysUser getById(@Param("id") Long id);

}
