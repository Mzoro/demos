package com.zoro.demodynamicdatasource.dynamicdatasource;

/**
 * Description: 保存一些常量
 *
 * @author zhaoxw
 * Create at: 2018/6/25 14:48
 * @version 1.0
 */
public class Consts {

    /**
     * 默认数据源名字，springboot 对默认数据源的名字就是dataSource
     */
    public static final String MARSTER_DATASOURCE_NAME = "dataSource";

    static final String DATASOURCE_DB_URL = "jdbc.master.url";
    static final String DATASOURCE_DB_DRIVER_CLASSNAME = "jdbc.master.className";
    static final String DATASOURCE_DB_USERNAME = "jdbc.master.username";
    static final String DATASOURCE_DB_PASSWORD = "jdbc.master.password";

}
