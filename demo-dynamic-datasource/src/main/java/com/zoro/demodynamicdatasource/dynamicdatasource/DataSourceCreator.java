package com.zoro.demodynamicdatasource.dynamicdatasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.zoro.demodynamicdatasource.utils.PropertiesReader;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * Description: 创建数据源的工具类
 *
 * @author zhaoxw
 * Create at: 2018/6/26 11:24
 * @version 1.0
 */
public class DataSourceCreator {

    /**
     * 这个保存着默认数据源，保存这个类的createDataSource方法返回的DataSource对象是同一个对象
     *
     * @see {@link #createDataSource()}
     */
    private static DataSource dataSource;

    /**
     * 用于创建主数据源
     *
     * @return 主数据源
     * @throws IOException 配置文件问题
     */
    public static DataSource createDataSource() throws
            IOException {

        if (dataSource != null) {
            return dataSource;
        }

        Properties properties = PropertiesReader.getPropreties("configs/db.properties");
        String username = properties.getProperty(Consts.DATASOURCE_DB_USERNAME);
        String password = properties.getProperty(Consts.DATASOURCE_DB_PASSWORD);
        String url = properties.getProperty(Consts.DATASOURCE_DB_URL);
        String classname = properties.getProperty(Consts.DATASOURCE_DB_DRIVER_CLASSNAME);
        dataSource = createDataSource(classname, url, username, password);
        return dataSource;
    }

    /**
     * 创建数据源
     *
     * @param classname 数据驱动类
     * @param url       连接url
     * @param username  用户名
     * @param password  密码
     * @return 需要创建的数据源
     */
    static DataSource createDataSource(String classname, String url, String username, String password) {

        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        druidDataSource.setUrl(url);
        druidDataSource.setDriverClassName(classname);
        return druidDataSource;
    }
}
