package com.zoro.demodynamicdatasource.dynamicdatasource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Description: 查询数据源的操作类
 *
 * @author zhaoxw
 * Create at: 2018/6/25 15:48
 * @version 1.0
 */
@Service
public class DataSourceDbService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String SQL = "SELECT * FROM datasource_info WHERE datasourcename = ?";

    private DataSource dataSource;

    /**
     * 通过名字查询DataSource
     *
     * @param name 数据源名字
     * @return 目标数据源
     */
    DataSource getByName(String name) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        //不用while 只取第一条记录
        if (resultSet.next()) {
            String url = resultSet.getString(2);
            String className = resultSet.getString(3);
            String username = resultSet.getString(4);
            String password = resultSet.getString(5);
            logger.info(
                    "\nurl:{}\n" +
                            "className:{}\n" +
                            "username:{}\n" +
                            "password:{}", url, className, username, password);
            return DataSourceCreator.createDataSource(className, url, username, password);
        }

        return null;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
