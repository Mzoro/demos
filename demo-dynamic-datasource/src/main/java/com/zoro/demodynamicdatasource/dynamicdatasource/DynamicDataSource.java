package com.zoro.demodynamicdatasource.dynamicdatasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Description: 需要在加载时注入 DataSourceDbService
 *
 * @author zhaoxw
 * Create at: 2018/6/25 15:07
 * @version 1.0
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    /**
     * 主数据源
     */
    private DataSource masterDataSource;

    /**
     * 保存所有已经加载的数据源
     */
    private Map<String, DataSource> dataSources = new ConcurrentHashMap<>(16);

    /**
     * 查询数据库
     */
    @Autowired
    private DataSourceDbService dataSourceDbService;

    @Override
    protected String determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceName();
    }

    @Override
    protected DataSource determineTargetDataSource() {
        String lookupKey = this.determineCurrentLookupKey();
        if (lookupKey == null) {
            return masterDataSource;
        } else {
            try {
                return findDataSource(lookupKey);
            } catch (SQLException e) {
                e.printStackTrace();
                DynamicDataSourceContextHolder.removeOldDataSource();
                DynamicDataSourceContextHolder.removeDataSourceName();
                throw new RuntimeException("查询数据源信息错误");
            }
        }
    }

    /**
     * 加同步销是防止两个线程同时 dataSources.containsKey(lookupKey) 时都是 false;<br>
     * 但是其实没有什么影响,顶多就是重复创建数据源
     *
     * @param lookupKey 数据源名字
     * @return 目标数据源
     */
    private synchronized DataSource findDataSource(String lookupKey) throws SQLException {
        if (dataSources.containsKey(lookupKey)) {
            return dataSources.get(lookupKey);
        } else {
            DataSource dataSource = createDataSource(lookupKey);
            dataSources.put(lookupKey, dataSource);
            return dataSource;
        }
    }

    /**
     * 通过数据源创建数据源
     *
     * @param lookupKey 数据源名字
     * @return 目标数据源
     */
    private DataSource createDataSource(String lookupKey) throws SQLException {
        DataSource dataSource = dataSourceDbService.getByName(lookupKey);
        if (dataSource == null) {
            DynamicDataSourceContextHolder.removeOldDataSource();
            DynamicDataSourceContextHolder.removeDataSourceName();
            throw new RuntimeException("创建数据源失败");
        }
        return dataSource;
    }

    @Override
    public void afterPropertiesSet() {
    }

    public void setMasterDataSource(DataSource masterDataSource) {
        this.masterDataSource = masterDataSource;
    }

    public void setDataSourceDbService(DataSourceDbService dataSourceDbService) {
        this.dataSourceDbService = dataSourceDbService;
    }
}
