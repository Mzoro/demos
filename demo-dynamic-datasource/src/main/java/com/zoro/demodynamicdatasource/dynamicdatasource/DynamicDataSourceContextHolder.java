package com.zoro.demodynamicdatasource.dynamicdatasource;

/**
 * 保存当前需要使用的数据源的名字
 *
 * @author: zhaoxw
 * Create at: 2018/6/25 14:37
 */
public class DynamicDataSourceContextHolder {

    private static ThreadLocal<String> dataSourceName = new ThreadLocal<>();

    private static ThreadLocal<String> oldDataSourceName = new ThreadLocal<>();

    static void setDataSourceName(String nowSourceName) {
        DynamicDataSourceContextHolder.dataSourceName.set(nowSourceName);
    }

    static String getDataSourceName() {
        return DynamicDataSourceContextHolder.dataSourceName.get();
    }

    static void removeDataSourceName() {
        DynamicDataSourceContextHolder.dataSourceName.remove();
    }

    static void setOldDataSourceName(String oldDataSourceName) {
        DynamicDataSourceContextHolder.oldDataSourceName.set(oldDataSourceName);
    }

    static String getOldDataSourceName() {
        return DynamicDataSourceContextHolder.oldDataSourceName.get();
    }

    static void removeOldDataSource() {
        DynamicDataSourceContextHolder.oldDataSourceName.remove();
    }

    /**
     * 将数据源改这之前的数据源
     */
    static void resetDataSourceName() {
        String oldDataSource = DynamicDataSourceContextHolder.getOldDataSourceName();
        DynamicDataSourceContextHolder.removeOldDataSource();
        DynamicDataSourceContextHolder.setDataSourceName(oldDataSource);
    }
}
