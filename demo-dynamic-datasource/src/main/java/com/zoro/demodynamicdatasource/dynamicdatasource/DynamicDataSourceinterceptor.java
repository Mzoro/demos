package com.zoro.demodynamicdatasource.dynamicdatasource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Description:拦截带有TargetDataSource注解的方法
 * Order(-10) 保证该AOP在@Transactional之前执行 它是在这个类中定义的 org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration
 * @author zhaoxw
 * Create at: 2018/6/26 13:16
 * @version 1.0
 */
@Component
@Aspect
@Order(-10)
public class DynamicDataSourceinterceptor {

    @Before("@annotation(TargetDataSource)")
    public void changeDataSource(JoinPoint joinPoint) throws NoSuchMethodException {
        //获取切入的Method
        Signature signature = joinPoint.getSignature();
        //获取方法除了需要方法名,还需要参数类型的列表
        Object[] args = joinPoint.getArgs();
        Class clazz = joinPoint.getSignature().getDeclaringType();
        Class[] argClass = new Class[0];
        if (args != null) {
            argClass = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                Class p = args[i].getClass();
                argClass[i] = p;
            }
        }

        //获取方法
        Method method = clazz.getMethod(signature.getName(), argClass);
        TargetDataSource targetDataSource = method.getDeclaredAnnotation(TargetDataSource.class);

        //如果没有注解使用默认的
        if (targetDataSource == null) {
            DynamicDataSourceContextHolder.removeDataSourceName();
        } else {
            //有注解,切换数据源名字
            String targetDataSourceName = targetDataSource.target();
            String oldDataSourceName = DynamicDataSourceContextHolder.getDataSourceName();
            DynamicDataSourceContextHolder.setOldDataSourceName(oldDataSourceName);
            DynamicDataSourceContextHolder.setDataSourceName(targetDataSourceName);
        }

    }

    @After("@annotation(TargetDataSource)")
    public void resetDataSource() {
        DynamicDataSourceContextHolder.resetDataSourceName();
    }
}
