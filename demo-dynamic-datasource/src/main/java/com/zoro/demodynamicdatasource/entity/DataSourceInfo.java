package com.zoro.demodynamicdatasource.entity;

/**
 * Description:数据源信息类
 *
 * @author zhaoxw
 * Create at: 2018/6/26 10:55
 * @version 1.0
 */
public class DataSourceInfo {
    private String dataSourceName;
    private String url;
    private String className;
    private String username;
    private String password;

    public String getDataSourceName() {
        return dataSourceName;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DataSourceInfo{");
        sb.append("dataSourceName='").append(dataSourceName).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", className='").append(className).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
