package com.zoro.demodynamicdatasource.service;

import com.zoro.demodynamicdatasource.dao.DataSourceDao;
import com.zoro.demodynamicdatasource.dynamicdatasource.DynamicDataSourceContextHolder;
import com.zoro.demodynamicdatasource.dynamicdatasource.TargetDataSource;
import com.zoro.demodynamicdatasource.entity.DataSourceInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Description: 数据源业务处理类
 *
 * @author zhaoxw
 * Create at: 2018/6/26 13:03
 * @version 1.0
 */
@Service
public class DataSourceService {

    @Resource
    private DataSourceDao dataSourceDao;

    /**
     * 查询数据源
     *
     * @param dataSourceInfo 查询条件
     * @return 查询到的数据源
     */
    @TargetDataSource(target = "testDataSource")
    public DataSourceInfo getDataSourceInfo(DataSourceInfo dataSourceInfo) {
        return dataSourceDao.getDataSourceByName(dataSourceInfo.getDataSourceName());
    }
}
