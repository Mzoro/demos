package com.zoro.demodynamicdatasource.service;

import com.zoro.demodynamicdatasource.dao.SysUserDao;
import com.zoro.helloservice.entity.SysUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Description: 用户业务处理类
 *
 * @author zhaoxw
 * Create at: 2018/6/26 10:22
 * @version 1.0
 */
@Service
public class SysUserService {

    @Resource
    private SysUserDao sysUserDao;

    /**
     * 通过ID查询用户
     *
     * @param id 用户id
     * @return 查询结果
     */
    public SysUser getById(Long id) {
        return sysUserDao.getById(id);
    }
}
