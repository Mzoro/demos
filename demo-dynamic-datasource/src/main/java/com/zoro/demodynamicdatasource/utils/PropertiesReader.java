package com.zoro.demodynamicdatasource.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Description:读取Properties文件
 *
 * @author: zhaoxw
 * Create at: 2018/6/26 9:54
 * @version: 1.0
 */
public class PropertiesReader {

    private static ClassLoader classLoader;

    static {
        classLoader = PropertiesReader.class.getClassLoader();
    }

    /**
     * 获取Properties
     *
     * @param path 相对于当前工程的classpath
     * @return Properties
     * @throws IOException io 错误
     */
    public static Properties getPropreties(String path) throws IOException {
        InputStream inputStream = classLoader.getResourceAsStream(path);
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

}
