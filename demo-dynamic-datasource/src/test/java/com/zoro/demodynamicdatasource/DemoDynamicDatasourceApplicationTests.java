package com.zoro.demodynamicdatasource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoDynamicDatasourceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
