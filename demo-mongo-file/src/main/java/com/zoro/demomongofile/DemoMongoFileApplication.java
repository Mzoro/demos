package com.zoro.demomongofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMongoFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMongoFileApplication.class, args);
    }
}
