package com.zoro.demomongofile.controller;

import com.mongodb.client.gridfs.GridFSBucket;
import com.zoro.demomongofile.entity.MongoFiles;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 文件操作类
 * TODO 下载
 *
 * @author zoro
 * create in 2018/7/15
 */
@RestController
public class FilesController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private GridFSBucket gridFSBucket;

    /**
     * 文件上传
     *
     * @param file 文件流
     * @return 保存在mongodb的ObjectId
     * @throws IOException io异常
     */
    @RequestMapping("/upload")
    public String upload(MultipartFile file) throws IOException {
        InputStream inputStream = file.getInputStream();
        String fileName = file.getOriginalFilename();
        long size = file.getSize();

        if (size == 0) {
            return "";
        }

        ObjectId objectId = gridFSBucket.uploadFromStream(fileName, inputStream);
        return objectId.toString();
    }

    /**
     * 通过文件名字查找文件
     *
     * @param fileName 文件名字
     * @return 文件信息对象
     */
    @RequestMapping("/find")
    public List<MongoFiles> find(String fileName) {
        Pattern pattern = Pattern.compile("^.*" + fileName + ".*$", Pattern.CASE_INSENSITIVE);

        //注意这里一定要指定 collectionName,否则就会找不到想要的结果,这个应该与 配置的 MongoTemplate实例没有指定conllection有关
        List<MongoFiles> mongoFiles = mongoTemplate.find(new Query(Criteria.where("filename").regex(pattern)),
                MongoFiles.class, "fs.files");

        return mongoFiles;
    }

    /**
     * 删除文件
     *
     * @param id 需要删除文件的id
     * @return 文件id
     */
    @RequestMapping("/delete")
    public String delete(String id) {
        gridFSBucket.delete(new ObjectId(id));
        return id;
    }

    @RequestMapping("/download/{id}")
    public void dowload(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        MongoFiles mongoFiles = new MongoFiles();
        mongoFiles.setId(id);
        MongoFiles fileInfo = mongoTemplate.findOne(new Query(Criteria.where("id").is(new ObjectId(id))), MongoFiles
                .class, "fs.files");

        //修改文件关信息
        response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        response.setHeader("Content-Disposition",
                "attachment; filename=" + java.net.URLEncoder.encode(fileInfo.getFilename(), "UTF-8"));
        response.setHeader("Content-Length", String.valueOf(fileInfo.getLength()));

        //执行下载
        gridFSBucket.downloadToStream(new ObjectId(id), response.getOutputStream());
    }


}
