package com.zoro.demomongofile.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zoro
 * create in 2018/7/16
 */
@Document(collection = "fs.files")
public class MongoFiles {

    @Id
    private String id;
    @Field("filename")
    private String filename;
    @Field("length")
    private Long length;
    @Field("chunkSize")
    private Long chunkSize;
    @Field("uploadDate")
    private Date uploadDate;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public String getUploadDateStr() {
        return sdf.format(uploadDate);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(Long chunkSize) {
        this.chunkSize = chunkSize;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MongoFiles{");
        sb.append("id='").append(id).append('\'');
        sb.append(", filename='").append(filename).append('\'');
        sb.append(", length='").append(length).append('\'');
        sb.append(", chunkSize='").append(chunkSize).append('\'');
        sb.append(", uploadDate='").append(uploadDate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
