package com.zoro.demomongofile.mongoconfigs;


import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zoro
 * create in 2018/6/27
 */
@Configuration
public class MongoDbConfig {


    /**
     * 登陆mongodb 的认证信息
     *
     * @return mongodb 认证信息
     */
    @Bean
    public MongoCredential mongoCredential() {
        return MongoCredential.createCredential("mongo", "admin", "123".toCharArray());
    }

    /**
     * 返回的是MongoClient 他保存连接信息
     * @return MongoClient
     */
    @Bean
    public MongoClient mongo() {

        //mongodb 的服务器信息，ip,端口
        ServerAddress serverAddress = new ServerAddress("localhost", 27017);
        //mongodb 在开启密码时，需要下面的 MongoCredential，第一个参数是登陆时的用户名，第三个参数是认证的数据库，也就是--authenticationDatabase 的参数，第三个是密码

        List<MongoCredential> mongoCredentials = new ArrayList<>();
        mongoCredentials.add(mongoCredential());
        return new MongoClient(serverAddress, mongoCredentials);
    }

    /**
     * mongoDb 操作的模板类,对普通类型的 数据,MongoTemplate 可以完成增删改查的工作,对于文件就不太方便
     *
     * @return mongodbTemplate
     */
    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), "files");
    }

    /**
     * girdFSBucket主要用于对文件的下载与上传，mongoTemplate 操作文件不是特别方便
     *
     * @return girdFSBucket
     */
    @Bean
    public GridFSBucket gridFSBucket() {
        MongoDatabase mongoDatabase = mongo().getDatabase("files");
        return GridFSBuckets.create(mongoDatabase);
    }


}
