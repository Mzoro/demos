/*
 Navicat Premium Data Transfer
 Source Server         : localhost_57_9487
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:9487
 Source Schema         : ubuntu
 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001
 Date: 30/06/2018 16:55:15
*/
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `createTime` timestamp(0) NULL DEFAULT NULL,
  `creatorId` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'hihi', NULL, NULL);
INSERT INTO `sys_role` VALUES (2, 'haha', NULL, NULL);
-- ----------------------------
-- Table structure for sys_source
-- ----------------------------
DROP TABLE IF EXISTS `sys_source`;
CREATE TABLE `sys_source`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uri` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '加入权限的地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_source
-- ----------------------------
INSERT INTO `sys_source` VALUES (1, '/hi');
INSERT INTO `sys_source` VALUES (2, '/hello');
-- ----------------------------
-- Table structure for sys_source_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_source_role`;
CREATE TABLE `sys_source_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) NULL DEFAULT NULL,
  `sourceId` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_source_role
-- ----------------------------
INSERT INTO `sys_source_role` VALUES (1, 1, 1);
INSERT INTO `sys_source_role` VALUES (2, 2, 2);
-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id ',
  `account` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `createTime` timestamp(0) NULL DEFAULT NULL,
  `creatorId` bigint(20) NULL DEFAULT NULL,
  `updateTime` timestamp(0) NULL DEFAULT NULL,
  `updatorId` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '管理员', 'a4ayc/80/OGda4BO/1o/V0etpOqiLx1JwB5S3beHW0s=', '2018-01-01 00:00:00', 1, NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 'test2-account', '测试人员2', '2', '2018-04-20 06:55:30', 1, NULL, NULL);
INSERT INTO `sys_user` VALUES (3, 'test3-account', '测试人员3', '2', '2018-04-20 06:55:30', 1, NULL, NULL);
INSERT INTO `sys_user` VALUES (4, 'hello', 'hello', 'a4ayc/80/OGda4BO/1o/V0etpOqiLx1JwB5S3beHW0s=', NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (5, 'hi', 'hi', 'a4ayc/80/OGda4BO/1o/V0etpOqiLx1JwB5S3beHW0s=', NULL, NULL, NULL, NULL);
-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NULL DEFAULT NULL COMMENT 'sys_user.id',
  `roleId` bigint(20) NULL DEFAULT NULL COMMENT 'sys_role.id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户－角色－关系表' ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 5, 1);
INSERT INTO `sys_user_role` VALUES (2, 4, 2);
SET FOREIGN_KEY_CHECKS = 1;