package com.zoro.demosecurity.controller;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 *
 * @author zhaoxw
 * Create at: 2018/6/27 19:18
 * @version 1.0
 */
@Controller
@RequestMapping("/zoro")
public class LoginController {

    @Resource
    private AuthenticationManager authenticationManager = null;

    @RequestMapping("/login")
    public ModelAndView login(@RequestParam String username, @RequestParam String password) {

        System.err.println(username + ":" + password);

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authRequest);
        //下面4行是为了让security知道已经登陆，是谁登陆
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return null;
    }
}
