package com.zoro.demosecurity.dao;

import com.zoro.demosecurity.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/29 15:36
 * @version: 1.0
 */
public interface SysRoleDao {

    List<SysRole> getByUri(@Param("uri") String uri);

    List<SysRole> getByAccount(@Param("account") String account);
}
