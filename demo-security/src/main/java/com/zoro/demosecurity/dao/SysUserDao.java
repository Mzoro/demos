package com.zoro.demosecurity.dao;

import com.zoro.demosecurity.entity.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 17:41
 * @version: 1.0
 */
public interface SysUserDao {

    SysUser getByAccount(@Param("account") String account);
}
