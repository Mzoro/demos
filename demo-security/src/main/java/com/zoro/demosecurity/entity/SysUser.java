package com.zoro.demosecurity.entity;

import com.zoro.demosecurity.dao.SysRoleDao;
import com.zoro.demosecurity.dao.SysUserDao;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 17:42
 * @version: 1.0
 */
public class SysUser implements UserDetails {

    private static final String AUTH_PERFIX = "ROLE_";


    private Long id;
    private String account;
    private String username;
    private String password;

    private SysRoleDao sysRoleDao;

    public void setSysRoleDao(SysRoleDao sysRoleDao) {
        this.sysRoleDao = sysRoleDao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String getUsername() {
        return account;
    }

    public String getCunsomName() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        Set<GrantedAuthority> roles = new HashSet<>();
        List<SysRole> sysRoles = sysRoleDao.getByAccount(this.account);
        for (SysRole sysRole : sysRoles) {
            roles.add(new SimpleGrantedAuthority(sysRole.getRoleName()));
        }
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SysUser{");
        sb.append("id=").append(id);
        sb.append(", account='").append(account).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
