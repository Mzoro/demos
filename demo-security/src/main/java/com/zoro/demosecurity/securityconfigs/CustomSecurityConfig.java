package com.zoro.demosecurity.securityconfigs;

import com.zoro.demosecurity.service.DetalsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;
import java.util.Collections;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 17:55
 * @version: 1.0
 */
@EnableWebSecurity
public class CustomSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private MyAccessDecisionManager myAccessDecisionManager;

    @Resource
    private MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource;

    @Bean
    public DetalsServiceImpl detalsService() {
        return new DetalsServiceImpl();
    }

    @Bean
    public CustomPasswordEncoder customPasswordEncoder() {
        return new CustomPasswordEncoder();
    }

    /**
     * 这种方法是放开自定义登陆页和登陆处理器的权限,然后在登陆处理器中手动创建一个 UsernamePasswordAuthenticationToken,
     * 然后把这个 UsernamePasswordAuthenticationToken 交给 AuthenticationManager管理;
     * 然后用SecurityContextHolder.getContext().setAuthentication(AuthenticationManager管理);方法通知Security 是
     * 谁登陆
     *
     * @param http
     * @throws Exception
     */
/*    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                //自定义登陆页面
                .loginPage("/login.html")
                //自定义登陆的处理器
                .loginProcessingUrl("/zoro/login")
                .and()
                // 定义哪些URL需要被保护、哪些不需要被保护
                .authorizeRequests()
                // 设置所有人都可以访问登录页面
                .antMatchers("/login.html", "/zoro/login")
                .permitAll()
                // 设置访问指定路径的角色
                .mvcMatchers("/hi").hasAnyRole("hihi")
                .mvcMatchers("/hello").hasAnyRole("haha")
                // 任何请求,登录后可以访问
                .anyRequest()
                .authenticated()
                .and();
    }*/


    /**
     * @param http
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/zoro/login")
                .and()
                .authorizeRequests()
                .antMatchers("/login.html", "/favicon.ico", "/login", "/js/*.js", "/test.html").permitAll()
                .anyRequest().authenticated().and()
                //用自定义的登陆处理器代替默认的处理器,但是没有什么必要,它只是取到用户的用户名密码,验证还是由后面来完成,
                //除非是需要在Request中获得其他参数的可以特殊处理一下,可以处理下面这些类
                //AuthenticationDetailsSource,存放Details ,可以自定义一些认证信息,默认是SESSIONID
                //
                //TODO 自定义成功处理器,自定义认证失败处理器..
//                .addFilterAt(myUsernamePasswordAuthenticationFilter,
//                        UsernamePasswordAuthenticationFilter.class).csrf().disable();
                .addFilterBefore(filterSecurityInterceptor(), FilterSecurityInterceptor.class)
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(new MyAuthenticationEntryPoint("/login.html"));

    }

    @Override
    public void configure(WebSecurity webSecurity){
        webSecurity.ignoring().antMatchers("/js/**");
    }

    public MyFilterSecurityInterceptor filterSecurityInterceptor() {
        MyFilterSecurityInterceptor securityInterceptor = new MyFilterSecurityInterceptor();
        securityInterceptor.setMyFilterInvocationSecurityMetadataSource(myFilterInvocationSecurityMetadataSource);
        securityInterceptor.setAccessDecisionManager(myAccessDecisionManager);
        return securityInterceptor;
    }

    @Bean
    public MyAccessDecisionManager myAccessDecisionManager() {
        return new MyAccessDecisionManager();
    }

    @Bean
    public MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource() {
        return new MyFilterInvocationSecurityMetadataSource();
    }
}
