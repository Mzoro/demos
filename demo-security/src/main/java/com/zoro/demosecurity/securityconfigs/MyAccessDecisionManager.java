package com.zoro.demosecurity.securityconfigs;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/29 14:26
 * @version: 1.0
 */
public class MyAccessDecisionManager implements AccessDecisionManager {
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
            throws AccessDeniedException, InsufficientAuthenticationException {

        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AccessDeniedException("还没有登陆！");
        }

        if (configAttributes.contains(SecurityConsts.CUSTOMER_ROLE)) {
            return;
        }
        Collection<GrantedAuthority> suthorites = (Collection<GrantedAuthority>) authentication.getAuthorities();
        Iterator<GrantedAuthority> it = suthorites.iterator();
        while (it.hasNext()) {
            GrantedAuthority g = it.next();
            String ga = g.getAuthority();
            if (configAttributes.contains(new SecurityConfig(ga))) {
                return;
            }
        }
        throw new AccessDeniedException("对不起,你没有访问该页面的权限!");


    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
