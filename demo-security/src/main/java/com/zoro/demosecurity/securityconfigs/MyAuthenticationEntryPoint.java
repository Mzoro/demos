package com.zoro.demosecurity.securityconfigs;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.PortResolver;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.util.RedirectUrlBuilder;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/30 14:16
 * @version: 1.0
 */
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

    // ~ Static fields/initializers
    // =====================================================================================

    private static final Log logger = LogFactory
            .getLog(MyAuthenticationEntryPoint.class);

    private static final String AJAX_HEADER = "XMLHttpRequest";

    // ~ Instance fields
    // ================================================================================================

    private String loginFormUrl;

    private PortResolver portResolver = new PortResolverImpl();

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    MyAuthenticationEntryPoint(String loginFormUrl) {
        this.loginFormUrl = loginFormUrl;
        Assert.notNull(loginFormUrl, "loginFormUrl must be specified");
    }


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException
            authException) throws IOException, ServletException {
        if (isAjax(request)) {
            StringBuilder contentType = new StringBuilder(response.getContentType() == null ? "" : response
                    .getContentType());
            contentType.append("application/json;charset=UTF-8");
            response.setContentType(contentType.toString());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("msg", "没有登陆");
            response.getWriter().write(jsonObject.toJSONString());
        } else {
            String redirectUrl = buildRedirectUrlToLoginPage(request);
            redirectStrategy.sendRedirect(request, response, redirectUrl);
        }

    }

    private boolean isAjax(HttpServletRequest request) {
        String requestType = request.getHeader("X-Requested-With");
        return AJAX_HEADER.equals(requestType);
    }

    /**
     * 生成登陆url
     *
     * @param request request
     * @return 登陆路径
     */
    private String buildRedirectUrlToLoginPage(HttpServletRequest request) {

        if (UrlUtils.isAbsoluteUrl(loginFormUrl)) {
            return loginFormUrl;
        }

        int serverPort = portResolver.getServerPort(request);
        String scheme = request.getScheme();

        RedirectUrlBuilder urlBuilder = new RedirectUrlBuilder();
        urlBuilder.setScheme(scheme);
        urlBuilder.setServerName(request.getServerName());
        urlBuilder.setPort(serverPort);
        urlBuilder.setContextPath(request.getContextPath());
        urlBuilder.setPathInfo(loginFormUrl);

        return urlBuilder.getUrl();
    }
}
