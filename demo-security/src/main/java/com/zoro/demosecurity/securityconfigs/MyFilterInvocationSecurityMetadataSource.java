package com.zoro.demosecurity.securityconfigs;

import com.zoro.demosecurity.dao.SysRoleDao;
import com.zoro.demosecurity.entity.SysRole;
import com.zoro.demosecurity.entity.SysSource;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/29 14:06
 * @version: 1.0
 */
public class MyFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Resource
    private SysRoleDao sysRoleDao;

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {

        Collection<ConfigAttribute> configAttributes = new HashSet<>();

        FilterInvocation fi = (FilterInvocation) object;
        String uri = fi.getRequestUrl();
        List<SysRole> sysRoleList = sysRoleDao.getByUri(uri);
        if (sysRoleList == null) {
            //说明uri不在管理范围内,任务人都可以访问;
            configAttributes.add(SecurityConsts.CUSTOMER_ROLE);
            return configAttributes;
        }

        for (SysRole role : sysRoleList) {
            configAttributes.add(new SecurityConfig(role.getRoleName()));
        }
        return configAttributes;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {

        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
