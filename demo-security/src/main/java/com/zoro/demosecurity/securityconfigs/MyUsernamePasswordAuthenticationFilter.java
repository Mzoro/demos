package com.zoro.demosecurity.securityconfigs;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/28 12:55
 * @version: 1.0
 */
public class MyUsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String PARAMETER_USERNAME = "username";
    private static final String PARAMETER_PASSWORD = "password";

    protected AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new
            WebAuthenticationDetailsSource();

    public MyUsernamePasswordAuthenticationFilter() {
        super(new AntPathRequestMatcher("/zoro/login", "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws
            AuthenticationException, IOException, ServletException {

        System.err.println("怎么怎么不好使呢?");

        String username = request.getParameter(PARAMETER_USERNAME);
        String password = request.getParameter(PARAMETER_PASSWORD);


        if (username == null) {
            username = "";
        }

        if (password == null) {
            password = "";
        }
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
                username, password);

        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
        Authentication authentication = this.getAuthenticationManager().authenticate(authRequest);
        // TODO 这里就应该有一个认证失败的处理器,

        return authentication;

    }
}
