package com.zoro.demosecurity.securityconfigs;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/29 15:45
 * @version: 1.0
 */
public class SecurityConsts {

    /**
     * 超级权限
     */
    public static final ConfigAttribute SUPER_ROLE = new SecurityConfig("SUPER_ROLE");
    /**
     * 用户权限
     */
    public static final ConfigAttribute USER_ROLE = new SecurityConfig("USER_ROLE");
    /**
     * 游客权限
     */
    public static final ConfigAttribute CUSTOMER_ROLE = new SecurityConfig("CUSTOMER_ROLE");
}
