package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:00
 * @version 1.0
 */
public interface Command {

    void execute();
}
