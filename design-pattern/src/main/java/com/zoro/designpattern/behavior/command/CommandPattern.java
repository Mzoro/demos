package com.zoro.designpattern.behavior.command;

/**
 * 命令模式（Command Pattern）
 * 命令模式（Command Pattern）是一种数据驱动的设计模式，它属于行为型模式。请求以命令的形式包裹在对象中，并传给调用对象。调用对象寻找可以处理该命令的合适的对象，并把该命令传给相应的对象，该对象执行命令。
 * <p>
 * 桥接模式 : 一个接口的实现对象  持有 另一个接口的实现对象 并使用持有的对象去执行任务;这个接口的实现对象是另一个接口的实现对象与其接口的桥,解了接口与实现的耦
 * 命令模式 : 将桥对象放入别一个对象C中,去调用桥的方法;桥就是 command 命令对象,就是invoker 调用都
 * <p>
 * 这里有个电视机与遥控器的例子 ---- 方便理解
 * http://design-patterns.readthedocs.io/zh_CN/latest/behavioral_patterns/command.html
 * 电视机是请求的接收者，遥控器是请求的发送者，遥控器上有一些按钮，不同的按钮对应电视机的不同操作。
 * 抽象命令角色由一个命令接口来扮演，有三个具体的命令类实现了抽象命令接口，
 * 这三个具体命令类分别代表三种操作：打开电视机、关闭电视机和切换频道。显然，电视机遥控器就是一个典型的命令模式应用实例。
 * <p>
 * 这里有个例子  ----  方便理解如何解耦
 * https://www.cnblogs.com/konck/p/4199907.html
 * <p>
 * 命令模式的优点
 * 降低系统的耦合度。
 * 新的命令可以很容易地加入到系统中。
 * 可以比较容易地设计一个命令队列和宏命令（组合命令）。
 * 可以方便地实现对请求的Undo和Redo。
 * <p>
 * 命令模式的缺点
 * 使用命令模式可能会导致某些系统有过多的具体命令类。因为针对每一个命令都需要设计一个具体命令类，因此某些系统可能需要大量具体命令类，这将影响命令模式的使用。
 * <p>
 * 代码举例
 * 有一个Util 类的 watch方法 ,很多地方都会用到,1000多个地方法吧;
 * 有一天,老板说watch这个方法太次,改名,改成 see;
 * 如果使用传统的方式,那完了,改吧;
 * 如果watch这个方法都是通过 Command来调用的,那我们就只改Command就行了, command 用最笨的方法看,就是Util每个方法的包装,扩展点就可以是多个方法的包装,到这,与桥接都很像;
 * 但是命令模式还会把 command 传给一个叫 invoke的对象,他就专门来调用Command 的指定方法,命令模式这里比较关键,也是为什么称为命令的的原因
 * 因为invoke 不关心你 command 包装的是什么方法,可以是 usr对象的say()方法,或是car对象的drive()方法,都有可能,他只是调用 command 的那一个方法就完了
 * <p>
 * <p>
 * 但是这需要看情况使用,如果每个方法都写一个command ,那程序得有10800个类
 * 应该把不稳定的,需要根据需求长期变动的用一下
 * <p>
 * TODO 目前没有看到好的,有实际意义的代码 ;这个例子也没有太大意义,有众多其他模式可以解决命令模式解决的问题
 *
 * @author zhaoxw
 * Create at 2018/8/4 11:12
 * @version 1.0
 */
public class CommandPattern {
    public static void main(String[] args) {
        Received received = new Received();
        ReceivedCar receivedCar = new ReceivedCar();

        Command songCommand = new SongCommandImpl(received);
        Command sayCommand = new SayCommandImpl(received);
        Command carCommand = new DriveCommand(receivedCar);

        Invoke invoke = new Invoke();
        invoke.addCommand(sayCommand);
        invoke.addCommand(songCommand);
        invoke.addCommand(carCommand);

        invoke.run();
        //可以看到 invoke不关心具体是谁执行了,反正有人执行,这应该可以使一个类可以有强大的扩展性

    }


}
