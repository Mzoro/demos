package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:23
 * @version 1.0
 */
public class DriveCommand implements Command {

    private ReceivedCar receivedCar;

    public DriveCommand(ReceivedCar receivedCar) {
        this.receivedCar = receivedCar;
    }

    @Override
    public void execute() {
        this.receivedCar.drive();
    }
}
