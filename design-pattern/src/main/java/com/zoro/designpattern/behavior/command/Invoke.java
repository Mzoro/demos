package com.zoro.designpattern.behavior.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:15
 * @version 1.0
 */
public class Invoke {

    private List<Command> commands = new ArrayList<>(16);

    public void run() {
        for (Command c : commands) {
            c.execute();
        }
    }

    public void addCommand(Command command) {
        this.commands.add(command);
    }
}
