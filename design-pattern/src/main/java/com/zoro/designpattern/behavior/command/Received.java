package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:15
 * @version 1.0
 */
public class Received {

    public void say() {
        System.out.println("说的了一句话!!!");
    }

    public void song() {
        System.out.println("唱了一首歌!!!");
    }

}
