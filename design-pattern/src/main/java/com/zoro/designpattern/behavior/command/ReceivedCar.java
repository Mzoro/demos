package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:23
 * @version 1.0
 */
public class ReceivedCar {

    public void drive() {
        System.out.println("开车8公里!!!");
    }
}
