package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:21
 * @version 1.0
 */
public class SayCommandImpl implements Command {

    private Received received;

    public SayCommandImpl(Received received) {
        this.received = received;
    }

    @Override
    public void execute() {
        received.say();
    }
}
