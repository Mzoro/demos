package com.zoro.designpattern.behavior.command;

/**
 * @author zhaoxw
 * Create at 2018/8/4 14:19
 * @version 1.0
 */
public class SongCommandImpl implements Command {

    private Received received;

    public SongCommandImpl(Received received) {
        this.received = received;
    }

    @Override
    public void execute() {
        received.song();
    }
}
