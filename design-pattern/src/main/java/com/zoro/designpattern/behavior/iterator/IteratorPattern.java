package com.zoro.designpattern.behavior.iterator;

import java.util.ArrayList;

/**
 * 迭代器模式(Iterator Pattern)
 * 在 面向对象编程里，迭代器模式是一种设计模式，是一种最简单也最常见的设计模式。它可以让用户透过特定的接口巡访容器中的每一个元素而不用了解底层的实现。
 * <p>
 * 这粟子好举  {@link ArrayList.Itr}
 * http://www.runoob.com/design-pattern/iterator-pattern.html
 * 对比说一下,
 * {@link ArrayList.Itr} 就是例子中的Iterator
 * {@link ArrayList} 就是例子中的 container
 *
 * @author zhaoxw
 * Create at 2018/8/4 15:26
 * @version 1.0
 */
public class IteratorPattern {

    public static void main(String[] args) {
        Sequence sequence = new Sequence(5);
        for (int i = 0; i < 5; i++) {
            sequence.add(i);
        }

        Selector selector = sequence.selector();
        while (!selector.end()) {
            System.out.println(selector.current());
            selector.next();
        }
        System.out.println(sequence.test());
        System.out.println(sequence.test());
    }
}
