package com.zoro.designpattern.behavior.iterator;

/**
 * @author https://gitee.com/Mzoro
 */
public interface Selector {

    boolean end();

    Object current();

    void next();

}
