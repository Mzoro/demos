package com.zoro.designpattern.behavior.iterator;

/**
 * @author https://gitee.com/Mzoro
 */
public class Sequence {

    private Object[] items;
    private int next = 0;

    public Sequence(int size) {
        this.items = new Object[size];
    }

    public void add(Object o) {
        if (next < items.length) {
            this.items[next++] = o;
        }
    }

    public int test() {
        return new SequenceSelector().i;
    }

    public Selector selector() {
        return new SequenceSelector();
    }

    private class SequenceSelector implements Selector {

        private int i = 0;

        @Override
        public boolean end() {
            return i == items.length;
        }

        @Override
        public Object current() {
            return items[i];
        }

        @Override
        public void next() {
            if (i < items.length) {
                i++;
            }
        }
    }
}
