package com.zoro.designpattern.behavior.memento;


import com.zoro.designpattern.behavior.memento.redisdao.RedisDao;
import com.zoro.designpattern.behavior.memento.redisdao.RedisDaoCreator;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 备忘录模式(Memento Pattern)
 * 个人觉得,他不是软件设计模式,他是需要解决的一类问题,比如版本回滚
 * 关键点 每次操作都保存一次状态,并给保存的状态一个唯一标识,想后悔的时间用这个标记去找回当时的状态
 * 在面向对象的语言中借助redis比较好如果一个对象在内存中无论用什么改变都是一起改,除非使用 clone方法;
 * <p>
 *
 * @author zhaoxw
 * Create at 2018/8/4 15:55
 * @version 1.0
 */
public class MementoPattern {

    public static void main(String[] args) {


        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 这里类似 facade/factory
        RedisDaoCreator redisDaoCreator = new RedisDaoCreator("localhost", 6379, "", jedisPoolConfig);
        //用它来保存与获取对象历史状态
        RedisDao redisDao = redisDaoCreator.getRedisDao();


        List<String> keys = new ArrayList<>();
        String key;
        for (int i = 1; i <= 10; i++) {
            UserForMemento userForMemento = new UserForMemento();
            userForMemento.setAge(i * 10);
            key = UUID.randomUUID().toString();
            redisDao.setCacheEx(key, userForMemento, 10000);
            keys.add(key);
        }

        //去后悔去吧
        for (int i = 0; i <= 10; i++) {
            int r = (int) (Math.random() * 10);
            UserForMemento userForMemento = (UserForMemento) redisDao.getCache(keys.get(r));
            System.out.println("index : " + r + "; key : " + keys.get(r));
            System.out.println(userForMemento);
        }

    }
}
