package com.zoro.designpattern.behavior.memento;

import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/8/6 9:16
 * @version 1.0
 */
public class UserForMemento implements Serializable {

    private static final long serialVersionUID = 5952337369612846622L;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserForMemento{");
        sb.append("age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
