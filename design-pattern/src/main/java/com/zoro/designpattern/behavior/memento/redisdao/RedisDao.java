package com.zoro.designpattern.behavior.memento.redisdao;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 这个类其实也可以传入 {@link JedisConnectionFactory} 来获取 connection 然后set get
 *
 * @author zhaoxw
 * Create at 2018/8/4 16:22
 * @version 1.0
 */
public class RedisDao {

    private StringRedisTemplate redisTemplate;

    public RedisDao(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 不过期cache
     *
     * @param key   key
     * @param value value
     */
    public void setCache(String key, Object value) {
        redisTemplate.execute((RedisCallback<Object>) connection -> {
            connection.set(key.getBytes(), SerializerUtil.serializeObj(value));
            return null;
        });
    }

    /**
     * 有时长cache
     *
     * @param key   key
     * @param value value
     * @param ex    过期时长
     */
    public void setCacheEx(String key, Object value, long ex) {
        redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {

                connection.setEx(key.getBytes(), ex, SerializerUtil.serializeObj(value));
                return null;
            }
        });
    }

    /**
     * 通过key 获取 value
     *
     * @param key key
     * @return value
     */
    public Object getCache(String key) {
        return redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                byte[] value = connection.get(key.getBytes());
                return SerializerUtil.deserializeObj(value);
            }
        });
    }
}
