package com.zoro.designpattern.behavior.memento.redisdao;

import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;

/**
 * @author zhaoxw
 * Create at 2018/8/6 8:40
 * @version 1.0
 */
public class RedisDaoCreator {

    private JedisConnectionFactory jedisConnectionFactory;
    private StringRedisTemplate template;
    RedisDao redisDao;

    public RedisDaoCreator(String host, int port, String password, JedisPoolConfig jedisPoolConfig) {
        jedisConnectionFactory = new JedisConnectionFactory();
        JedisShardInfo jedisShardInfo = new JedisShardInfo(host, port);

        jedisConnectionFactory.setShardInfo(jedisShardInfo);
        jedisConnectionFactory.setDatabase(0);
        jedisConnectionFactory.setPassword(password);
        jedisConnectionFactory.setPoolConfig(jedisPoolConfig);

        template = new StringRedisTemplate();
        template.setConnectionFactory(jedisConnectionFactory);
        template.afterPropertiesSet();

        redisDao = new RedisDao(template);
    }

    public JedisConnectionFactory getJedisConnectionFactory() {
        return jedisConnectionFactory;
    }

    public StringRedisTemplate getTemplate() {
        return template;
    }

    public RedisDao getRedisDao() {
        return redisDao;
    }
}
