package com.zoro.designpattern.behavior.nullobject;

/**
 * 空对象模式 ( Null Object Pattern)
 * 他用一个空的,或者默认的属性的对象来代表一个对象;
 * 比如,我们数据库查询数据,没有查询到结果,但是调用者没有对空指针操作进行验证,这时候可以给个空的List,而不是null;
 * <p>
 * 这个如果学校考试就是一个送分题
 * <p>
 * 意思意思 来句场面话吧: 空对象模式 （null object Pattern）是一种软件设计模式。可以用于返回无意义的对象时，它可以承担处理null的责任。有时候空对象也被视为一种设计模式。  --- wiki
 *
 * @author zhaoxw
 * Create at 2018/8/6 18:42
 * @version 1.0
 */
public class NullObjectPattern {
}
