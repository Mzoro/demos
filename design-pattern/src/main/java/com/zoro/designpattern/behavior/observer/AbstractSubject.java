package com.zoro.designpattern.behavior.observer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 这个是目标对象的抽象类,他持有众多观察者
 * update()方法 将this 传给观察者,供观察者使用
 * {@link #addObserver(String, IObserver)}{@link #removeObserver(String)} 这两个方法就是建立观察都与被观察者的联系,因为所有的被观察都都是这个类的子类,
 * 所有我们只需要将观察都保存在这个类的List中就自动建立了两者的联系 ,当观察者与被观察者是多对多的关系,这样就可以了
 * <p>
 * {@link #updateData()}{@link #toObserver(String)} 这两个方法就是通知观察都被观察都已经更新
 * <p>
 * 这种方式相当于这个抽象类充当了控制中心的角色,他的子类是具体的数据.
 *
 * @author zhaoxw
 * Create at 2018/8/6 11:39
 * @version 1.0.
 */
public abstract class AbstractSubject {
    private Map<String, IObserver> observerMap = new HashMap<>(16);

    /**
     * 通知所有观察者
     */
    public void updateData() {
        Set<String> keys = observerMap.keySet();
        for (String k : keys) {
            System.out.println("/***************        "+k+"         *************/");
            observerMap.get(k).update(this);
        }
    }

    /**
     * 通知指定的观察者
     *
     * @param name 观察者的标识
     */
    public void toObserver(String name) {
        System.out.println("/****************       "+name+"           *****************/");
        IObserver observer = observerMap.get(name);
        if (observer != null) {
            observer.update(this);
        }
    }

    /**
     * 添加观察者
     *
     * @param name     观察者的名字
     * @param observer 观察者
     */
    public void addObserver(String name, IObserver observer) {
        observerMap.put(name, observer);
    }

    /**
     * 删除观察者
     *
     * @param name
     */
    public void removeObserver(String name) {
        observerMap.remove(name);
    }


}
