package com.zoro.designpattern.behavior.observer;


/**
 * @author zhaoxw
 * Create at 2018/8/6 11:38
 * @version 1.0
 */
public interface IObserver {

    void update(AbstractSubject subject);
}
