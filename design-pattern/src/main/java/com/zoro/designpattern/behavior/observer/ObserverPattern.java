package com.zoro.designpattern.behavior.observer;

import com.zoro.designpattern.behavior.observer.observerimpl.Parent;
import com.zoro.designpattern.behavior.observer.subjectimpl.Sun;

/**
 * 观察者模式 (Observer Pattern )
 * http://www.runoob.com/design-pattern/observer-pattern.html  ----基础
 * https://www.cnblogs.com/java-my-life/archive/2012/05/16/2502279.html  ----原理
 * https://quanke.gitbooks.io/design-pattern-java/%E8%A7%82%E5%AF%9F%E8%80%85%E6%A8%A1%E5%BC%8F-Observer%20Pattern.html
 * <p>
 * 　观察者模式所涉及的角色有：
 * 抽象主题(Subject)角色：抽象主题角色把所有对观察者对象的引用保存在一个聚集（比如ArrayList
 * 对象）里，每个主题都可以有任何数量的观察者。抽象主题提供一个接口，可以增加和删除观察者对象，抽象主题角色又叫做抽象被观察者(Observable)角色。
 * <p>
 * 具体主题(ConcreteSubject)角色：将有关状态存入具体观察者对象；在具体主题的内部状态改变时，给所有登记过的观察者发出通知。具体主题角色又叫做具体被观察者(Concrete Observable)角色。
 * <p>
 * 抽象观察者(Observer)角色：为所有的具体观察者定义一个接口，在得到主题的通知时更新自己，这个接口叫做更新接口。
 * <p>
 * 具体观察者(ConcreteObserver)角色：存储与主题的状态自恰的状态。具体观察者角色实现抽象观察者角色所要求的更新接口，以便使本身的状态与主题的状态
 * 像协调。如果需要，具体观察者角色可以保持一个指向具体主题对象的引用。
 * <p>
 * 注  1、JAVA 中已经有了对观察者模式的支持类。 2、避免循环引用。 3、如果顺序执行，某一观察者错误会导致系统卡壳，一般采用异步方式。
 * <p>
 * <p>
 * 本例中 为了解耦,将目标定义了接口,观察者定义了类;观察都没有定义为接口是因为观察都有一些行为是需要固定的不能由实现来决定
 * <p>
 * 观察者关键的地方 1. 有一个被观察的对象  2. 有众多观察者对象  3. 一个将 观察都与被观察者关联起来的过程 4. 一个通知观察都,被观察都已经变化的方法
 * 1.3.4  这三个部分混在一起,这是java 中 Observer的方式 ,但现在如果有这样一个需求: LOL 中任意一个队友被杀需要通知其他队友,那么 每个队友需要持有其他队友
 * 这时候就可以将 1.与3.4.分开;
 * 3.4.的工作可以相像成联络总线,或者游戏时的语音软件
 * <p>
 * <p>
 * 这个例子中 三个功能
 * 我们让父母观察儿子
 * 删除父亲观察者
 * 指定母亲观察
 *
 * @author zhaoxw
 * Create at 2018/8/6 11:11
 * @version 1.0
 */
public class ObserverPattern {

    public static void main(String[] args) {
        Sun sun = new Sun(100, "happy!!");
        Parent father = new Parent();
        Parent mather = new Parent();
        sun.addObserver("father", father);
        sun.addObserver("mother", mather);

        sun.setHight(105);
        sun.setHight(110);
        sun.setHight(120);

        sun.setMood("cry");

    }
}
