package com.zoro.designpattern.behavior.observer.observerimpl;

import com.zoro.designpattern.behavior.observer.AbstractSubject;
import com.zoro.designpattern.behavior.observer.IObserver;
import com.zoro.designpattern.behavior.observer.subjectimpl.Sun;

/**
 * @author zhaoxw
 * Create at 2018/8/6 13:46
 * @version 1.0
 */
public class Parent implements IObserver {
    @Override
    public void update(AbstractSubject subject) {
        Sun haha;
        if (!(subject instanceof Sun)) {
            return;
        }
        haha = (Sun) subject;
        System.out.println("care about you : \n" + haha.toString());
    }
}
