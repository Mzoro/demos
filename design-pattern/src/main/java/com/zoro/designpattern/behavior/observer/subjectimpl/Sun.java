package com.zoro.designpattern.behavior.observer.subjectimpl;

import com.zoro.designpattern.behavior.observer.AbstractSubject;

/**
 * @author zhaoxw
 * Create at 2018/8/6 13:39
 * @version 1.0
 */
public class Sun extends AbstractSubject {

    public Sun(int height, String mood) {
        this.hight = height;
        this.mood = mood;
    }

    /**
     * 身高
     */
    private int hight;

    /**
     * 情绪变化
     */
    private String mood;

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
        updateData();
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
        toObserver("mother");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Sun{");
        sb.append("hight=").append(hight);
        sb.append(", mood='").append(mood).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
