/**
 * 行为型模式
 * 责任链模式（Chain of Responsibility Pattern）{@link com.zoro.designpattern.behavior.responsebility.ChainOfResponsibility}
 * 命令模式（Command Pattern）{@link com.zoro.designpattern.behavior.command.CommandPattern}
 * 解释器模式（Interpreter Pattern）{@link com.zoro.designpattern.behavior.interpretor.InterpretorPattern}
 * 迭代器模式（Iterator Pattern）{@link com.zoro.designpattern.behavior.iterator.IteratorPattern}{@link java.util.ArrayList}
 * 中介者模式（Mediator Pattern）{@link com.zoro.designpattern.behavior.mediator.MediatorPattern}
 * 备忘录模式（Memento Pattern）{@link com.zoro.designpattern.behavior.memento.MementoPattern}
 * 观察者模式（Observer Pattern）{@link com.zoro.designpattern.behavior.observer.ObserverPattern}
 * 状态模式（State Pattern）{@link com.zoro.designpattern.behavior.status.StatusPattern}
 * <p>
 * <p>
 * 访问者模式（Visitor Pattern）{@link com.zoro.designpattern.behavior.visitor.VisitorPattern}
 * <p>
 * 下面三个有凑数的嫌疑
 * 空对象模式（Null Object Pattern）{@link com.zoro.designpattern.behavior.nullobject.NullObjectPattern}
 * 策略模式（Strategy Pattern） {@link com.zoro.designpattern.behavior.strategy.StrategyPattern}
 * 模板模式（Template Pattern）{@link com.zoro.designpattern.behavior.template.TemplatePattern}
 */
package com.zoro.designpattern.behavior;