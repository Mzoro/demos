package com.zoro.designpattern.behavior.responsebility;

import com.zoro.designpattern.structure.decorator.Transportation;

/**
 * @author zhaoxw
 * Create at 2018/8/4 10:25
 * @version 1.0
 */
public abstract class AbstractFilter implements IFilter {
    private IFilter nextfilter;

    @Override
    public IFilter addFilter(IFilter iFilter) {
        this.nextfilter = iFilter;
        return iFilter;
    }

    @Override
    public IFilter getFilter() {
        return this.nextfilter;
    }

    @Override
    public boolean doCheck(Transportation car) {
        return checkCar(car, this.nextfilter);
    }
}
