package com.zoro.designpattern.behavior.responsebility;

import com.zoro.designpattern.behavior.responsebility.filterimpl.FirstFilter;
import com.zoro.designpattern.behavior.responsebility.filterimpl.SecondFilter;
import com.zoro.designpattern.behavior.responsebility.filterimpl.ThirdFilter;
import com.zoro.designpattern.structure.decorator.impl.Car;

/**
 * 责任链模式(Chain of Responsibility Pattern )
 * 责任链模式在面向对象程式设计里是一种软件设计模式，它包含了一些命令对象和一系列的处理对象。
 * 每一个处理对象决定它能处理哪些命令对象，它也知道如何将它不能处理的命令对象传递给该链中的下一个处理对象。
 * 该模式还描述了往该处理链的末尾添加新的处理对象的方法。
 * 例 Servlet 的 Filter
 * <p>
 * 他与过滤器模式( Filter Pattern ){@link com.zoro.designpattern.structure.filter.FilterPattern}不同
 * 过滤器是筛子,主要任务是在众多对象中将不符合一个规则的对象与符合的分开
 * 责任链是汽车出厂前的检测线,每个责任链中的对象规则可能不同
 * 当然,也可以认为是描述的角度不同  一对多与多对一的关系,谁是多,谁是一要看目标是什么
 * <p>
 * <p>
 * 这个例子中 我们要过对一个汽车的最大里程,车灯寿命,最大载重进行检查:
 * 思路 :
 * 1 按顺序给 filter 加入下一个filter对象
 * 2 调用第一个 filter 的开始方法 ,如果通过 就调用下一个filter的 检查方法,并将下的个第三个filter方法传入
 * ******
 * 转弯的地方是 在检查过程中有三个 filter 对象,正在检查的对象,传入的下一个对象,下一个对象的下一个对象,这样才能连上,需要充分利用继承
 *
 * @author zhaoxw
 * Create at 2018/8/3 19:02
 * @version 1.0
 */
public class ChainOfResponsibility {
    public static void main(String[] args) {
        IFilter first = new FirstFilter();
        IFilter second = new SecondFilter();
        IFilter third = new ThirdFilter();

        //这部分有点像 build pattern,引导用户创建责任链
        first.addFilter(second).addFilter(third);

        for (int i = 0; i < 10; i++) {
            System.out.println("\n\n\n开始第     " + i + "    次");
            Car car = new Car();
            car.setLight((int) (Math.random() * 10));
            car.setLoad((int) (Math.random() * 10));
            car.setMilage((int) (Math.random() * 10));
            first.doCheck(car);
        }
    }
}
