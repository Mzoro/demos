package com.zoro.designpattern.behavior.responsebility;

import com.zoro.designpattern.structure.decorator.Transportation;

/**
 * 过滤器接口
 *
 * @author zhaoxw
 * Create at 2018/8/3 19:05
 * @version 1.0
 */
public interface IFilter {

    boolean checkCar(Transportation car, IFilter iFilter);

    IFilter addFilter(IFilter iFilter);

    IFilter getFilter();

    public boolean doCheck(Transportation car);
}
