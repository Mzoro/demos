package com.zoro.designpattern.behavior.responsebility.filterimpl;


import com.zoro.designpattern.behavior.responsebility.AbstractFilter;
import com.zoro.designpattern.behavior.responsebility.IFilter;
import com.zoro.designpattern.structure.decorator.Transportation;
import com.zoro.designpattern.structure.decorator.impl.Car;

/**
 * @author zhaoxw
 * Create at 2018/8/4 8:23
 * @version 1.0
 */
public class FirstFilter extends AbstractFilter {

    @Override
    public boolean checkCar(Transportation car, IFilter iFilter) {

        System.out.println("first:check light");

        if (!(car instanceof Car)) {
            return false;
        }
        Car c = (Car) car;
        System.out.println(c.getLight());
        if (c.getLight() < 5) {
            return false;
        } else {
            if (this.getFilter() != null) {
//                return iFilter.checkCar(car, iFilter.getFilter());
                return iFilter.doCheck(car);
            } else {
                return false;
            }
        }
    }
}
