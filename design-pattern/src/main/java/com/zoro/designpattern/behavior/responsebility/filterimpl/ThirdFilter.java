package com.zoro.designpattern.behavior.responsebility.filterimpl;

import com.zoro.designpattern.behavior.responsebility.AbstractFilter;
import com.zoro.designpattern.behavior.responsebility.IFilter;
import com.zoro.designpattern.structure.decorator.Transportation;
import com.zoro.designpattern.structure.decorator.impl.Car;

/**
 * @author zhaoxw
 * Create at 2018/8/4 10:35
 * @version 1.0
 */
public class ThirdFilter extends AbstractFilter {
    @Override
    public boolean checkCar(Transportation car, IFilter iFilter) {

        System.out.println("third:check milage");

        if (!(car instanceof Car)) {
            return false;
        }
        Car c = (Car) car;
        System.out.println(c.getMilage());
        if (c.getMilage() < 5) {
            return false;
        } else {
            if (this.getFilter() != null) {
                return iFilter.checkCar(car, iFilter.getFilter());
            } else {
                return false;
            }
        }
    }
}
