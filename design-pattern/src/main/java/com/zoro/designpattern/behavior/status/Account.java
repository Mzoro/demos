package com.zoro.designpattern.behavior.status;

import com.zoro.designpattern.behavior.status.impl.NormalState;
import com.zoro.designpattern.behavior.status.impl.OverdraftState;
import com.zoro.designpattern.behavior.status.impl.RestrictedState;

/**
 * @author zhaoxw
 * Create at 2018/8/6 17:02
 * @version 1.0
 */
public class Account {

    /**
     * 存款金额
     */
    private double balance;

    /**
     * 代表了状态
     */
    private AccountStatus accountStatus;


    /**
     * 存钱方法
     * 任何状态可以存
     *
     * @param money 要存多少钱
     * @return 存后全额
     */
    public double saveMoney(double money) {
        return accountStatus.saveMoney(money);
    }

    /**
     * 取钱
     * 需要
     *
     * @param money 需要存的金额
     * @return 存后余额
     */
    public double withdraw(double money) {
        return accountStatus.withdraw(money);
    }

    /**
     * 计算利息
     * 除了 NormalState状态的其他状态才计算利息
     *
     * @return 利息
     */
    public double computeInterest() {
        System.out.println("计算利息");
        return accountStatus.computeInterest();
    }


    /**
     * 更改状态
     */
    public void changeStatus() {
        if (this.balance > 0) {
            this.accountStatus = new NormalState(this);
        } else if (this.balance > -2000) {
            this.accountStatus = new OverdraftState(this);
        } else {
            this.accountStatus = new RestrictedState(this);
        }
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }


}
