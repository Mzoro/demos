package com.zoro.designpattern.behavior.status;

/**
 * @author zhaoxw
 * Create at 2018/8/6 17:02
 * @version 1.0
 */
public interface AccountStatus {

    /**
     * 这个状态对应的Account
     *
     * @return 对应的Account
     */
    Account getAccount();

    /**
     * 存钱
     *
     * @param money 在存多少
     * @return 存完有多少
     */
    double saveMoney(double money);

    /**
     * 取钱
     *
     * @param money 取多少
     * @return 取完有多少
     */
    double withdraw(double money);

    /**
     * 计算利息
     *
     * @return 利息
     */
    double computeInterest();

}
