package com.zoro.designpattern.behavior.status;

/**
 * 这是一个正常写法的 账户类,如果我增加一种状态 ,那么所有有关状态的方法都需要改,来吧,快改呀,反正有,大把时光
 *
 * @author zhaoxw
 * Create at 2018/8/6 16:19
 * @version 1.0
 */
public class OldAccount {

    /**
     * 存款金额
     */
    private double balance;

    /**
     * 当前状态
     * balance > 0              NormalState(正常状态,可存可取)
     * -2000 < balance <0       OverdraftState (此时用户既可以向该账户存款也可以从该账户取款，但需要按天计算利息)
     * balance < -2000          Restricted (此时用户只能向该账户存款，不能再从中取款，同时也将按天计算利息)
     */
    private String status;

    /**
     * 存钱方法
     * 任何状态可以存
     *
     * @param money 要存多少钱
     * @return 存后全额
     */
    public double saveMoney(double money) {
        this.balance = this.balance + money;
        System.out.println("目前全额:" + this.balance);
        changeStatus();
        return this.balance;
    }

    /**
     * 取钱
     * 需要
     *
     * @param money 需要存的金额
     * @return 存后余额
     */
    public double withdraw(double money) {
        if ("Restricted".equals(status)) {
            System.out.println(status + "不能取钱");
            return this.balance;
        }
        this.balance -= money;
        System.out.println("目前全额:" + this.balance);
        changeStatus();
        return this.balance;
    }

    /**
     * 计算利息
     * 除了 NormalState状态的其他状态才计算利息
     *
     * @return 利息
     */
    public double computeInterest() {

        if ("NormalState".equals(status)) {
            System.out.println(status + "不计算利息");
            return 0;
        }
        System.out.println("计算利息");
        return 0;
    }

    /**
     * 更改状态
     */
    public void changeStatus() {
        if (this.balance > 0) {
            this.status = "NormalState";
        } else if (this.balance > -2000) {
            this.status = "OverdraftState";
        } else {
            this.status = "Restricted";
        }
    }
}
