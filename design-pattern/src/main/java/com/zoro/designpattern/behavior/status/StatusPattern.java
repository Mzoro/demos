package com.zoro.designpattern.behavior.status;

/**
 * 状态模式 (Status Pattern)
 * 状态模式主要解决的问题是 一个类(Context)的方法,或者说一个对象的方法依赖这个对象的很多个状态;当这个方法运行时需要做大量的判断,比如if ...else if ... else if ...(省略80个) ... else
 * 这样如果多了一个状态,再处理 80个if 就比较恶心,如果n个方法依赖状态 呵呵.. 离职吧
 * 正常写法{@link OldAccount}
 * 他的关键代码是 每个 else if 创建一个实体类 ,将这个 else if 花括号内的逻辑写到这个实体类的方法内,一般这个方法与 80个 else if 的方法同名
 * 创建一个ContextStatus接口,创建多个实现, Context 对象中持有一个 ContextStatus,这个对象中也持有这个Context对象(相互持有),每次调用方法时,
 * 调用的是ContentStatus中对应的方法 ,ContentStatus中方法执行后改变他持有的ContentStatus对象
 * 例子中的写法 <pre>自己看吧!</pre>
 *
 * @author zhaoxw
 * Create at 2018/8/6 16:08
 * @version 1.0
 */
public class StatusPattern {
    public static void main(String[] args) {
        Account account = new Account();
        account.setBalance(500);
        account.changeStatus();
        account.saveMoney(5);
        account.withdraw(50000);
        account.withdraw(1);
        account.saveMoney(48000);
        account.withdraw(1);
    }
}
