package com.zoro.designpattern.behavior.status.impl;

import com.zoro.designpattern.behavior.status.Account;
import com.zoro.designpattern.behavior.status.AccountStatus;

/**
 * @author zhaoxw
 * Create at 2018/8/6 18:15
 * @version 1.0
 */
public class RestrictedState implements AccountStatus {

    private Account account;

    public RestrictedState(Account account) {
        this.account = account;
    }

    @Override
    public Account getAccount() {
        return this.account;
    }

    @Override
    public double saveMoney(double money) {
        System.out.println(this.getClass().getSimpleName());
        account.setBalance(account.getBalance() + money);
        account.changeStatus();
        return account.getBalance();
    }

    @Override
    public double withdraw(double money) {
        System.out.println(this.getClass().getSimpleName() + "欠的太多了");
        return account.getBalance();
    }

    @Override
    public double computeInterest() {
        System.out.println(this.getClass().getSimpleName() + "计算利息!!");
        return 0;
    }
}
