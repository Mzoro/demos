package com.zoro.designpattern.behavior.strategy;

/**
 * 策略模式(Strategy Pattern)
 * 有 很多种模式在写法上与策略模式相同,比如 command pattern 比它解耦解的还要好,比如 proxy ,只是书上说的所谓的目的不同,但是对我写代码,没有太大不同有可以我写出来了,没有人能说明白这到底是什么样的模式
 * 如果设计模式在写代码角度来说,还要看写代码的目的或依赖情况来区分是哪种模式,我觉得没有太大意义,不写了
 * 这就是考试时候的拉开分数的题,书呆子题
 * http://www.runoob.com/design-pattern/strategy-pattern.html
 *
 * @author zhaoxw
 * Create at 2018/8/6 18:55
 * @version 1.0
 */
public class StrategyPattern {
}
