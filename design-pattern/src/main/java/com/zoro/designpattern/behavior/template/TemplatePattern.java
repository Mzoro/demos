package com.zoro.designpattern.behavior.template;

/**
 * 模板模式 (Template Pattern)
 * 这个就是对继承的应用,说定义了一个接口或类,写一些默认的实现,我们继承这些类或接口,然后可以使用默认实现,也可以按需要扩展,我觉得GOF作者在出面试题
 *
 * @author zhaoxw
 * Create at 2018/8/6 19:01
 * @version 1.0
 */
public class TemplatePattern {
}
