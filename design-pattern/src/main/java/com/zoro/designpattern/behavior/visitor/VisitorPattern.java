package com.zoro.designpattern.behavior.visitor;

/**
 * 访问者模式 ( Visitor Pattern)
 * 他主要的代码是使用方法的重载 ,
 * 但为什么要这么写http://www.runoob.com/design-pattern/visitor-pattern.html,我觉得抽疯吧
 * 调用者传入一个对象A,再调用这个对象A的方法传入this(调用者); 再在A这个方法中调用调用者的方法,我还能这样写100000行
 *
 * @author zhaoxw
 * Create at 2018/8/6 19:13
 * @version 1.0
 */
public class VisitorPattern {
}
