package com.zoro.designpattern.creational.abstractfactory;

import com.zoro.designpattern.creational.abstractfactory.factories.DellFactory;
import com.zoro.designpattern.creational.abstractfactory.factories.HpFactory;
import com.zoro.designpattern.creational.abstractfactory.keyboards.DellKeyBoard;
import com.zoro.designpattern.creational.abstractfactory.keyboards.HpKeyBoard;
import com.zoro.designpattern.creational.abstractfactory.mouses.DellMouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.HpMouse;

/**
 * 抽象工厂模式
 * 对比工厂模式,抽象工厂模式多了一个超级工厂的概念 ,
 * 多了一个产品族的概念,比如 鼠标族中包含dell鼠标,hp鼠标;keyboard族中包含dell键盘,hp键盘(当然也可以按鼠标与键盘来分族)
 * 超级工厂会抽象两个方法 createMouse(),createKeyboard();
 * 他的实现类是工厂的角色 实现这两个方法;
 * 抽象工厂与工厂的区别在于抽象工厂可以创建同族的多种产品
 * <p>
 * Abstract Factory is often implemented with factory methods.
 * ——《设计模式》
 * <p>
 * 实例 -> 类 -> 类工厂
 * 实例 -> 类 -> 类工厂 -> 抽象工厂
 * <p>
 * 抽象工厂有一个缺点,就是一但高层接口更改,下面全部需要改,一但代码结构差异太大,就 不适用,比如 hp生产主板,打印机,但是dell没有,但是hp的factory依然要实现方法
 * 抽象工厂对比工厂,是为了保存同族产品之间的关系与表现是可控制的,比如,生成dell族的工厂生成的都是dell的,表现:dell的东西都不好,hp的就好;
 * 参考 https://zh.wikipedia.org/wiki/%E6%8A%BD%E8%B1%A1%E5%B7%A5%E5%8E%82
 * @author zhaoxw
 * Create at 2018/7/31 14:38
 * @version 1.0
 */
public class AbstractFactoryPattern {

    public static void main(String[] args) {
        Factory dellFactory = new DellFactory();

        Mouse dellmouse = new DellMouse();
        KeyBoard dellKeyBoard = new DellKeyBoard();
        say(dellmouse, dellKeyBoard);
        System.err.println("/**********************************/");

        Factory hpFactory = new HpFactory();
        Mouse hpMouse = new HpMouse();
        KeyBoard hpKeyboard = new HpKeyBoard();
        say(hpMouse, hpKeyboard);
        System.err.println("/**********************************/");
        System.err.println("下面看看是不是配套");
        System.err.println(dellKeyBoard.isAssort(dellmouse));
        System.err.println(dellKeyBoard.isAssort(hpMouse));

    }

    private static void say(Mouse mouse, KeyBoard keyBoard) {
        System.err.println(mouse.click());
        System.err.println(keyBoard.keyDown());
    }
}
