package com.zoro.designpattern.creational.abstractfactory;

/**
 * @author zhaoxw
 * Create at 2018/7/31 15:44
 * @version 1.0
 */
public interface Factory {

    /**
     * 生产鼠标
     *
     * @return 鼠标
     */
    Mouse createMouse();

    /**
     * 生产键盘
     *
     * @return 键盘
     */
    KeyBoard createKeyBoard();
}
