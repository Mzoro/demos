package com.zoro.designpattern.creational.abstractfactory;

/**
 * @author zhaoxw
 * Create at 2018/7/31 15:42
 * @version 1.0
 */
public interface KeyBoard {

    /**
     * 按一下键盘
     *
     * @return 没啥
     */
    String keyDown();

    /**
     * 是否配套
     *
     * @param mouse 鼠标
     * @return 是否配套
     */
    boolean isAssort(Mouse mouse);
}
