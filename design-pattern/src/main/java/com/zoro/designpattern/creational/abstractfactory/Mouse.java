package com.zoro.designpattern.creational.abstractfactory;

/**
 * @author zhaoxw
 * Create at 2018/7/31 15:41
 * @version 1.0
 */
public interface Mouse {

    /**
     * 点一下鼠标
     * @return
     */
    String click();

    int quality();
}
