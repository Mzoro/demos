package com.zoro.designpattern.creational.abstractfactory.factories;

import com.zoro.designpattern.creational.abstractfactory.Factory;
import com.zoro.designpattern.creational.abstractfactory.KeyBoard;
import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.keyboards.DellKeyBoard;
import com.zoro.designpattern.creational.abstractfactory.mouses.DellMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 16:03
 * @version 1.0
 */
public class DellFactory implements Factory {
    @Override
    public Mouse createMouse() {
        return new DellMouse();
    }

    @Override
    public KeyBoard createKeyBoard() {
        return new DellKeyBoard();
    }
}
