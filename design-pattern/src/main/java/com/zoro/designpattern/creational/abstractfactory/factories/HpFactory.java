package com.zoro.designpattern.creational.abstractfactory.factories;

import com.zoro.designpattern.creational.abstractfactory.Factory;
import com.zoro.designpattern.creational.abstractfactory.KeyBoard;
import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.keyboards.HpKeyBoard;
import com.zoro.designpattern.creational.abstractfactory.mouses.HpMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 16:04
 * @version 1.0
 */
public class HpFactory implements Factory {
    @Override
    public Mouse createMouse() {
        return new HpMouse();
    }

    @Override
    public KeyBoard createKeyBoard() {
        return new HpKeyBoard();
    }
}
