package com.zoro.designpattern.creational.abstractfactory.keyboards;

import com.zoro.designpattern.creational.abstractfactory.KeyBoard;
import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.DellMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 16:02
 * @version 1.0
 */
public class DellKeyBoard implements KeyBoard {



    @Override
    public String keyDown() {
        return "dell键盘按了一下";
    }

    @Override
    public boolean isAssort(Mouse mouse) {
        return mouse instanceof DellMouse;
    }
}
