package com.zoro.designpattern.creational.abstractfactory.keyboards;

import com.zoro.designpattern.creational.abstractfactory.KeyBoard;
import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.HpMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 15:59
 * @version 1.0
 */
public class HpKeyBoard implements KeyBoard {
    @Override
    public String keyDown() {
        return "hp键盘按了一下";
    }

    @Override
    public boolean isAssort(Mouse mouse) {
        return mouse instanceof HpMouse;
    }
}
