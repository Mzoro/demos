package com.zoro.designpattern.creational.abstractfactory.mouses;

import com.zoro.designpattern.creational.abstractfactory.Mouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 15:57
 * @version 1.0
 */
public class HpMouse implements Mouse {

    private int quality = (int) (Math.random() * 10);

    @Override
    public String click() {
        return "hp鼠标点了一下";
    }

    @Override
    public int quality() {
        return quality;
    }
}
