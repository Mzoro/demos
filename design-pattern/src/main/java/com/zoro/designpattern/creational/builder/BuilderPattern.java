package com.zoro.designpattern.creational.builder;

import com.zoro.designpattern.creational.builder.items.Chicken;
import com.zoro.designpattern.creational.builder.items.Coffe;

import java.util.List;

/**
 * 建造者模式(生成器) 例 : GenericResponse.class,quartz的schame jobdetail等
 * 以点餐为例 http://www.runoob.com/design-pattern/builder-pattern.html
 * 1. 首先定义 Item 接口,代表产品接口
 * 2. 定义包装 Packing 接口,他代表产品是什么样的包装
 * 3. 包装与产品的实现类若干
 * 4. 一个定单类 Order
 * 5. 一个定单的builder ,这个builder 创建的是 Order类的具体对象,builder 控制Order对象的产生过程
 * <p>
 * 优点:需要此对象的调用者不用考虑构建过程
 * 缺点:所有依赖接口完成的代码都有这个问题,顶层接口改变,会带来太多的改变
 *
 * @author zhaoxw
 * Create at 2018/8/1 11:38
 * @version 1.0
 */
public class BuilderPattern {
    public static void main(String[] args) {
        Order order = OrderBuidler.buidler().prepareOrder();
        say(order);

        Order order1 = OrderBuidler.buidler().addItem(new Chicken()).addItem(new Chicken()).addItem(new Coffe())
                .addItem(new Coffe()).build();
        say(order1);

    }

    private static void say(Order order) {
        System.err.println("订单内容:\n购买了");
        List<Item> items = order.getItems();
        for (Item item : items) {
            System.err.println(item.name() + "        " + item.price() + "元;    " + item.packing().name() + "包装,费用" +
                    (item.packing().isFree() ? 0 : item.packing().price()));
        }

        System.err.println(order.sumPrice());

    }
}
