package com.zoro.designpattern.creational.builder;

/**
 * 产品类
 * @author zhaoxw
 * Create at 2018/8/1 12:59
 * @version 1.0
 */
public interface Item {

    /**
     * 获取 Item 的 名字
     *
     * @return 产品名字
     */
    String name();

    /**
     * 包装类型
     *
     * @return 包装
     */
    Packing packing();

    /**
     * 产品价格
     *
     * @return 产品价格
     */
    double price();

}
