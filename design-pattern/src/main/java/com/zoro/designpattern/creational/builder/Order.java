package com.zoro.designpattern.creational.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * 定单类
 *
 * @author zhaoxw
 * Create at 2018/8/1 13:05
 * @version 1.0
 */
public class Order {

    private List<Item> items = new ArrayList<>(16);

    public void addItem(Item item) {
        items.add(item);
    }

    public List<Item> getItems() {
        return items;
    }

    /**
     * 合计
     *
     * @return 合计价格
     */
    public double sumPrice() {
        double sum = 0;
        for (Item i : items) {
            sum += i.price();
            if (!i.packing().isFree()) {
                sum += i.packing().price();
            }
        }
        return sum;
    }
}
