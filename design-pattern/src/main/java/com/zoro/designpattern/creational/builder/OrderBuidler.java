package com.zoro.designpattern.creational.builder;

import com.zoro.designpattern.creational.builder.items.Chicken;
import com.zoro.designpattern.creational.builder.items.Coffe;

/**
 * @author zhaoxw
 * Create at 2018/8/1 13:12
 * @version 1.0
 */
public class OrderBuidler {
    private Order order = new Order();

    private OrderBuidler() {
    }

    public static OrderBuidler buidler() {
        return new OrderBuidler();
    }

    public Order prepareOrder() {
        Order order = new Order();
        order.addItem(new Chicken());
        order.addItem(new Coffe());
        return order;
    }

    /**
     * 点餐方法,这个例子比较简单,一行代码完成任务,如果功能复杂,比如需要去操作数据库,需要构建更多的对象支持,这个方法就显示出来了;
     * 在这个例子中如果addItem 方法中需要去通知厨师,减去库存等操作,那么调用者就不用去考虑这些问题了,全由这个方法完成
     *
     * @param item 需要点的东西
     * @return build
     */
    public OrderBuidler addItem(Item item) {
        order.addItem(item);
        return this;
    }

    /**
     * 结单
     *
     * @return 结单
     */
    public Order build() {
        return this.order;
    }

}
