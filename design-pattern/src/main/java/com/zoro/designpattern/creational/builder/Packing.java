package com.zoro.designpattern.creational.builder;

/**
 * 包装类
 *
 * @author zhaoxw
 * Create at 2018/8/1 13:00
 * @version 1.0
 */
public interface Packing {

    /**
     * 包装名字
     *
     * @return name
     */
    String name();

    /**
     * 是否免费
     *
     * @return 免true;不免 false;
     */
    boolean isFree();

    /**
     * 包装价格
     *
     * @return 包装价格
     */
    double price();
}
