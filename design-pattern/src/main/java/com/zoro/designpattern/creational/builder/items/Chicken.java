package com.zoro.designpattern.creational.builder.items;

import com.zoro.designpattern.creational.builder.Item;
import com.zoro.designpattern.creational.builder.Packing;
import com.zoro.designpattern.creational.builder.packings.Paper;

/**
 * @author zhaoxw
 * Create at 2018/8/1 13:26
 * @version 1.0
 */
public class Chicken implements Item {
    @Override
    public String name() {
        return "chicken";
    }

    @Override
    public Packing packing() {
        return Paper.paper();
    }

    @Override
    public double price() {
        return 16.0;
    }
}
