package com.zoro.designpattern.creational.builder.items;

import com.zoro.designpattern.creational.builder.Item;
import com.zoro.designpattern.creational.builder.Packing;
import com.zoro.designpattern.creational.builder.packings.Bottle;

/**
 * @author zhaoxw
 * Create at 2018/8/1 13:39
 * @version 1.0
 */
public class Coffe implements Item {
    @Override
    public String name() {
        return "coffe";
    }

    @Override
    public Packing packing() {
        return Bottle.bottle();
    }

    @Override
    public double price() {
        return 8.0;
    }
}
