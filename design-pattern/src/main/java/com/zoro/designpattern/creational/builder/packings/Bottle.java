package com.zoro.designpattern.creational.builder.packings;

import com.zoro.designpattern.creational.builder.Packing;

/**
 * @author zhaoxw
 * Create at 2018/8/1 13:33
 * @version 1.0
 */
public class Bottle implements Packing {

    private static class BottleHolder {
        private static final Bottle BOTTLE = new Bottle();
    }

    public static Bottle bottle() {
        return BottleHolder.BOTTLE;
    }

    private Bottle() {

    }

    @Override
    public String name() {
        return "玻璃瓶包装";
    }

    @Override
    public boolean isFree() {
        return true;
    }

    @Override
    public double price() {
        return 0;
    }
}
