package com.zoro.designpattern.creational.builder.packings;

import com.zoro.designpattern.creational.builder.Packing;

/**
 * @author zhaoxw
 * Create at 2018/8/1 13:27
 * @version 1.0
 */
public class Paper implements Packing {

    private static class PaperHolder {
        private static final Paper PAPER = new Paper();
    }

    public static Paper paper() {
        return PaperHolder.PAPER;
    }

    private Paper() {
    }

    @Override
    public String name() {
        return "纸袋包装";
    }

    @Override
    public boolean isFree() {
        return false;
    }

    @Override
    public double price() {
        return 0.5;
    }
}
