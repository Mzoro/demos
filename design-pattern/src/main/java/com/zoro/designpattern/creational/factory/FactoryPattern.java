package com.zoro.designpattern.creational.factory;

import com.zoro.designpattern.creational.factory.factories.DellFactory;
import com.zoro.designpattern.creational.factory.factories.HpFactory;

/**
 * 工厂模式,也被称为工厂方法模式,他是简单工厂的升级
 * 产生理解三个概念:1,工厂接口;2,工厂实现;3,产品接口;4,产品实现
 * 举个栗子:
 * 工厂接口定义一个生产鼠标的接口,产品接口定义一个type()方法;
 * A工厂实现工厂接口的方法,只生产实现产品接口的A产品
 * B工厂实现工厂接口的方法,只生产实现产品接口的B产品
 * 当使用时如果需要A产品,就创建A工厂实例,需要B产品就创建B工厂实例
 * <p>
 * 这里网上就有多种实现方法了,
 * 有些会创建一个工厂的简单工厂(这种方式会弱化工厂的解耦特性,如果需要添加工厂,还需要修改工厂的工厂),
 * 有些就会直接new 一个具体的工厂(下面用这种方式),
 * 但是最重要的与简单工厂的区别是,
 * 工厂模式会针对每种产品有一个具体的工厂,例:dbcp,druid,c3p0的DataSource
 * <p>
 * 工厂“方法”而非工厂“类”
 * 如果抛开设计模式的范畴，“工厂方法”这个词也可以指作为“工厂”的方法，这个方法的主要目的就是创建对象，而这个方法不一定在单独的工厂类中(也就是说可以有一个类有多个方法来创建不同的产品)。
 * 这些方法通常作为静态方法，定义在方法所实例化的类中。
 * 每个工厂方法都有特定的名称。在许多面向对象的编程语言中，构造方法必须和它所在的类具有相同的名称，
 * 这样的话，如果有多种创建对象的方式（重载）就可能导致歧义。工厂方法没有这种限制，所以可以具有描述性的名称。
 * 举例来说，根据两个实数创建一个复数，而这两个实数表示直角坐标或极坐标，如果使用工厂方法，方法的含义就非常清晰了。
 * 当工厂方法起到这种消除歧义的作用时，构造方法常常被设置为私有方法，从而强制客户端代码使用工厂方法创建对象(但是方法改起来不方便,挺烦,好处与坏处都不少)。
 * 参考 https://zh.wikipedia.org/wiki/%E5%B7%A5%E5%8E%82%E6%96%B9%E6%B3%95
 *
 * @author zhaoxw
 * Create at 2018/7/31 12:59
 * @version 1.0
 */
public class FactoryPattern {

    public static void main(String[] args) {
        MouseFactory mouseFactory = new HpFactory();
        Mouse mouse = mouseFactory.createMouse();
        say(mouse);

        MouseFactory mouseFactory1 = new DellFactory();
        Mouse mouse2 = mouseFactory1.createMouse();
        say(mouse2);
    }

    private static void say(Mouse mouse) {
        System.err.println("我买了一个" + mouse.type());
    }


}
