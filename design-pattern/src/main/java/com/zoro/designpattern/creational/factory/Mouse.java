package com.zoro.designpattern.creational.factory;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:39
 * @version 1.0
 */
public interface Mouse {
    /**
     * 类型
     * @return 类型
     */
    String type();
}
