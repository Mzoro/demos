package com.zoro.designpattern.creational.factory;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:36
 * @version 1.0
 */
public interface MouseFactory {

    /**
     * 创建鼠标
     * @return 鼠标
     */
    public Mouse createMouse();
}
