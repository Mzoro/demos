package com.zoro.designpattern.creational.factory.factories;

import com.zoro.designpattern.creational.factory.Mouse;
import com.zoro.designpattern.creational.factory.MouseFactory;
import com.zoro.designpattern.creational.factory.mouses.DellMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:46
 * @version 1.0
 */
public class DellFactory implements MouseFactory {
    @Override
    public Mouse createMouse() {
        return new DellMouse();
    }
}
