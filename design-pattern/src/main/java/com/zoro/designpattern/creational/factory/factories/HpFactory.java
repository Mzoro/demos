package com.zoro.designpattern.creational.factory.factories;

import com.zoro.designpattern.creational.factory.Mouse;
import com.zoro.designpattern.creational.factory.MouseFactory;
import com.zoro.designpattern.creational.factory.mouses.HpMouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:41
 * @version 1.0
 */
public class HpFactory implements MouseFactory {
    @Override
    public Mouse createMouse() {
        return new HpMouse();
    }
}
