package com.zoro.designpattern.creational.factory.mouses;

import com.zoro.designpattern.creational.factory.Mouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:39
 * @version 1.0
 */
public class DellMouse implements Mouse {
    @Override
    public String type() {
        return "dell鼠标";
    }
}
