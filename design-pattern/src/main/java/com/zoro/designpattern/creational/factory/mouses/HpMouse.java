package com.zoro.designpattern.creational.factory.mouses;

import com.zoro.designpattern.creational.factory.Mouse;

/**
 * @author zhaoxw
 * Create at 2018/7/31 13:40
 * @version 1.0
 */
public class HpMouse implements Mouse {
    @Override
    public String type() {
        return "hp鼠标";
    }
}
