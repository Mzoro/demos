/**
 * 创建型模式
 * 这些设计模式提供了一种在创建对象的同时隐藏创建逻辑的方式，而不是使用 new 运算符直接实例化对象。这使得程序在判断针对某个给定实例需要创建哪些对象时更加灵活。
 * 工厂模式（Factory Pattern）{@link com.zoro.designpattern.creational.factory.FactoryPattern}
 * 抽象工厂模式（Abstract Factory Pattern）{@link com.zoro.designpattern.creational.abstractfactory.AbstractFactoryPattern}
 * 单例模式（Singleton Pattern）{@link com.zoro.designpattern.creational.singleton.SingletonPattern}
 * 建造者模式（Builder Pattern）{@link com.zoro.designpattern.creational.builder.BuilderPattern}
 * 原型模式（Prototype Pattern）{@link com.zoro.designpattern.creational.prototype.PrototypePattern}
 */
package com.zoro.designpattern.creational;

