package com.zoro.designpattern.creational.prototype;

/**
 * @author zhaoxw
 * Create at 2018/8/1 14:27
 * @version 1.0
 */
public abstract class BaseShape implements Cloneable {
    public abstract String type();

    @Override
    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
