package com.zoro.designpattern.creational.prototype;

import com.zoro.designpattern.creational.prototype.shapes.Circle;

/**
 * 原型模式
 * 原型模式是创建型模式的一种，其特点在于通过“复制”一个已经存在的实例来返回新的实例,而不是新建实例。被复制的实例就是我们所称的“原型”，这个原型是可定制的。
 * 原型模式多用于创建复杂的或者耗时的实例，因为这种情况下，复制一个已经存在的实例使程序运行更高效；或者创建值相等，只是命名不一样的同类数据。
 * JavaScript 中用到的比较多,但是实现方法不相同,js中有class 关键字,但他只是一个说法糖 http://es6.ruanyifeng.com/#docs/class
 * java中我们经常把需要复制的对象保存在Cache中,反复调用 ,如果想使用 .clone()方法,那么这个类在定义时还需要实现 Cloneable 接口,重写clone方法
 * <p>
 * 没见过这种例子,这个测试中也没有测试出有多节省时间,可能是对象太简单
 *
 * @author zhaoxw
 * Create at 2018/8/1 14:01
 * @version 1.0
 */
public class PrototypePattern {
    public static void main(String[] args) {
        BaseShape circle = new Circle();
        System.out.println(circle.toString());
        ShapeCache.addShape(circle);

        long start = System.currentTimeMillis();
        System.out.println(start);
        for (int i = 0; i < 10000000; i++) {

            BaseShape circlecloned = ShapeCache.cloneShape("circle");
        }
        System.out.println(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            BaseShape haha = new Circle();
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}
