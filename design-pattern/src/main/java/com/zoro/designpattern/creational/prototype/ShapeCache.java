package com.zoro.designpattern.creational.prototype;

import java.util.HashMap;

/**
 * @author zhaoxw
 * Create at 2018/8/1 14:31
 * @version 1.0
 */
public class ShapeCache {

    private static final HashMap<String, BaseShape> SHAPES = new HashMap<>(16);

    public static void addShape(BaseShape baseShape) {
        SHAPES.put(baseShape.type(), baseShape);
    }

    public static BaseShape cloneShape(String shapename) {
        return (BaseShape) SHAPES.get(shapename).clone();
    }
}
