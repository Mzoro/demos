package com.zoro.designpattern.creational.prototype.shapes;

import com.zoro.designpattern.creational.prototype.BaseShape;

/**
 * @author zhaoxw
 * Create at 2018/8/1 14:30
 * @version 1.0
 */
public class Circle extends BaseShape {
    @Override
    public String type() {
        return "circle";
    }
}
