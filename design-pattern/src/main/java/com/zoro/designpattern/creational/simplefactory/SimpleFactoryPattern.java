package com.zoro.designpattern.creational.simplefactory;

/**
 * 简单工厂模式
 * 在工厂模式中，我们在创建对象时不会对客户端暴露创建逻辑，并且是通过使用一个共同的接口来指向新创建的对象。
 * 简单工厂模式不算是23种模式中的一种,但是做为入门可以帮助理解工厂模式
 * <a href="http://www.runoob.com/design-pattern/factory-pattern.html">菜鸟:工厂模式</a>
 * <a href="https://www.cnblogs.com/zailushang1996/p/8601808.html">菜鸟:工厂模式</a>
 * leanote 设计模式--工厂
 *
 * @author zhaoxw
 * Create at 2018/7/31 11:10
 * @version 1.0
 */
public class SimpleFactoryPattern {
    public static void main(String[] args) {
        SimpleMouseFactory.createMouse(1).mouseType();
        SimpleMouseFactory.createMouse(2).mouseType();

    }

}

/**
 * 定义一个工厂类
 */
class SimpleMouseFactory {

    public static Mouse createMouse(int type) {
        switch (type) {
            case 1:
                return new HpMouse();
            case 2:
                return new DellMouse();
            default:
                System.err.println("没有找到对应各类!");
                return null;
        }
    }
}

/**
 * 鼠标接口
 */
interface Mouse {
    /**
     * 鼠标接口
     */
    void mouseType();
}

/**
 * 鼠标接口的实现类
 */
class HpMouse implements Mouse {

    @Override
    public void mouseType() {
        System.err.println("生产黑屏鼠标!");
    }
}

class DellMouse implements Mouse {

    @Override
    public void mouseType() {
        System.err.println("生产呆啊鼠标!");
    }
}

