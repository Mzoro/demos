package com.zoro.designpattern.creational.singleton;

/**
 * 这里使用的是饿汉单例,增加双检锁与 volatile 保证线程安全
 * volatile https://www.cnblogs.com/dolphin0520/p/3920373.html,http://www.importnew.com/18126.html
 * synchronized https://www.cnblogs.com/QQParadise/articles/5059824.html
 * 双检锁(Double-checked Lock/DCL) https://blog.csdn.net/li295214001/article/details/48135939/,https://blog.csdn
 * .net/maritimesun/article/details/7831065
 *
 * @author zhaoxw
 * Create at 2018/7/31 17:36
 * @version 1.0
 */
public class DoubleCheckSingle {

    private volatile static DoubleCheckSingle singleton;

    private DoubleCheckSingle() {
    }

    public static DoubleCheckSingle getSingleton() {
        if (singleton == null) {
            synchronized (DoubleCheckSingle.class) {
                if (singleton == null) {
                    singleton = new DoubleCheckSingle();
                }
            }
        }
        return singleton;
    }

    /*
    这样写线程安全的原因:
    需要先了解3件事:
        1.指令重排
        2.volatile原理
        3.synchronized 锁住的是对象还是代码
    1. 指令重排 :
        java代码在Cpu中运行时,一行代码可能会分成多行指令,在一个线程中保证结果正确情况下指令的执行顺序可能不一样,对于这个例子
        在 singleton = new DoubleCheckSingle();时假设需要三步1,分配一个内存空间;2,初始化对象 3, 将内容引用地址给 singleton变量;
        但是有可能 2与3 反过来;因为一个线程中最后的结果一样;
    2.synchronized :作用
       DoubleCheckSingle.class 的 那一部分代码同一时间只能一个线程执行完才能给下一个线程,同时synchronized 可以将变量同步到主内存,也就是说synchronized
       代码块中的变化其他线程可见,注意,这里是static方法,对象是DoubleCheckSingle的类对象
    3.volatile :
        因为volatile 修饰的是 singleton 这个变量,所以在 new 的过程中 就会保证 2,初始化对象 不会放在 3, 将内容引用地址给 singleton变量这一句的后面
     */
}
