package com.zoro.designpattern.creational.singleton;

/**
 * 饿汉型,不存在线程安全,但是如果大量单例,会使系统启动慢
 *
 * @author zhaoxw
 * Create at 2018/7/31 16:30
 * @version 1.0
 */
public class HungarySingle {

    private static final HungarySingle HUNGARY_SINGLE = new HungarySingle();

    private HungarySingle() {

    }

    public static HungarySingle newInstance() {
        return HUNGARY_SINGLE;
    }

}
