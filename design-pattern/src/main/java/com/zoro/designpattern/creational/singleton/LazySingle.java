package com.zoro.designpattern.creational.singleton;

/**
 * 懒汉型
 * 存在线程不安全,但是在系统启动时会节省内存
 *
 * @author zhaoxw
 * Create at 2018/7/31 16:26
 * @version 1.0
 */
public class LazySingle {
    private static LazySingle lazySingle;

    private LazySingle() {

    }

    /**
     * 当两个线程同时调用这个方法就会出面两个LazySingle对象
     *
     * @return
     */
    public static LazySingle newInstance() {
        if (lazySingle == null) {
            lazySingle = new LazySingle();
        }
        return lazySingle;
    }
}
