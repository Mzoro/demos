package com.zoro.designpattern.creational.singleton;

/**
 * 饿汉的加锁模式
 *
 * @author zhaoxw
 * Create at 2018/7/31 16:34
 * @version 1.0
 */
public class LazySingleSafe {

    private static LazySingleSafe lazySingle;

    private LazySingleSafe() {

    }

    private static synchronized LazySingleSafe newInstance() {

        if (lazySingle == null) {
            lazySingle = new LazySingleSafe();
        }
        return lazySingle;
    }
}
