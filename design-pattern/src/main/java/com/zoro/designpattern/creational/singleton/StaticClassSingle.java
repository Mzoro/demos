package com.zoro.designpattern.creational.singleton;

/**
 * 登记式 单例
 *
 * @author zhaoxw
 * Create at 2018/8/1 11:20
 * @version 1.0
 */
public class StaticClassSingle {

    private static class SingletonHolder {
        private static final StaticClassSingle INSTANCE = new StaticClassSingle();
    }

    private StaticClassSingle() {
    }

    public static final StaticClassSingle getInstance() {
        return SingletonHolder.INSTANCE;
    }
}

/*
    个人认为这种方式避开的 懒汉,饿汉的所有弊端
     SingletonHolder这个类 是静态内部类,他不会主动加载,只有在访问的时候才会加载,这个时间才会 new; 而且jvm 可以保证类对象是单例的,那么 INSTANCE就会是单例的
 */