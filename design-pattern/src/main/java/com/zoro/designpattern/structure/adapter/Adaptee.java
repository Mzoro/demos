package com.zoro.designpattern.structure.adapter;

/**
 * 这个类是已经开发好的类,有可能他存在于某个 jar包中,我们不能修改
 *
 * @author zhaoxw
 * Create at 2018/8/1 18:43
 * @version 1.0
 */
public class Adaptee {

    /**
     * 具体的方法
     * spicefic 具体,特定
     */
    public void spiceficMethod() {
        System.out.println("spicefic");
    }
}
