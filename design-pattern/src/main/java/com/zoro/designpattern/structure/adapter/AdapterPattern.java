package com.zoro.designpattern.structure.adapter;


/**
 * 定义：将一个系统的接口转换成另外一种形式，从而使原来不能直接调用的接口变得可以调用。
 * 优点
 * 适配器模式也是一种包装模式，它与装饰模式同样具有包装的功能，此外，对象适配器模式还具有委托的意思。总的来说，适配器模式属于补偿模式，专用来在系统后期扩展、修改时使用。
 * <p>
 * 缺点
 * 过多的使用适配器，会让系统非常零乱，不易整体进行把握。比如，明明看到调用的是 A 接口，其实内部被适配成了 B 接口的实现，一个系统如果太多出现这种情况，无异于一场灾难。因此如果不是很有必要，可以不使用适配器，而是直接对系统进行重构。
 * <p>
 * 适配器模式有两种, 对象适配器与类配置器
 * 对象适配器是通过
 * <p>
 * 参考 : https://www.ibm.com/developerworks/cn/java/j-lo-adapter-pattern/index.html
 *        http://www.runoob.com/design-pattern/adapter-pattern.html
 *
 * @author zhaoxw
 * Create at 2018/8/1 17:08
 * @version 1.0
 * <p>
 * 例子:
 * @see java.util.Arrays#asList
 * @see javax.swing.JTable(javax.swing.table.TableModel)
 * @see java.io.InputStreamReader(java.io.InputStream)
 * @see java.io.OutputStreamWriter(java.io.OutputStream)
 */
public class AdapterPattern {
    public static void main(String[] args) {


        new TargetAdapter().targetRequest();

    }
}
