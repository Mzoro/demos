package com.zoro.designpattern.structure.adapter;

/**
 * 这是我们的目标类,比如他是开发人员想要实现的接口
 * 这个接口除了想完成新增的接口外还想有之前已经实现的接口的功能
 *
 * @author zhaoxw
 * Create at 2018/8/1 18:39
 * @version 1.0
 */
public interface Target {

    /**
     * 想要实现的方法
     */
    void targetRequest();
}
