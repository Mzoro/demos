package com.zoro.designpattern.structure.adapter;

/**
 * @author zhaoxw
 * Create at 2018/8/1 18:47
 * @version 1.0
 */
public class TargetAdapter implements Target {

    private Adaptee adaptee;

    public TargetAdapter() {
        this.adaptee = new Adaptee();
    }

    /**
     * 这里还可以写成传入一个参数,根据一个参数来判断怎么执行,但是如果Target接口就没有参数就只能这样了吧
     */
    @Override
    public void targetRequest() {
        adaptee.spiceficMethod();
        System.out.println("targetRequest");
    }
}
