package com.zoro.designpattern.structure.bridge;

import com.zoro.designpattern.structure.bridge.drawimpl.GreenCircle;
import com.zoro.designpattern.structure.bridge.drawimpl.RedCircle;
import com.zoro.designpattern.structure.bridge.shapeimpl.Circle;

/**
 * 桥接模式
 * 桥接模式是软件设计模式中最复杂的模式之一，它把事物对象和其具体行为、具体特征分离开来，使它们可以各自独立的变化。事物对象仅是一个抽象的概念。
 * 如“圆形”、“三角形”归于抽象的“形状”之下，而“画圆”、“画三角”归于实现行为的“画图”类之下，然后由“形状”调用“画图”。 ---wiki 总是能一语中的
 * <p>
 * 桥接模式与 适配器在代码上很难区分 ,结构上可能一样一样的,区别 : https://blog.csdn.net/njutony1991/article/details/17039079
 * 我就呵呵了 难怪不好找例子,因为在代码上可能根本分辩不出来,
 * 桥接模式是所有接口都可以修改,在软件设计初需要考虑如何设计软件结构;
 * 适配器模式多发生在已经有一个已经开发完成的软件,我们需要使用它,与现在的接口结合
 * 顺便说一下 Facade 外观模式,他也是在设计初,一个面向用户的接口包含系统中大量的接口(已经存在的和正在开发的),方便用户(高层开发人员,码农)调用,来完成一个任务
 * 就代码而言
 * 1. Facade很多时候是1:m的关系
 * 2. Adapter很多是候是1:1的关系
 * 3. Bridge很多时候是m:n的关系    呵呵呵呵呵呵
 * <p>
 * 桥接（Bridge）是用于把抽象化与实现化解耦，使得二者可以独立变化。这种类型的设计模式属于结构型模式，它通过提供抽象化和实现化之间的桥接结构，来实现二者的解耦。
 * 他可以保证不论实现怎么变,Bridge是不用变的
 *
 * @author zhaoxw
 * Create at 2018/8/1 19:13
 * @version 1.0
 */
public class BridgePattern {
    public static void main(String[] args) {
        new Circle(1, 2, 3, new GreenCircle()).draw();
        new Circle(1, 2, 3, new RedCircle()).draw();
    }
}
