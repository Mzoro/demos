package com.zoro.designpattern.structure.bridge;

/**
 * @author zhaoxw
 * Create at 2018/8/2 9:41
 * @version 1.0
 */
public interface DrawAPI {
    public void drawCircle(int radius, int x, int y);
}