package com.zoro.designpattern.structure.bridge;

/**
 * @author zhaoxw
 * Create at 2018/8/2 9:44
 * @version 1.0
 */
public interface Shape {


    DrawAPI getDrawApi();

    void draw();
}