package com.zoro.designpattern.structure.bridge.drawimpl;

import com.zoro.designpattern.structure.bridge.DrawAPI;

/**
 * @author zhaoxw
 * Create at 2018/8/2 9:43
 * @version 1.0
 */
public class GreenCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: green, radius: "
                + radius +", x: " +x+", "+ y +"]");
    }
}