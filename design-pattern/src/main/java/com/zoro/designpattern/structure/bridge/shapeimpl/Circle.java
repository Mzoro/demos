package com.zoro.designpattern.structure.bridge.shapeimpl;

import com.zoro.designpattern.structure.bridge.DrawAPI;
import com.zoro.designpattern.structure.bridge.Shape;

/**
 * @author zhaoxw
 * Create at 2018/8/2 9:45
 * @version 1.0
 */
public class Circle implements Shape {
    private int x, y, radius;
    private DrawAPI drawAPI;

    public Circle(int x, int y, int radius, DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
        this.x = x;
        this.y = y;
        this.radius = radius;
    }


    @Override
    public DrawAPI getDrawApi() {
        return this.drawAPI;
    }

    @Override
    public void draw() {
        getDrawApi().drawCircle(radius, x, y);
    }
}
