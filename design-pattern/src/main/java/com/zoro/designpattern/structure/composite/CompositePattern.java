package com.zoro.designpattern.structure.composite;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * 组合模式
 * 对于树形结构，当容器对象（如文件夹）的某一个方法被调用时，将遍历整个树形结构，寻找也包含这个方法的成员对象（可以是容器对象，也可以是叶子对象）并调用执行，
 * 牵一而动百，其中使用了递归调用的机制来对整个结构进行处理。
 * 由于容器对象和叶子对象在功能上的区别，在使用这些对象的代码中必须有区别地对待容器对象和叶子对象，
 * 而实际上大多数情况下我们希望一致地处理它们，因为对于这些对象的区别对待将会使得程序非常复杂。
 * 组合模式为解决此类问题而诞生，它可以让叶子对象和容器对象的使用具有一致性。
 * <p>
 * 处理这种东西一般需要递归,他的特点就是一个类的对象持有同类的对象的集合
 * <p>
 * 例:树、 解析java语法结构 JavaParser ....
 *
 * @author zhaoxw
 * Create at 2018/8/2 13:29
 * @version 1.0
 */
public class CompositePattern {

    //D:\files\git-codes\demos\design-pattern\src\main\java\com\zoro\designpattern\structure\facade\ItemMacker.java
    public static void main(String[] args) throws FileNotFoundException {

        InputStream in = new FileInputStream(new File("D:\\files\\git-codes\\demos\\design-pattern\\src\\main\\java" +
                "\\com\\zoro\\designpattern\\structure\\facade\\ItemMacker.java"));

        CompilationUnit cu = JavaParser.parse(in);


        /**
         * Node 就是这种组合模式
         */
        List<ClassOrInterfaceDeclaration> nodes = findClass(cu);

        TypeDeclaration typeDeclaration = cu.getType(0);
        cu.getPrimaryType();
        List<MethodDeclaration> methodDeclarations = typeDeclaration.getMethods();
    }

    private static List<ClassOrInterfaceDeclaration> findClass(CompilationUnit compilationUnit) {

        List<Node> nodes = compilationUnit.getChildNodes();
        List<ClassOrInterfaceDeclaration> clazz = new LinkedList<>();
        for (Node n : nodes) {
            if (n instanceof ClassOrInterfaceDeclaration) {
                clazz.add((ClassOrInterfaceDeclaration) n);
            }
        }
        return clazz;
    }

}
