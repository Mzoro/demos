package com.zoro.designpattern.structure.decorator;

import com.zoro.designpattern.structure.decorator.impl.Bike;
import com.zoro.designpattern.structure.decorator.impl.Car;
import com.zoro.designpattern.structure.decorator.impl.Train;

/**
 * 装饰器模式(这个很有用)
 * 装饰器对象，所有具体装饰器对象父类。它最经典的特征就是：
 * 1.必须有一个它自己的父类为自己的成员变量；
 * 2.必须继承公共父类。这是因为装饰器也是一种成分，只不过是那些具体具有装饰功能的成分的公共抽象罢了。
 * 他的优点:他是继承的代替;可以动态的组合功能,而不是依靠继承
 * <p>
 * 参考 : http://miaoxiaodong78.blog.163.com/blog/static/18765136200701232434996/#comment
 * =fks_082064084083081074084081082095085094086069081087085069
 * <p>
 * 修饰类的接口依然是目标接口,但是不是通过继承已有的实现类来扩展功能,而是通过创建一个抽象的修饰类,这个抽象类持有一个目标接口的实现类的对象,然后随意实现这个抽象类,达到扩展功能的目的
 * 在类的继承角度看,这个抽象修饰类的子类都是平级的,不存在谁是谁的子类;
 * 在功能上看,确是在某个具体的子类上扩展的功能;
 * <p>
 * 作用 : https://zh.wikipedia.org/wiki/%E4%BF%AE%E9%A5%B0%E6%A8%A1%E5%BC%8F
 * 比如有一个顶级的接口 ,他已经有很多实现类A,B,C,D,现在需要一个顶级接口的实现类完成 ABCD的所有功能,
 * 在运行阶段使用继承是不可能的,这时候就需要装饰器,这个装饰器持有一个顶级接口的属性,
 * 即使需求不是发生在运行阶段,使用继承也不合理,可以使用外观模板,如果不想改动调用方的接口,就使用装饰器,
 * 这里我们还发现装饰器与外观模式的区别,外观模式接口不统一 {@link com.zoro.designpattern.structure.facade.FacadePattern}
 *
 * @author zhaoxw
 * Create at 2018/8/2 16:18
 * @version 1.0
 */
public class DecoratorPattern {

    /**
     * 假设 say方法是系统中已经封装好的稳定的调用程序,不可修改
     * impl 包中是现有的已经实现的功能
     * 现在想对这几个接口组合 ,或者扩展,或者完成运行中不确定的调用
     *
     * @param args 运行参数
     */
    public static void main(String[] args) {
//        Transportation transportation = new TransportationDecoratorImpl(new Car());
//        say(transportation);

        /*
        下面这个例子不知道是不是还符不符合装饰器模式:
        装饰器是包含的是一个List<Transportation> 属性;声明时需要声明装饰类,而不是顶级接口
        需求,按照用户的需求按顺序安排交通工具
         */
        TransportationDecoratorMultiple transportation1 = new TransportationDecoratorMultipleImpl();
        int[] ints = {3, 1, 3};
        for (int i : ints) {
            transportation1.addTransportation(getTransportation(i));
        }

        say(transportation1);

    }

    private static void say(Transportation transportation) {
        transportation.go();
    }

    private static Transportation getTransportation(int i) {
        switch (i) {
            case 1:
                return new Train();
            case 2:
                return new Car();
            default:
                return new Bike();
        }
    }
}
