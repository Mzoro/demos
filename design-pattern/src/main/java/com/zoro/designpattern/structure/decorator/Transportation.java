package com.zoro.designpattern.structure.decorator;

/**
 * @author zhaoxw
 * Create at 2018/8/3 9:45
 * @version 1.0
 */
public interface Transportation {

    void go();
}
