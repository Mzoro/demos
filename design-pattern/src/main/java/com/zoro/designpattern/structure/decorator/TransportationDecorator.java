package com.zoro.designpattern.structure.decorator;

/**
 * 这个类是为了规范/表达 decorator的结构
 *
 * @author zhaoxw
 * Create at 2018/8/3 10:12
 * @version 1.0
 */
public abstract class TransportationDecorator implements Transportation {
    protected Transportation transportation;

    public TransportationDecorator(Transportation transportation) {
        this.transportation = transportation;
    }

    @Override
    public abstract void go();

}
