package com.zoro.designpattern.structure.decorator;

/**
 * @author zhaoxw
 * Create at 2018/8/3 10:16
 * @version 1.0
 */
public class TransportationDecoratorImpl extends TransportationDecorator {


    public TransportationDecoratorImpl(Transportation transportation) {
        super(transportation);
    }

    @Override
    public void go() {
        transportation.go();
        System.out.println("修饰完了");
    }
}
