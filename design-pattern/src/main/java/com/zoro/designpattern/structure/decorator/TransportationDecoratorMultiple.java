package com.zoro.designpattern.structure.decorator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/3 10:36
 * @version 1.0
 */
public abstract class TransportationDecoratorMultiple implements Transportation {

    protected List<Transportation> transportations = new LinkedList<>();


    @Override
    abstract public void go();

    public void addTransportation(Transportation transportation) {
        this.transportations.add(transportation);
    }

}
