package com.zoro.designpattern.structure.decorator;

/**
 * @author zhaoxw
 * Create at 2018/8/3 10:38
 * @version 1.0
 */
public class TransportationDecoratorMultipleImpl extends TransportationDecoratorMultiple {
    @Override
    public void go() {
        for (Transportation t : transportations) {
            t.go();
        }
    }
}
