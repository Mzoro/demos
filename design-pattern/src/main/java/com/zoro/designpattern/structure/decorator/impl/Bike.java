package com.zoro.designpattern.structure.decorator.impl;

import com.zoro.designpattern.structure.decorator.Transportation;

/**
 * @author zhaoxw
 * Create at 2018/8/3 9:57
 * @version 1.0
 */
public class Bike implements Transportation {
    @Override
    public void go() {
        System.out.println("bike走了一会");
    }
}
