package com.zoro.designpattern.structure.decorator.impl;

import com.zoro.designpattern.structure.decorator.Transportation;

/**
 * @author zhaoxw
 * Create at 2018/8/3 9:45
 * @version 1.0
 */
public class Car implements Transportation {

    /**
     * 灯的寿命
     */
    private int light;

    /**
     * 最长里程数
     */
    private int milage;

    /**
     * 最大载重
     */
    private int load;


    @Override
    public void go() {
        System.out.println("汽车走一会!");
    }


    public int getLight() {
        return light;
    }

    public void setLight(int light) {
        this.light = light;
    }

    public int getMilage() {
        return milage;
    }

    public void setMilage(int milage) {
        this.milage = milage;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }
}
