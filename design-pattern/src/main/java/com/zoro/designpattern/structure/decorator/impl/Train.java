package com.zoro.designpattern.structure.decorator.impl;

import com.zoro.designpattern.structure.decorator.Transportation;

/**
 * @author zhaoxw
 * Create at 2018/8/3 9:58
 * @version 1.0
 */
public class Train implements Transportation {
    @Override
    public void go() {
        System.out.println("火车走了一会");
    }
}
