package com.zoro.designpattern.structure.facade;


/**
 * 外观模式（Facade Pattern）
 * 隐藏系统的复杂性，并向客户端提供了一个客户端可以访问系统的接口。这种类型的设计模式属于结构型模式，它向现有的系统添加一个接口，来隐藏系统的复杂性。
 * 这种模式涉及到一个单一的类，该类提供了客户端请求的简化方法和对现有系统类方法的委托调用。
 * 下面的例子,我们需要 hp键鼠套装,dell套装,并相应的库存减少
 * 正常情况下我们需要自己创建各种产品对象,并去将他的库存减少,但是我们将这些操作封装好,类似我们写的  controller-service 的关系,controller层就只需要调用就可以
 * facade 与 builder {@link  com.zoro.designpattern.creational.builder.BuilderPattern} 在代码结构上也很像,这应该是面向对象的特性,
 * 但是 facade 与builder 强调的作用不同,一个是创建对象,一个是封装过程 ,设计模式不可分开独立理解,也不能做为软件标准,只能是一种思路,一种参考
 * <p>
 * <p>
 * 这个类中完成了单品的功能,做没有import任何类,如果,产品与调用者的耦合很小;
 *
 * @author zhaoxw
 * Create at 2018/8/2 10:16
 * @version 1.0
 */
public class FacadePattern {

    public static void main(String[] args) {
        ItemMacker itemMacker = new ItemMacker();
        ItemHolder dellSuite = itemMacker.dellSuite();
        say(dellSuite);
        ItemHolder hpSuite = itemMacker.hpSuite();
        say(hpSuite);
    }

    private static void say(ItemHolder itemHolder) {
        System.err.println(itemHolder.getKeyBoard().keyDown());
        System.err.println(itemHolder.getMouse().click());
    }
}
