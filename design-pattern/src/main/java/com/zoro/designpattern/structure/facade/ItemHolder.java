package com.zoro.designpattern.structure.facade;

import com.zoro.designpattern.creational.abstractfactory.KeyBoard;
import com.zoro.designpattern.creational.abstractfactory.Mouse;

/**
 * @author zhaoxw
 * Create at 2018/8/2 10:42
 * @version 1.0
 */
public class ItemHolder {

    private Mouse mouse;

    private KeyBoard keyBoard;

    public KeyBoard getKeyBoard() {
        return keyBoard;
    }

    public void setKeyBoard(KeyBoard keyBoard) {
        this.keyBoard = keyBoard;
    }

    public Mouse getMouse() {

        return mouse;
    }

    public void setMouse(Mouse mouse) {
        this.mouse = mouse;
    }
}
