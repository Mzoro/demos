package com.zoro.designpattern.structure.facade;

import com.zoro.designpattern.creational.abstractfactory.Factory;
import com.zoro.designpattern.creational.abstractfactory.factories.DellFactory;
import com.zoro.designpattern.creational.abstractfactory.factories.HpFactory;

/**
 * 这个类中 没有 Mouse 接口/KeyBoard 接口 和他们的子类,这应该就是解耦了吧
 *
 * @author zhaoxw
 * Create at 2018/8/2 10:40
 * @version 1.0
 */
public class ItemMacker {

    /**
     * Dell工厂
     */
    private DellFactory dellFactory;

    /**
     * Hp工厂
     */
    private HpFactory hpFactory;

    /**
     * 构造器
     */
    public ItemMacker() {
        dellFactory = new DellFactory();
        hpFactory = new HpFactory();
    }

    /**
     * 返回Hp套装
     *
     * @return Hp套装
     */
    public ItemHolder hpSuite() {
        ItemHolder itemHolder = this.makeSuite(hpFactory);
        System.err.println("hp鼠标减1,hp键盘减1");
        return itemHolder;
    }

    /**
     * 返回 dell套装
     *
     * @return dell套装
     */
    public ItemHolder dellSuite() {
        ItemHolder itemHolder = this.makeSuite(dellFactory);
        System.err.println("dell鼠标减1,dell键盘减1");
        return itemHolder;
    }

    /**
     * 生成套装
     *
     * @param factory 生产工厂
     * @return 套装
     */
    private ItemHolder makeSuite(Factory factory) {
        ItemHolder itemHolder = new ItemHolder();
        itemHolder.setKeyBoard(factory.createKeyBoard());
        itemHolder.setMouse(factory.createMouse());
        return itemHolder;
    }


}
