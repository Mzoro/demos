package com.zoro.designpattern.structure.filter;

import com.zoro.designpattern.creational.abstractfactory.Mouse;

import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/2 11:47
 * @version 1.0
 */
public interface Filter {

    List<Mouse> filterMouse(List<Mouse> mouseList);
}
