package com.zoro.designpattern.structure.filter;

import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.DellMouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.HpMouse;
import com.zoro.designpattern.structure.filter.filterimpl.DellFilter;
import com.zoro.designpattern.structure.filter.filterimpl.QualityFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * 过滤器模式（Filter Pattern）或标准模式（Criteria Pattern）是一种设计模式，
 * 这种模式允许开发人员使用不同的标准来过滤一组对象，通过逻辑运算以解耦的方式把它们连接起来。这种类型的设计模式属于结构型模式，它结合多个标准来获得单一标准。
 * JS中数组的过滤 https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
 * <p>
 * 没有找到解耦的好例子
 *
 * @author zhaoxw
 * Create at 2018/8/2 11:28
 * @version 1.0
 */
public class FilterPattern {

    public static void main(String[] args) {
        List<Mouse> mice = new ArrayList<>(16);

        for (int i = 0; i < 10; i++) {
            mice.add(new DellMouse());
            mice.add(new HpMouse());
        }

        List<Mouse> dell = new DellFilter().filterMouse(mice);
        List<Mouse> quality = new QualityFilter().filterMouse(mice);
    }
}
