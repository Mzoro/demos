package com.zoro.designpattern.structure.filter.filterimpl;

import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.creational.abstractfactory.mouses.DellMouse;
import com.zoro.designpattern.structure.filter.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/2 12:51
 * @version 1.0
 */
public class DellFilter implements Filter {
    @Override
    public List<Mouse> filterMouse(List<Mouse> mouseList) {
        List<Mouse> mice = new ArrayList<>(16);
        for (Mouse m : mouseList) {
            if (m instanceof DellMouse) {
                mice.add(m);
            }
        }
        return mice;
    }
}
