package com.zoro.designpattern.structure.filter.filterimpl;

import com.zoro.designpattern.creational.abstractfactory.Mouse;
import com.zoro.designpattern.structure.filter.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/8/2 11:54
 * @version 1.0
 */
public class QualityFilter implements Filter {
    @Override
    public List<Mouse> filterMouse(List<Mouse> mouseList) {
        List<Mouse> mice = new ArrayList<>(16);
        for (Mouse m : mouseList) {
            if (m.quality() > 5) {
                mice.add(m);
            }
        }
        return mice;
    }
}
