package com.zoro.designpattern.structure.flyweight;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @author zhaoxw
 * Create at 2018/8/3 14:22
 * @version 1.0
 */
public class Computer {

    private String cpu;

    public Computer() throws IOException {
        Reader reader = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("flyweight-test" +
                ".txt"));
        int len = 0;
        char[] test = new char[1024];
        String text = "";
        while ((len = reader.read(test)) > 0) {
            text += String.valueOf(test);
        }
        System.out.println(text);
    }

    public Computer(String cpu) {
        this.cpu = cpu;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

}
