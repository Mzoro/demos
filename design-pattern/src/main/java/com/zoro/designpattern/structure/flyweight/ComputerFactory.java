package com.zoro.designpattern.structure.flyweight;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/8/3 14:24
 * @version 1.0
 */
public class ComputerFactory {

    private static Map<String, Computer> computerMap = new HashMap<>(16);

    public Computer createComputer(String cpu) throws IOException {
        Computer computer = computerMap.get(cpu);
        if (computer == null) {
            computer = new Computer();
            computer.setCpu(cpu);
            computerMap.put(cpu, computer);
        }
        return computer;

    }
}
