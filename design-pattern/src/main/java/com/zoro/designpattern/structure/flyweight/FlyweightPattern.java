package com.zoro.designpattern.structure.flyweight;

import java.io.IOException;

/**
 * 享元模式(Flyweight Pattern) ,
 * 缓存已经创建好的可以重复利用的对象,例 多数据源的实现,连接池等
 * <p>
 * 享元模式（Flyweight Pattern）主要用于减少创建对象的数量，以减少内存占用和提高性能。这种类型的设计模式属于结构型模式，它提供了减少对象数量从而改善应用所需的对象结构的方式。
 * <p>
 * 不要理解为原型模式,原型模式是保存一个原型对象,产生多个,但享元模式缓存的对象没有任何关系,可以是同类型也可以不同,而不会根据已有对象产生新对象
 * <p>
 * 例子: https://gitee.com/Mzoro/demos/blob/master/demo-dynamic-datasource/src/main/java/com/zoro/demodynamicdatasource
 * /dynamicdatasource/DynamicDataSource.java
 * <p>
 * 在构造复杂对象,或者构建特别消耗资源时才会显得优势明显,下面这个就不行 ,redis 存在的意义80%在这
 *
 * @author zhaoxw
 * Create at 2018/8/3 13:54
 * @version 1.0
 */
public class FlyweightPattern {

    public static void main(String[] args) throws IOException {

        ComputerFactory computerFactory = new ComputerFactory();

        long start = System.currentTimeMillis();
        for (int i = 1; i < 100; i++) {
            Computer computer = computerFactory.createComputer(getCpu());
//            System.out.println(computer + "cpu:" + computer.getCpu());
        }

        System.out.println(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 1; i < 100; i++) {
            Computer computer = new Computer();
        }

        System.out.println(System.currentTimeMillis() - start);

    }

    private static String getCpu() {
        String[] cpus = {"i3", "i5", "i7", "Xeon", "amd"};
        int i = (int) (Math.random() * 5);
        return cpus[i];
    }
}
