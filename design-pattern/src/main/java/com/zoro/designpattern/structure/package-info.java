/**
 * 结构型模式
 * 这些设计模式关注类和对象的组合。继承的概念被用来组合接口和定义组合对象获得新功能的方式。
 * 在软件工程中结构型模式是设计模式，借由一以贯之的方式来了解元件间的关系，以简化设计。---wiki
 * <p>
 * 这三个主要是代码结构,代码重组
 * 适配器模式（Adapter Pattern）{@link com.zoro.designpattern.structure.adapter.AdapterPattern}
 * 桥接模式（Bridge Pattern）{@link com.zoro.designpattern.structure.bridge.BridgePattern}
 * 外观模式（Facade Pattern）{@link com.zoro.designpattern.structure.facade.FacadePattern}
 * <p>
 * 桥 指的是接口与其实现类的桥 ,目的是不论接口的实现如何变化,调用者不用改变什么
 * 适配 目的是"利用"老接口迎合新接口的调用者
 * 外观 目的是调用方便
 * <p>
 * <p>
 * 过滤器模式（Filter、Criteria Pattern）{@link com.zoro.designpattern.structure.filter.FilterPattern}
 * 组合模式（Composite Pattern）{@link com.zoro.designpattern.structure.composite.CompositePattern}
 * 装饰器模式（Decorator Pattern）{@link com.zoro.designpattern.structure.decorator.DecoratorPattern}
 * 享元模式（Flyweight Pattern）{@link com.zoro.designpattern.structure.flyweight.FlyweightPattern}
 * 代理模式（Proxy Pattern）{@link com.zoro.designpattern.structure.proxy.ProxyPattern}
 */
package com.zoro.designpattern.structure;