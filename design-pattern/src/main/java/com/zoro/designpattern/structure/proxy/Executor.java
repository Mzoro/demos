package com.zoro.designpattern.structure.proxy;

/**
 * @author zhaoxw
 * Create at 2018/8/3 15:20
 * @version 1.0
 */
@Mapping
public class Executor {

    @MappingMethod(target = "joke")
    public void jokor(String str) {
        System.out.println(str);
    }

    @MappingMethod(target = "subject")
    public void teacher(String str) {
        System.out.println(str);
    }
}
