package com.zoro.designpattern.structure.proxy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/8/3 15:27
 * @version 1.0
 */
public class MapExecutor {

    private Map<String, Method> methodMap = new HashMap<>(16);

    /**
     * 按包查找目标方法 : 类上有@Mapping 方法上有@MappingMethod
     *
     * @param packageName 包名
     * @throws ClassNotFoundException 未找到类
     * @throws IOException            io
     * @throws URISyntaxException     uri 语法不对
     */
    public void mapExecutor(String packageName) throws ClassNotFoundException, IOException, URISyntaxException {
        String path = packageName.replace(".", "/");
        Enumeration<URL> urlEnumeration = this.getClass().getClassLoader().getResources(path);

        while (urlEnumeration.hasMoreElements()) {
            URL url = urlEnumeration.nextElement();
            String filename = url.toURI().getPath();
            File file = new File(filename);
            String classname;
            if (file.exists()) {
                File[] classes = file.listFiles();
                assert classes != null;
                for (File f : classes) {
                    classname = this.getClassname(f.getName(), packageName);
                    Class clazz = Class.forName(classname);
                    this.mapExecutor(clazz);
                }

            }
        }

    }

    /**
     * 解析类全名
     *
     * @param filename    class文件名
     * @param packagename 包名
     * @return 类全名
     */
    private String getClassname(String filename, String packagename) {
        if (filename == null || packagename == null || (filename + packagename).length() == 0) {
            new NullPointerException("filename或者packagename 为空").printStackTrace();
            System.exit(1);
        }

        filename = filename.substring(0, filename.lastIndexOf("."));
        return packagename + "." + filename.replace("$", ".");
    }

    /**
     * 装载方法
     *
     * @param clazz Class对象
     */
    private void mapExecutor(Class clazz) {

        Mapping mapping = (Mapping) clazz.getDeclaredAnnotation(Mapping.class);
        if (mapping == null) {
            return;
        }

        Method[] methods = clazz.getDeclaredMethods();

        for (Method m : methods) {
            MappingMethod mappingMethod = m.getDeclaredAnnotation(MappingMethod.class);
            if (mappingMethod == null) {
                continue;
            }
            String target = mappingMethod.target();
            this.methodMap.put(target, m);
        }
    }

    /**
     * 执行方法
     *
     * @param target 目标方法
     * @param param  传入参数
     * @return 目标方法返回值
     */
    public Object execute(String target, Object... param) throws IllegalAccessException, InstantiationException,
            InvocationTargetException, NoSuchMethodException {
        Method targetMethod = this.methodMap.get(target);
        if (targetMethod == null) {
            System.err.println("没有找到对应方法!");
            return null;
        }

        //这个方法可能会出错,如果类没有默认构造器就会报错,保险点,就需要先.
        // TODO Object clazz = targetMethod.getDeclaringClass().getConstructor(String.class).newInstance("");
        Object clazz = targetMethod.getDeclaringClass().newInstance();
        // 这里还可以验证参数类型是不是对,做点权限验证之类的,不写了
        // TODO 这里还需要验证参数数量与类型对不对,否则这里也出错
        return targetMethod.invoke(clazz, param);
    }

}
