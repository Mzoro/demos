package com.zoro.designpattern.structure.proxy;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

/**
 * 代理模式
 * 在直接访问对象时带来的问题，比如说：要访问的对象在远程的机器上。在面向对象系统中，
 * 有些对象由于某些原因（比如对象创建开销很大，或者某些操作需要安全控制，或者需要进程外的访问），
 * 直接访问会给使用者或者系统结构带来很多麻烦，我们可以在访问此对象时加上一个对此对象的访问层。
 * <p>
 * 代理经常会与享元模式一起使用 ,来提高效率,很多代理会消耗很多资源
 * <p>
 * ******************************
 * 代理模式个人理解:实现上与 与装饰器模式(DecoratorPattern){@link com.zoro.designpattern.structure.decorator.DecoratorPattern} 特别类似,
 * 都是实现同一接口的对象持有一个实现同样接口的对象,让这个对象去写成任务,
 * 区别(来源于网上的一些人的胡话) : 说是持有的实现使用的对象创建的时间不同,Decorator 是在创建装饰器时通过构造器传入 ,Proxy是通过构造器运行时 new出来 (呵呵),这对代码结构有多大影响?装饰器可能灵活一点;
 * proxy 与decorator 可以达到同样的效果;就结果而言都 一样吧
 * ************************************************
 * <p>
 * TODO 这个包中的例子 与代理没有什么关系,就是突然想到就写了一下,类似SpringMvc的实现
 * 想看真正的例子:
 * http://www.runoob.com/design-pattern/proxy-pattern.html
 * https://zh.wikipedia.org/wiki/%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F
 * <p>
 * <p>
 * 想在思想上升华 看明白区别
 * http://www.jasongj.com/design_pattern/proxy_decorator/
 * 说是proxy 重在隐藏,限制访问目标对象  decorator 重在对这类对象的追加功能
 *
 * @author zhaoxw
 * Create at 2018/8/3 15:08
 * @version 1.0
 */
public class ProxyPattern {
    public static void main(String[] args) throws IOException, ClassNotFoundException, URISyntaxException,
            IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        MapExecutor mapExecutor = new MapExecutor();
        mapExecutor.mapExecutor("com.zoro.designpattern.structure.proxy");
        mapExecutor.execute("joke", "不传不行,最少传个null");
    }
}
