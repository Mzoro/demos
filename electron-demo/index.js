const {app, BrowserWindow} = require('electron')
const path = require('path')
const mainFunctions = require('./src/main/index')
const configUtil = require('./src/app-config')
const tools = require('./src/app-config/tools')
const httpConfig = require('./src/http/index')

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        // frame:false,// 没有边框,但不全屏
        //fullscreen:true,// 全屏，没有边框，没有工具栏，没有关闭按键
        // kiosk:true,// 和fullscreen 差不多
        autoHideMenuBar: true,// 取消工具栏 ，按 Alt键时显示
        titleBarStyle: "hiddenInset",// 隐藏标题栏，但 ubuntu 20.10上没有效果
        webPreferences: {
            nodeIntegration: true,// 正常应该为true 的 但了是因为jquery 的关系 先设为false  https://newsn.net/say/electron-jquery.html;https://www.cnblogs.com/wonyun/p/10991984.html
            // sandbox: true,// 不开这个 远程页面加载jquery文件会有问题，但是开启这个 nodejs 的相关功能就会被停用
            v8CacheOptions: "none",// 不缓存页面
            // webSecurity: false,// 关掉同源策略
            contextIsolation:false, // 这个在 12版本以后如果不写，会导致preload加载，但是 在 preload.js中为 window 添加的方法无效
            preload: path.join(app.getAppPath(), 'preload.js')
        },
        icon: path.join(app.getAppPath(), 'assets', 'app-tray.png')
    })

    // 加载本地文件
    // win.loadFile(path.join(app.getAppPath(), 'index.html')).then(function () {
    //    // win.webContents.openDevTools()
    // });

    //加载远程文件
    win.loadURL('http://localhost:56661/index/electron').then(function () {
        // win.webContents.openDevTools ()
        console.log("invoke init function")
        mainFunctions()
        configUtil.configBrowserWindow(app, win)
        httpConfig.httpStart(win)
    });
    if (tools.isDebug()) {
        win.webContents.openDevTools();
    }
}

app.whenReady().then(function () {
    createWindow();
    configUtil.configApp();

});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})
