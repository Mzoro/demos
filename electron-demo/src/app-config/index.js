const {Notification, Menu, BrowserWindow, Tray, app} = require('electron')
const path = require('path')

function configBrowserWindow(application, window) {
    window.on('close', function (event) {
        // 阻止窗口关闭，将其隐藏
        event.preventDefault();
        const pl = process.platform;
        if (pl !== 'linux') {
            event.preventDefault();
            window.hide()
            const notificationOption = {
                title: '提示',
                body: '程序最小化到托盘！'
            }
            const notification = new Notification(notificationOption);
            notification.on('click', function () {
                window.show()
            });
            notification.show()
        } else {
            // 因为linux 平台下的 tray 没有什么用，所以不能让他hide
            window.minimize()
        }
    });
    const menus = Menu.buildFromTemplate([...Menu.getApplicationMenu().items.slice(1).concat([{
        label: 'Quit', click: function () {
            // 强制退出，这个退出不会被阻止，因为不会触发 'close'事件
            app.exit(0)
        }
    }])])
    Menu.setApplicationMenu(menus)
}

// 这个方法要在ready之后调用
function configApp() {
    // 这个事件在 linux 平台会得不到响应
    // 这个事件在 linux 平台会得不到响应
    // 这个事件在 linux 平台会得不到响应 ,基本上就是一个摆设 ， 所以这里应该判断一下 平台
    const pl = process.platform;
    console.log("platform ,",pl)
    if (pl !== 'linux') {

        let tray = new Tray(path.join(app.getAppPath(), 'assets', 'app-tray.png'));
        tray.on('click', function () {
            console.log(arguments)
        });
        const contextMenu = Menu.buildFromTemplate([
            {
                label: '还原', click: function (menuItem, browserWindow) {

                    console.log('show window button clicked!')
                    if (browserWindow) {
                        browserWindow.show()
                    }
                }
            }, {
                label: '退出', role: 'quit'
            }
        ])
        tray.setToolTip('electron demo!')
        tray.setContextMenu(contextMenu)
        console.log("tray config set over")
    }
}

module.exports = {configBrowserWindow, configApp}