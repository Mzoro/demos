function isDebug() {
    const args = process.argv;
    if (!args || !args.length) {
        return false
    }

    const debugFlag = args.find(value => '--debug' === value);
    return !!debugFlag
}

module.exports = {isDebug}