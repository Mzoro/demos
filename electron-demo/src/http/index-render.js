const {ipcRenderer} = require('electron')
const Common = require('../common/commonConstants')

function listenHttp() {
    console.log( `listening ${Common.HTTP_EVENT} event ...`)
    ipcRenderer.on(Common.HTTP_EVENT, function () {
        console.log(`recieved ipcMain message ${arguments[1]}`)
    });
}

module.exports = {listenHttp}