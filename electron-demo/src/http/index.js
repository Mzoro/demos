const http = require('http')
const {app} = require('electron')
const Common = require('../common/commonConstants')
let server = null

function httpStart(win) {
    if (server != null) {
        console.log(`http server has already been running at ${server}`)
        return
    }
    const args = process.argv;
    let port = 4200;
    for (let i = 0; i < args.length; i++) {
        if (args[i] === '--server-port') {
            if (args[i + 1]) {
                port = Number(args[i + 1]);
            }
        }
    }
    portIsOccupied(port).then(value => {
        doHttpStart(win, value)
    }).catch(value => {
        console.error(`start http server failed`, value)
        app.quit()
    })
}

function doHttpStart(win, port) {


    console.log(`http server is going to listen port ${port}`)
    // 启动端口监听
    server = http.createServer((req, res) => {
        console.log("http request is coming")
        // console.log(webContents.getAllWebContents())
        // 发送事件，这样就可以在render进程执行
        win.webContents.send(Common.HTTP_EVENT, "ssss")
        // 在主线程中调用页面的js
        win.webContents.executeJavaScript(`
            console.log(window)
        `).then(function () {
            console.log("executeJavaScript finished")
        });
        res.statusCode = 200
        res.end()
        res.writeProcessing()
    }).listen(port)

    // 程序退出时停下
    app.on('quit', function () {
        console.log(`http server is going to die`)
        server.close()
    });
}

function portIsOccupied(port) {
    const s = http.createServer().listen(port)
    return new Promise((resolve, reject) => {
        s.on('listening', () => {
            console.log(`the server is running on port ${port}`)
            s.close()
            resolve(port)
        })

        s.on('error', (err) => {
            if (err.code === 'EADDRINUSE') {
                resolve(portIsOccupied(port + 1))//注意这句，如占用端口号+1
                console.log(`this port ${port} is occupied.try another.`)
            } else {
                reject(err)
            }
        })
    })
}


module.exports = {httpStart};