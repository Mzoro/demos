const screen = require('./mainScreen');
const file = require('./mainFile');
const mainHttpTools = require('./mainHttpTools');

function initAllEventListener() {
    if (typeof screen.fullScreenEvent === 'function') {
        screen.fullScreenEvent()
    }
    if (typeof screen.exitFullScreenEvent === 'function') {
        screen.exitFullScreenEvent()
    }
    if (typeof file.readFileEvent === 'function') {
        file.readFileEvent()
    }
    if (typeof mainHttpTools.listenRequest === 'function') {
        mainHttpTools.listenRequest()
    }
}

module.exports = initAllEventListener