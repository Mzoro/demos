const path = require('path');
const fs = require('fs');
const {ipcMain, app} = require('electron');
const Common = require('../common/commonConstants')

function readFileEvent() {
    ipcMain.on(Common.READ_FILE_EVENT, function (event) {
        var filePath = path.join(app.getAppPath(), "assets", "test.txt");
        var bf = fs.readFileSync(filePath);
        event.returnValue = bf.toString()
    });
}

module.exports = {readFileEvent}
