const {ipcMain, BrowserWindow, webContents, Notification} = require('electron')
const Common = require('../common/commonConstants')
const axios = require('axios');

function listenRequest() {
    ipcMain.on(Common.HTTP_REQUEST_EVENT, function (event, args) {
        const url = args.url;
        const data = args.data;
        console.log("recieve " + Common.HTTP_REQUEST_EVENT,url,data)

        axios.post(url, data).then(function () {
            console.log(arguments)
        });

    });
}

module.exports = {listenRequest}
