const {ipcMain, BrowserWindow, webContents,Notification} = require('electron')
const Common = require('../common/commonConstants')

function fullScreenEvent() {
    console.log(`start to listen ${Common.FULL_SCREEN_EVENT} event`)
    ipcMain.on(Common.FULL_SCREEN_EVENT, function (event) {
        console.log("into full screen model");
        /* 1. 这里使用的是当前拥有焦点的窗口，如果程序有很多窗口需要精准的控制，那么可以使用 BrowserWindow.fromId(id),
              但是这个id 不是那么的好得到 ，他是创建BrowserWindow对象时的返回对象的一个readonly属性
           2. 还有一个比较绕的方法 ，第一个入参是 eventEmitter 对象，这个对象中的sender的id 是触发事件对应的 WebContent的id,
              官网在这里说的 https://www.electronjs.org/docs/api/structures/ipc-renderer-event#ipcrendererevent%E5%AF%B9%E8%B1%A1%E7%BB%A7%E6%89%BFevent
              通过这个id找到WebContent ,然后通过  BrowserWindow.fromWebContents()来全屏
        */
        // BrowserWindow.getFocusedWindow().setFullScreen(true);
        BrowserWindow.fromWebContents(webContents.fromId(event.sender.id)).setFullScreen(true)

        // 弹出系统提示窗 ,在 main 线程中这样写 而且要在 'electron'模块中导入 ，在 render 线程中不用
        const notification = {
            title: '提示',
            body: '全屏成功!'
        }
        new Notification(notification).show()
    });
}

function exitFullScreenEvent() {
    console.log(`start to listen ${Common.EXIT_FULL_SCREEN_EVENT} event`)
    //  ipcMain.handle 和 ipcRender.invoke一起使用，ipcRender.invoke返回值是一个Promise对象 ，参数就是handle的返回值
    ipcMain.handle(Common.EXIT_FULL_SCREEN_EVENT, async function (event) {
        console.log("into exit full screen model");
        await BrowserWindow.getFocusedWindow().setFullScreen(false);
        console.log(new Date().getTime())
        return 'success'
    });
}

module.exports = {fullScreenEvent, exitFullScreenEvent}