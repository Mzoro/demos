const axios = require('axios');
const {ipcRenderer} = require('electron')
const Common = require('../common/commonConstants')

// 这个方法运行在ipcRenderer线程中，相关于运行在浏览器中 ，所以会产生跨域问题
function post(url, data, callback) {
    return axios.post(url, data).then(value => {
        if (typeof callback === 'function') {
            callback(value)
        }
    }).catch(err => {
        console.log(err)
    })
}

function postInMain(url, data, callback) {
    console.log(url,data)
    ipcRenderer.send(Common.HTTP_REQUEST_EVENT, {url, data})
}


module.exports = {post, postInMain}
