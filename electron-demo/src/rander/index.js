const ScreenApi = require('./randerScreen');
const FileApi = require('./renderFile');
const httpTools = require('./httpTools');

module.exports = {ScreenApi, FileApi, httpTools}