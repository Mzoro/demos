const {ipcRenderer} = require('electron')
const Common = require('../common/commonConstants')


function fullScreen() {
    console.log("it's going to full screen");
    // 向 ipcMain 发送一个事件，事件的名字自定义 后面的参数都会被传入 ipcMain监听方法的第二个参数中(数组) ，
    ipcRenderer.send(Common.FULL_SCREEN_EVENT)
}

function exitFullScreen() {
    console.log("it's going to exit full screen");
    ipcRenderer.invoke(Common.EXIT_FULL_SCREEN_EVENT).then(function () {
        // 弹出系统提示窗 , 在render 线程中这样写  ，不需要在 'electron ' 中导入 ，它是 WebApi 提供的接口
        // https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification
        const myNotification = new Notification('Title', {
            body: 'Notification from the Renderer process'
        })

        myNotification.onclick = () => {
            console.log('Notification clicked')
        }

    });
}

module.exports = {fullScreen, exitFullScreen}