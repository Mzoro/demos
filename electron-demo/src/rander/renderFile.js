const {ipcRenderer} = require('electron');
const Common = require('../common/commonConstants')

function readFile() {
   return  ipcRenderer.sendSync(Common.READ_FILE_EVENT);
}

module.exports = {readFile}