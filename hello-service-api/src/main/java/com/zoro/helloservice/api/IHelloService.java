package com.zoro.helloservice.api;

import com.zoro.helloservice.entity.SysRole;
import com.zoro.helloservice.entity.SysUser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

public interface IHelloService {

    /**
     * 通过ID 查找 角色信息
     *
     * @param sysRole 角色查询条件
     * @return 查找结果
     */
    @RequestMapping("/findRole")
    public SysRole findRole(@RequestBody SysRole sysRole);

}
