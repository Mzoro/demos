package com.zoro.helloservice.entity;

/**
 * @author zoro
 * create in 2018/6/23
 */
public class SysRole {

    private Long id;
    private String roleName;
    private String createTime;
    private String creatorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", creatorId='" + creatorId + '\'' +
                '}';
    }
}
