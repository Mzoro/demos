package com.example.hibernate.practice.dbconfig;

import bitronix.tm.resource.jdbc.PoolingDataSource;

/**
 * META-INF/persistence.xml是 hibernate的配置文件
 * jndi.properties是 Bitronix的配置文件，Bitronix 是JTA的一种实现 ，网站在这里，官网中也说明了如何支持hibernate
 * http://web.archive.org/web/20150520175151/https://docs.codehaus.org/display/BTM/Home
 * https://github.com/bitronix/btm/blob/master/btm-docs/README.md
 *
 * 只要在程序中生成了DataSource hibernate 就可以使用了，其他的不用管
 *
 * @author zhaoxingwu
 */
public class InitDb {

    static {
        init();
    }

    public static PoolingDataSource init() {
                /*
        Bitronix 生成一个DataSource ，hibernate就可以用了，
         */
        PoolingDataSource dataSource = new PoolingDataSource();
        // 这个 uniqueName 要和persistence.xml的 <jta-data-source>maria</jta-data-source> 对应
        dataSource.setUniqueName("maria");
        dataSource.setClassName("org.mariadb.jdbc.MariaDbDataSource");
        dataSource.setMaxPoolSize(3);
//        dataSource.getDriverProperties().put("databaseName", "test");
        dataSource.getDriverProperties().put("user", "root");
        dataSource.getDriverProperties().put("password", "123");
        dataSource.getDriverProperties().put("url", "jdbc:mariadb://localhost:3306/zoro?allowMultiQueries=true");
        dataSource.setAllowLocalTransactions(true);
        dataSource.init();

        System.out.println("数据库连接加载成功");
        return dataSource;

    }
}
