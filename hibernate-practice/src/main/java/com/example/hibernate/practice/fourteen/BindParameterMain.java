package com.example.hibernate.practice.fourteen;

import com.example.hibernate.practice.dbconfig.InitDb;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * @author zhaoxingwu
 */
public class BindParameterMain extends InitDb {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select u from User u where u.name = :name")
                // 绑定参数
                .setParameter("name", "赵赵");
        System.out.println(query.getResultList());

        query = em.createQuery("select u from User u where u.name = ?1")
                // 按照索引绑定参数
                .setParameter(1, "张三");
        System.out.println(query.getResultList());

        // 对于Hibernate jpql 可以自动绑定内嵌对象的标识属性(@Id),所以，如果你想静态内嵌对象的id，就可以省略不写，但我猜只限于一个Id的

    }
}
