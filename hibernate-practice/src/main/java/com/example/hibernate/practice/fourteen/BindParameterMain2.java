package com.example.hibernate.practice.fourteen;

import com.example.hibernate.practice.dbconfig.InitDb;
import com.example.hibernate.practice.entity.User;
import org.hibernate.ScrollMode;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * @author zhaoxingwu
 */
public class BindParameterMain2 extends InitDb {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");
        EntityManager em = emf.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);

        // 从这里开发相当于使用java的方法 描述出应该怎么生成 jpql
        //  select u from User u where name = #{p_name}
        // 以这个 jpql 为例 ，顺序是下面这样的
        Root<User> i = query.from(User.class); // 2. from User
        CriteriaQuery<User> cc =
                query.select(i)                // 1. select u
                        .where(                // 3. where
                                cb.equal(              // 5. =
                                        i.get("name"), // 4. u.name
                                        cb.parameter(String.class, "p_name") // :p_name
                                )
                        );
        TypedQuery<User> query1 = em.createQuery(cc)
                // 绑定参数
                .setParameter("p_name", "张1三");

        // CriteriaBuilder 用于创建 一些声明部分，操作符部分 ，声明参数占位符部分
        // CriteriaQuery   用于创建jpql 语句的select,where,from ,group by, order 部分
        // Root            代表 jpql中Entity的 别名，所以所有jpql中要用的 u.xxx的地方就用Root对象
        // TypedQuery      代表整个jpql 语句，绑定参数,设置分页，执行查询
        // EntityManager   用于创建 TypeQuery 和 CriteriaBuilder
        // TypedQuery<User> query1 = em.createQuery(cc) 这个一定要在参数构造完成之后再调用，否则可能找不到绑定参数

        // 如果你要查  User 中的某几个列，并且要返回一个特别的对象，就比较麻烦，有下面几个不同
        // 1. cb.createQuery(User.class);这里的User 要改为你的特别的对象，这个对象可以不用Entity修饰，createQuery的意思就是我要查这个指定类型的对象了
        // 2. query.from(User.class) ; 这里还是不用变，里为这个代表了数据库中哪个表
        // 3. query.select(i) 这里有点变化 要写成 cq.multiselect(root.get(prop1),root.get(prop2)),这样查询语句中就只有这两个列了


        // 下面这几行是用hibernate 的api 查询最大行数，说是这种方式效率好，但是不是所以数据库都可用，-_-
        org.hibernate.query.Query<?> hQuery = query1.unwrap(org.hibernate.query.internal.QueryImpl.class);

        org.hibernate.ScrollableResults cursor = hQuery.scroll(ScrollMode.SCROLL_INSENSITIVE);
        cursor.last();
        int count = cursor.getRowNumber() + 1;
        cursor.close();
        System.out.println(count);
        System.out.println(query1.getResultList());


    }
}
