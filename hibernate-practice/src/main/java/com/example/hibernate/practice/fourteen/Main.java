package com.example.hibernate.practice.fourteen;

import com.example.hibernate.practice.dbconfig.InitDb;
import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;

/**
 * @author zhaoxingwu
 */
public class Main {

    private static final DataSource DATA_SOURCE = InitDb.init();

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select u from User u");
        List result = query.getResultList();

        System.out.println(result);
    }
}
