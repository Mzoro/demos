package com.example.hibernate.practice.fourteen;

import com.example.hibernate.practice.dbconfig.InitDb;
import com.example.hibernate.practice.entity.User;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Selection;
import javax.sql.DataSource;
import java.util.List;

/**
 * @author zhaoxingwu
 */
public class Main2 {

    private static final DataSource DATA_SOURCE = InitDb.init();

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");
        // EntityManager 主要负责 创建Query对象，创建 CriteriaBuilder对象，创建各种图 ，查询作用很少
        EntityManager em = emf.createEntityManager();

        //
        CriteriaBuilder cb = em.getCriteriaBuilder();

        //criteriaQuery 用于描绘如何创建sql,参数代表返回结果的实体类型
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
        // 设置 根类，这是一必须的
        Selection<User> selection = criteriaQuery.from(User.class);
        // 我没理解错的话，这个方法用于更换根类型
        criteriaQuery.select(selection);

        // TypedQuery 类型安全的查询对象 ,用于执行查询
        TypedQuery<User> query = em.createQuery(criteriaQuery);

        List<User> result = query.getResultList();
        System.out.println(result);
    }
}
