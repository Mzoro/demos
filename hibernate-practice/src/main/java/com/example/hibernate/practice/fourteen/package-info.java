/**
 * @see com.example.hibernate.practice.fourteen.Main 最基本的
 * @see com.example.hibernate.practice.fourteen.Main2 类型安全的
 * @see com.example.hibernate.practice.fourteen.BindParameterMain 绑定参数 jpql 方式
 * @see com.example.hibernate.practice.fourteen.BindParameterMain2 绑定参数 编程方式
 */
package com.example.hibernate.practice.fourteen;
