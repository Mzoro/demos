package com.example.hibernate.practice.multiple;

import com.example.hibernate.practice.dbconfig.InitDb;
import com.example.hibernate.practice.multiple.entities.ResultEntity;
import com.example.hibernate.practice.multiple.entities.User;
import com.example.hibernate.practice.multiple.entities.UserAddress;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

/**
 * @author zhaoxingwu
 */
public class MultipleSelectMain extends InitDb {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();


        CriteriaQuery<ResultEntity> criteriaQuery = builder.createQuery(ResultEntity.class);
        Metamodel metamodel = em.getMetamodel();
        EntityType<User> _user = metamodel.entity(User.class);

        Root<User> userRoot = criteriaQuery.from(User.class);
        Root<UserAddress> userAddressRoot = criteriaQuery.from(UserAddress.class);
//        userRoot.join(_user.getSet());
        criteriaQuery.where(userRoot.get("id"));

        criteriaQuery.multiselect(userRoot.get("id"), userRoot.get("name"));
        TypedQuery<ResultEntity> resultEntityTypedQuery = em.createQuery(criteriaQuery);


        System.out.println(resultEntityTypedQuery.getResultList());
    }

}
