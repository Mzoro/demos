package com.example.hibernate.practice.multiple.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author zhaoxingwu
 */
@Entity
public class Address {
    @Id
    private String id;
    private String addressName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }
}
