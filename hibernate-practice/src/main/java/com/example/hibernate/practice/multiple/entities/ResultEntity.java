package com.example.hibernate.practice.multiple.entities;

/**
 * @author zhaoxingwu
 */
public class ResultEntity {
    private String userId;
    private String addrId;
    private String userName;
    private String addressName;

    public ResultEntity() {
    }

    public ResultEntity(String userId , String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public ResultEntity(String userId, String addrId, String userName, String addressName) {
        this.userId = userId;
        this.addrId = addrId;
        this.userName = userName;
        this.addressName = addressName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddrId() {
        return addrId;
    }

    public void setAddrId(String addrId) {
        this.addrId = addrId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String toString() {
        return "ResultEntity{" +
                "userId='" + userId + '\'' +
                ", addrId='" + addrId + '\'' +
                ", userName='" + userName + '\'' +
                ", addressName='" + addressName + '\'' +
                '}';
    }
}
