package com.example.hibernate.practice.multiple.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author zhaoxingwu
 */
@Entity(name = "User_Two")
@Table(name = "USER")
public class User {
    private String name;
    @Id
    private String id;
    private Date createTime;

    @OneToMany
//    @JoinTable(name = "user_address",
//            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "addr_id", referencedColumnName = "id"))
    private List<Address> address;

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
