package com.example.hibernate.practice.two;

import bitronix.tm.TransactionManagerServices;
import com.example.hibernate.practice.dbconfig.InitDb;
import com.example.hibernate.practice.entity.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import javax.transaction.*;
import java.sql.SQLException;
import java.util.List;

/**
 * @author zhaoxingwu
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);


    private static final DataSource DATA_SOURCE = InitDb.init();

    /**
     * 这个类使用的都是jpa 中的 api ，可以看到代码中没有出现 hibernate 相关内容
     *
     * @param args args
     * @throws HeuristicRollbackException e
     * @throws HeuristicMixedException    e
     * @throws RollbackException          e
     * @throws SystemException            e
     * @throws NotSupportedException      e
     * @throws ClassNotFoundException     e
     * @throws SQLException               e
     */
    public static void main(String[] args) throws HeuristicRollbackException, HeuristicMixedException, RollbackException, SystemException, NotSupportedException, ClassNotFoundException, SQLException {

        // 1. 构建对象管理工场
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");

        // 2. 事务管理
        UserTransaction tx = TransactionManagerServices.getTransactionManager();
        tx.begin();

        // 3. 对象管理器
        EntityManager entityManager = emf.createEntityManager();
        Message message = new Message();
        message.setText("Hello 中国");

        // 保存
        entityManager.persist(message);
        // 提交事务
        tx.commit();


        tx = TransactionManagerServices.getTransactionManager();
        tx.begin();
        EntityManager em2 = emf.createEntityManager();

        // 查询 注意这里不是 sql ,这里的Message 是 Message类名，区分大小写的
        List list = em2.createQuery("select  m from Message m ").getResultList();

        LOGGER.error(list.toString());
        tx.commit();
        em2.close();
    }
}
