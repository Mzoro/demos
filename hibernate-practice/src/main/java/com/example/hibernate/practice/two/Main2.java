package com.example.hibernate.practice.two;

import bitronix.tm.TransactionManagerServices;
import com.example.hibernate.practice.dbconfig.InitDb;
import com.example.hibernate.practice.entity.Message;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.dialect.MariaDBDialect;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.CriteriaQueryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.sql.DataSource;
import javax.transaction.*;
import java.security.cert.Certificate;
import java.sql.SQLException;
import java.util.List;

/**
 * META-INF/persistence.xml是 hibernate的配置文件
 * jndi.properties是 Bitronix的配置文件，Bitronix 是JTA的一种实现 ，网站在这里，官网中也说明了如何支持hibernate
 * http://web.archive.org/web/20150520175151/https://docs.codehaus.org/display/BTM/Home
 * https://github.com/bitronix/btm/blob/master/btm-docs/README.md
 *
 * @author zhaoxingwu
 */
public class Main2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main2.class);


    private static final DataSource DATA_SOURCE = InitDb.init();

    /**
     * 这个测试使用的是 Hibernate 的api
     *
     * @param args args
     */
    public static void main(String[] args) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {

        // 1. 加载配置项
        StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();
        // 这里的配置和 hibernate.cfg.xml的内容是对应的
        standardServiceRegistryBuilder.applySetting("hibernate.format_sql", false);
        standardServiceRegistryBuilder.applySetting("hibernate.use_sql_comments", true);
        standardServiceRegistryBuilder.applySetting("hibernate.dialect", MariaDBDialect.class.getName());
        standardServiceRegistryBuilder.applySetting("hibernate.connection.datasource", "maria");
//        standardServiceRegistryBuilder.applySetting("hibernate.hbm2ddl.auto", "create-drop");

        // 也可以通过配置文件的形式加载
//        standardServiceRegistryBuilder.getConfigLoader();
//        standardServiceRegistryBuilder.loadProperties()
        // 2. 创建SessionFactory
        SessionFactory sessionFactory = new MetadataSources(standardServiceRegistryBuilder.build())
                // 3. 增加Class
                .addAnnotatedClass(Message.class)
                .buildMetadata()
                // 4. 创建
                .buildSessionFactory();

        // 如果用jpa 上面4个可以合并为一个 EntityManagerFactory emf = Persistence.createEntityManagerFactory("HelloWorldPU");

        Session session = sessionFactory.openSession();
        // 5. 事务
        Transaction transaction = session.beginTransaction();
        Message message = new Message();
        message.setText("Hi, 沈阳");

        // 6. insert
        session.save(message);
        // 7. 提交
        transaction.commit();
        session.close();

        Session session1 = sessionFactory.openSession();
        transaction =  session1.beginTransaction();
        Message message1 = session1.find(Message.class, 1L);
        LOGGER.error("查询结果：{}", message1.toString());

        List list = session1.createQuery("select m from Message m").getResultList();
        LOGGER.error("查询结果2：{}", list);
        transaction.commit();
        session1.close();

    }
}
