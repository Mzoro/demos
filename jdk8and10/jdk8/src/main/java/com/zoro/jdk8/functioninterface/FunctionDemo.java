package com.zoro.jdk8.functioninterface;

/**
 * @author zhaoxw
 * Create at 2018/9/18 9:43
 * @version 1.0
 */
@FunctionalInterface
public interface FunctionDemo {

    void haha();

    //jdk8 以后可以, 为了即可以扩展原有接口,又可以兼容原有实现
    default void hehe() {
        System.out.println("FunctionDemo.hehe");
    }

    //jdk8 以后可以,
    static void yaya() {
        System.out.println("FunctionDemo.yaya");
    }
    String toString();
}
