package com.zoro.jdk8.functioninterface;


/**
 * 函数接口
 *
 * @author zhaoxw
 * Create at 2018/9/18 9:41
 * @version 1.0
 */
public class FunctionInterfaceDemo {

    public static void main(String[] args) {
        TestFunctionInterface.testFunction(() -> System.out.println("functionInterface,lambda"));

        TestFunctionInterface.testNormalInterface(new NormalInterface() {
            @Override
            public void haha() {
                System.out.println("NormalInterface.haha");
            }

            @Override
            public void hehe() {
                System.out.println("NormalInterface.hehe");
            }
        });
    }
}
