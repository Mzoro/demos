package com.zoro.jdk8.functioninterface;

/**
 * @author zhaoxw
 * Create at 2018/9/18 11:27
 * @version 1.0
 */
public interface NormalInterface {

    void haha();

    void hehe();
}
