package com.zoro.jdk8.functioninterface;

/**
 * @author zhaoxw
 * Create at 2018/9/18 11:27
 * @version 1.0
 */
public class TestFunctionInterface {

    public static void testFunction(FunctionDemo functionDemo) {
        functionDemo.haha();
        functionDemo.hehe();
        FunctionDemo.yaya();
    }

    public static void testNormalInterface(NormalInterface normalInterface) {
        normalInterface.haha();
        normalInterface.haha();
    }
}
