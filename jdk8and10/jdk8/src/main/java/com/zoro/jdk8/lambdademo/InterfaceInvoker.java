package com.zoro.jdk8.lambdademo;

/**
 * @author zhaoxw
 * Create at 2018/9/18 14:24
 * @version 1.0
 */
public class InterfaceInvoker {

    public void testNoParameterInterface(NoParameterInterface noParameterInterface) {
        noParameterInterface.test();
    }

    public User testMultipleParameterInterface(MultipleParameterInterface multipleParameterInterface) {
        return multipleParameterInterface.test("张三", 10);
    }
}
