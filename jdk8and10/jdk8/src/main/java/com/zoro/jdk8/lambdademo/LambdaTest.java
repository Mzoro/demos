package com.zoro.jdk8.lambdademo;

/**
 * lambda 表达式
 *
 * @author zhaoxw
 * Create at 2018/9/18 14:23
 * @version 1.0
 */
public class LambdaTest {

    public static void main(String[] args) {
        InterfaceInvoker interfaceInvoker = new InterfaceInvoker();
/*
        //first Test
        interfaceInvoker.testNoParameterInterface(() -> System.out.println("lambda expression!!"));

        //second Test
        interfaceInvoker.testNoParameterInterface(() -> {
            System.out.println("turn into lambda expression!!");
            System.out.println("turn out lambda expression!!");
        });

        //third Test
        User user = interfaceInvoker.testMultipleParameterInterface(((name, age) -> new User(name, age)));
        System.out.println(user);
*/
        //fourth Test 访问域
        //1. 匿名类的访问域
        //2. lambda 与匿名类 可以访问调用方法所在域的变量,但需要是final的或者实际上是final(jdk8)的
        User user2 = interfaceInvoker.testMultipleParameterInterface(new MultipleParameterInterface() {
            @Override
            public User test(String name, int age) {
                //lambda中不能调用这个方法
                return getDefaultUser();
            }
        });
        System.out.println(user2);

        User user3 = interfaceInvoker.testMultipleParameterInterface(((String name, int age) -> {
            return new User(name, age);
        }));
        System.out.println(user3);
    }
}
