package com.zoro.jdk8.lambdademo;

/**
 * @author zhaoxw
 * Create at 2018/9/18 13:45
 * @version 1.0
 */
public interface MultipleParameterInterface {

    User test(String name, int age);

    default User getDefaultUser() {
        return new User("李四", 20);
    }

}
