package com.zoro.jdk8.lambdademo;

/**
 * @author zhaoxw
 * Create at 2018/9/18 13:41
 * @version 1.0
 */
public interface NoParameterInterface {

    void test();

}
