package com.zoro.jdk8.lambdademo;

/**
 * @author zhaoxw
 * Create at 2018/9/18 13:45
 * @version 1.0
 */
public class User {

    private String name;
    private int age;
    private String familyName;

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public User(String name, int age, String familyName) {
        this.name = name;
        this.age = age;
        this.familyName = familyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", familyName='").append(familyName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
