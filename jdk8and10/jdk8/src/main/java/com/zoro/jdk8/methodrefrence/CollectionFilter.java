package com.zoro.jdk8.methodrefrence;

/**
 * @author zhaoxw
 * Create at 2018/9/18 16:00
 * @version 1.0
 */
public interface CollectionFilter<E, T> {

    T filter(E e);
}
