package com.zoro.jdk8.methodrefrence;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhaoxw
 * Create at 2018/9/18 15:59
 * @version 1.0
 */
public class MapCollection<E, T> {

    private Collection<E> source;
    private CollectionFilter<E, T> collectionFilter;

    public MapCollection(Collection<E> source) {
        this.source = source;
    }

    public MapCollection<E, T> setFilter(CollectionFilter<E, T> collectionFilter) {
        this.collectionFilter = collectionFilter;
        return this;
    }

    public Set<T> get() {
        //对某个对象的引用 collectionFilter.filter会执行多次,但是你没有debug的地方
        return source.stream().map(collectionFilter::filter).collect(Collectors.toSet());
    }
}
