package com.zoro.jdk8.methodrefrence;

import com.zoro.jdk8.lambdademo.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 方法引用
 *
 * @author zhaoxw
 * Create at 2018/9/18 15:09
 * @version 1.0
 */
public class MethodRefrenceTest {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("张三", 15));
        users.add(new User("张四", 15));
        users.add(new User("张五", 15));
        users.add(new User("张六", 15));
        users.add(new User("张七", 15));
        users.add(new User("张八", 15));
        users.add(new User("张九", 15));
        users.add(new User("张十", 15));

        //引用静态方法
        users.forEach(StaticMethodDemo::printUser);
        //分步写
        //forEach 方法需要一个 Consumer的实现类
        Consumer<User> consumer = new Consumer<User>() {
            @Override
            public void accept(User user) {
                //这个方法只有一个操作,就是调用StaticMethodDemo的静态方法printUser
                StaticMethodDemo.printUser(user);
            }
        };
        users.forEach(consumer);

        //一个类的任意对象的方法引用
        Set<String> names = new MapCollection<User, String>(users).setFilter(User::getName).get();
        System.out.println(names.size());

        //对构造方法的引用
        List<User> users1 = Stream.of("王二", "王三", "王四", "王五", "王六")
                .map(User::new)
                .collect(Collectors.toCollection(ArrayList::new));


    }
}
