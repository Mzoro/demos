package com.zoro.jdk8.methodrefrence;

import com.zoro.jdk8.lambdademo.User;

/**
 * @author zhaoxw
 * Create at 2018/9/18 15:30
 * @version 1.0
 */
public class StaticMethodDemo {

    public static void printUser(User user) {
        System.out.println("My name is " + user.getName() + "!");
        System.out.println("I am  " + user.getAge() + " years old!");
    }
}
