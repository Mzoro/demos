package com.zoro.jdk8.multiple;

/**
 * @author zhaoxw
 * Create at 2018/9/18 13:01
 * @version 1.0
 */
public interface Something {

    default String describe() {
        return "I'm Something!";
    }
}
