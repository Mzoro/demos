package com.zoro.jdk8.multiple;

/**
 * 多级继承
 *
 * @author zhaoxw
 * Create at 2018/9/18 12:58
 * @version 1.0
 */
public class TestMultiple {

    public static void main(String[] args) {
        System.out.println(new ChenGuangPen().describe());
    }
}
