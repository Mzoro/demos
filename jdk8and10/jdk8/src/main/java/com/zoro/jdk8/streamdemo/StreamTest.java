package com.zoro.jdk8.streamdemo;

import com.zoro.jdk8.lambdademo.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * {@link Stream} 类的用法 需要配合 {@link Collector},{@link Collectors},{@link Predicate},{@link Supplier}等等等等等等 使用
 *
 * @author zhaoxw
 * Create at 2018/9/18 16:52
 * @version 1.0
 * @see java.util.function
 */
public class StreamTest {

    public static void main(String[] args) {
        List<User> userZhang = new ArrayList<>();
        userZhang.add(new User("张三", 15, "张"));
        userZhang.add(new User("张四", 16, "张"));
        userZhang.add(new User("张五", 17, "张"));
        userZhang.add(new User("张六", 18, "张"));
        userZhang.add(new User("张七", 19, "张"));
        userZhang.add(new User("张八", 20, "张"));
        userZhang.add(new User("张九", 21, "张"));
        userZhang.add(new User("张十", 22, "张"));

        List<User> userWang = new ArrayList<>();
        userWang.add(new User("王三", 15, "王"));
        userWang.add(new User("王四", 16, "王"));
        userWang.add(new User("王五", 17, "王"));
        userWang.add(new User("王六", 18, "王"));
        userWang.add(new User("王七", 19, "王"));
        userWang.add(new User("王八", 20, "王"));
        userWang.add(new User("王九", 21, "王"));
        userWang.add(new User("王十", 22, "王"));

        //找出所有成年人
        List<User> adult = userZhang.stream().filter(user -> user.getAge() >= 18).collect(Collectors.toList());
        System.out.println(adult.size());
        /*
        filter()方法是一个惰性求值的方法,他仅仅胜于描述操作这个Collection的方法,但不会执行,lambda执行的时机是调用及早求值方法时
        filter()方法的返回值的类型与 原始Stream的类型是一至的  :  Stream<T> filter(Predicate<? super T> predicate);
        collect()方法是一个及早求值方法,他决定了最后的返回值的具体类型,List/Set,还可以决定使用具体哪一种实现 ArrayList/LinkedList
        collect()方法有有两个重载方法,
           1.<R, A> R collect(Collector<? super T, A, R> collector);
           2.<R> R collect(Supplier<R> supplier,BiConsumer<R, ? super T> accumulator,BiConsumer<R, R> combiner);不会
         */

        //找出成年人的另一种方式
        Predicate<User> predicate = user -> user.getAge() >= 18;
        List<User> adult2 = userZhang.stream().collect(ArrayList::new, (arr, u) -> {
            if (predicate.test(u)) {
                arr.add(u);
            }
        }, (first, second) -> {
            //这里没玩明白,根本没执行呀
            System.out.println("haha");
            first.addAll(second);
        });

        //找出所有姓名
        Set<String> names = userZhang.stream().map(User::getName).collect(Collectors.toCollection(HashSet::new));

        //多维度转低维度
        Stream<List<User>> userAll = Stream.of(userZhang, userWang);
        List<User> users = userAll.flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println(users);

        //stream.reduce  具说这个是万能的,Stream的所有功能可以用这个方法完成
        List<User> userList = users.stream().reduce(new ArrayList<User>(), (arr, u) -> {
//            ArrayList<User> utemp = new ArrayList<>();
//            utemp.addAll(arr);
//            utemp.add(u);
//            return utemp;
            arr.add(u);
            return arr;
        }, (one, two) -> {
//            two.addAll(one);
//            return two;
            one.addAll(two);
            return one;
        });

        /*
        第一个是默认值,或者说初始值,
        第二个lambda 表达式是具体要做的代码,第一个参数是上一次执行代码的返回值,第二个参数是Stream中的元素,调用这个方法的返回值由第一个参数决定
        第三个lambda一直没有执行过
         */

        String names2 = Stream.of("张三", "李四", "王二麻子").reduce((one, two) -> {
            StringBuilder temp = new StringBuilder(one);
            temp.append(two);
            return temp.toString();
        }).get();

        String names3 = Stream.of("张三", "李四", "王二麻子").reduce("啥也没有", (one, two) -> {
            StringBuilder temp = new StringBuilder(one);
            temp.append(two);
            return temp.toString();
        });

        users.stream().flatMapToInt(user -> IntStream.of(user.getAge())).forEach(System.out::println);


        /*****************************下面主要是collect()与Collector使用**************************************/

        //collection有两个重载方法
        // <R, A> R collect(Collector<? super T, A, R> collector);
        //<R> R collect(Supplier<R> supplier,BiConsumer<R, ? super T> accumulator,BiConsumer<R, R> combiner); 类似reduce
        Map<String, User> userMap = users.stream().collect(Collectors.<User, String, User>toMap(User::getName, u -> u));

        Map<String, User> userMap1 = users.stream().collect(HashMap::new, (m, u) -> {
            m.put(u.getName(), u);
        }, (m1, m3) -> System.out.println("看看这里有没有用"));

        //groupingBy(<返回key值>)
        Map<String, List<User>> usersGroupBy = users.stream().collect(Collectors.groupingBy(User::getFamilyName));
        //groupingBy(<返回key值  , value的实现:ArrayList/LinkedList>)
        Map<String, List<User>> usersGroupBy2 = users.stream().collect(Collectors.groupingBy(User::getFamilyName,Collectors.toCollection(LinkedList::new)));
        //groupingBy(<返回key值> , <最后结果Map的实现:HashMap/TreeMap> , <value的实现:ArrayList/LinkedList>)
        Map<String, List<User>> usersGroupBy3 = users.stream().collect(Collectors.groupingBy(User::getFamilyName,HashMap::new,Collectors.toCollection(LinkedList::new)));

    }
}
