package com.zoro.openjdk10;

import com.zoro.openjdk10.entities.People;
import com.zoro.openjdk10.entities.Person;
import com.zoro.openjdk10.entities.PredicateImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 1.lambda 表达式可以使用当前方法作用域内的变量
 * 2.lambda 表达式内不能调用接口的默认实现,匿名类可以
 * 3.lambda 表达式可以推断出变量类型,所以形参数的声明类型可以写也可以不写
 * 4.lambda 表达式引用的其所在方法中的变量必须是final 的,或者是实际上是final的(就是只赋值一次),但是类的属性就不用是final的;
 * <p>
 * 一切需要接口(只有一个抽象方法)的地方都可以改为 lambda
 *
 * @author zhaoxw
 * Create at 2018/9/5 10:34
 * @version 1.0
 * @see Stream
 * @see Collectors
 * @see Comparator
 */
public class Go {

    public static int jj = 5;

    public static void main(String[] args) {
   /*     String nn = "haha";
        int hh = 5;
        // 使用 lambda 表达式
        ISay iSay = (String name, int age) -> {
            System.out.println("My name is " + name + " " + nn);
            //1.
            System.out.println("I'm " + (age + hh + jj) + " years old!");
        };
        //4.
        //nn = "dsdf";
        //jj=0;
        iSay.say("lambda", 1);
        iSay.sayOver();

        // 不使用 lambda 表达式,使用显示的匿名类
        ISay iSay1 = new ISay() {
            @Override
            public void say(String name, int age) {
                System.out.println("My name is " + name + " " + nn);
                System.out.println("I'm " + (age + hh + jj) + " years old!");
                //2.
                sayOver();
            }
        };
        iSay1.say("Anonymous inner class", 2);

        //使用实现类
        ISay iSay2 = new SayImpl();
        iSay2.say("Implement Class", 3);
        iSay2.sayOver();
*/
        List<People> peopleList = new ArrayList<>();
        peopleList.add(new People("张三", 15, "张"));
        peopleList.add(new People("张四", 17, "张"));
        peopleList.add(new People("张五", 18, "张"));
        peopleList.add(new People("张六", 19, "张"));
        peopleList.add(new People("张七", 20, "张"));
        peopleList.add(new People("张八", 21, "张"));
        peopleList.add(new People("张九", 22, "张"));
        peopleList.add(new People("张十二", 23, "张"));

        /********************* Stream.filter ***************************/
        /*
        返回值是 Stream<T> 这个返回值与 source List<T>的类型一样,多用于对这个集合的筛选
         */
        Long count = peopleList.stream().filter(new Predicate<People>() {
            @Override
            public boolean test(People people) {
                System.out.println(people.getName());
                return people.getAge() > 20;
            }
        }).count();

        System.out.println(count);

        //如果只有一句,就可以省略 return,废话,业务这么简单还用你? 大大语法糖
        long count2 = peopleList.stream().filter(people -> people.getAge() > 15).count();
        System.out.println(count2);

        //所谓的笨写法
        //如果不调用返回值是除了Stream以外其他类型的方法,那么表达式/实现类是不会执行的,他就像 过滤链,准备好了这后需要doChain一下过会你的过滤规则
        long count3 = peopleList.stream().filter(new PredicateImpl()).count();
        System.out.println(count3);

        //Stream 就像数据流一样,count,collect只能操作一次,下面这个例子,想再次调用会报错
        Stream<People> peopleStream = peopleList.stream().filter(people -> people.getAge() > 20);
        List<People> peopleResult = peopleStream.collect(Collectors
                .toList());


        /********************* Stream.map ***************************/
        //Stream.filter 与 Stream.map可以同时出现
        List<Person> peopleResult2 = peopleList.stream()
                .filter(person -> person.getAge() > 20)
                .map(people -> new Person(people.getName(), people.getAge(), people.getFamilyName()))
                .collect(Collectors.toList());

        List<People> peopleResoult3 = peopleResult2.stream()
                .map(new Function<Person, People>() {
                    @Override
                    public People apply(Person person) {
                        return new People(person.getName(), person.getAge(), person.getFamilyName());
                    }
                })
                .collect(Collectors.toList());

        /********************* Stream.flatMap*******************************/
        //没啥意思,List合并
        List<People> peoples = new ArrayList<>();
        List<People> peoples2 = new ArrayList<>();
        peoples.add(new People("李四", 15, "李"));
        peoples.add(new People("李五", 16, "李"));
        peoples2.add(new People("李六", 17, "李"));
        peoples2.add(new People("李七", 18, "李"));
        List<People> flatResout = Stream.of(peoples, peoples2).flatMap(Collection::stream).collect
                (Collectors.toList());

        // 双冒号用法 : 前面的类就代表方法中传入的对象,返回的代表这个对象的方法
        List<People> flatResult2 = Stream.of(peoples, peoples2).flatMap(new Function<List<People>, Stream<People>>() {
            @Override
            public Stream<People> apply(List<People> people) {
                return people.stream();
            }
        }).collect(Collectors.toList());

        /********************* Stream.max/Stream.min*******************************/
        Comparator.comparing(People::getAge);
        Optional<People> minResult = peopleList.stream().min(Comparator.comparing(People::getAge));

        //人类 写法:
        Optional<People> minResult2 = peopleList.stream().min(Comparator.comparing(new Function<People, Integer>() {

            @Override
            public Integer apply(People people) {
                return people.getAge();
            }
        }));

        /*
        上面的非人类写法要从里向外看
        1. 我们向 comparing 方法传入了一个 lambda 表达式,这个表达式代表的是一个Function的匿名实现类;
        2. comparing 的返回值要求是 Comparable的实现类(这个例子里指Integer),因为我们用的是 jdk8以上,所以,可以返回 lambda ,所以就写成了
                 return (Comparator<T> & Serializable)
                    (c1, c2) -> keyExtractor.apply(c1).compareTo(keyExtractor.apply(c2));
            给它翻译回来,它的返回值应该是这样的
                return (Comparator<T> & Serializable)
                    new Comparator(){
                        @Override
                        int compare( return function.apply(c1).compareTo(function.apply(c2)))
                    }
         */
        /*Comparator.comparing() 这个方法就是返回一个Comparator 对象*/
        /*
            public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
            Function<? super T, ? extends U> keyExtractor)
            {
                Objects.requireNonNull(keyExtractor);
                return (Comparator<T> & Serializable)
                    (c1, c2) -> keyExtractor.apply(c1).compareTo(keyExtractor.apply(c2));
            }
         */

        //这是一个正常人写的方法 Comparator.comparing() 完全可以作为大学期末考试题,哼哼,玩吧
        //下面一步步看他自身变的 idea-> alt+enter
        Optional<People> minResult3 = peopleList.stream().min(new Comparator<People>() {
            @Override
            public int compare(People o1, People o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        /************************ 这他妈的叫代码好维护? debug 都看不到值******************************/
        /********************************** Stream.reduce ************************************/

        //求和,没有研究出来其他用法
        //第一个参数是前面计算的和,第二个参数是本次计算的元素
        int count5 = Stream.of(1, 2, 3, 4).reduce(new BinaryOperator<Integer>() {
            @Override
            //第一个参数是前面计算的和,第二个参数是本次计算的元素
            public Integer apply(Integer integer, Integer integer2) {
                return integer + integer2;
            }
        }).get();
        int count6 = Stream.of(1, 2, 3, 4).reduce((integer, integer2) -> integer + integer2).get();

        //使用reduce 实现filter
        List<People> s = new ArrayList<>();
        s.add(new People("王五", 15, "王"));
        s.add(new People("王六", 15, "王"));
        s.add(new People("王七", 15, "王"));
        s.add(new People("王八", 15, "王"));
        s.add(new People("王九", 15, "王"));

        List<People> r = new ArrayList<>();
        People t = s.stream().reduce((People p1, People p2) -> {
            r.add(p1);
            return p2;
        }).get();

        r.add(t);

        //找年纪最大的
        People maxAge = peopleList.stream().collect(Collectors.maxBy(Comparator.comparing(People::getAge))).get();

        //按familyName分组
        peopleList.addAll(s);
        peopleList.addAll(peoples);
        peopleList.addAll(peoples2);
        Map<String, List<People>> families = peopleList.stream().collect(Collectors.groupingBy(People::getFamilyName));
        //按familyName计算人数
        Map<String, Long> familiesCount = peopleList.stream().collect(Collectors.groupingBy(People::getFamilyName,
                Collectors.counting()));

        //分步操作
        Stream<People> streamPeople = peopleList.stream();
        Function<People, String> familyName = new Function<People, String>() {
            @Override
            public String apply(People people) {
                return people.getFamilyName();
            }
        };
        Collector<Object, ?, Long> countCollector = Collectors.counting();
        Collector<People, ?, Map<String, Long>> groupByCollector = Collectors.groupingBy(familyName, HashMap::new, countCollector);
        Map<String, Long> familiesCount2 = streamPeople.collect(groupByCollector);


    }

}
