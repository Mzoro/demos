package com.zoro.openjdk10;

import com.zoro.openjdk10.entities.People;
import com.zoro.openjdk10.reactive.PeoPleSubscriptor;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;

/**
 * 完全懵逼
 *
 * @author zhaoxw
 * Create at 2018/9/11 13:13
 * @version 1.0
 */
public class JustDoIt {

    public static long startlong = 0;

    public static void main(String[] args) throws InterruptedException {

        List<People> peopleList = new ArrayList<>();
        peopleList.add(new People("张三", 15, "张"));
        peopleList.add(new People("张四", 17, "张"));
        peopleList.add(new People("张五", 18, "张"));
        peopleList.add(new People("张六", 19, "张"));
        peopleList.add(new People("张七", 20, "张"));
        peopleList.add(new People("张八", 21, "张"));
        peopleList.add(new People("张九", 22, "张"));
        peopleList.add(new People("张十二", 23, "张"));
//        Flux<People> peopleFlux = Flux.fromIterable(peopleList);
//        peopleFlux.subscribeOn(Schedulers.parallel());
//
//        peopleFlux.subscribe(peoPleSubscriptor);
//        System.out.println("haha");
        PeoPleSubscriptor peoPleSubscriptor = new PeoPleSubscriptor();
        int i = 10000000;

        startlong = System.currentTimeMillis();
        Flux<People> peopleFlux1 = Flux.generate(
                () -> 0,
                (p, sink) -> {
                    sink.next(new People("张", p, "张"));
//                    System.out.println(p);
//                    if (p > 3 && p < 7) {
//                        try {
//                            System.out.println("sleep--"+Thread.currentThread().getName());
//                            Thread.sleep(5000);
//
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
                    if (p > i) {
                        sink.complete();
                    }
                    return p + 1;
                });
        peopleFlux1.parallel(1)
                .runOn(Schedulers.parallel())
                .subscribe(peoPleSubscriptor);
//                .subscribe(new PeoPleSubscriptor());
//        peopleFlux1.publishOn(Schedulers.elastic());
//        peopleFlux1.subscribeOn(Schedulers.elastic());

//        peopleFlux1.subscribe(peoPleSubscriptor);
        System.out.println("用时" + (System.currentTimeMillis() - startlong));

        People p = new People("", 2, "");
        synchronized (p) {
            p.wait();
        }
    }
}
