package com.zoro.openjdk10.entities;

/**
 * @author zhaoxw
 * Create at 2018/9/5 16:21
 * @version 1.0
 */
public class Person {
    private String name;
    private int age;
    private String familyName;
    public Person(String name, int age,String familyName) {
        this.name = name;
        this.age = age;
        this.familyName = familyName;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", familyName='").append(familyName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
