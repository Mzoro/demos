package com.zoro.openjdk10.entities;

import java.util.function.Predicate;

/**
 * @author zhaoxw
 * Create at 2018/9/5 15:23
 * @version 1.0
 */
public class PredicateImpl implements Predicate<People> {
    @Override
    public boolean test(People people) {
        System.out.println("impl:"+people.getName());
        return people.getAge() > 15;
    }
}
