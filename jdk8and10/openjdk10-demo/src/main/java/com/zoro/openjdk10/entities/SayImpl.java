package com.zoro.openjdk10.entities;

import com.zoro.openjdk10.interfaces.ISay;

import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/9/5 10:43
 * @version 1.0
 */
public class SayImpl implements ISay {

    @Override
    public void say(String name, int age) {
        System.out.println("my name is : " + name + ",");
        System.out.println("I'm " + age + " years old!");
        sayOver();
    }

}
