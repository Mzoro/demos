package com.zoro.openjdk10.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaoxw
 * Create at 2018/9/7 10:13
 * @version 1.0
 */
public class GenericTest {

    public static void main(String[] args) {

        //这种写法,在add时 只能是null,其他任何类型者错,
        //<? extends Fruit> 告诉编译器返回值只能是 Fruit及其子类
        List<? extends Fruit> fruitList = new ArrayList<>();
        Fruit apple = fruitList.get(0);

        List<? super Apple> fruitList2 = new ArrayList<>();
        fruitList2.add(new Apple());
        fruitList2.add(new AppleSon());
        //fruitList2.add(new Fruit());
    }
}
