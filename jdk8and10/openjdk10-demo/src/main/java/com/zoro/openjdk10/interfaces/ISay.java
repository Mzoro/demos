package com.zoro.openjdk10.interfaces;

/**
 * FunctionalInterface 这个注解可加可不加,它是在编译时告诉编译器,我这个接口只能有一个 public abstract 方法,但如果这个接口是Object的覆盖不算
 *
 * @author zhaoxw
 * Create at 2018/9/5 10:35
 * @version 1.0
 * @see FunctionalInterface
 */
@FunctionalInterface
public interface ISay {

    void say(String name, int age);

    default void sayOver() {
        System.out.println("say over!");
    }
}
