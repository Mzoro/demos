package com.zoro.openjdk10.reactive;

import com.zoro.openjdk10.JustDoIt;
import com.zoro.openjdk10.entities.People;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;

/**
 * @author zhaoxw
 * Create at 2018/9/11 13:25
 * @version 1.0
 */
public class PeoPleSubscriptor extends BaseSubscriber<People> {

    @Override
    protected void hookOnSubscribe(Subscription subscription) {
        System.out.println("subscription.request(3);");
        request(1);
    }

    @Override
    protected void hookOnNext(People value) {
        request(1);
//        System.out.println(value.getName() + "-" + value.getAge() + "来了!" + "--" + Thread.currentThread().getName());

//        if (value.getAge() < 18) {
//
//            System.out.println(value.getName() + "大于18,睡3秒!");
//            try {
//                Thread.sleep(50);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(value.getName() + "睡醒了!");
//        }

    }

    @Override
    protected void hookOnComplete() {
        System.out.println("complete" + (System.currentTimeMillis() - JustDoIt.startlong));
        System.exit(1);
    }
}
