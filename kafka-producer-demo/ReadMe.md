### com.zoro.kafka.topic
 这个包中包含对topic 的创建,增加partitions ,修改配置的例子;
 1. partitions 不能减少,只能增加这个动作一般是在增加broker后重新分配分区时需要的操作
 2. 最后一定要 调用 KafkaAdmin.close(); 才能让配置生效
### com.zoro.kafka.producer
 这个包中是一个生产者 使用比较简单
 1. 注意一定要 flush才能发送

### com.zoro.kafka.consumer
 1.这个包是一个消费者,它需要自己实现不接拉取消息的过程 (通过调用 poll方法);它一次会拉取很多消息
 2.每处理一个消息需要手动提交一次commitSync/commitAsync
 
### kafka中的一些概念
 > kafka 使用时需要对它的实现原理有大量的理解,它的broker/topic/consumer/producer 都有自己的配置,也可以在
 程序中动态修改,但是这些配置的修改很有可以带来很多联动的影响;
 
    message： kafka中最基本的传递对象，有固定格式。
    topic： 一类消息，如page view，click行为等。
    producer： 产生信息的主体，可以是服务器日志信息等。
    consumer： 消费producer产生话题消息的主体。
    broker： 消息处理结点，多个broker组成kafka集群。
    partition： topic的物理分组，每个partition都是一个有序队列。
    segment： 多个大小相等的段组成了一个partition。
    offset： 一个连续的用于定位被追加到分区的每一个消息的序列号，最大值为64位的long大小，19位数字字符长度。
 
 
 自己的大概理解(很可能有错误):
 1. message 是保存在具体的某个 partition上的;
 2. producer 会指定消息发送到哪个分区上(但是我在发送时并没有明确操作过,官网上也说过会均衡到每个分区上)
 3. consumer 监听的也是一个具体的分区,而不是哪个主题
 4. 这里有个问题,分区上没有所有的消息,那么它每次需要从其他分区上得到数据吗?
 5. replication-factor 可以配置一个topic 需要有几个复制因子
 6. offset 当代码中consumer调用了 poll方法  ,他可能会拉取了很多方法,如果在处理中这个consumer 挂了,
 其他consumer在接手这个分区时,就会从它commitSync的最后一个offset开始处理;所以我们每处理一个消息,都要调用commitSync/commitAsync方法通知 partition 处理到哪里了
 7. 6 中说的一次拉取多少个,每次接取消息的大小都可以配置