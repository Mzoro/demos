package com.zoro.kafka.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author zhaoxw
 * Create at 2018/8/18 16:03
 * @version 1.0
 */
public class ConsumerDemo {

    public static void main(String[] args) {

        Map<String, Object> config = new HashMap<>(16);

        //下面的四个是必须的,其他的可以在ConsumerConfig中找到
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization" +
                ".StringDeserializer");
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization" +
                ".StringDeserializer");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "inJava");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(config);
        ConsumerThread consumerThread = new ConsumerThread(consumer);
        Thread thread = new Thread(consumerThread);
        thread.start();
    }

    /**
     * 参考 https://www.cnblogs.com/rainwang/p/7493742.html
     */
    static class ConsumerThread implements Runnable {
        private KafkaConsumer<String, String> kafkaConsumer;

        public ConsumerThread(KafkaConsumer<String, String> consumer) {
            this.kafkaConsumer = consumer;
        }


        /**
         * kafka 的KafkaConsumer对象调用poll方法从kafka服务器上拉取消息,确切的说是从他对应的 partition 上拉取,需要自己手动调用 ,调用的间隔不能大于
         * ·max.poll.interval.ms的时间
         */
        @Override
        public void run() {
            this.kafkaConsumer.subscribe(Pattern.compile(".*test.*"));
            while (true) {
                ConsumerRecords<String, String> records = this.kafkaConsumer.poll(Duration.ofSeconds(5));
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println(record.value());
                    //这个是为了保证一个消息不会被重复发送,当它commitSync时,kafka就知道这个组的这个消费者已经读到了这里,如果这个consumer死了,其他consumer接手时,就会从这里开始处理消息
                    record.offset();
                    this.kafkaConsumer.commitSync(Duration.ofMillis(record.offset()));
                    //当想结束这个consumer时,可以这样
                    this.kafkaConsumer.wakeup();
                }
            }

        }
    }
}
