package com.zoro.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * 参考命名 <code>kafka-console-producer.sh --bootstrap-list localhost:9092 --topic test</code>
 *
 * @author zhaoxw
 * Create at 2018/8/18 14:37
 * @version 1.0
 */
public class Sender {

    public static void main(String[] args) {
        Map<String, Object> config = new HashMap<>(16);
        //下面三个参数是必须的
        //其他可以配置的参数名 在 ProducerConfig中可以找到
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9090");

        //key.serializer与value.serializer的值是一个类名,这个类只要实现 org.apache.kafka.common.serialization.Serializer 这个接口就可以,
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> producer = new KafkaProducer<>(config);

        //第一个参数是topic 第二个参数是内容,第二个参数如果是Object 有可能需要实现 serializeable接口
        ProducerRecord<String, String> record = new ProducerRecord<>("test", "java is the best computer language!");
        Future<RecordMetadata> future = producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);
        producer.send(record);

        //需要调用这个方法消息才会发出去
        producer.flush();
        producer.close();

    }
}
