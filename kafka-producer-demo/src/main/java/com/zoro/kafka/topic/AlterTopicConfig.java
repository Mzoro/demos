package com.zoro.kafka.topic;


import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.ConfigEntry;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.config.TopicConfig;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author zhaoxw
 * Create at 2018/8/18 9:51
 * @version 1.0
 */
public class AlterTopicConfig {

    private String topic;
    private String zookeeper;
    private String bootstrap;

    public AlterTopicConfig(String topic, String zookeeper, String bootstrap, String value) {
        this.zookeeper = zookeeper;
        this.topic = topic;
        this.bootstrap = bootstrap;
    }


    /**
     * 修改配置,这个例子是修改 指定topic 的 config
     * config中可以配置的值,可以参考命令 kafka-topics.sh/cmd 中的config选项
     *
     * @return topic 修改的结果,类似创建topic的返回值 {@link TopicCreater#create()}
     * @see TopicConfig
     * TODO 这个例子目前不成功.因为我不知道config中的参数的的作用,以后再说吧
     */
    public AlterConfigsResult alter() {
        Map<String, Object> param = new HashMap<>(16);
        param.put("zookeeper", zookeeper);
        param.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrap);

        AdminClient adminClient = AdminClient.create(param);

        //指定要修改哪个kafka对象的配置,比如我们要修改 topic 这里的第一个参数就是 ConfigResource.Type.TOPIC,第二个参数是topic 的名字
        //如果要修改 broker 的配置 ,这里要写 ConfigResource.Type.BROKER,后面对应应该是broker的host:port ,这个不太会,对kafka的配置不太熟练
        ConfigResource configResource = new ConfigResource(ConfigResource.Type.TOPIC, topic);

        //上面是配置需要修改的对象,这几行配置需要修改具体的配置的值,比如要 修改 topic 的 flush.messages ,这里第一个参数就写 flush.messages,第二个参数写改后的值
        ConfigEntry entry = new ConfigEntry("xxx.xxx.xxx", "");

        //kafka 提供的接口是对一个 configResource 的多个属性修改,所以这里可以配置 多个 ConfigEntry 保存在一个Set中,将这个Set放入 config 对象中
        Set<ConfigEntry> entries = new HashSet<>(16);
        entries.add(entry);
        Config config = new Config(entries);

        //kafka 还支持一次修改多种配置,就是说可以一个操作即修改 topic 的配置,又修改 broker的配置,也支持多个topic一起修改,所以可以将上面几行代码修改参数再执行一次,
        //将产生的 ConfigResource ,Config 对象 都放入这个map,一起修改
        Map<ConfigResource, Config> configMap = new HashMap<>(16);
        configMap.put(configResource, config);

        AlterConfigsResult result = adminClient.alterConfigs(configMap);
        adminClient.close();

        return result;
    }

}
