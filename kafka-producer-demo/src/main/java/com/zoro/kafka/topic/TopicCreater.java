package com.zoro.kafka.topic;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.internals.KafkaFutureImpl;
import org.apache.kafka.common.protocol.Errors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author zhaoxw
 * Create at 2018/8/17 17:19
 * @version 1.0
 */
public class TopicCreater {
    private String[] topic;
    private String bootstrap;
    private String zookeeper;

    public TopicCreater(String bootstrap, String zookeeper, String... topic) {

        this.topic = topic;
        this.bootstrap = bootstrap;
        this.zookeeper = zookeeper;
    }

    /**
     * 创建一个topic 参考命令
     * kafka-topics.bat --zookeeper localhost:2181  --replication-factor 1 --partitions 1  --create --topic haha
     *
     * @return CreateTopicsResult它包含创建topic集合的结果集, key 是 topic ; value 是创建的结果
     *
     * @see Errors
     * @see CreateTopicsResult
     * @see KafkaFutureImpl
     */
    public CreateTopicsResult create() {

        Map<String, Object> param = new HashMap<>(16);
        param.put("zookeeper", zookeeper);
        param.put("partitions",10);
        param.put("replication-factor",3);
        param.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrap);

        //创建 AdminClient 对象,这个是父类,实际都了它的子类 KafkaAdminClient 完成任务
        //它的主要作用是对topic 的增删查,我们创建的topic 都可以在 对应的zookeeper上 的 /brokers/topics节点查到
        //参数 是一个Map 可传的参数 参考 kafka服务器中 kafka-topics.bat的参数
        AdminClient adminClient = AdminClient.create(param);
        Set<NewTopic> topics = new HashSet<>(16);
        for (String t : this.topic) {
            //第一个参数是topic 的名字,第二个参数是 分区数,消费者的数量只能小于等于分区数,第三个参数是多少个备份
            //https://blog.csdn.net/OiteBody/article/details/80595971
            //https://blog.csdn.net/zhaishujie/article/details/71713794?utm_source=itdadao&utm_medium=referral
            NewTopic newTopic = new NewTopic(t, 3, (short) 3);
            topics.add(newTopic);
        }
        //这个方法是创建topic 的准备过程,它会一次创建一组topic
        //返回值对象中有一个map ,key 是 topic ; value 是 这个topic的创建状态的对象,
        // 这个状态对象有四个状态,其中 done 代表是否已经向kafka 提交了创建 topic 的请求 true/false
        // exception 如果创建成功,这个是空,否则这对象保存一个异常, 是这些异常的一个,但是不会全产生,找不到哪里产生的Errors
        CreateTopicsResult result = adminClient.createTopics(topics);
        //还有一个重点,这个close不调用就不会创建这个topic
        // TODO 没整明白这个close怎么回事  应该与线程有关
        adminClient.close();

        return result;
    }
}
