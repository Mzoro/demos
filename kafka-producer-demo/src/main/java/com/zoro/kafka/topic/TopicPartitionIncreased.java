package com.zoro.kafka.topic;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.NewPartitions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/8/18 13:07
 * @version 1.0
 */
public class TopicPartitionIncreased {

    private String topic;
    private String zookeeper;
    private String bootstrap;
    private short partitions;

    public TopicPartitionIncreased(String topic, String zookeeper, String bootstrap, short partitions) {
        this.zookeeper = zookeeper;
        this.topic = topic;
        this.bootstrap = bootstrap;
        this.partitions = partitions;
    }

    /**
     * 增加 topic的分区数量,这种操作一般发生在扩展集群时,partitions 的数量是不能减少的,只参增加
     * 这里需要进一步理解下面这行命令:
     * kafka-topics.sh --zookeeper localhost:2181 --create --topic myTopic --replication-factor 3 --partitions 3
     * <p>
     * --replication-factor 3代表的是这个topic需要三个复制因子,这个数量不能大于集群中的broker的数量;
     * --partitions 3代表的是分区数量,一个topic 创建时默认partitions的数量是1 可以创建很多个,与broker的数量无关,但是它决定了consumer的数量,消费者的数量不能大于分区数量
     * 一个broker加入到集群时,因为它没有topic 分区数量,所以它没有任务关于topic的消息,这时可以增加分区,同时也可以重新规划分区与broker之间的分布关系
     * 所以一个集群如果要加入一个broker还需要做的操作是重新规划分区 或者转移topic
     * {@link NewPartitions#increaseTo(int, List)}这个方法的第二个参数就是broker与分区的对应关系,但是太难了我不会用
     * 参考 http://kafka.apachecn.org/documentation.html#basic_ops_cluster_expansion
     *
     * @return
     */
    public CreatePartitionsResult increase() {

        Map<String, Object> param = new HashMap<>(16);
        param.put("zookeeper", zookeeper);
        param.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrap);

        AdminClient adminClient = AdminClient.create(param);

        Map<String, NewPartitions> partitionsMap = new HashMap<>(16);
        NewPartitions newPartitions = NewPartitions.increaseTo(partitions);
        partitionsMap.put(topic, newPartitions);

        CreatePartitionsResult result = adminClient.createPartitions(partitionsMap);
        adminClient.close();
        return result;
    }
}
