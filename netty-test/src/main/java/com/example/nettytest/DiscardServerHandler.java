package com.example.nettytest;

import com.example.nettytest.handlers.AnyStringHandler;
import com.example.nettytest.handlers.PacsHandler;
import com.example.nettytest.handlers.UserHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;

/**
 * @author zhaoxingwu
 */
public class DiscardServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {




    protected void messageReceived(ChannelHandlerContext ctx, FullHttpRequest fullHttpRequest) throws Exception {

        try {

//             pacs 桌前刷卡接口
            FullHttpResponse response = PacsHandler.bandler(fullHttpRequest);
//             plus
//            FullHttpResponse response = UserHandler.bandler(fullHttpRequest);
            // 返回json数据
//            FullHttpResponse response = AnyStringHandler.bandler(fullHttpRequest);
            ctx.write(response);
            System.out.println("channel read over");
            ctx.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ctx.flush();
            ctx.close();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }

}
