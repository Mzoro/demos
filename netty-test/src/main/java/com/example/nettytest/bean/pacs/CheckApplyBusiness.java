package com.example.nettytest.bean.pacs;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author zhaoxingwu
 */
public class CheckApplyBusiness {
    @JacksonXmlProperty(localName = "requestset")
    private RequestSet requestSet;

    public RequestSet getRequestSet() {
        return requestSet;
    }

    public void setRequestSet(RequestSet requestSet) {
        this.requestSet = requestSet;
    }

    public static class RequestSet {
        @JacksonXmlProperty(localName = "reqcondition")
        private ReqCondition reqCondition;

        public ReqCondition getReqCondition() {
            return reqCondition;
        }

        public void setReqCondition(ReqCondition reqCondition) {
            this.reqCondition = reqCondition;
        }

        public static class ReqCondition {
            @JacksonXmlProperty(localName = "condition")
            private Condition condition;

            public Condition getCondition() {
                return condition;
            }

            public void setCondition(Condition condition) {
                this.condition = condition;
            }

            public static class Condition {
                @JacksonXmlProperty(localName = "cardno")
                private String cardNo;
                @JacksonXmlProperty(localName = "checktype")
                private String checkType;
                @JacksonXmlProperty(localName = "orgcode")
                private String orgCode;

                public String getCardNo() {
                    return cardNo;
                }

                public void setCardNo(String cardNo) {
                    this.cardNo = cardNo;
                }

                public String getCheckType() {
                    return checkType;
                }

                public void setCheckType(String checkType) {
                    this.checkType = checkType;
                }

                public String getOrgCode() {
                    return orgCode;
                }

                public void setOrgCode(String orgCode) {
                    this.orgCode = orgCode;
                }
            }
        }
    }
}
