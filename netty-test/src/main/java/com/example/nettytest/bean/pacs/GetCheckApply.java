package com.example.nettytest.bean.pacs;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author zhaoxingwu
 */
public class GetCheckApply extends PacsMessages<CheckApplyBusiness> {
    @Override
    public CheckApplyBusiness getValue() {
        return business;
    }

    @JacksonXmlProperty(localName = "business")
    private CheckApplyBusiness business;

    public CheckApplyBusiness getBusiness() {
        return business;
    }

    public void setBusiness(CheckApplyBusiness business) {
        this.business = business;
    }
}
