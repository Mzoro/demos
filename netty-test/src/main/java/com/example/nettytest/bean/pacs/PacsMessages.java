package com.example.nettytest.bean.pacs;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * @author zhaoxingwu
 */
@JacksonXmlRootElement(localName = "messages")
public abstract class PacsMessages<T> {
    @JacksonXmlProperty(localName = "switchset")
    private SwitchSet switchset;

    abstract public T getValue();

    public SwitchSet getSwitchset() {
        return switchset;
    }

    public void setSwitchset(SwitchSet switchset) {
        this.switchset = switchset;
    }

    public static class SwitchSet {

        @JacksonXmlProperty(localName = "serviceinf")
        private ServiceInf serviceInf;

        public ServiceInf getServiceInf() {
            return serviceInf;
        }

        public void setServiceInf(ServiceInf serviceInf) {
            this.serviceInf = serviceInf;
        }

        public static class ServiceInf {
            @JacksonXmlProperty(localName = "servicecode")
            private String serviceCode;

            public String getServiceCode() {
                return serviceCode;
            }

            public void setServiceCode(String serviceCode) {
                this.serviceCode = serviceCode;
            }
        }
    }
}
