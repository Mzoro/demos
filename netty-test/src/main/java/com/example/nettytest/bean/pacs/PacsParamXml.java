package com.example.nettytest.bean.pacs;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * @author zhaoxingwu
 */
@JacksonXmlRootElement(localName = "Envelope")
public class PacsParamXml {

    @JacksonXmlProperty
    private String soapenv = "http://schemas.xmlsoap.org/soap/envelope/";


    @JacksonXmlProperty
    private String web = "http://webservice.pacs.neusoft.com/";

    @JacksonXmlProperty(localName = "Header")
    private String header;
    @JacksonXmlProperty(localName = "Body")
    private Body body;

    public String getSoapenv() {
        return soapenv;
    }

    public void setSoapenv(String soapenv) {
        this.soapenv = soapenv;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public static class Body {

        @JacksonXmlProperty
        private HsService hsService;

        public HsService getHsService() {
            return hsService;
        }

        public void setHsService(HsService hsService) {
            this.hsService = hsService;
        }

        public static class HsService {
            @JacksonXmlProperty(localName = "xmldata")
            private String xmlData;

            public String getXmlData() {
                return xmlData;
            }

            public void setXmlData(String xmlData) {
                this.xmlData = xmlData;
            }
        }
    }
}
