package com.example.nettytest.bean.pacs;

import com.example.nettytest.util.JsonUtil;
import com.example.nettytest.util.XmlUtil;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 用于接收 【查询患者检查明细】webservice 的响应值
 *
 * @author zhaoxingwu
 */
@JacksonXmlRootElement(localName = "messages")
public class PatientCheckApply {

    public static final String SUCCESS_CODE = "0";
    public static final String SUCCESS_ERROR_SE_005 = "SE_005";

    public static String loadFromFile() throws URISyntaxException, IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("mock.xml");
        assert url != null;
        File data = Paths.get(url.toURI()).toFile();
        BufferedReader br = new BufferedReader(new FileReader(data));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    public static PatientCheckApply build(String applyNum, String count, String regId) {
        PatientCheckApply result = new PatientCheckApply();
        result.business.businessData.dataSets.setDetails = new ArrayList<>();
        Business.BusinessData.DataSets.SetDetail setDetail = new Business.BusinessData.DataSets.SetDetail();
        setDetail.validateresult = applyNum;
        setDetail.studycount = count;
        setDetail.regId = regId;
        result.business.businessData.dataSets.setDetails.add(setDetail);
        return result;
    }

    @JacksonXmlProperty(localName = "switchset")
    private SwitchSet switchSet = new SwitchSet();

    @JacksonXmlProperty(localName = "business")
    public Business business = new Business();

    public SwitchSet getSwitchSet() {
        return switchSet;
    }

    public void setSwitchSet(SwitchSet switchSet) {
        this.switchSet = switchSet;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String code() {
        return business.businessData.dataSets.retCode;
    }

    public String message() {
        return business.businessData.dataSets.retText;
    }

    public List<Business.BusinessData.DataSets.SetDetail> setDetails() {
        return business.businessData.dataSets.setDetails;
    }

    @Override
    public String toString() {
        return "PatientCheckApply{" +
                "serviceCode=" + switchSet.serviceInf.serviceCode +
                ", recordCode=" + business.businessData.dataSets.retCode +
                ", recordText=" + business.businessData.dataSets.retText +
                ", list=" + (business.businessData.dataSets.setDetails == null ? null :
                business.businessData.dataSets.setDetails.size()) +
                '}';
    }

    public static class SwitchSet {

        @JacksonXmlProperty(localName = "serviceinf")
        private ServiceInf serviceInf = new ServiceInf();

        public ServiceInf getServiceInf() {
            return serviceInf;
        }

        public void setServiceInf(ServiceInf serviceInf) {
            this.serviceInf = serviceInf;
        }

        public static class ServiceInf {
            @JacksonXmlProperty(localName = "servicecode")
            private String serviceCode = "HS1006";

            public String getServiceCode() {
                return serviceCode;
            }

            public void setServiceCode(String serviceCode) {
                this.serviceCode = serviceCode;
            }
        }
    }

    public static class Business {

        @JacksonXmlProperty(localName = "businessdata")
        private BusinessData businessData = new BusinessData();

        public BusinessData getBusinessData() {
            return businessData;
        }

        public void setBusinessData(BusinessData businessData) {
            this.businessData = businessData;
        }

        public static class BusinessData {
            @JacksonXmlProperty(localName = "datasets")
            private DataSets dataSets = new DataSets();

            public DataSets getDataSets() {
                return dataSets;
            }

            public void setDataSets(DataSets dataSets) {
                this.dataSets = dataSets;
            }

            public static class DataSets {

                @JacksonXmlProperty(localName = "retcode")
                private String retCode = SUCCESS_CODE;
                @JacksonXmlProperty(localName = "rettext")
                private String retText = "";
                @JacksonXmlProperty(localName = "setdetails")
                private List<SetDetail> setDetails;

                public String getRetCode() {
                    return retCode;
                }

                public void setRetCode(String retCode) {
                    this.retCode = retCode;
                }

                public String getRetText() {
                    return retText;
                }

                public void setRetText(String retText) {
                    this.retText = retText;
                }

                public List<SetDetail> getSetDetails() {
                    return setDetails;
                }

                public void setSetDetails(List<SetDetail> setDetails) {
                    this.setDetails = setDetails;
                }

                public static class SetDetail {


                    /**
                     * out
                     * 检查单号
                     */
                    @JacksonXmlProperty(localName = "checkserialnum")
                    private String validateresult;

                    /**
                     * out
                     * 检查次数
                     */
                    @JacksonXmlProperty(localName = "checktimes")
                    private String studycount;
                    /**
                     * out
                     * 检查部位 左眼L，右眼R,双眼LR
                     */
                    @JacksonXmlProperty(localName = "checkarea")
                    private String partorsample;
                    /**
                     * out
                     * 检查项目名字
                     */
                    @JacksonXmlProperty(localName = "checkserialname")
                    private String itemname;

                    /**
                     * out
                     * 检查方法
                     */
                    @JacksonXmlProperty(localName = "checkmethod")
                    private String checkmethod;
                    @JacksonXmlProperty(localName = "regid")
                    private String regId;

                    public String getRegId() {
                        return regId;
                    }

                    public void setRegId(String regId) {
                        this.regId = regId;
                    }

                    public String getValidateresult() {
                        return validateresult;
                    }

                    public void setValidateresult(String validateresult) {
                        this.validateresult = validateresult;
                    }

                    public String getStudycount() {
                        return studycount;
                    }

                    public void setStudycount(String studycount) {
                        this.studycount = studycount;
                    }

                    public String getPartorsample() {
                        return partorsample;
                    }

                    public void setPartorsample(String partorsample) {
                        this.partorsample = partorsample;
                    }

                    public String getItemname() {
                        return itemname;
                    }

                    public void setItemname(String itemname) {
                        this.itemname = itemname;
                    }

                    public String getCheckmethod() {
                        return checkmethod;
                    }

                    public void setCheckmethod(String checkmethod) {
                        this.checkmethod = checkmethod;
                    }
                }

            }
        }
    }

}
