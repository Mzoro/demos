package com.example.nettytest.bean.user;

import com.example.nettytest.util.JsonUtil;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

/**
 * @author zhaoxingwu
 */
public class DoctorInfo {

    public static DoctorInfo defaultDoctor() {
        try {

            URL url = Thread.currentThread().getContextClassLoader().getResource("data-set.json");
            assert url != null;
            File data = Paths.get(url.toURI()).toFile();
            ObjectNode dataJson = JsonUtil.readJson(data);
            if (dataJson != null) {
                ObjectNode doctor = JsonUtil.getJson(dataJson, "defaultDoctor");
                return JsonUtil.toJavaType(doctor.toString(), DoctorInfo.class);
            } else {
                throw new RuntimeException("the context in the file " + data.getAbsolutePath() + " is not correct");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static DoctorInfo getDoctor(String doctorCode) {
        try {

            URL url = Thread.currentThread().getContextClassLoader().getResource("data-set.json");
            assert url != null;
            File data = Paths.get(url.toURI()).toFile();
            ObjectNode dataJson = JsonUtil.readJson(data);
            if (dataJson != null) {
                ObjectNode doctor = JsonUtil.getJson(dataJson, doctorCode);
                if (doctor == null) {
                    doctor = JsonUtil.getJson(dataJson, "defaultDoctor");
                }
                return JsonUtil.toJavaType(doctor.toString(), DoctorInfo.class);
            } else {
                throw new RuntimeException("the context in the file " + data.getAbsolutePath() + " is not correct");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String doctorPhoto;
    private String doctorIntroduced;
    private String doctorName;
    private String recauthority;
    private String doctorCode;

    public DoctorInfo(String doctorPhoto, String doctorIntroduced, String doctorName, String recauthority, String doctorCode) {
        this.doctorPhoto = doctorPhoto;
        this.doctorIntroduced = doctorIntroduced;
        this.doctorName = doctorName;
        this.recauthority = recauthority;
        this.doctorCode = doctorCode;
    }

    public DoctorInfo() {
    }

    public String getDoctorPhoto() {
        return doctorPhoto;
    }

    public void setDoctorPhoto(String doctorPhoto) {
        this.doctorPhoto = doctorPhoto;
    }

    public String getDoctorIntroduced() {
        return doctorIntroduced;
    }

    public void setDoctorIntroduced(String doctorIntroduced) {
        this.doctorIntroduced = doctorIntroduced;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getRecauthority() {
        return recauthority;
    }

    public void setRecauthority(String recauthority) {
        this.recauthority = recauthority;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    @Override
    public String toString() {
        return "DoctorInfo{" +
                ", doctorIntroduced='" + doctorIntroduced + '\'' +
                ", doctorName='" + doctorName + '\'' +
                ", recauthority='" + recauthority + '\'' +
                ", doctorCode='" + doctorCode + '\'' +
                '}';
    }
}
