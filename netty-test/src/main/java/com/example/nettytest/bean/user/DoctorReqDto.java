package com.example.nettytest.bean.user;

/**
 * @author zhaoxingwu
 */
public class DoctorReqDto {
    private String doctorcode;
    private String hospitalId;
    public String getDoctorcode() {
        return doctorcode;
    }

    public void setDoctorcode(String doctorcode) {
        this.doctorcode = doctorcode;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    @Override
    public String toString() {
        return "DoctorReqDto{" +
                "doctorcode='" + doctorcode + '\'' +
                ", hospitalId='" + hospitalId + '\'' +
                '}';
    }
}
