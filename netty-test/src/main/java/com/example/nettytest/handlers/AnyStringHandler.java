package com.example.nettytest.handlers;

import com.example.nettytest.util.ResponseUtil;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * @author zhaoxingwu
 */
public class AnyStringHandler {
    public static FullHttpResponse bandler(FullHttpRequest fullHttpRequest) throws IOException, URISyntaxException {
        return response();
    }


    private static DefaultFullHttpResponse response() throws IOException, URISyntaxException {
        String result = read();
        return ResponseUtil.makeResponseJson(result);

    }

    private static String read() throws URISyntaxException, IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("any-data.json");
        File dataFile = Paths.get(url.toURI()).toFile();
        BufferedReader br = new BufferedReader(new FileReader(dataFile));
        StringBuilder data = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            data.append(line);
        }
        return data.toString();

    }
}
