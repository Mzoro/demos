package com.example.nettytest.handlers;

import com.example.nettytest.bean.pacs.GetCheckApply;
import com.example.nettytest.bean.pacs.PacsParamXml;
import com.example.nettytest.bean.pacs.PatientCheckApply;
import com.example.nettytest.util.XmlUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.AsciiString;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author zhaoxingwu
 */
public class PacsHandler {

    private static final AsciiString contentType = HttpHeaderValues.TEXT_PLAIN;

    public static FullHttpResponse bandler(FullHttpRequest fullHttpRequest) throws IOException, URISyntaxException {
        GetCheckApply apply = getCheckApply(fullHttpRequest);
        String checkType = apply.getBusiness().getRequestSet().getReqCondition().getCondition().getCheckType();
        return response(checkType);
    }

    private static GetCheckApply getCheckApply(FullHttpRequest fullHttpRequest) throws IOException {
        ByteBuf byteBuf = fullHttpRequest.content();
        byte[] result = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(result);
        String s = new String(result);
        System.out.println(new String(result));
        PacsParamXml xml = XmlUtil.toBean(s, PacsParamXml.class);
        System.out.println(xml.getBody().getHsService().getXmlData());
        return XmlUtil.toBean(xml.getBody().getHsService().getXmlData(), GetCheckApply.class);
    }

    private static DefaultFullHttpResponse response(String checkType) throws IOException, URISyntaxException {
        PatientCheckApply apply;
        String regId = "5";
        if ("263".equals(checkType)) {
            apply = PatientCheckApply.build("TEST-01", "1", regId);
        } else {
            apply = PatientCheckApply.build("", "", regId);
        }

        String xml = PatientCheckApply.loadFromFile();
        String res = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                "<soap:Body>" +
                "<ns2:hsServiceResponse xmlns:ns2=\"http://webservice.pacs.neusoft.com/\">" +
                "<return><?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                xml +
                "</return></ns2:hsServiceResponse></soap:Body></soap:Envelope>";
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.wrappedBuffer(res.getBytes())); // 2
        HttpHeaders heads = response.headers();
        heads.add(HttpHeaderNames.CONTENT_TYPE, contentType + "; charset=UTF-8");
        heads.add(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes() + ""); // 3
        heads.add(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        return response;

    }
}
