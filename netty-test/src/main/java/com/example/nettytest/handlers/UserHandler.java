package com.example.nettytest.handlers;

import com.example.nettytest.bean.user.DoctorInfo;
import com.example.nettytest.bean.user.DoctorReqDto;
import com.example.nettytest.util.JsonUtil;
import com.example.nettytest.util.ReadRequestUtil;
import com.example.nettytest.util.ResponseUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaoxingwu
 */
public class UserHandler {

    public static FullHttpResponse bandler(FullHttpRequest request) {
        try {
            String result = ReadRequestUtil.read(request);
            if (StringUtils.isBlank(result)) {
                result = JsonUtil.toString(ReadRequestUtil.readParam(request));
            }
            System.out.println(result);
            DoctorReqDto dataSource = JsonUtil.toJavaType(result, DoctorReqDto.class);
            System.out.println(dataSource);
            return response(dataSource);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static DefaultFullHttpResponse response(DoctorReqDto info) throws JsonProcessingException {

        String code = info.getDoctorcode();
        DoctorInfo result;
        if (StringUtils.isBlank(code)) {
            result = DoctorInfo.defaultDoctor();
        } else {
            result = DoctorInfo.getDoctor(code);
        }
        String res = JsonUtil.toString(result);
        if (StringUtils.isBlank(res)) {
            res = "{}";
        }
        return ResponseUtil.makeResponse(res);

    }
}
