package com.example.nettytest.util;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxingwu
 */
public class ReadRequestUtil {
    public static String read(FullHttpRequest fullHttpRequest) {
        ByteBuf byteBuf = fullHttpRequest.content();
        byte[] result = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(result);
        return new String(result);
    }

    public static Map<String, String> readParam(FullHttpRequest fullHttpRequest) {
        String uri = fullHttpRequest.uri();
        if (!uri.contains("?")) {
            return new HashMap<>();
        }
        String search = uri.split("\\?")[1];
        String[] paramArr = search.split("&");
        if (paramArr.length > 0) {
            Map<String, String> result = new HashMap<>();
            for (String t : paramArr) {
                String[] tmp = t.split("=");
                if (tmp.length == 1) {
                    result.put(tmp[0], null);
                } else if (tmp.length > 1) {
                    result.put(tmp[0], tmp[1]);
                } else {
                    // do nothing
                }
            }
            return result;
        } else {
            return new HashMap<>();
        }
    }
}
