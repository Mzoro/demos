package com.example.nettytest.util;

import io.netty.buffer.Unpooled;
import io.netty.handler.codec.AsciiString;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * @author zhaoxingwu
 */
public class ResponseUtil {
    public static DefaultFullHttpResponse makeResponse(String responseBody) {
        return makeResponse(responseBody, HttpHeaderValues.TEXT_PLAIN);
    }

    public static DefaultFullHttpResponse makeResponseJson(String responseBody) {
        return makeResponse(responseBody, new AsciiString("application/json"));
    }



    public static DefaultFullHttpResponse makeResponse(String responseBody, AsciiString contentType) {
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.wrappedBuffer(responseBody.getBytes())); // 2
        HttpHeaders heads = response.headers();
        heads.add(HttpHeaderNames.CONTENT_TYPE, contentType + "; charset=UTF-8");
        heads.add(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes() + ""); // 3
        heads.add(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        return response;
    }
}
