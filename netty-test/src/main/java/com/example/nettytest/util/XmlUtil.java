package com.example.nettytest.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;

/**
 * @author zhaoxingwu
 */
public class XmlUtil {

    private static final XmlMapper XML_MAPPER = (XmlMapper) new XmlMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(SerializationFeature.INDENT_OUTPUT, true);

    public static <T> T toBean(String xml, Class<T> clazz) throws IOException {
        return XML_MAPPER.readValue(xml, clazz);
    }

    public static String toXml(Object data) throws JsonProcessingException {
        return XML_MAPPER.writeValueAsString(data);
    }
}
