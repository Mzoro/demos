package com.example.other;

import com.example.nettytest.bean.pacs.GetCheckApply;
import com.example.nettytest.bean.pacs.PacsParamXml;
import com.example.nettytest.util.XmlUtil;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.regex.Pattern;

/**
 * @author zhaoxingwu
 */
@Ignore
public class NotImportantTest {

    private final String[] LOG_FILES = {"app.61.log", "app.log", "appMap.61.log", "appMap.log"};

    @Test
    @Ignore
    public void getLog() throws IOException {

        Pattern pattern = Pattern.compile("^2020.+");

        final String LOG_PATH = "/root/Desktop/log/";
        final String RESULT_FILE = "log.result.log";
        final String PATIENT_ID = "A4D3FB7052F4426DB3253ECF9C9D7064";
        final String PATIENT_CARD = "0007180708";
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(LOG_PATH + RESULT_FILE));
        BufferedWriter bw = new BufferedWriter(osw);
        for (String s : LOG_FILES) {

            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(LOG_PATH + s)))) {
                String line;
                int lineNum = 1;
                while ((line = br.readLine()) != null) {
//                    if (pattern.matcher(line).find() && (line.contains(PATIENT_ID) || line.contains(PATIENT_CARD))) {
                    if (line.contains(PATIENT_ID) || line.contains(PATIENT_CARD)) {
                        bw.write(String.format("[%25s]", s) + String.format("[%6s]", lineNum) + ":" + line);
                        bw.newLine();
                    }
                    lineNum++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bw.flush();
        bw.close();
        osw.close();
    }

    @Test
    @Ignore
    public void getLog2() throws IOException {

        Pattern pattern = Pattern.compile("^2020.+");

        final String LOG_PATH = "/root/Desktop/log/";
        final String RESULT_FILE = "log.result.log";
        final String PATIENT_ID = "95B02C7A2A574051937A60A8C70D9CA0";
        final String PATIENT_CARD = "0012243414";
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(LOG_PATH + RESULT_FILE));
        BufferedWriter bw = new BufferedWriter(osw);
        File root = new File(LOG_PATH);
        if (!root.exists() || !root.isDirectory()) {
            return;
        }
        File[] logFiles = root.listFiles();
        if (logFiles == null || logFiles.length == 0) {
            return;
        }

        for (File s : logFiles) {
            if (!s.isFile()) {
                continue;
            }
            System.out.println(s.getName());
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(s)))) {
                String line;
                int lineNum = 1;
                while ((line = br.readLine()) != null) {
                    if (pattern.matcher(line).find() && (line.contains(PATIENT_ID) || line.contains(PATIENT_CARD))) {
//                    if (line.contains(PATIENT_ID) || line.contains(PATIENT_CARD)) {
                        bw.write(String.format("[%25s]", s) + String.format("[%6s]", lineNum) + ":" + line);
                        bw.newLine();
                    }
                    lineNum++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bw.flush();
        bw.close();
        osw.close();
    }

    @Test
    @Ignore
    public void testFormatString() {

        System.out.println(String.format("[%6s]", "1"));
    }

    @Test
    @Ignore
    public void testXmlUtil() throws JAXBException, IOException {
        String s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "\n" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:web=\"http://webservice.pacs.neusoft.com/\">  \n" +
                "  <soapenv:Header/>  \n" +
                "  <soapenv:Body> \n" +
                "    <web:hsService> \n" +
                "      <!--Optional:-->  \n" +
                "      <xmldata> <![CDATA[\n" +
                "         <messages>\n" +
                "    <switchset>\n" +
                "        <serviceinf>\n" +
                "            <servicecode>HS1006</servicecode>\n" +
                "        </serviceinf>\n" +
                "    </switchset>\n" +
                "    <business>\n" +
                "        <requestset>\n" +
                "            <reqcondition>\n" +
                "                <condition>\n" +
                "                             <cardno>0002150312</cardno>\n" +
                "                            <checktype>201</checktype>\n" +
                "                             <orgcode>320115024</orgcode> \n" +
                "                </condition>\n" +
                "            </reqcondition>\n" +
                "        </requestset>\n" +
                "    </business>\n" +
                "</messages>\n" +
                "]]> </xmldata> \n" +
                "    </web:hsService> \n" +
                "  </soapenv:Body> \n" +
                "</soapenv:Envelope>";
        PacsParamXml xml = XmlUtil.toBean(s, PacsParamXml.class);
        System.out.println(xml.getBody().getHsService().getXmlData());
        GetCheckApply apply = XmlUtil.toBean(xml.getBody().getHsService().getXmlData(), GetCheckApply.class);

    }
}
