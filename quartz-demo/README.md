# quartz 普通用法的例子

> compile group: 'org.quartz-scheduler' ,name: 'quartz' ,version: '2.3.0'
 
## quartz 中的三个重要的概念
 1. 调度器 Scheduler <br>
    例子中是通过StdSchedulerFactory获得一个StdScheduler类型的对象,它是Scheduler的一个实现类
 2. 触发器 Trigger <br>
    主要保存 定时任务的触发规则 比如多长时间触发一次,从什么时候开始触发
 3. 任务明细 JobDetail <br>
    主要保存对任务的描述,运行任务时的一些参数,具体执行任务的类
 4. 任务类  Job<br>
    任务类是自定义的类,需要 实现 Job接口
 
 ## 使用说明:
 
 1. 首先需要一个Scheduler 用来总体上控制多个任务的 开始,停止;添加任务和删除任务;
 2. build 一个 trigger ,设置定时任务的运行信息
 3. 创建一个 JobDetail 用来指定 具体执行任务的类,任务的名字,jobkey等
 3. scheduler.scheduleJob(job, trigger); 将任务(Job)与触发器(Trigger)绑定,
 3. scheduler.start(); 启动
 3. scheduler.shutdown; 停止
 3. 实现Job接口的任务类需要实现execute方法,这个方法传入一个对象 JobExecutionContext,
    他可以获得在JobDetail中定义的参数与当前任务的一些信息
 4. Job实现类的属性如果有set方法可以不用 JobDataMap来取得Jobdetail传入的参数
 
 
## 注意
1. 同一个job不能重复加入scheduler 同一个JobDetail也不能同时加入
2. JobDetail 的group,name 与Trigger的group,name不知道有什么联系,无论是否相同,都没有问题
3. scheduler 启动后也可以向scheduler加入任务 
 