package demo;

import org.quartz.*;

import java.util.Date;

public class HelloQuartz implements Job {
    private String name;

    @Override
    public void execute(JobExecutionContext context)  {

        JobDetail detail = context.getJobDetail();
        //方法一：获得JobDataMap
        JobDataMap map = detail.getJobDataMap();
        try{
            System.out.println("say hello to " + name + "[" + map.get("age") + "]" + " at "
                    + new Date());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * 方法二：属性的setter方法，会将JobDataMap的属性自动注入
     */
    public void setName(String name) {
        this.name = name;
    }
}