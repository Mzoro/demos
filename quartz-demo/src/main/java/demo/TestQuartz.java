package demo;


import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class TestQuartz {

    public void ranTask() {
        try {
            /*
            创建一个总的控制任务开始/关闭等操作的高度器
             */
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            /*
            创建一个触发器,他保存的是与这个触发器关联的Job的执行规则,与Job关联的方式是同时放在 一个 高度器中
             */
            Trigger trigger = TriggerBuilder.newTrigger()
                    //定义name/group
                    .withIdentity("trigger1", "group1")
                    //一旦加入scheduler，立即生效
                    .startNow()
                    //使用SimpleTrigger
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                    //每隔一秒执行一次
                                    .withIntervalInSeconds(1)
                            //一直执行，奔腾到老不停歇
//                            .repeatForever()
                    )
                    .build();

            /*
            这是一个具体的任务,他主要保存的是任务的业务逻辑
             */
            JobDetail job = JobBuilder.newJob(HelloQuartz.class)
                    //定义name/group
                    .withIdentity("job1", "group1")
                    //定义属性
                    .usingJobData("name", "quartz").usingJobData("age", 18)
                    .build();


            //定义一个Trigger
            Trigger trigger2 = TriggerBuilder.newTrigger()
                    //定义name/group
                    .withIdentity("trigger2", "group2")
                    //一旦加入scheduler，立即生效
                    .startNow()
                    //使用SimpleTrigger
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            //每隔一秒执行一次
                            .withIntervalInSeconds(1)
                            //一直执行，奔腾到老不停歇
                            .repeatForever()
                    )
                    .build();

            JobDetail job2 = JobBuilder.newJob(HelloQuartz.class)
                    //定义name/group
                    .withIdentity("job3", "group3")
                    //定义属性 这些属性可以直接被 Job对象的属性接收,需要有set方法
                    //也可以通过JobDataMap获取
                    .usingJobData("name", "quartz2").usingJobData("age", 36)
                    .build();
            System.out.println("加入 scheduler");



            Trigger trigger4 = TriggerBuilder.newTrigger()
                    //定义name/group
                    .withIdentity("trigger4", "group4")
                    //一旦加入scheduler，立即生效
//                    .startNow()
                    //使用SimpleTrigger
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            //每隔一秒执行一次
                            .withIntervalInSeconds(1)
                            //一直执行，奔腾到老不停歇
                            .repeatForever()
                    )
                    .build();

            JobDetail job4= JobBuilder.newJob(HelloQuartz.class)
                    //定义name/group
                    .withIdentity("job4", "group4")
                    //定义属性 这些属性可以直接被 Job对象的属性接收,需要有set方法
                    //也可以通过JobDataMap获取
                    .usingJobData("name", "quartz4").usingJobData("age", 54)
                    .build();

            /*
            将任务(Job)与触发器(Trigger)绑定,
             */
            scheduler.scheduleJob(job, trigger);
            scheduler.scheduleJob(job2, trigger2);

            /*
            启动所有的任务
             */
            Thread.sleep(2000);
            scheduler.start();
            scheduler.scheduleJob(job4, trigger4);
            Thread.sleep(2000);


            /*
            从调度器中删除一个任务,删除后任务就停了
             */
            scheduler.deleteJob(trigger2.getJobKey());
            //运行一段时间后关闭
            Thread.sleep(5000);

            /*
            关闭所有的任务
             */
            scheduler.shutdown(true);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause().toString());
        }
    }
}
