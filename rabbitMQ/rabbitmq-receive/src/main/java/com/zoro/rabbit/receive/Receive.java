package com.zoro.rabbit.receive;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Rabbitmq接收消息须知:
 * 1.我们需要告诉 rabbit 服务器当前消费者  channel.basicQos(1);这样就可以在一个消费者拥堵情况下将消息发给其他消费者
 * tell RabbitMQ not to give more than one message to a worker at a time
 * 2.在一个合适的地方应答rabbit
 * 3.监听的是队列,不是exchange
 *
 * @author zhaoxw
 * Create at 2018/8/7 13:54
 * @version 1.0
 */
public class Receive {

    private static final String QUEUE_NAME = "hello-world-2";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        channel.basicQos(1);
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");

                //手动应答
                channel.basicAck(envelope.getDeliveryTag(), false);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    channel.basicAck(envelope.getDeliveryTag(), true);
                    e.printStackTrace();
                }
            }
        };
        //第二个参数用来关闭自动应答,
        channel.basicConsume(QUEUE_NAME, false, consumer);
    }
}
