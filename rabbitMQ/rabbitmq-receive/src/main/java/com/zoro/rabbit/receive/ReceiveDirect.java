package com.zoro.rabbit.receive;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.zoro.rabbit.receive.customerconsumer.CustomerConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 这个需要启动 3 次,每次传的参数不同  error,info or debug
 * @author zhaoxw
 * Create at 2018/8/8 11:13
 * @version 1.0
 */
public class ReceiveDirect {

    private static final String EXCHANGE_NAME = "log-level";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        System.err.println("args[0]:"+args[0]);
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue, EXCHANGE_NAME, args[0]);

        Consumer consumer = new CustomerConsumer(channel);
        //第二个参数用来关闭自动应答,
        channel.basicConsume(queue, true, consumer);


    }
}
