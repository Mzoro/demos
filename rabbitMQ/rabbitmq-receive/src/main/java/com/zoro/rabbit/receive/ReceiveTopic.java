package com.zoro.rabbit.receive;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.zoro.rabbit.receive.customerconsumer.CustomerConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhaoxw
 * Create at 2018/8/9 10:21
 * @version 1.0
 */
public class ReceiveTopic {
    private static final String EXCHANGE_NAME = "log-level-topic";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        //启动时传入
        System.err.println("args[0]:" + args[0]);
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, args[0]);

        Consumer consumer = new CustomerConsumer(channel);
        channel.basicConsume(queueName, true, consumer);

    }


}
