package com.zoro.rabbit.send;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Rabbitmq 的消息的须知:
 * 1.需要指名发给哪个 exchange,因为我们是发给exchange,不是发给queue
 * 2.要确保你发 exchange 能根据你传的参数找到对应的queue,
 * 比如,发给direct exchange 时,要确保有至少一个queue的routingkey 和你发的routingkey 是一样的
 *
 * @author zhaoxw
 * Create at 2018/8/7 11:54
 * @version 1.0
 */
public class SendDirect {

    private static final String QUEUE_NAME = "hello-world";

    private static final Logger LOGGER = LoggerFactory.getLogger(SendDirect.class);

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");

        //获取连接
        Connection connection = factory.newConnection();
        //获取频道,对Rabbit mp 的所有操作都是通过一个频道来操作的
        Channel channel = connection.createChannel();

        //声明一个队列 queue ,队列的名字是QUEUE_NAME(它也是routingkey) ,是持久存在的,不自动删除,没有其他参数
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);


        for (int i = 0; i < 10; i++) {
            String message = "message-" + (i + 1) + " : Hello World!";
            //1.exchange名字(这里用的是默认的),2.routingkey(要与上面的queuedeclare第一个参数一样,如果没有这个routingkey的queue ,这条消息就丢失),
            //3.消息的各类 (这里选择的是可以重启不删除的那种), 4.消息内容
            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
        }
        channel.close();
        connection.close();

    }
}
