package com.zoro.rabbit.send;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhaoxw
 * Create at 2018/8/8 11:01
 * @version 1.0
 */
public class SendDirect2 {

    private static final String EXCHANGE_NAME = "log-level";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);

        for (int i = 0; i < 10; i++) {
            String level = getLevel();
            channel.basicPublish(EXCHANGE_NAME, level,
                    MessageProperties.PERSISTENT_TEXT_PLAIN, ("logmessage--" + level).getBytes());
        }

        channel.close();
        connection.close();

    }

    public static String getLevel() {
        String[] routingKeys = {"info", "error", "debug"};
        int index = (int) (Math.random() * 3);
        return routingKeys[index];
    }
}
