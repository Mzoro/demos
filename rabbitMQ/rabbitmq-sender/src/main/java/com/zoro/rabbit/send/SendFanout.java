package com.zoro.rabbit.send;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * fan 风扇
 * 广播模式,只要与这个exchange 绑定的 queue都会收到同样消息
 *
 * @author zhaoxw
 * Create at 2018/8/8 9:59
 * @version 1.0
 */
public class SendFanout {
    private static final String EXCHANGE_NAME = "hello-world";

    private static final Logger LOGGER = LoggerFactory.getLogger(SendDirect.class);

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        //一定要先有这个 exchange
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        String queue = channel.queueDeclare().getQueue();
        //使用Fanout类型的exchange时,需要先将queue与exchange 绑定routingkey被 ignore
        channel.queueBind(queue, EXCHANGE_NAME, "");

        for (int i = 0; i < 1000; i++) {
            String message = "message-" + (i + 1) + " : Hello World!";
            channel.basicPublish(EXCHANGE_NAME, "", MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
        }
        channel.close();
        connection.close();

    }
}
