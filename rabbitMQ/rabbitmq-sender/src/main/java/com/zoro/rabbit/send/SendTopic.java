package com.zoro.rabbit.send;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * * (star) can substitute for exactly one word.(星号)
 * # (hash) can substitute for zero or more words.(井号)
 * topic 模式的exchange 会以类似正则匹配的形式找到对应的queue
 * 比如queue 的规则
 * *.orange.*会配置 blue.orange.haha,red.orange.hehe,但是不会匹配 orange.blue,orange, *代表两个点之间的字母(one word);
 * #.orange.# 会配置上面所有的例子
 *
 * @author zhaoxw
 * Create at 2018/8/9 9:01
 * @version 1.0
 */
public class SendTopic {

    private static final String EXCHANGE_NAME = "log-level-topic";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setVirtualHost("/rabbit");
        factory.setUsername("rabbit");
        factory.setPassword("rabbit");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        for (int i = 0; i < 10; i++) {
            if (i < 3) {

                channel.basicPublish(EXCHANGE_NAME, "logs.not.error.info", MessageProperties.PERSISTENT_TEXT_PLAIN,
                        ("logs.not.error.info").getBytes());
            } else if (i < 6) {

                channel.basicPublish(EXCHANGE_NAME, "logs.not.error.debug", MessageProperties.PERSISTENT_TEXT_PLAIN,
                        ("logs.not.error.debug").getBytes());
            } else {
                channel.basicPublish(EXCHANGE_NAME, "logs.error.error", MessageProperties.PERSISTENT_TEXT_PLAIN,
                        ("logs.error.error").getBytes());

            }
        }
        channel.close();
        connection.close();
    }


}
