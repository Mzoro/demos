http://www.rabbitmq.com/tutorials/tutorial-three-java.html
官方教程,写的很详细

# rabbitmq-sender/

> 项目中都是 消息生产者

 1. com.zoro.rabbit.send.SendDirect是一官网上入门的例子;

 2. com.zoro.rabbit.send.SendDirect2是介绍 direct 类型的 exchange 如何与队列绑定;程序发10个消息 routingkey 在数组{"info","debug","error"}中随机产生,对应的,在rabbitmq-receive/项目中的com.zoro.rabbit.receive.ReceiveDirect类需要启动3次,每次启动时需要传入一个参数 分别是 info/debug/error 做为routingkey 与 发消息的那个exchange绑定 .这就会有三个消费者对应不同的三个队列接收不同的消息;
 3. com.zoro.rabbit.send.SendFanout是广播模式,这里会创建一个 fanout 类型的 exchange ,在rabbitmq-receive/项目中的com.zoro.rabbit.receive.ReceiveFanout类启动多次,生成多个消费者监听的队列,都与发送消息的exchange 绑定 ,这个例子与第1个不同的是 加入了手动 ack ,加入的消费者任务最大数量的控制(保证一个消费者忙时发送给另一个消费者)
 4. com.zoro.rabbit.send.SendTopic是匹配类型的 exchange 介绍; 他会向 一个 exchange 发送10 个消费 ,但是发送时 的routingkey 不一样 ,分别是logs.not.error.info/logs.not.error.debug/logs.error.error;  在rabbitmq-receive/项目中的com.zoro.rabbit.receive.ReceiveTopic 启动多次,分别与 发送消息的exchange 绑定,但绑定时的 routingkey 不同,例子中是 
	logs.error.*/logs.not.error.*/logs.#  

# rabbitmq-sender
> 项目是都是 消息消费者 用法对应上面

# spring-rabbitmq

> spring集成 rabbitmq 

# 注意
1. 对于消费者,监听的queue一定要存在;
2. 对于一个队列,当有一个监听者而且不限制最大消息处理数量,他会全发给这个监听者,如果在这个监听者处理的过程中另一个上线监听同一个队列,他会没有任何消息接收到;<br>
   当一个队列已经有多个监听者,当队列有大量消息发送时,他会用一定的策略发给多个监听者
2. 对于一个队列,当有一个监听者***但是***监听者限制最大消息处理数量,他就会等监听者处理结束再向这个监听者发下一个消息 
2. Rabbit默认有一个没有名字的 direct类型的exchange 只有通过这个exchange 向队列发送请求时不用提前 bind 自定义的exchange 都需要自定义,否则就用不了

# TODO 

1. spring-boot/spring 与rabbitMQ集成时,rabbitListenerContainer有一个属性:container.setPrefetchCount(5),功能类似 channel.basicQos(1);但是还没有测试过