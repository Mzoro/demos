package com.zoro.rabbitmq.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhaoxw
 * Create at 2018/8/9 13:50
 * @version 1.0
 */
@Controller
public class ConnectionTestController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        amqpTemplate.convertAndSend("log-topic", "logs.error.error", "log-topinc-logs.error.error");
        //这条应该收不到
        amqpTemplate.convertAndSend("log-topic", "99.error.error", "log-topinc-99.error.error");

        User user = new User();
        user.setName("haha");
        user.setSex("男");

        amqpTemplate.convertAndSend("log-topic", "logs.error.error", user);

        System.err.println("通通通通通通通通通通");
        return "通通通通通通通通通通";
    }
}
