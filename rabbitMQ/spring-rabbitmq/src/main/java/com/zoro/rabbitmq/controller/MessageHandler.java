package com.zoro.rabbitmq.controller;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

/**
 * @author zhaoxw
 * Create at 2018/8/9 16:13
 * @version 1.0
 */
public class MessageHandler implements MessageListener {

    @Override
    public void onMessage(Message message) {

        System.err.println(new String(message.getBody()));
    }
}
