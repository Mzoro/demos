package com.zoro.rabbitmq.controller;

import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/8/9 16:45
 * @version 1.0
 */
public class User implements Serializable{

    private static final long serialVersionUID = -4208157038758193982L;
    private String name ;
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", sex='").append(sex).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
