package com.zoro.rabbitmq.controller;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.utils.SerializationUtils;

/**
 * @author zhaoxw
 * Create at 2018/8/9 16:50
 * @version 1.0
 */
public class UserMessageHandler implements MessageListener {
    @Override
    public void onMessage(Message message) {
        Object object = SerializationUtils.deserialize(message.getBody());
        if (object instanceof User) {
            System.err.println(object);
        }
    }
}
