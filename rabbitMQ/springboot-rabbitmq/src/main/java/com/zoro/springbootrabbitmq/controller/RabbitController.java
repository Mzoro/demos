package com.zoro.springbootrabbitmq.controller;

import com.zoro.springbootrabbitmq.entity.BigMouth;
import com.zoro.springbootrabbitmq.entity.LittleGirl;
import com.zoro.springbootrabbitmq.util.WebContextUtil;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * RabbitMQ官网将 direct exchange 绑定queue这种方式称为 routing
 *
 * @author zhaoxw
 * Create at 2018/8/10 15:35
 * @version 1.0
 */
@RestController
public class RabbitController {

    @Resource
    private AmqpAdmin rabbitAdmin;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/direct")
    public String direct() {

        Integer count = WebContextUtil.getSessionAttributes("count", Integer.class);
        if (count == null) {
            count = 0;
        }
        count++;
        WebContextUtil.setSessionAttribute("count", count);
        LittleGirl littleGirl = new LittleGirl(count);

        rabbitTemplate.convertAndSend("boot.direct.exchange", "boot.direct.queue", littleGirl);

        System.err.println(littleGirl);
        return littleGirl.toString();

    }

    @RequestMapping("/fanout")
    public String fanout(String say) {
        BigMouth bigMouth = new BigMouth(say);

        //第二个参数 routingkey 写什么都无所谓,只要写对exchange就可以
        rabbitTemplate.convertAndSend("boot.fanout.exchange", "", bigMouth);
        return bigMouth.toString();
    }

    @RequestMapping("/topic")
    public String topic() {

        rabbitTemplate.convertAndSend("boot.topic.exchange.one", "color.red.orange", "routingkey:color.red.orange");
        rabbitTemplate.convertAndSend("boot.topic.exchange.one", "color.red", "routingkey:color.red");
        rabbitTemplate.convertAndSend("boot.topic.exchange.one", "color.orange", "routingkey:color.orange");
        rabbitTemplate.convertAndSend("boot.topic.exchange.one", "color.orange.red", "routingkey:color.orange.red");
        rabbitTemplate.convertAndSend("boot.topic.exchange.one", "red.orange", "routingkey:red.orange");
        rabbitTemplate.convertAndSend("boot.topic.exchange.two", "color.asdf", "routingkey:color.asdf");
        rabbitTemplate.convertAndSend("boot.topic.exchange.two", "color.asdf.asd", "routingkey:color.asdf.asd");
        return "哦哈哈";
    }
}
