package com.zoro.springbootrabbitmq.entity;

import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/8/10 18:36
 * @version 1.0
 */
public class BigMouth implements Serializable {
    private static final long serialVersionUID = 6888450992194874681L;
    private String msg;

    public BigMouth(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BigMouth{");
        sb.append("msg='").append(msg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
