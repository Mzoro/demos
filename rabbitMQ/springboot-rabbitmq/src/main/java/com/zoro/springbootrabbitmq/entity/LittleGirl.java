package com.zoro.springbootrabbitmq.entity;

import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/8/10 16:34
 * @version 1.0
 */
public class LittleGirl implements Serializable {
    private static final long serialVersionUID = 8791486085662994615L;
    private int age;

    public LittleGirl(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(age + " 岁的小仙女!");
        return sb.toString();
    }
}
