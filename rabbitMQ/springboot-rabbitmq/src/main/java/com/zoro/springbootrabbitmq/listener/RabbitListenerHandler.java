package com.zoro.springbootrabbitmq.listener;

import com.zoro.springbootrabbitmq.entity.BigMouth;
import com.zoro.springbootrabbitmq.entity.LittleGirl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaoxw
 * Create at 2018/8/10 17:27
 * @version 1.0
 */
@Component
public class RabbitListenerHandler {

    @RabbitListener(queues = {"boot.direct.queue"})
    public void directListener(LittleGirl littleGirl) {
        System.err.println(littleGirl);
    }

    @RabbitListener(queues = {"boot.fanout.queue.A"})
    public void fanoutListenerA(BigMouth bigMouth) {
        System.err.println("大嘴巴 A 说:" + bigMouth.getMsg());
    }

    @RabbitListener(queues = {"boot.fanout.queue.B"})
    public void fanoutListenerB(BigMouth bigMouth) {
        System.err.println("大嘴巴 B 说:" + bigMouth.getMsg());
    }

    @RabbitListener(queues = {"boot.fanout.queue.C", "boot.fanout.queue.D"})
    public void fanoutListenerCD(BigMouth bigMouth) {
        System.err.println("大嘴巴 C 和 D 说:" + bigMouth.getMsg());
    }

    @RabbitListener(queues = {"boot-topic-queue-orange"})
    public void topicListenerOrange(String message) {
        System.err.println("[ x ]orange listener : " + message);
    }

    @RabbitListener(queues = {"boot-topic-queue-red"})
    public void topicListenerRed(String message) {
        System.err.println("[ x ]red listener    : " + message);
    }

    @RabbitListener(queues = {"boot-topic-queue-color"})
    public void topicListenerColor(String message) {
        System.err.println("[ x ]color listener  : " + message);
    }

}
