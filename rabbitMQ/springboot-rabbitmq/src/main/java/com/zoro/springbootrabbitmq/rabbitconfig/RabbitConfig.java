package com.zoro.springbootrabbitmq.rabbitconfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;

/**
 * 当 compile('org.springframework.boot:spring-boot-starter-amqp') 时,spring boot 会自动注入一些bean
 * {@link RabbitAutoConfiguration} ,
 * 对我们有用的 :
 * 1.RabbitAdmin {@link RabbitAdmin} 用于操作 exchange ,queue,另一个做用是将根据环境中的所有Queue对象与Exchange对象者在Rabbit服务中准备好
 * {@link RabbitAdmin#afterPropertiesSet()} {@link RabbitAdmin#initialize()}
 * 2.RabbitTemplate {@link RabbitTemplate}与RabbitMessagingTemplate {@link RabbitMessagingTemplate } 发送消息
 * RabbitMessagingTemplate 类似于RabbitTemplate 的 配置器或者代表,它持有一个RabbitTemplate 对象,真正的工作是RabbitTemplate去完成
 * 3.RabbitProperties {@link RabbitProperties} 它保存 配置RabbitMQ的基本信息  v-host/username/password/host/port .....
 * 4.上面这些通过 actuator 都能找到
 * <p>
 * 所以这个配置类主要用于配置需要监听的 queue,创建 exchange ,绑定 queue与exchange的
 *
 * @author zhaoxw
 * Create at 2018/8/10 13:03
 * @version 1.0
 */
@Configuration
@EnableRabbit
public class RabbitConfig {

    /* ***************************** direct ************************************ */

    /**
     * 这个queue用于与一个 direct 类型的 exchange 绑定 ;
     * 当我们使用 channel.basicPublish()或rabbitTemplate发送时,第一个参数指定的 exchange 如果是direct类型,第二个参数routingkey 就要它这个queue的名字
     * 这相当于 channel.queueDeclare();
     * 例子中我们需要监听这个queue,如果我们不监听,可以不声明这个bean;
     *
     * @return Queue
     * @see com.rabbitmq.client.Channel#queueDeclare
     */
    @Bean
    public Queue directQueue() {
        return new Queue("boot.direct.queue");
    }

    /**
     * 这是一个 direct 类型的exchange ,例子中用来与上面的 directQueue 一起使用
     * 相当于 channel.exchangeDeclare();
     *
     * @return direct exchange
     * @see com.rabbitmq.client.Channel#exchangeDeclare(String, String) ;
     */
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("boot.direct.exchange");
    }

    /**
     * 将 queue 与 exchange 绑定
     *
     * @param directQueue    队列
     * @param directExchange exchange
     * @return binding
     */
    @Bean
    public Binding directBinding(Queue directQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(directQueue).to(directExchange).with(directQueue.getName());
    }



    /* ***************************** fanout ************************************ */

    /* ************** Queue **************** */
    @Bean
    public Queue fanoutQueueA() {
        return new Queue("boot.fanout.queue.A");
    }

    @Bean
    public Queue fanoutQueueB() {
        return new Queue("boot.fanout.queue.B");
    }

    @Bean
    public Queue fanoutQueueC() {
        return new Queue("boot.fanout.queue.C");
    }

    @Bean
    public Queue fanoutQueueD() {
        return new Queue("boot.fanout.queue.D");
    }

    /* ************** Exchange **************** */

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("boot.fanout.exchange");
    }

    /* ************** Bind **************** */
    @Bean
    public Binding fanoutBindingA(Queue fanoutQueueA, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQueueA).to(fanoutExchange);
    }

    @Bean
    public Binding fanoutBindingB(Queue fanoutQueueB, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQueueB).to(fanoutExchange);
    }

    @Bean
    public Binding fanoutBindingC(Queue fanoutQueueC, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQueueC).to(fanoutExchange);
    }

    @Bean
    public Binding fanoutBindingD(Queue fanoutQueueD, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQueueD).to(fanoutExchange);
    }


    /* ***************************** topic ************************************ */

    /* ************** Queue **************** */

    /**
     * 为了与 routingkey区分开,queue的名字就不用点分割了,用-
     *
     * @return queue
     */
    @Bean
    public Queue topicQueueOrange() {
        return new Queue("boot-topic-queue-orange");
    }

    @Bean
    public Queue topicQueueRed() {
        return new Queue("boot-topic-queue-red");
    }

    @Bean
    public Queue topicQueueColor() {
        return new Queue("boot-topic-queue-color");
    }

    /* ************** exchange **************** */

    @Bean
    public TopicExchange topicExchangeOne() {
        return new TopicExchange("boot.topic.exchange.one");
    }

    @Bean
    public TopicExchange topicExchangeTwo() {
        return new TopicExchange("boot.topic.exchange.two");
    }

    /* ************** bind **************** */
    @Bean
    public Binding topicBindingOrange(Queue topicQueueOrange, TopicExchange topicExchangeOne) {
        return BindingBuilder.bind(topicQueueOrange)
                //这个to方法是访问者模式 通过方法的重载返回不同类型的返回值
                .to(topicExchangeOne).with("#.orange");
    }

    @Bean
    public Binding topicBindingRed(Queue topicQueueRed, TopicExchange topicExchangeOne) {
        return BindingBuilder.bind(topicQueueRed)
                //这个to方法是访问者模式 通过方法的重载返回不同类型的返回值
                .to(topicExchangeOne).with("#.red.#");
    }

    @Bean
    public Binding topicBindingColor(Queue topicQueueColor, TopicExchange topicExchangeOne) {
        return BindingBuilder.bind(topicQueueColor).to(topicExchangeOne).with("color.#");
    }

    @Bean
    public Binding topicBindingColor2(Queue topicQueueColor, TopicExchange topicExchangeTwo) {
        return BindingBuilder.bind(topicQueueColor).to(topicExchangeTwo).with("color.*");
    }


//    @Bean
//    public SimpleMessageListenerContainer messageListenerContainer(ConnectionFactory factory) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(factory);
//        container.addQueueNames("boot.direct.queue");
//        container.setMessageListener(exampleListener());
//        return container;
//    }
//
//    public MessageListenerAdapter messageListenerAdapter(){
//        return new MessageListenerAdapter();
//    }

}
