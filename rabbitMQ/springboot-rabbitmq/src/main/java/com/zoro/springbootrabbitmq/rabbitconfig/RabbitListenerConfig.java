package com.zoro.springbootrabbitmq.rabbitconfig;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 这个类用来自定义监听器,使用注解时,默认是自动应答的,现在想不自动应答,需要等到业务完成才应答
 *
 * @author zhaoxw
 * Create at 2018/8/11 10:23
 * @version 1.0
 */
@Configuration
public class RabbitListenerConfig {


    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();

        //配置手工应答
        simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory);

        ChannelAwareMessageListener channelAwareMessageListener = (message, channel) -> {
            System.err.println("自定义收到");
            System.err.println(new String(message.getBody()));

            //这qkd句就是手工应答,不想应答就不写这句,但这条消息可能会永远不会被处理 ,因为他是 unacked状态
//         channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            //这句是将这条消息回到 ready状态,并放在队列头,queue会再次发给其他监听者,但是如果这条消息本身有问题,他就会永远被重新发送,会造成消息拥阻
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            //可以处理后重新发送回这个队列,让他排在队尾
//            channel.basicPublish();
        };

        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
        messageListenerAdapter.setDelegate(channelAwareMessageListener);

        simpleMessageListenerContainer.setMessageListener(messageListenerAdapter);
        simpleMessageListenerContainer.setQueueNames("boot.direct.queue");
        return simpleMessageListenerContainer;

    }

    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer2(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory);

        ChannelAwareMessageListener channelAwareMessageListener = (message, channel) -> {
            System.err.println("自定义收到2");
            System.err.println(new String(message.getBody()));
//            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        };

        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
        messageListenerAdapter.setDelegate(channelAwareMessageListener);

        simpleMessageListenerContainer.setMessageListener(messageListenerAdapter);
        simpleMessageListenerContainer.setQueueNames("boot.direct.queue");
        return simpleMessageListenerContainer;

    }


}
