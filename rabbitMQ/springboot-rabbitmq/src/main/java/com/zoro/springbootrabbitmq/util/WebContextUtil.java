package com.zoro.springbootrabbitmq.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/8/10 16:10
 * @version 1.0
 */
public class WebContextUtil {

    /**
     * 获取 HttpServletRequest
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取 HttpSession
     *
     * @return HttpSession
     */
    public static HttpSession getHttpSession() {
        return getHttpServletRequest().getSession();
    }

    /**
     * 返回保存在 Session中的数据
     *
     * @param attributeName Session中的数据的名字
     * @return 目标数据
     */
    public static <S extends Serializable> S getSessionAttributes(String attributeName, Class<S> sClass) {
        return (S) getHttpSession().getAttribute(attributeName);
    }

    public static void setSessionAttribute(String attributeName, Object attribute) {
        getHttpSession().setAttribute(attributeName, attribute);
    }
}
