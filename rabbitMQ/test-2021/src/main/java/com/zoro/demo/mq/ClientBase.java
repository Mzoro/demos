package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxingwu
 */
public class ClientBase {
    protected final Channel channel;
    protected final String exchangeName;
    protected final String queueName;
    protected final String bindKey;
    protected final BuiltinExchangeType exchangeType;

    ClientBase(Channel channel, String exchangeName, String queueName, String bindKey) {
        this.channel = channel;
        this.exchangeName = exchangeName;
        this.queueName = queueName;
        this.bindKey = bindKey;
        this.exchangeType = null;
    }

    ClientBase(Channel channel, String exchangeName, String queueName, String bindKey,BuiltinExchangeType exchangeType) {
        this.channel = channel;
        this.exchangeName = exchangeName;
        this.queueName = queueName;
        this.bindKey = bindKey;
        this.exchangeType = exchangeType;
    }


    void init() throws IOException {
        /*
        1. 为确保 publish,consume 之前exchange 与 queue 存在，这里可以声明一下，如果存在没有影响，如果不存在就会创建一个
        2. 但是如果声明之前服务器已经存在这个exchange 或者 queue ,那么声明时的 参数一定要和服务器保持一样，否则就会出错
         */
        Map<String, Object> arg = new HashMap<>();
//        arg.put("alternate-exchange", "2");
        this.channel.exchangeDeclare(exchangeName, BuiltinExchangeType.DIRECT, true, false, arg);

        this.channel.queueDeclare(queueName, true, false, false, arg);
        this.channel.queueBind(queueName, exchangeName, bindKey);
    }

    /**
     * 如果不需要保证消息一定到达队列，生产者只需要声明exchange就可以了
     * @throws IOException e
     *
     */
    void initExchange() throws IOException {
        Map<String, Object> arg = new HashMap<>();
//        arg.put("alternate-exchange", "2");
        this.channel.exchangeDeclare(exchangeName, this.exchangeType, true, false, arg);

    }

    void initQueue() throws IOException {
        Map<String, Object> arg = new HashMap<>();
        initExchange();
        this.channel.queueDeclare(queueName, true, false, false, arg);
        this.channel.queueBind(queueName, exchangeName, bindKey);
    }
}
