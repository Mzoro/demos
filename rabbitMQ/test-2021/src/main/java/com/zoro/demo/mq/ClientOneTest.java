package com.zoro.demo.mq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @author zhaoxingwu
 */
public class ClientOneTest {

    private static final String EXCHANGE_NAME = "FirstExchange";
    private static final String FUNOUT_EXCHANGE_NAME = "FunoutFirstExchange";
    private static final String QUEUE_NAME = "FirstQueue";
    private static final String FUNOUT_QUEUE_NAME = "FunoutQueue";
    private static final String ROUTING_KEY = "FirstBindKey";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.187.131");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("1");
        factory.setVirtualHost("/");

        Connection connection = factory.newConnection();

        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入：\n1. 发送消息。\n2.接收消息\n");
        while (scanner.hasNext()){
            String in = scanner.next();
            if ("1".equals(in)) {
//                Publisher publisher = new Publisher(connection.createChannel(), EXCHANGE_NAME, QUEUE_NAME, ROUTING_KEY);
//                Publisher publisher = new FunoutProductor(connection.createChannel(), FUNOUT_EXCHANGE_NAME, FUNOUT_QUEUE_NAME, ROUTING_KEY);
                Publisher publisher = new TopicProduct(connection.createChannel(), FUNOUT_EXCHANGE_NAME, FUNOUT_QUEUE_NAME,
                        ROUTING_KEY);
                publisher.sendMsg();
            }else if ("2".equals(in)){
                // 直接exchange
//                Consumer consumer = new Consumer(connection.createChannel(), EXCHANGE_NAME, QUEUE_NAME, ROUTING_KEY);
                // funout exchange  ,l因为 每个队列中的消息只能被一个消费者消费，所以这队列外用 启动参数传入
//                Consumer consumer = new FunoutConsumer(connection.createChannel(), FUNOUT_EXCHANGE_NAME, args[0], ROUTING_KEY);
                Consumer consumer = new TopicConsumer(connection.createChannel(), FUNOUT_EXCHANGE_NAME, args[0],
                        args[1]);
                consumer.listen();
            } else if ("x".equals(in)) {
                connection.close();
                System.exit(0);
            }else {
                System.out.println("输入错误！");
                System.out.println();
                System.out.print("请输入：\n1. 发送消息。\n2.接收消息");
            }
        }
    }
}
