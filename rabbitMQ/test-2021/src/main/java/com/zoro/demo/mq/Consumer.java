package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxingwu
 */
public class Consumer extends ClientBase {
    Consumer(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        super(channel, exchangeName, queueName, bindKey);
        super.init();
    }
    Consumer(Channel channel, String exchangeName, String queueName, String bindKey, BuiltinExchangeType builtinExchangeType) throws IOException {
        super(channel, exchangeName, queueName, bindKey,builtinExchangeType);
        super.initQueue();
    }

    public void listen() throws IOException {
        System.out.printf("routing key: %s, exchange: %s, queue: %s \n",this.bindKey,this.exchangeName,this.queueName);
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");
            /*
            手动应答
            deliver 可以理解为快递员，一个channel中可能会有多个 deliver ,所以以便应答时，服务器知道是哪个消息被接收到了
            手动应答的第一个参数是 deliver 的唯一标识，是每次服务器向消费都发消息时生成的，这样当你收到消息，新起线程处理消息时，应答时需要带着这个标识
            第二个参数，代表是否一次确认多个，如果为true , delivery tag 小于这个消息的 delivery tag的 都会被确认，这样可以减少网络开销
             */
            try {
                // 如果消息队列有其他消费者会发送给其他消费者
                TimeUnit.SECONDS.sleep(5);
                System.out.println("sleep over, call basicAck");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            // 拒绝 ，一次可以拒绝多个消息
            //channel.basicNack();
            // 拒绝， 一次只能拒绝一个消息，这是 amqp 的，basic.nack是 rabbitmq的
            //channel.basicReject();
        };

        /*
        basicConsume 有很多个重载
        com.rabbitmq.client.Channel.basicConsume(...)
         */
        /*
        自动应答模式的目的是提高 rabbitmq 的吞吐，但降低数据的安全性，因为这个模式下，只要消息发出就被认为成功消费了。但是如果消息经过网络传送，没有到达消费者，这个数据就丢了。
        另外，如果这时候就怕消费者能力跟不上，rabbitmq 的高吞吐体现不出来的，所以生产最好不用。
        下面的方法第二个参数控制是否自动应答
         */
        channel.basicConsume(this.queueName, false, deliverCallback, consumerTag -> {
        });

    }
}
