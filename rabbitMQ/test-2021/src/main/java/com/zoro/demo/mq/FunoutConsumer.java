package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 * @author zhaoxingwu
 */
public class FunoutConsumer extends Consumer{

    /**
     *
     * @param channel
     * @param exchangeName
     * @param queueName
     * @param bindKey 广播模式 这个参数是没有用的
     * @throws IOException
     */
    FunoutConsumer(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        // ExchangeType 设置成 FUNOUT
        super(channel, exchangeName, queueName, bindKey, BuiltinExchangeType.FANOUT);
        super.initQueue();
    }
}
