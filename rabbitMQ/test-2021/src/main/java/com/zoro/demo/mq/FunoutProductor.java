package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 * @author zhaoxingwu
 */
public class FunoutProductor extends Publisher{


    FunoutProductor(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        // ExchangeType 设置成 FUNOUT
        super(channel, exchangeName, queueName, bindKey, BuiltinExchangeType.FANOUT);
        this.initExchange();
    }

}
