package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * @author zhaoxingwu
 */
public class Publisher extends ClientBase {


    Publisher(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        super(channel, exchangeName, queueName, bindKey);
        super.init();
    }
    protected Publisher(Channel channel, String exchangeName, String queueName, String bindKey, BuiltinExchangeType exchangeType) throws IOException {
        super(channel, exchangeName, queueName, bindKey, exchangeType);
        this.initExchange();
    }


    void sendMsg() throws IOException {
        Scanner scanner = new Scanner(System.in);
        String msg ;
        System.out.printf("routing key: %s, exchange: %s, queue: %s \n",this.bindKey,this.exchangeName,this.queueName);
        System.out.println("请输入，回车确认，'x'退出");
        while (scanner.hasNext()) {
            msg = scanner.next();
            if (msg == null || msg.length() == 0) {
                System.out.println("不能输入空消息");
                continue;
            }

            if ("x".equals(msg)) {
                System.out.println("退出");
                System.exit(0);
                return;
            }
            this.channel.basicPublish(exchangeName, bindKey, null, msg.getBytes(StandardCharsets.UTF_8));
            System.out.println("成功，请继续");
        }
        System.exit(0);
    }
}
