package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 *
 * topic 的 routing key  有两个匹配的符号
 *  {@code *} 代表一个单词，注意是单词 例子：*.log 可以匹配 error.log,success.log ，但不能匹配 eureka.error.log
 *  {@code #} 代表一个或更多个单词，注意是单词 例子：#.log 可以匹配 error.log,success.log ，也可以匹配 eureka.error.log
 * @author zhaoxingwu
 */
public class TopicConsumer extends Consumer{
    TopicConsumer(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        super(channel, exchangeName, queueName, bindKey, BuiltinExchangeType.TOPIC);
        initQueue();
    }
}
