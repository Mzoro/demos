package com.zoro.demo.mq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * topic 的 routing key  有两个匹配的符号
 * {@code *} 代表一个单词，注意是单词 例子：*.log 可以匹配 error.log,success.log ，但不能匹配 eureka.error.log
 * {@code #} 代表一个或更多个单词，注意是单词 例子：#.log 可以匹配 error.log,success.log ，也可以匹配 eureka.error.log
 *
 * @author zhaoxingwu
 */
public class TopicProduct extends Publisher {

    private static final Scanner scanner = new Scanner(System.in);
    TopicProduct(Channel channel, String exchangeName, String queueName, String bindKey) throws IOException {
        super(channel, exchangeName, queueName, bindKey, BuiltinExchangeType.TOPIC);
        initExchange();
    }


    @Override
    void sendMsg() throws IOException {

        System.out.printf("routing key: %s, exchange: %s, queue: %s \n", this.bindKey, this.exchangeName,
                this.queueName);

        while (true) {
            String topic = topic();
            String msg = msg();

            if ("x".equals(topic) || "x".equals(msg)) {
                System.exit(0);
                return;
            }
            this.channel.basicPublish(exchangeName, topic, null, msg.getBytes(StandardCharsets.UTF_8));
            System.out.println("成功，请继续");
        }
    }

    private String topic() {
            System.out.println("请输入routing key，回车确认，'x'退出");
            while (scanner.hasNext()) {
                String msg = scanner.next();
                if ("x".equals(msg)) {
                    System.exit(0);
                    return null;
                }
                if (msg == null || msg.length() == 0) {
                    System.out.println("routing key 不能为空，可能的格式：*.log,#.log");
                    continue;
                }

                return msg;
            }
            return null;
    }

    private String msg() {
            System.out.println("请输入消息，回车确认，'x'退出");
            while (scanner.hasNext()) {
                String msg = scanner.next();
                if ("x".equals(msg)) {
                    System.exit(0);
                    return null;
                }
                if (msg == null || msg.length() == 0) {
                    System.out.println("消息不能为空");
                    continue;
                }

                return msg;
            }
            return null;
    }
}
