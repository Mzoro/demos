package com.zoro.redisexpireddemo.controller;

import com.zoro.redisexpireddemo.entity.SysUser;
import com.zoro.redisexpireddemo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaoxw
 * Create at 2018/7/14 11:25
 * @version 1.0
 */
@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/hello")
    public String hello(@RequestParam String name) {
        return testService.test(name);
    }

    @RequestMapping("/hello2")
    public SysUser hello2(SysUser sysUser) {
        return testService.test(sysUser);
    }
}
