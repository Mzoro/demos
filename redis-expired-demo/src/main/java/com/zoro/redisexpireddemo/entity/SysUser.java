package com.zoro.redisexpireddemo.entity;

import java.io.Serializable;

/**
 * @author zhaoxw
 * Create at 2018/7/14 13:53
 * @version 1.0
 */
public class SysUser implements Serializable {

    private static final long serialVersionUID = 6824648164862111162L;
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SysUser{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
