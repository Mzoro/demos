package com.zoro.redisexpireddemo.redisconfigs;

import com.zoro.redisexpireddemo.redislistener.MessageDelegate;
import com.zoro.redisexpireddemo.redislistener.impl.MessageDelegateImpl;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/7/14 10:52
 * @version 1.0
 */
@Configuration
public class RedisConfigBeans {

    @Bean
    public JedisPoolConfig jedisPoolConfig() {
        return new JedisPoolConfig();
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("localhost");
        jedisConnectionFactory.setPort(6379);
        jedisConnectionFactory.setDatabase(0);
        jedisConnectionFactory.setPoolConfig(jedisPoolConfig());
        return jedisConnectionFactory;
    }


    @Bean
    public RedisTemplate redisTemplate2() {
        RedisTemplate template = new RedisTemplate();
        template.setConnectionFactory(jedisConnectionFactory());
        JdkSerializationRedisSerializer serializationRedisSerializer = new JdkSerializationRedisSerializer();
        template.setStringSerializer(serializationRedisSerializer);
        template.setDefaultSerializer(serializationRedisSerializer);
        return template;
    }

    @Bean
    public RedisCacheManager redisCacheManager2() {

        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate2());
        redisCacheManager.setDefaultExpiration(10);
        return redisCacheManager;
    }

    @Bean
    public MessageDelegate messageDelegate() {
        return new MessageDelegateImpl();
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter() {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
        messageListenerAdapter.setDelegate(messageDelegate());
        messageListenerAdapter.setSerializer(redisTemplate2().getDefaultSerializer());
        return messageListenerAdapter;
    }

    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        MessageListener messageListenerAdapter = messageListenerAdapter();
        List<Topic> topics = new ArrayList<>();
        topics.add(new ChannelTopic("__keyevent@0__:expired"));
        container.addMessageListener(messageListenerAdapter, topics);
        container.setConnectionFactory(jedisConnectionFactory());
        return container;
    }
}
