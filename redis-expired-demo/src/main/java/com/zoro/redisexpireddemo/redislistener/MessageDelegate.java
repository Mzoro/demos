package com.zoro.redisexpireddemo.redislistener;

import java.io.Serializable;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/7/14 13:17
 * @version 1.0
 */
public interface MessageDelegate {
    void handleMessage(String message);

    //
//    void handleMessage(Map message);
//
//    void handleMessage(byte[] message);
//
    void handleMessage(Serializable message);
//
//    void handleMessage(Serializable message, String channel);

}
