package com.zoro.redisexpireddemo.redislistener.impl;

import com.zoro.redisexpireddemo.redislistener.MessageDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

/**
 * @author zhaoxw
 * Create at 2018/7/14 13:18
 * @version 1.0
 */
public class MessageDelegateImpl implements MessageDelegate {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void handleMessage(String message) {
        LOGGER.info("handleMessage(String message)");
        LOGGER.info(message);
    }
//
//    @Override
//    public void handleMessage(Map message) {
//        LOGGER.info("handleMessage(Map message)");
//        LOGGER.info(message.toString());
//    }
//
//    @Override
//    public void handleMessage(byte[] message) {
//        LOGGER.info("handleMessage(byte[] message)");
//        LOGGER.info(Arrays.toString(message));
//    }
//
    @Override
    public void handleMessage(Serializable message) {
        LOGGER.info("handleMessage(Serializable message)");
        LOGGER.info(message.getClass().getName());
        LOGGER.info(message.toString());
    }
//
//    @Override
//    public void handleMessage(Serializable message, String channel) {
//        LOGGER.info("handleMessage(Serializable message, String channel)");
//        LOGGER.info(message.toString());
//    }
}
