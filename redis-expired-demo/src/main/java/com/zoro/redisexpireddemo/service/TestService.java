package com.zoro.redisexpireddemo.service;

import com.zoro.redisexpireddemo.entity.SysUser;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author zhaoxw
 * Create at 2018/7/14 11:26
 * @version 1.0
 */
@Service
public class TestService {

    @Cacheable(value = "testhaha", key = "#root.args[0]")
    public String test(String name) {
        System.err.println("执行了这个方法test(String name) ");
        return "return-" + name;
    }

    @Cacheable(value = "testhaha", key = "#root.args[0]")
    public SysUser test(SysUser user) {
        System.err.println("执行了这个方法test(SysUser user)");
        user.setName("keykeykeykeykey");
        return user;
    }

    public SysUser test2(SysUser user) {
        System.err.println("执行了这个方法test2(SysUser user)");
        user.setName("hahahahaha");
        return user;
    }

}
