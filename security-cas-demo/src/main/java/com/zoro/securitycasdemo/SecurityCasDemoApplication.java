package com.zoro.securitycasdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zoro.securitycasdemo.dao")
public class SecurityCasDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCasDemoApplication.class, args);
    }
}
