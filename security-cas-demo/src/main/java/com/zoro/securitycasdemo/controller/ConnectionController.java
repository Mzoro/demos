package com.zoro.securitycasdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 17:19
 * @version: 1.0
 */
@RestController
public class ConnectionController {

    @RequestMapping("/hello")
    public String hello() {
        return "hello-world";
    }

    @RequestMapping("/hi")
    public String hi() {
        return "hi-world";
    }

    @RequestMapping("/anyone")
    public String anyone() {
        return "anyone";
    }
}
