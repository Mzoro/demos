package com.zoro.securitycasdemo.securityconfigs;

import com.zoro.securitycasdemo.service.DetalsServiceImpl;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.Cas30ServiceTicketValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.annotation.Resource;

/**
 * @author: zhaoxw
 * Create at: 2018/6/27 17:55
 * @version: 1.0
 */
@EnableWebSecurity
public class CustomSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private Environment environment;

    /********************************         Security 配置部分              *****************************/


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/favicon.ico", "/login/cas").permitAll()
                .anyRequest().authenticated().and()
                .addFilterBefore(filterSecurityInterceptor(), FilterSecurityInterceptor.class)
                .addFilterAt(casAuthenticationFilter(), CasAuthenticationFilter.class)
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(casAuthenticationEntryPoint());
    }

    @Override
    public void configure(WebSecurity webSecurity) {
        webSecurity.ignoring().antMatchers("/js/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.authenticationProvider(casAuthenticationProvider());
    }

    /********************************         cas认证部分                   ****************************/


    @Bean
    public CasAuthenticationFilter casAuthenticationFilter() throws Exception {

        //使用默认的认证路径"/login/cas"
        CasAuthenticationFilter casAuthenticationFilter = new CasAuthenticationFilter();
        SavedRequestAwareAuthenticationSuccessHandler successHandler = new
                SavedRequestAwareAuthenticationSuccessHandler();
        successHandler.setAlwaysUseDefaultTargetUrl(false);
        successHandler.setDefaultTargetUrl("/index.html");
        casAuthenticationFilter.setAuthenticationManager(authenticationManager());
        casAuthenticationFilter.setAuthenticationSuccessHandler(successHandler);

        return casAuthenticationFilter;
    }

    @Bean
    public CasAuthenticationEntryPoint casAuthenticationEntryPoint() {
        CasAuthenticationEntryPoint casAuthenticationEntryPoint = new CasAuthenticationEntryPoint();
        casAuthenticationEntryPoint.setServiceProperties(serviceProperties());
        casAuthenticationEntryPoint.setLoginUrl(environment.getProperty("cas.login.url"));

        return casAuthenticationEntryPoint;
    }

    @Bean
    public ServiceProperties serviceProperties() {
        ServiceProperties serviceProperties = new ServiceProperties();
        serviceProperties.setService(environment.getProperty("client.address"));
        return serviceProperties;
    }

    @Bean
    public CasAuthenticationProvider casAuthenticationProvider() {
        CasAuthenticationProvider casAuthenticationProvider = new CasAuthenticationProvider();
        casAuthenticationProvider.setKey("cas_provider");
        Cas20ServiceTicketValidator cas20ServiceTicketValidator = new Cas20ServiceTicketValidator(environment
                .getProperty("cas.server.host"));
        casAuthenticationProvider.setTicketValidator(cas20ServiceTicketValidator);
        casAuthenticationProvider.setUserDetailsService(detailsService());
        casAuthenticationProvider.setServiceProperties(serviceProperties());
        return casAuthenticationProvider;
    }


    /********************************         权限认证部分                   ****************************/


    public MyFilterSecurityInterceptor filterSecurityInterceptor() {
        MyFilterSecurityInterceptor securityInterceptor = new MyFilterSecurityInterceptor();
        securityInterceptor.setMyFilterInvocationSecurityMetadataSource(myFilterInvocationSecurityMetadataSource());
        securityInterceptor.setAccessDecisionManager(myAccessDecisionManager());
        return securityInterceptor;
    }

    @Bean
    public MyAccessDecisionManager myAccessDecisionManager() {
        return new MyAccessDecisionManager();
    }

    @Bean
    public MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource() {
        return new MyFilterInvocationSecurityMetadataSource();
    }

    @Bean
    public DetalsServiceImpl detailsService() {
        return new DetalsServiceImpl();
    }

    @Bean
    public CustomPasswordEncoder customPasswordEncoder() {
        return new CustomPasswordEncoder();
    }
}
