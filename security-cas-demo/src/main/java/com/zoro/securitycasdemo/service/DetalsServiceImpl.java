package com.zoro.securitycasdemo.service;

import com.zoro.securitycasdemo.dao.SysRoleDao;
import com.zoro.securitycasdemo.dao.SysUserDao;
import com.zoro.securitycasdemo.entity.SysUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;

/**
 *
 * @author: zhaoxw
 * Create at: 2018/6/27 17:40
 * @version: 1.0
 */
public class DetalsServiceImpl implements UserDetailsService {

    @Resource
    private SysUserDao sysUserDao;

    @Resource
    private SysRoleDao sysRoleDao;

    /**
     * security 通过这个方法去数据来获取用户信息,所以只要实现这个方法,按照自己的逻辑去判断是否返回用户就可以了
     *
     * @param username 用户登陆的标识
     * @return 查询的用户
     * @throws UsernameNotFoundException 登陆失败
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = sysUserDao.getByAccount(username);
        sysUser.setSysRoleDao(sysRoleDao);
        return sysUser;
    }
}
