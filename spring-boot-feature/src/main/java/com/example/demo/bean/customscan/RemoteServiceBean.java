package com.example.demo.bean.customscan;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RemoteServiceBean {

    String serviceName();

    String defaultService();

    ServiceType serviceType() default ServiceType.REMOTE;

    enum ServiceType {
        BEAN, REMOTE
    }
}
