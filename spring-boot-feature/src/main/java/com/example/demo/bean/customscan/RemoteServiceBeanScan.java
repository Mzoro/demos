package com.example.demo.bean.customscan;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(RemoteServiceBeanScanRegistrar.class)
public @interface RemoteServiceBeanScan {

    /**
     * package name that be supposed to scan
     *
     * @return package names
     */
    String[] basePackage() default {};
}
