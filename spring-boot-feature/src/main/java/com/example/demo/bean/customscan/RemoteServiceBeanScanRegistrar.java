package com.example.demo.bean.customscan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.ClassUtils;

import java.util.Arrays;

public class RemoteServiceBeanScanRegistrar implements ImportBeanDefinitionRegistrar {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        AnnotationAttributes mapperScanAttrs = AnnotationAttributes
                .fromMap(importingClassMetadata.getAnnotationAttributes(RemoteServiceBeanScan.class.getName()));
        String[] packages;
        if (mapperScanAttrs != null) {
            packages = getPackageArr(mapperScanAttrs, importingClassMetadata);
        } else {
            packages = defaultPackageArr(importingClassMetadata.getClassName());
        }
        logger.debug("scan package : {}", Arrays.toString(packages));

        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(ClassPathRemoteServiceBeanScanner.class);
        builder.addConstructorArgValue(registry);
        builder.addConstructorArgValue(packages);
        String beanName = importingClassMetadata.getClassName() + "#" + getClass().getSimpleName() + "#" + 0;
        logger.debug("scanner's bean name is {}",beanName);
        builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
        registry.registerBeanDefinition(beanName, builder.getBeanDefinition());

    }

    private String[] getPackageArr(AnnotationAttributes attributes, AnnotationMetadata metadata) {
        String[] packages = attributes.getStringArray("basePackage");

        if (packages.length == 0) {
            packages = defaultPackageArr(metadata.getClassName());
        }

        return packages;

    }

    private String[] defaultPackageArr(String className) {
        return new String[]{ClassUtils.getPackageName(className)};
    }
}
