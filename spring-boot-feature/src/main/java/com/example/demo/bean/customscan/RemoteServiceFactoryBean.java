package com.example.demo.bean.customscan;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.data.redis.core.RedisTemplate;

import java.lang.reflect.Proxy;

public class RemoteServiceFactoryBean<T> implements FactoryBean<T> {
    private RedisTemplate<String, String> remoteServiceBeanOperator;

    private final Class<T> tClass;

    public RemoteServiceFactoryBean(Class<T> tClass) {
        this.tClass = tClass;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getObject() throws Exception {
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), new Class[] { tClass }, new RemoteServiceProxy(remoteServiceBeanOperator));
    }

    @Override
    public Class<T> getObjectType() {
        return tClass;
    }

    public RedisTemplate<String, String> getRemoteServiceBeanOperator() {
        return remoteServiceBeanOperator;
    }

    public void setRemoteServiceBeanOperator(RedisTemplate<String, String> remoteServiceBeanOperator) {
        this.remoteServiceBeanOperator = remoteServiceBeanOperator;
    }
}
