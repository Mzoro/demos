package com.example.demo.bean.customscan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class RemoteServiceProxy implements InvocationHandler, Serializable {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final RedisTemplate<String, String> remoteServiceBeanOperator;

    public RemoteServiceProxy(RedisTemplate<String, String> remoteServiceBeanOperator) {
        if (remoteServiceBeanOperator == null) {
            logger.error("remoteServiceBeanOperator should not be null");
            throw new RuntimeException("remoteServiceBeanOperator should not be null");
        }
        this.remoteServiceBeanOperator = remoteServiceBeanOperator;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())) {
            return method.invoke(this, args);
        } else {
            // 调用远程方法
            System.out.println("进入代理方法 ！！！！");
            System.out.println(method.getName());
            System.out.println(Arrays.toString(args));
            System.out.println("remoteServiceBeanOperator:" + this.remoteServiceBeanOperator);
            return null;
        }
    }

}
