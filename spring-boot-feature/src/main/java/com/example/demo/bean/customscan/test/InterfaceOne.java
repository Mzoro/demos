package com.example.demo.bean.customscan.test;

import com.example.demo.bean.customscan.RemoteServiceBean;

@RemoteServiceBean(serviceName = "oneService",defaultService = "one",serviceType = RemoteServiceBean.ServiceType.BEAN)
public interface InterfaceOne {

    String getName();
}
