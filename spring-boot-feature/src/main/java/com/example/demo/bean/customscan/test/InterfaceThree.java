package com.example.demo.bean.customscan.test;

import com.example.demo.bean.customscan.RemoteServiceBean;

@RemoteServiceBean(serviceName = "threeService",defaultService = "three",serviceType = RemoteServiceBean.ServiceType.BEAN)
public interface InterfaceThree {

    String threeMethod();
}
