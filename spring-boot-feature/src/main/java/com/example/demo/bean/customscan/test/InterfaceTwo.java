package com.example.demo.bean.customscan.test;

import com.example.demo.bean.customscan.RemoteServiceBean;

@RemoteServiceBean(serviceName = "twoService",defaultService = "two")
public interface InterfaceTwo {

    String twoMethod(String p1,int p2);
}
