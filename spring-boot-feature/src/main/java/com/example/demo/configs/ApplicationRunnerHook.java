package com.example.demo.configs;

import com.example.demo.controller.mq.MqConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @author zhaoxingwu
 */
@Component
@ConditionalOnProperty(name = "test.rabbit.enable", havingValue = "true")
public class ApplicationRunnerHook implements ApplicationRunner {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MqConsumer mqConsumer;

    public ApplicationRunnerHook(MqConsumer mqConsumer) {
        this.mqConsumer = mqConsumer;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("程序启动");
        logger.info("启动消息监听");
        this.mqConsumer.doReceiveAndReply();
        this.mqConsumer.doReceive();
        this.mqConsumer.doReceiveAndConvert();
    }
}
