package com.example.demo.configs;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class MybatisPlusConfig {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(@Autowired(required = false) DataSource dataSource) {
        if (dataSource == null) {
            logger.warn("======no datasource provided.========");
            MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
            //向Mybatis过滤器链中添加分页拦截器
            interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.ORACLE_12C));
            //还可以添加i他的拦截器
            return interceptor;
        } else {
            DbType dbType = this.getDbType(dataSource);
            logger.info("database is {}", dbType);
            MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
            interceptor.addInnerInterceptor(new PaginationInnerInterceptor(dbType));
            return interceptor;
        }
    }

    private DbType getDbType(DataSource source) {
        try (Connection connection = source.getConnection()) {
            String prod = connection.getMetaData().getDatabaseProductName();
            if ("oracle".equalsIgnoreCase(prod)) {
                return DbType.ORACLE_12C;
            } else if ("PostgreSQL".equalsIgnoreCase(prod)) {
                return DbType.POSTGRE_SQL;
            } else {
                return DbType.ORACLE_12C;
            }

        } catch (SQLException e) {
            logger.error("Can't get Connection from datasource, use {}", DbType.ORACLE_12C, e);
            return DbType.ORACLE_12C;
        }
    }

}
