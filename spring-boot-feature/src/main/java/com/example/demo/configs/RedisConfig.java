package com.example.demo.configs;

import com.example.demo.configs.enums.RedisConnectionType;
import io.lettuce.core.ReadFrom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

@Configuration
public class RedisConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final TestConfigProperties properties;

    public RedisConfig(TestConfigProperties properties) {
        this.properties = properties;
        logger.info("redis config is : {}", properties.getRedis());
    }

    @Bean
    public RedisTemplate<String, String> remoteServiceBeanOperator() {
        TestConfigProperties.Redis redis = properties.getRedis();
        RedisConnectionType connectionType = redis.getType();
        LettuceConnectionFactory factory;
        if (connectionType == RedisConnectionType.STANDALONE) {
            factory = standaloneConfigurationFactory(redis);
        } else if (connectionType == RedisConnectionType.REPLICATION) {
            factory = this.replicationConfigurationFactory(redis);
        } else if (connectionType == RedisConnectionType.SENTINEL) {
            factory = this.sentinelConfigurationFactory(redis);
        } else {
            // 默认使用 Standalone
            factory = standaloneConfigurationFactory(redis);
        }
        factory.afterPropertiesSet();
        return new StringRedisTemplate(factory);
    }

    /**
     * 单机模式
     *
     * @param redis 配置
     * @return ConnectionFactory
     */
    private LettuceConnectionFactory standaloneConfigurationFactory(TestConfigProperties.Redis redis) {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(redis.getHost());
        configuration.setPassword(redis.getPassword());
        configuration.setUsername(redis.getUsername());
        configuration.setDatabase(redis.getDatabase());
        configuration.setPort(redis.getPort());
        return new LettuceConnectionFactory(configuration,
                LettuceClientConfiguration.builder().clientName(redis.getClientName()).build());

    }

    /**
     * 主从模式，静态主从集群，不通过 sentinel 连接
     * <p>
     * ReadFrom.REPLICA_PREFERRED 从副本读取数据，如果副本不可用，从主节点读取
     * ReadFrom.MASTER_PREFERRED 从主节点读取数据，如果主节点不可用，从副本节点读取
     * <p>
     * 这个功能是通过 redis 主节点的 info replication 命令完成的， 这个命令可以列出当前所有的从节点信息,
     * 当使用docker ,虚拟机等复杂网络环境时，可能得到的从节点IP 不准确，这时候要在从节点配置上增加以下项目：
     * replica-announce-ip 5.5.5.5
     * replica-announce-port 1234
     * <p>
     * 用这两个配置告诉主节点，从节点真正的访问IP
     * <p>
     * 这个功能不是 failover ,只是在程序启动时候查询一次， failover 还是要使用 sentinel
     *
     * @param redis 配置
     * @return factory
     * @see ReadFrom
     */
    private LettuceConnectionFactory replicationConfigurationFactory(TestConfigProperties.Redis redis) {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(redis.getHost());
        configuration.setPassword(redis.getPassword());
        configuration.setUsername(redis.getUsername());
        configuration.setDatabase(redis.getDatabase());
        configuration.setPort(redis.getPort());
        LettuceClientConfiguration client = LettuceClientConfiguration.builder()
                .clientName(redis.getClientName())
                .readFrom(ReadFrom.REPLICA_PREFERRED)
                .build();
        return new LettuceConnectionFactory(configuration, client);
    }

    /**
     * 通过sentinel 连接， 连接redis 时不要直接连接到数据节点。
     * <p>
     * 程序启动时会先向sentinel 查询当前的主节点信息，连接主节点，
     * 当主节点下线，会再向sentinel查询主节点信息
     * <p>
     * TODO 这里的密码设置有收下几个问题
     *   1. 如果数据节点的密码都不一样怎么设置?
     *   2. 如果sentinel的密码都不一样怎么办?
     *   这两个问题redis官方也没有解决办法:
     *   <a href="https://github.com/redis/redis/issues/7292">this issue</a>
     * <p>
     *   可以考虑tls
     *
     * @param redis 配置参数
     * @return factory
     */
    private LettuceConnectionFactory sentinelConfigurationFactory(TestConfigProperties.Redis redis) {
        List<TestConfigProperties.SentinelInfo> sentinelInfoList = redis.getSentinel();

        RedisSentinelConfiguration sentinelConfig = new RedisSentinelConfiguration()
                .master(redis.getMasterName());
        // master 的密码
        sentinelConfig.setPassword(redis.getPassword());
        if (sentinelInfoList != null) {
            for (TestConfigProperties.SentinelInfo sentinelInfo : sentinelInfoList) {
                sentinelConfig.sentinel(sentinelInfo.getHost(), sentinelInfo.getPort());
                // TODO 这里的sentinel密码可能只能设置一个
                sentinelConfig.setSentinelPassword(sentinelInfo.getPassword());
            }
        }
        LettuceClientConfiguration client = LettuceClientConfiguration.builder()
                .clientName(redis.getClientName())
                .build();
        return new LettuceConnectionFactory(sentinelConfig, client);
    }

}
