package com.example.demo.configs;

import com.example.demo.configs.enums.RedisConnectionType;
import com.example.demo.util.Uuid;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@ConfigurationProperties(prefix = "zoro.test")
@Component
public class TestConfigProperties {

    private Redis redis = new Redis();

    public Redis getRedis() {
        return redis;
    }

    public void setRedis(Redis redis) {
        this.redis = redis;
    }

    @Override
    public String toString() {
        return "TestConfigProperties{" +
                "redis=" + redis +
                '}';
    }

    public static class Redis{
        private RedisConnectionType type;
        private String host = "localhost";
        private int port = 6379;
        private int database = 0;
        private String password;
        private String username;
        /**
         * 当一个客户端连接到server时，用于区分连接来自哪个客户端， 在服务器上使用 client list 命令可以查看
         */
        private String clientName = Uuid.uuid();
        /**
         * for sentinel
         */
        private String masterName = "master";
        private List<SentinelInfo> sentinel;

        public String getMasterName() {
            return masterName;
        }

        public void setMasterName(String masterName) {
            this.masterName = masterName;
        }

        public List<SentinelInfo> getSentinel() {
            return sentinel;
        }

        public void setSentinel(List<SentinelInfo> sentinel) {
            this.sentinel = sentinel;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public int getDatabase() {
            return database;
        }

        public void setDatabase(int database) {
            this.database = database;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public RedisConnectionType getType() {
            return type;
        }

        public void setType(RedisConnectionType type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "Redis{" +
                    "type=" + type +
                    ", host='" + host + '\'' +
                    ", port=" + port +
                    ", database=" + database +
                    ", password='" + password + '\'' +
                    ", username='" + username + '\'' +
                    ", clientName='" + clientName + '\'' +
                    ", masterName='" + masterName + '\'' +
                    ", sentinel=" + sentinel +
                    '}';
        }
    }

    public static class SentinelInfo{
        private String host;
        private int port;
        private String password;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "SentinelInfo{" +
                    "host='" + host + '\'' +
                    ", port=" + port +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
}
