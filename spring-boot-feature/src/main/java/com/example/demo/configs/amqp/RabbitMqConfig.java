package com.example.demo.configs.amqp;

import com.rabbitmq.client.ShutdownSignalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionListener;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoxingwu
 */
@Configuration
@ConditionalOnProperty(name = "test.rabbit.enable", havingValue = "true")
public class RabbitMqConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory factory) {

        RabbitTemplate template = new RabbitTemplate(factory);
        /*
        1. 如果发送消息需要得到 mq server 的回复，这个需要设置为true , returnCallback 一定要设置, spring.rabbitmq.publisher-returns 一定要设置为true
        2. 无论只哪种回调，都不是发生在“发送消息”的线程的
         */
        template.setMandatory(true);
        template.setReturnsCallback((ReturnedMessage message) -> {
            /*
            1. 当发送到一个不存在的 exchange 时不会进入到这里，会进入到 setConfirmCallback replyCode = 404
            2. 当发送到exchange但没有路由到队列时会返回 replyCode = 302, replyText = NO_ROUTE
             */
            logger.debug("replyCode:" + message.getReplyCode());
            logger.debug("replyText:" + message.getReplyText());
            logger.debug("exchange:" + message.getExchange());
            logger.debug("routingKey:" + message.getRoutingKey());
        });

        template.setConfirmCallback((CorrelationData correlationData, boolean ack, String cause) -> {
            try {
                if (correlationData != null) {

                    logger.debug("correlation.toString " + correlationData);
                    logger.debug("correlation.getReturnedMessage " + (correlationData.getReturned() == null ?
                            null : correlationData.getReturned().getMessage()));
                    logger.debug("correlation.getFuture.get" + correlationData.getFuture().get());
                    logger.debug("correlation.getFuture.get.getReason" + correlationData.getFuture().get().getReason());
                } else {
                    logger.debug("correlation is null");
                }

                logger.debug("confirm ack: " + ack);
                logger.debug("confirm cause: " + cause);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // 发送消息对象的 转换器 ，就是如何序列化对象，默认是 SimpleMessageConverter, jdk序列化
        // Jackson2JsonMessageConverter,
        // https://docs.spring.io/spring-amqp/docs/2.2.20.RELEASE/reference/html/#message-converters
        // template.setMessageConverter();

        // TODO 这个为什么不起作用？
        //      https://docs.spring.io/spring-amqp/docs/2.2.20.RELEASE/reference/html/#publishing-is-async
        factory.addConnectionListener(new ConnectionListener() {
            @Override
            public void onCreate(Connection connection) {

            }

            public void onShutDown(ShutdownSignalException signal) {
                logger.debug("getReason:" + signal.getReason());
                logger.debug("getReference:" + signal.getReference());
            }
        });

         /*
         设置默认的 exchange ,如果发送方法没有指定exchange ，就用这个
         template.setExchange();
         template.setRoutingKey();
         template.setDefaultReceiveQueue();
         */

        // amqpTemplate 序列化对象时使用的转换器
        Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter();
        template.setMessageConverter(converter);


        return template;
    }


}

