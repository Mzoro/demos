package com.example.demo.configs.enums;

public enum RedisConnectionType {
    STANDALONE, REPLICATION, SENTINEL
}
