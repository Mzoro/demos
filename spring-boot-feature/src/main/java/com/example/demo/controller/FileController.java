package com.example.demo.controller;

import com.example.demo.bean.customscan.test.InterfaceOne;
import com.example.demo.bean.customscan.test.InterfaceThree;
import com.example.demo.bean.customscan.test.InterfaceTwo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxingwu
 */
@RestController
@RequestMapping("file")
public class FileController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private InterfaceOne interfaceOne;
    private InterfaceTwo interfaceTwo;
    private InterfaceThree interfaceThree;

    public FileController(InterfaceOne interfaceOne, InterfaceTwo interfaceTwo, InterfaceThree interfaceThree) {
        this.interfaceOne = interfaceOne;
        this.interfaceTwo = interfaceTwo;
        this.interfaceThree = interfaceThree;
    }

    @RequestMapping("/upload")
    public Map<String, String> upload(MultipartFile file, String name) {

        return new HashMap<>();
    }

    @RequestMapping("/testBean")
    public Map<String, String> testBean() {

        logger.debug(this.interfaceOne.getClass().getName());
        logger.debug(this.interfaceTwo.getClass().getName());
        logger.debug(this.interfaceThree.getClass().getName());

        Map<String, String> result = new HashMap<>();

        result.put("interfaceOne", interfaceOne.getClass().getName());
        result.put("interfaceTwo", interfaceTwo.getClass().getName());
        result.put("interfaceThree", interfaceThree.getClass().getName());
        logger.debug(interfaceOne.toString());
        logger.debug(interfaceOne.getName());
        logger.debug(interfaceTwo.twoMethod("p1", 3));
        logger.debug(interfaceThree.threeMethod());

        return result;

    }
}
