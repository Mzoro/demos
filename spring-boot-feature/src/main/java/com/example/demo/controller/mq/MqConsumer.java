package com.example.demo.controller.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.demo.controller.mq.QueueInfoConstants.*;

/**
 * @author zhaoxingwu
 */
@Component
public class MqConsumer {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AmqpTemplate amqpTemplate;

    private volatile Thread consumeThread = null;
    private volatile Thread consumeFanoutThread = null;
    private volatile Thread consumeTopicThread = null;

    public MqConsumer(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    public synchronized void doReceiveAndReply() {
        if (consumeThread != null && consumeThread.isAlive()) {
            logger.warn("监听线程正在运行");
            return;
        }
        consumeThread = new Thread(() -> {
            while (true) {
                // 这个方法是收到一个消息后，会将方法的返回值发送到 replyExchange
                amqpTemplate.receiveAndReply(QUEUE_NAME, (a) -> {

                    if (a.getClass().isArray()) {
                        // 数组
                        if (byte[].class.equals(a.getClass())) {
                            logger.info("doReceiveAndReply：{}", new String((byte[]) a));
                        } else {
                            logger.info("doReceiveAndReply：{}", Arrays.toString((Object[]) a));
                        }
                    } else if (a instanceof String || a.getClass().isPrimitive()) {
                        logger.info("doReceiveAndReply：{}", a);
                    } else {
                        logger.info("" + a);
                    }

                    return null;
                }, "", "FirstQueue");
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        consumeThread.setDaemon(true);
        consumeThread.start();
    }

    public synchronized void doReceive() {
        if (consumeFanoutThread != null && consumeFanoutThread.isAlive()) {
            logger.warn("监听线程正在运行");
            return;
        }
        consumeFanoutThread = new Thread(() -> {
            while (true) {

                // 阻塞式的调用，第二个参数代表超时时间，毫秒
                // amqpTemplate.receive(QUEUE_NAME_F, 1000);
                // 非阻塞式的接收，没有消息时返回null
                Message message = amqpTemplate.receive(QUEUE_NAME_F);
                if (message == null) {
                    logger.debug("doReceive:暂时没有消息");
                } else {
                    logger.info("doReceive:{}", new String(message.getBody()));

                }

                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        consumeFanoutThread.setDaemon(true);
        consumeFanoutThread.start();
    }

    public synchronized void doReceiveAndConvert() {
        if (consumeTopicThread != null && consumeTopicThread.isAlive()) {
            logger.warn("监听线程正在运行");
            return;
        }
        consumeTopicThread = new Thread(() -> {
            while (true) {
                //  这里 匿名类的泛型不写，在编译的时候会报错
                //  这里 匿名类的泛型不写，在编译的时候会报错
                //  这里 匿名类的泛型不写，在编译的时候会报错
                String s = this.amqpTemplate.receiveAndConvert(QUEUE_NAME_T, new ParameterizedTypeReference<String>() {
                    @Override
                    public Type getType() {
                        return String.class;
                    }
                });
                if (s == null) {
                    logger.debug("doReceiveAndConvert:暂时没有消息");
                } else {

                    logger.info("doReceiveAndConvert:{}", s);
                }
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        consumeTopicThread.setDaemon(true);
        consumeTopicThread.start();
    }
}
