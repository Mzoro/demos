package com.example.demo.controller.mq;

import com.example.demo.controller.vo.MqTestMessage;
import org.springframework.amqp.core.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

import static com.example.demo.controller.mq.QueueInfoConstants.*;

/**
 * @author zhaoxingwu
 */
@RestController
@RequestMapping("/mq-test")
@ConditionalOnProperty(name = "test.rabbit.enable", havingValue = "true")
public class MqTestController {
    private final AmqpTemplate amqpTemplate;

    public MqTestController(AmqpTemplate amqpTemplate, AmqpAdmin amqpAdmin) {
        this.amqpTemplate = amqpTemplate;

        // direct 方式
        Exchange exchange = new DirectExchange(EX_NAME);
        amqpAdmin.declareExchange(exchange);
        Queue queue = new Queue(QUEUE_NAME);
        amqpAdmin.declareQueue(queue);
        Binding binding = BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY).noargs();
        amqpAdmin.declareBinding(binding);

        // 广播方式
        Exchange exchangeF = new FanoutExchange(EX_NAME_F);
        amqpAdmin.declareExchange(exchangeF);
        Queue queueF = new Queue(QUEUE_NAME_F);
        amqpAdmin.declareQueue(queueF);
        Binding bindingF = BindingBuilder.bind(queueF).to(exchangeF).with("").noargs();
        amqpAdmin.declareBinding(bindingF);

        // topic 多播方式
        Exchange exchangeT = new TopicExchange(EX_NAME_T);
        amqpAdmin.declareExchange(exchangeT);
        Queue queueT = new Queue(QUEUE_NAME_T);
        amqpAdmin.declareQueue(queueT);
        Binding bindingT = BindingBuilder.bind(queueT).to(exchangeT).with(ROUTING_KEY_T).noargs();
        amqpAdmin.declareBinding(bindingT);
    }

    @RequestMapping("/message")
    public void message(@RequestBody MqTestMessage msg) {
        System.out.println(msg.getMsg());
        if (msg.getMsg() == null) {
            return;
        }
//        amqpTemplate.send(MessageBuilder.withBody(msg.getMsg().getBytes(StandardCharsets.UTF_8)).build());
        Message message = MessageBuilder.withBody(msg.getMsg().getBytes(StandardCharsets.UTF_8)).build();
        amqpTemplate.send(EX_NAME, ROUTING_KEY, message);
        amqpTemplate.send(EX_NAME_F, "not-important", message);
        amqpTemplate.send(EX_NAME_ANNOTATION, ROUTING_KEY_ANNOTATION, message);
        //amqpTemplate.send(EX_NAME_T, ROUTING_KEY_T, message);
        amqpTemplate.convertAndSend(EX_NAME_T, ROUTING_KEY_T, msg);
    }
}
