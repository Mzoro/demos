package com.example.demo.controller.mq;

/**
 * @author zhaoxingwu
 */
public class QueueInfoConstants {

     public final static String EX_NAME = "spring-amqp-ex";
     public final static String EX_NAME_F = "spring-amqp-ex-f";
     public final static String EX_NAME_T = "spring-amqp-ex-t";
     public final static String EX_NAME_ANNOTATION = "spring-amqp-ex-annotation";
     public final static String QUEUE_NAME = "spring-amqp-q";
     public final static String QUEUE_NAME_F = "spring-amqp-q-f";
     public final static String QUEUE_NAME_T = "spring-amqp-q-t";
     public final static String QUEUE_NAME_ANNOTATION = "spring-amqp-q-annotation";
     public final static String ROUTING_KEY = "spring-amqp-q";
     public final static String ROUTING_KEY_T = "#.spring.topic.#";
     public final static String ROUTING_KEY_ANNOTATION = "routing.key.annotation";

}
