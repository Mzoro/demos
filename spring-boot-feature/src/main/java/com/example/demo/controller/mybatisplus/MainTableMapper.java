package com.example.demo.controller.mybatisplus;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MainTableMapper {

    List<Map<String, String>> query(IPage<Map<String,String>> page);
}
