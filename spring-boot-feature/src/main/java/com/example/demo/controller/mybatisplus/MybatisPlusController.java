package com.example.demo.controller.mybatisplus;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/mp")
public class MybatisPlusController {

    private final MainTableMapper mapper;

    public MybatisPlusController(MainTableMapper mapper) {
        this.mapper = mapper;
    }

    @GetMapping("testPage")
    public void testPage() {
        Page<Map<String, String>> page = new Page<>(1, 100);
        page.setOptimizeCountSql(false);
        this.mapper.query(page);
    }
}
