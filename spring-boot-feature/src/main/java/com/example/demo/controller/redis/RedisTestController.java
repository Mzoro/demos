package com.example.demo.controller.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("test/redis")
public class RedisTestController {

    private final RedisTemplate<String, String> template;

    public RedisTestController(@Qualifier("remoteServiceBeanOperator") RedisTemplate<String, String> template) {
        this.template = template;
    }

    @GetMapping("getKeys")
    public Map<String,String> getKeys(){
        Map<String, String> result = new HashMap<>();
        Cursor<String> cursor = this.template.scan(ScanOptions.NONE);
        while (cursor.hasNext()) {
            String next = cursor.next();
            String value = this.template.opsForValue().get(next);
            System.out.println(next + ":" + value);
            result.put(next, value);
        }
        return result;

    }
}
