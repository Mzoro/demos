package com.example.demo.controller.spel;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaoxingwu
 */
@RestController
@RequestMapping("/spel")
public class SpelController {

    @RequestMapping("")
    public void test() {
        // TODO 这个还不行，不知道怎么灵活使用 spel
        String el = "#{spring.rabbitmq.password}/123";
        EvaluationContext context = SimpleEvaluationContext.forReadOnlyDataBinding().build();
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(el);
        Object r = exp.getValue(context);
        System.err.println(r);
    }
}
