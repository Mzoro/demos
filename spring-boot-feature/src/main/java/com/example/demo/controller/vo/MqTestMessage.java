package com.example.demo.controller.vo;

/**
 * @author zhaoxingwu
 */
public class MqTestMessage {

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MqTestMessage{" +
                "msg='" + msg + '\'' +
                '}';
    }
}
