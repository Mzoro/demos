package com.example.demo.listener.annotationlistener;

import com.example.demo.controller.mq.MqTestController;
import com.example.demo.controller.vo.MqTestMessage;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.example.demo.controller.mq.QueueInfoConstants.*;

/**
 * 这是一个基于注解的listener  , 节省不少代码。
 *
 * @author zhaoxingwu
 */
@Component
@ConditionalOnProperty(name = "test.rabbit.enable", havingValue = "true")
public class MyAnnotationListener {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 最简单的，这样就可以监听这个队列了，但是这个队列一定是已经在rabbitmq 上声明的
     */
    @RabbitListener(queues = QUEUE_NAME)
    public void directListener(Object o) {
        logger.debug("directListener,{}:{}", o.getClass().getName(), o);
    }


    /**
     * 可以声明需要队列,exchange，
     * 传入的参数要看 使用的是哪个converter, 入参会根据 消息头的 ContentType 进行处理，
     * 所以这里要充分了解发送方是如何发送的
     * <p>
     * 可以在注解中设置应答模式为手动 {@code ackMode="MANUAL"} ,这里需要在方法上声明 Channel 与 delivery tag 用于应答
     *
     * <p>
     * 第二个参数 ，{@link Header}是用于将你想要的消息头信息传入方法中
     * <p>
     * {@code @SendTo} 是用于表示将返回传发送到哪具exchange
     *
     * @see SimpleMessageConverter
     * @see org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
     * @see MqTestController#message(MqTestMessage)
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(value = QUEUE_NAME_ANNOTATION, declare = "true"),
                    exchange = @Exchange(value = EX_NAME_ANNOTATION, declare = Exchange.TRUE, type =
                            ExchangeTypes.DIRECT),
                    key = ROUTING_KEY_ANNOTATION),
            @QueueBinding(
                    value = @Queue(value = QUEUE_NAME_T, declare = "false"),
                    exchange = @Exchange(value = EX_NAME_T, declare = Exchange.FALSE),
                    key = ROUTING_KEY_T, declare = "false")}, ackMode = "MANUAL")
//    @SendTo
    public void otherListener(Object o, @Header(AmqpHeaders.CONSUMER_QUEUE) String queueName, Channel channel,
                              @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        logger.debug("otherListener,queue name {},{}:{}", queueName, o.getClass().getName(), o);
        if (QUEUE_NAME_ANNOTATION.equals(queueName)) {
            channel.basicAck(tag, false);
        }
    }
}
