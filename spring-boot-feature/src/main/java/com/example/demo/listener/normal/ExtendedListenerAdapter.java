package com.example.demo.listener.normal;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;

import static com.example.demo.controller.mq.QueueInfoConstants.QUEUE_NAME_F;
import static com.example.demo.controller.mq.QueueInfoConstants.QUEUE_NAME_T;

/**
 * @author zhaoxingwu
 */
public class ExtendedListenerAdapter extends MessageListenerAdapter {

    public ExtendedListenerAdapter(Object delegate) {
        super(delegate);
    }

    /**
     * 这个类是对 MessageListenerAdapter 的扩展，MessageListenerAdapter 只能将消息内容传递给listener ,但是这个方法可以将消息内容，Channel,Message
     * 传给listener,还可以重写更多方法达到对不同队列用不同的方法消息,只要能正确构造参数列表就行了
     *
     * @param extractedMessage 消息内容
     * @param channel          channel
     * @param message          message
     * @return arguments
     */
    @Override
    protected Object[] buildListenerArguments(Object extractedMessage, Channel channel, Message message) {
        return new Object[]{channel, message};
    }

    /**
     * @param originalMessage  message
     * @param extractedMessage message 内容
     * @return method name
     */
    @Override
    protected String getListenerMethodName(Message originalMessage, Object extractedMessage) {
        String queueName = originalMessage.getMessageProperties().getConsumerQueue();
        if (QUEUE_NAME_F.equals(queueName)) {
            return "fanoutHandle";
        } else if (QUEUE_NAME_T.equals(queueName)) {
            return "topicHandle";
        }
        return super.getListenerMethodName(originalMessage, extractedMessage);
    }
}

class CustomListenerForAdapter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void fanoutHandle(Channel channel, Message message) {
        logger.debug("fanoutHandle 收到消息：");
    }

    public void topicHandle(Channel channel, Message message) {
        logger.debug("topicHandle 收到消息：");
    }

}
