package com.example.demo.listener.normal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.converter.SimpleMessageConverter;

/**
 * @author zhaoxingwu
 */
public class ListenerForAdapter {

    private final static Logger LOGGER = LoggerFactory.getLogger(ListenerForAdapter.class);

    /**
     * 这里收到的参数是什么类型很难说，
     * 1. 取决于在发送时使用的是什么方式发送了，不同的方式发送，消息格式不一样，消息送ContentType不一样，
     * 默认的 SimpleMessageConverter , ContentType=application/octet-stream 或  application/x-java-serialized-object 或
     * text/plain ，
     * 这取决于调用 convertAndSend() 方法时 message 对象的类型
     * {@link SimpleMessageConverter#createMessage(java.lang.Object, org.springframework.amqp.core.MessageProperties)}
     * <p>
     * 如果使用的 Jackson2JsonMessageConverter , ContentType=application/json
     * <p>
     * 2. 取决于在配置 MessageListenerAdapter 时设置的是哪一种Converter ,如果使用的默认的  SimpleMessageConverter ，那么
     * 会按照前面说的反序列化，所以收到的参数就是发送时的参数类型，但要注意不同服务间的jdk与类版本是否兼容
     * 如果使用的是 Jackson2JsonMessageConverter ,同样的，如果可以按照json 格式反序列化，收到的就是反序列化后的对象
     *
     * 所以综上所束 ，发送方与接收方要统一好消息的格式
     *
     * @param message
     */
    public void handle(Object message) {
        if (message != null && message.getClass() == byte[].class) {
            LOGGER.debug("收到消息：{}", new String((byte[]) message));
        } else {
            LOGGER.debug("收到消息：{}", message);
        }
    }
}
