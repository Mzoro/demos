package com.example.demo.listener.normal;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.example.demo.controller.mq.QueueInfoConstants.*;

/**
 * @author zhaoxingwu
 */
@Configuration
@ConditionalOnProperty(name = "test.rabbit.enable", havingValue = "true")
public class MessageListenerConfig {

    /**
     * 这是最基本的用法,收到消息,处理消息
     *
     * @param cachingConnectionFactory connection factory
     * @return container
     */
    @Bean
    public MessageListenerContainer messageListenerContainer(CachingConnectionFactory cachingConnectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(cachingConnectionFactory);


        /*
         应答方式默认是自动应答，即rabbitmq 发送消息给消息者，就算消息被消费了，
         如果这里设置了手动应答，那么一定要设计好什么时候应答消息，否则消息会一直保留在rabbitmq 中，过多会影响rabbitmq
         */
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setQueueNames(QUEUE_NAME);
//        container.setMessageListener(new MyMessageListener());
        container.setMessageListener(new MyChannelAwareMessageListener());
        return container;
    }

    @Bean
    public MessageListenerContainer messageListenerContainer2(CachingConnectionFactory cachingConnectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(cachingConnectionFactory);

        /*
        这个 adapter 是一个特殊的 Listener , 它不对消息本身做任何处理，只负责通将消息转交线传入的对象（ListenerForAdapter），调用这个对象的方法；
        默认的会调用 handleMessage 这个方法，当然也可以修改setDefaultListenerMethod(String),
        传入的参数根据消息的ContentType 可能是 String,或者是反序列化的对象，或者是普通的 byte[]
         */
        MessageListenerAdapter adapter = new MessageListenerAdapter(new ListenerForAdapter());
        adapter.setMessageConverter(new Jackson2JsonMessageConverter());
        adapter.setDefaultListenerMethod("handle");

        container.setMessageListener(adapter);
        container.setQueueNames(QUEUE_NAME_F, QUEUE_NAME_T);
        return container;
    }

    @Bean
    public MessageListenerContainer messageListenerContainer3(CachingConnectionFactory cachingConnectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(cachingConnectionFactory);

        MessageListenerAdapter adapter = new ExtendedListenerAdapter(new CustomListenerForAdapter());
        adapter.setMessageConverter(new Jackson2JsonMessageConverter());

        container.setMessageListener(adapter);
        container.setQueueNames(QUEUE_NAME_F, QUEUE_NAME_T);

        return container;
    }
}
