package com.example.demo.listener.normal;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

/**
 * 这个消息监听可以将，接收消息的 Channel 传进来。从而完成手动 应答或者拒绝
 *
 * @author zhaoxingwu
 */
public class MyChannelAwareMessageListener implements ChannelAwareMessageListener {
    private final static Logger LOGGER = LoggerFactory.getLogger(MyChannelAwareMessageListener.class);

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        LOGGER.debug(String.format("queue name: %-20s,  exchange: %-20s,  routing key: %-20s， 收到消息:%s", message.getMessageProperties().getConsumerQueue(),
                message.getMessageProperties().getReceivedExchange(), message.getMessageProperties().getReceivedRoutingKey(), new String(message.getBody())));
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
