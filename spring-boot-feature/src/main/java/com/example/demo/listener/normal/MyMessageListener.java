package com.example.demo.listener.normal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;


/**
 * 最基础的消息监听，没有其他功能，只能根据消息内容处理业务
 *
 * @author zhaoxingwu
 */
public class MyMessageListener implements MessageListener {

    private final static Logger LOGGER = LoggerFactory.getLogger(MyMessageListener.class);

    @Override
    public void onMessage(Message message) {
        LOGGER.debug(String.format("queue name: %-20s,  exchange: %-20s,  routing key: %-20s， 收到消息:%s", message.getMessageProperties().getConsumerQueue(),
                message.getMessageProperties().getReceivedExchange(), message.getMessageProperties().getReceivedRoutingKey(), new String(message.getBody())));
    }
}
