package com.example.demo.schedules;

import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.ScriptOutputType;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.support.ConnectionPoolSupport;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhaoxingwu
 */
@EnableScheduling
@Component
public class RedisKeyTTLSchedule {

    private final RedisClient client =
            RedisClient.create(RedisURI.builder().withHost("localhost").withPassword("123456".toCharArray()).withDatabase(1).build());

    private final GenericObjectPool<StatefulRedisConnection<String, String>> pool = ConnectionPoolSupport
            .createGenericObjectPool(client::connect, new GenericObjectPoolConfig<>());

//    @Scheduled(cron = "0/10 * * * * ?")
    public void runTask() throws Exception {

        // 对于 lua 的使用，请 https://github.com/wg/lettuce/blob/master/src/test/java/com/lambdaworks/redis/ScriptingCommandTest.java
        StatefulRedisConnection<String, String> connection = pool.borrowObject();
        RedisCommands<String, String> command = connection.sync();
        List<String> c = client.connect().sync().eval("return {KEYS[1],KEYS[2],ARGV[1],ARGV[2]}",
                ScriptOutputType.MULTI,new String[]{"f","s"},"2","3");

        System.out.println("borrow connection");
        System.out.println("db size : " + command.dbsize());
        KeyScanCursor<String> scanCursor = new KeyScanCursor<>();
        scanCursor.setCursor("0");
        scanCursor.setFinished(false);
        String cursor ;

        do {

            scanCursor = command.scan(scanCursor);
            List<String> keys = scanCursor.getKeys();
            cursor = scanCursor.getCursor();
            System.out.println("keys   :" + keys.size() + "  cursor: " + cursor);
        } while (!"0".equals(cursor));

        connection.close();
    }
}
