package com.example.demo.util;

import java.util.UUID;

/**
 * @author zhaoxingwu
 */
public class Uuid {

    public static String uuid(){
        return UUID.randomUUID().toString();
    }
}
