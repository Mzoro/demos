if (!window.TemperatureSvg) {
    window.TemperatureSvg = function (svg, domId) {

        let svg$ = $(svg);

        // 鼠标进入点，移出点事件
        let pointMouseoverEventHandler = []
        let pointMouseleaveEventHandler = []

        // 鼠标点击点事件
        let pointClickEventHandler = []

        // 文字点击事件
        let dateClickHandler = []

        let holder = $("#" + domId);
        holder.html("")
        holder.css("display", "inline-block")
        holder.css("position", "relative")
        holder.append(svg$)

        function initDateClickEvent() {
            let pointEle = svg$.find("[form-type=date]");
            pointEle.on("click", function (event) {
                let target = $(event.currentTarget);
                let date = target.attr("date-value");
                if (dateClickHandler && dateClickHandler.length) {
                    for (let i = 0; i < dateClickHandler.length; i++) {
                        dateClickHandler[i](date)
                    }
                }

            })
        }

        function initPointEvent() {
            let pointEle = svg$.find("[form-type=point]");
            pointEle.on("click", function (event) {
                let target = $(event.currentTarget);
                let children = event.currentTarget.children
                let dataSet = collectData(children)
                let x = Number(target.attr("cx"))
                let y = Number(target.attr("cy"))
                if (pointClickEventHandler && pointClickEventHandler.length) {
                    for (let i = 0; i < pointClickEventHandler.length; i++) {
                        pointClickEventHandler[i](dataSet, {x, y}, event)
                    }
                }
            });
            pointEle.on("mouseover", function (event) {
                let target = $(event.currentTarget)
                let children = event.currentTarget.children
                let dataSet = collectData(children)
                let x = Number(target.attr("x"))
                let y = Number(target.attr("y"))
                if (pointMouseoverEventHandler && pointMouseoverEventHandler.length) {
                    for (let i = 0; i < pointMouseoverEventHandler.length; i++) {
                        pointMouseoverEventHandler[i](dataSet, {x, y}, event);
                    }
                }
            });
            pointEle.on("mouseleave", function (event) {
                let target = $(event.currentTarget)
                let children = event.currentTarget.children
                let dataSet = collectData(children)
                let x = Number(target.attr("x"))
                let y = Number(target.attr("y"))
                if (pointMouseleaveEventHandler && pointMouseleaveEventHandler.length) {
                    for (let i = 0; i < pointMouseleaveEventHandler.length; i++) {
                        pointMouseleaveEventHandler[i](dataSet, {x, y}, event);
                    }
                }
            });
        }

        initPointEvent()
        initDateClickEvent()

        /**
         * 收集节点上的 data 属性的数据
         * @param domSet
         * @returns {*[]}
         */
        function collectData(domSet) {
            let dataSet = []
            if (domSet && domSet.length) {
                for (let i = 0; i < domSet.length; i++) {
                    let data = (domSet[i].attributes && domSet[i].attributes.data) ? domSet[i].attributes.data.nodeValue : "{}"
                    dataSet.push(JSON.parse(data ? data : "{}"));
                }
            }
            return dataSet
        }

        /**
         * 添加鼠标进入事件
         * @param fn
         */
        this.addPointMouseoverEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            for (let i = 0; i < pointMouseoverEventHandler.length; i++) {
                if (pointMouseoverEventHandler[i] === fn) {
                    return
                }
            }
            pointMouseoverEventHandler.push(fn);
        }


        this.removePointMouseoverEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            let index = pointMouseoverEventHandler.findIndex(value => value === fn);
            if (index !== -1) {
                pointMouseoverEventHandler.splice(index, 1)
            }
        };

        /**
         * 添加鼠标移出事件
         * @param fn
         */
        this.addPointMouseleaveEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            for (let i = 0; i < pointMouseleaveEventHandler.length; i++) {
                if (pointMouseleaveEventHandler[i] === fn) {
                    return
                }
            }
            pointMouseleaveEventHandler.push(fn);
        }

        this.removePointMouseleaveEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            let index = pointMouseleaveEventHandler.findIndex(value => value === fn);
            if (index !== -1) {
                pointMouseleaveEventHandler.splice(index, 1)
            }
        };

        /**
         * 添加鼠标点击事件
         * @param fn
         */
        this.addPointClickEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            for (let i = 0; i < pointClickEventHandler.length; i++) {
                if (pointClickEventHandler[i] === fn) {
                    debugger
                    return
                }
            }
            pointClickEventHandler.push(fn);
        }

        this.removePointClickEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            let index = pointClickEventHandler.findIndex(value => value === fn);
            if (index !== -1) {
                pointClickEventHandler.splice(index, 1)
            }
        };

        /**
         * 添加鼠标点击日期事件
         * @param fn
         */
        this.addDateClickEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            for (let i = 0; i < dateClickHandler.length; i++) {
                if (dateClickHandler[i] === fn) {
                    return
                }
            }
            dateClickHandler.push(fn);
        }

        this.removeDateClickEventHandler = function (fn) {
            if (typeof fn !== 'function') {
                return;
            }
            let index = dateClickHandler.findIndex(value => value === fn);
            if (index !== -1) {
                dateClickHandler.splice(index, 1)
            }
        };

        this.collectAllData = function () {
            let pointEle = svg$.find("[form-type=point]");
            let result = []
            if (pointEle && pointEle.length) {
                for (let i = 0; i < pointEle.length; i++) {
                    let ele = $(pointEle[i]);
                    let data = {}
                    data.x = ele.attr('x')
                    data.y = ele.attr('y')
                    let children = ele[0].children
                    data.dataset = collectData(children);
                    result.push(data)
                }
            }

            return result;
        };

    }
} else {
    console.log("window.TemperatureSvg has bean loaded!!")
}
