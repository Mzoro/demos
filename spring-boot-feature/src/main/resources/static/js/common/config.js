
$.ajaxSetup({
    dataType: "json",
    headers: {"Content-Type": "application/json;charset=UTF-8"},
    processData: false,
    type: "post",
    timeout:10000,
    error: function (xhr) {
        console.log(xhr);
    },
    beforeSend: function (xhr) {
        beforeAjaxSent(xhr, this);
    }
});

function logError() {
    const args = arguments;
    for (let i = 0; i < args.length; i++) {
        console.error(args[i])
    }
}

/**
 * 发送前处理参数
 * @param xhr ajax对象
 * @param sendObject $.ajax发送时所以用到的参数
 */
function beforeAjaxSent(xhr, sendObject) {
    if (sendObject.data) {
        if (typeof sendObject.data === "object") {
            //参数格式正确进行转换
            sendObject.data = JSON.stringify(sendObject.data);
        } else {
            //参数格式不正确
            let url = sendObject.url;
            if (!url) {
                throw new Error("接口地址错误!");
            }
        }
    } else if (typeof sendObject.data !== "undefined") {
        // 这个分支说明 data 可能是 0,空字符串
        console.warn("上传数据可能不正确:", sendObject.data);
    }
}
