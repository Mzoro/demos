if (window.DynamicForm) {
    console.warn("DynamicForm is exists!!");
} else {

    /**
     *  动态表单类
     * @param id 容器id
     * @param title  表单标题
     * @constructor
     */
    window.DynamicForm = function (id, title) {

        var pluginId = 1;

        window.DynamicForm.genId = function () {
            return pluginId++;
        }

        // 容器
        this._title = title;
        this._containerEle = $("#" + id)[0];
        this._containerEle.appendChild(this.makeTitle())
        this._formEle = $("<div class='form-horizontal'></div>")[0];
        this._containerEle.appendChild(this._formEle);
        // 窗口中的控件
        this.pluginArr = [];
    }

    /**
     * 组件的数据验证类
     * @param formPlugin 组件对象
     * @param param 构建验证器的参数，一json对象
     * @constructor
     */
    window.DynamicForm.Check = function (formPlugin, param) {
        this.formFlugin = formPlugin;
        this.checkType = param.type
    };

    /**
     * 添加组件
     * @param type 组件类型
     */
    window.DynamicForm.prototype.addPlugin = function (type) {
        var plugin = null
        if (type === DynamicForm.Type.INPUT) {
            plugin = new DynamicForm.FormInputPlugin(this, type);
        } else if (type === DynamicForm.Type.RADIO) {
            plugin = new DynamicForm.FormRadioPlugin(this, type)
        } else if (type === DynamicForm.Type.CHECKBOX) {
            plugin = new DynamicForm.FormCheckBoxPlugin(this, type)
        } else if (type === DynamicForm.Type.TEXTAREA) {
            plugin = new DynamicForm.FormTextareaPlugin(this, type);
        } else if (type === DynamicForm.Type.SELECT) {
            plugin = new DynamicForm.FormSelectPlugin(this, type, false);
        }

        if (plugin != null) {
            plugin.init();
            this.pluginArr.push(plugin);
        }
        return plugin;

    };

    /**
     * 表彰标题
     * @returns {*}
     */
    window.DynamicForm.prototype.makeTitle = function () {
        var outer = $("<div class=\"page-header\"></div>");
        var inner = $("<h1></h1>");
        inner.html(this._title)
        outer[0].appendChild(inner[0]);
        return outer[0];
    }


    // 表彰控件的类型
    window.DynamicForm.Type = {
        INPUT: 'INPUT', SELECT: 'SELECT', RADIO: 'RADIO', CHECKBOX: 'CHECKBOX', TEXTAREA: 'TEXTAREA'
    }

    window.DynamicForm.eventHandlerFactory = function (content, eventMap) {
        return function (event) {
            var tp = event.type;
            var arr = eventMap[tp];
            if (!arr && !arr.length) {
                return;
            }
            for (var j = 0; j < arr.length; j++) {
                var h = arr[j];
                if (typeof h === 'function') {
                    h(event, content);
                }
            }
        }

    };

    window.DynamicForm.showMessageDialog = function (message) {
        var outer = $("<div class='dnf modal fade' tabindex='-1' role='dialog'></div>")[0];
        var modalDocument = $("<div class='modal-dialog' role='document'></div>")[0];
        outer.appendChild(modalDocument);

        var modalContent = $('<div class="modal-content"></div>')[0];
        modalDocument.appendChild(modalContent);

        var modalHeader = $("<div class='modal-header'></div>")[0];
        modalContent.appendChild(modalHeader);

        // 标题
        var modalHeaderText = $("<h4 class='modal-title'></h4>")[0];
        modalHeaderText.innerHTML = this.title;
        modalHeader.appendChild(modalHeaderText);

        // 候选主体
        var modalBody = $("<div class='modal-body'></div>")[0];
        modalContent.appendChild(modalBody);
        modalBody.innerHTML = message;

        var modalFooter = $("<div class='modal-footer'></div>")[0];
        modalContent.appendChild(modalFooter);

        var modalCloseBtn = $("<button type='button' class='btn btn-default'>取消</button>")[0];
        modalFooter.appendChild(modalCloseBtn);

        $(outer).on('hidden.bs.modal', function () {
            outer.remove();
        })

        $(modalCloseBtn).on('click', function () {
            $(outer).modal('hide');
        })
        $('body')[0].appendChild(outer);
        $(outer).modal('show');
    };

    /**
     * 收集配置数据
     * @returns {[]|*[]}
     */
    window.DynamicForm.prototype.configData = function () {

        if (this.pluginArr.length > 0) {
            var configArr = [];
            var configMap = {};
            for (var i = 0; i < this.pluginArr.length; i++) {
                var c = this.pluginArr[i].configData();
                var old = configMap[c.propertyName];
                if (old) {
                    DynamicForm.showMessageDialog("属性名称重复:" + c.title + "," + old.title);
                    return [];
                }
                configArr.push(c);
                configMap[c.propertyName] = c;
            }
            return configArr;
        } else {
            return [];
        }
    };

    /**
     * 通过数据生成表单
     * @param formEleConfigData 表单组件配置数据
     */
    window.DynamicForm.prototype.configFormEle = function (formEleConfigData) {
        if (!formEleConfigData || !formEleConfigData.length) {
            return;
        }

        for (var i = 0; i < formEleConfigData.length; i++) {
            var type = formEleConfigData[i].type;
            var p = this.addPlugin(type);
            p.initConfig(formEleConfigData[i]);
            p.refresh();
        }
    };

    /**
     * 收集表单中的数据
     */
    window.DynamicForm.prototype.collectData = function () {
        if (this.pluginArr.length > 0) {
            var result = {};
            for (var i = 0; i < this.pluginArr.length; i++) {
                var property = this.pluginArr[i].propertyName;
                if (property && property.length > 0) {
                    result[this.pluginArr[i].propertyName] = this.pluginArr[i].collectionValue();
                }
            }
            return result;
        } else {
            return {};
        }
    }

}