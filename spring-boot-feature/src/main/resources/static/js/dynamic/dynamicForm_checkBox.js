window.DynamicForm.FormCheckBoxPlugin = function (dynamicForm, type) {
    this.values = [{name: "选项1", value: ''}];
    DynamicForm.FormInputPlugin.call(this, dynamicForm, type);
    // 候选值
}
window.DynamicForm.FormCheckBoxPlugin.prototype = new DynamicForm.FormRadioPlugin();

window.DynamicForm.FormCheckBoxPlugin.prototype.collectionValue = function () {
    var outer = this.element;
    var selected = $(outer).find('input[type=checkbox]:checked');
    if (selected && selected.length > 0) {

        var resultStr = '';

        for (var i = 0; i < selected.length; i++) {
            var n = selected[i].getAttribute('item-name');
            resultStr += n;
            if (i !== selected - 1) {
                resultStr += ",";
            }
        }

        return resultStr;
    } else {
        return ""
    }
}