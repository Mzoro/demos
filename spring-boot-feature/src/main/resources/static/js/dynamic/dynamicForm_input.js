/**
 * 表单的组件类
 * @param dynamicForm 组件所属的表单类
 * @param type 组件类型
 * @constructor
 */
window.DynamicForm.FormInputPlugin = function (dynamicForm, type) {

    var _type = type;

    this.dynamicForm = dynamicForm;
    this.propertyName = "";
    this.title = '标题';
    this.defaultValue = "";

    this.getType = function () {
        return _type;

    };

    // 数据验证
    var checkList = [];
    this.getCheckList = function () {
        return checkList;
    }

    // 事件集合与绑定
    var eventMap = {
        click: [], change: [], focus: []
    }
    this.getEventMap = function () {
        return eventMap;
    }
};

/**
 * 初始化
 */
window.DynamicForm.FormInputPlugin.prototype.init = function () {
    var _init = false;
    var id = DynamicForm.genId();
    this.element = this.makeFormEle(this.getType());
    this.label = this.makeFormEleLabel(this.title);
    this.pluginDom = this.makePluginDom(this.element, this.label);
    this.dynamicForm._formEle.appendChild(this.pluginDom);
    this.bindEventHandler();
    this.getId = function () {
        return id;
    }

    _init = true;
    this.isInit = function () {
        return _init;
    };


};

/**
 * 事件绑定
 */
window.DynamicForm.FormInputPlugin.prototype.bindEventHandler = function () {
    var eventMap = this.getEventMap();
    for (var t in eventMap) {
        var that = this;
        this.element.addEventListener(t, DynamicForm.eventHandlerFactory(that, eventMap))
    }
};

/**
 * 添加校验
 * 1. 先查看是否已经有了
 * 2. 再添加进去
 */
window.DynamicForm.FormInputPlugin.prototype.addCheck = function (check) {

    var checkList = this.getCheckList();

    for (var i = 0; i < checkList.length; i++) {
        if (checkList[i].type === check.checkType) {
            //  todo 这里要弹出提示了！
            return;
        }
    }
    checkList.push(check)
}

/**
 * 添加绑定的事件
 * @param type
 * @param handler
 */
window.DynamicForm.FormInputPlugin.prototype.addEventHandler = function (type, handler) {
    var eventMap = this.getEventMap();
    var eventArr = eventMap[type];
    if (!eventArr) {
        throw new Error("不支持的事件类型");
    }
    eventArr.push(handler);
};

/**
 * 刷新页面数据
 */
window.DynamicForm.FormInputPlugin.prototype.refresh = function () {
    this.label.innerHTML = this.title;
    this.element.value = this.defaultValue;
};

/**
 * 将组件删除
 */
window.DynamicForm.FormInputPlugin.prototype.remove = function () {
    var arr = this.dynamicForm.pluginArr
    var myId = this.getId();

    for (var i = 0; i < arr.length; i++) {
        if (myId === arr[i].getId()) {
            arr.splice(i, 1);
            break;
        }
    }

    // 在dom中删除他
    this.pluginDom.remove();
};

window.DynamicForm.FormInputPlugin.prototype.makeFormEle = function () {
    return $("<input type='text' class=\"form-control\">")[0];
};

/**
 * 构建表单控件的标题
 * @param label
 * @returns {*}
 */
window.DynamicForm.FormInputPlugin.prototype.makeFormEleLabel = function (label) {
    var lb = $("<label class='col-lg-2 control-label'></label>")
    lb.html(label);
    return lb[0]
};

/**
 * 组件的整体dom
 * @param formEle
 * @param labelEle
 * @returns {*}
 */
window.DynamicForm.FormInputPlugin.prototype.makePluginDom = function (formEle, labelEle) {
    var outer = $("<div class='form-group'></div>")[0];
    var formEleDiv = $("<div class='col-lg-10'></div>")[0]
    outer.appendChild(labelEle);
    formEleDiv.appendChild(formEle);
    outer.appendChild(formEleDiv);
    return outer;
};

/**
 * 生成配置数据
 * @returns {{}}
 */
window.DynamicForm.FormInputPlugin.prototype.configData = function () {
    var result = {};
    result.type = this.getType();
    result.title = this.title;
    result.defaultValue = this.defaultValue;
    result.propertyName = this.propertyName;
    return result;
};

window.DynamicForm.FormInputPlugin.prototype.initConfig = function (data) {

    if (typeof data !== 'object') {
        return
    }

    this.propertyName = data.propertyName;
    this.title = data.title;
    this.defaultValue = data.defaultValue;
}

/**
 * 收集输入值
 * @returns {*}
 */
window.DynamicForm.FormInputPlugin.prototype.collectionValue = function () {
    return this.element.value;
}