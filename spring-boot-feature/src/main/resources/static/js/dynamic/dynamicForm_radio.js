window.DynamicForm.FormRadioPlugin = function (dynamicForm, type) {

    this.values = [{name: "选项1", value: ''}];
    DynamicForm.FormInputPlugin.call(this, dynamicForm, type);
    // 候选值
}
window.DynamicForm.FormRadioPlugin.prototype = new DynamicForm.FormInputPlugin();

window.DynamicForm.FormRadioPlugin.prototype.makeFormEle = function () {

    var type = this.getType() === DynamicForm.Type.RADIO ? 'radio' : 'checkbox';

    var values = this.values;

    var outer = $("<div class='" + type + "'></div>")[0]

    if (values && values.length) {
        for (var i = 0; i < values.length; i++) {
            var str = "<input item-name='" + values[i].name + "' type='" + type + "' name='" + this.propertyName + " ' value=' " + values[i].value + "' />";
            var inputEle = $(str);
            var span = $("<span>" + values[i].name + "</span>")[0]
            var ele = $("<label class='radio-input-label'></label>")[0];
            ele.appendChild(inputEle[0]);
            ele.appendChild(span);
            outer.appendChild(ele);
        }
    } else {
        var inputEle = $("<input item-name='" + values[i].name + "' type='" + type + "' name='" + this.propertyName + "' />");
        var span = $("<span>" + values[i].name + "</span>")[0]
        var ele = $("<label class='radio-input-label'> </label>")[0];
        ele.appendChild(inputEle[0]);
        ele.appendChild(span);
        outer.appendChild(ele);
    }
    return outer;
}


window.DynamicForm.FormRadioPlugin.prototype.makePluginDom = function (formEle, labelEle) {

    var outer = $("<div class='form-group'></div>")[0];
    outer.appendChild(labelEle);
    var formEleDiv = $("<div class=' col-sm-10'></div>")[0];
    formEleDiv.appendChild(formEle);
    outer.appendChild(formEleDiv);
    return outer;
};

window.DynamicForm.FormRadioPlugin.prototype.bindEventHandler = function () {
    var eventMap = this.getEventMap();
    for (var t in eventMap) {
        var that = this;

        // 需要向每一个元素添加事件
        var labels = $(that.element).find('label');
        for (var i = 0; i < labels.length; i++) {
            labels[i].addEventListener(t, DynamicForm.eventHandlerFactory(that, eventMap));
        }
    }
}

window.DynamicForm.FormRadioPlugin.prototype.refresh = function () {
    this.label.innerHTML = this.title;
    this.pluginDom.innerHTML = '';
    this.element = this.makeFormEle(this.getType());
    var dom = this.makePluginDom(this.element, this.label);
    for (var i = 0; i < dom.children.length;) {
        this.pluginDom.appendChild(dom.children[0]);
    }
    this.bindEventHandler();
};

window.DynamicForm.FormRadioPlugin.prototype.configData = function () {
    var result = {};
    result.type = this.getType();
    result.title = this.title;
    result.propertyName = this.propertyName;
    result.values = this.values;
    return result;
};

window.DynamicForm.FormRadioPlugin.prototype.initConfig = function (data) {

    if (typeof data !== 'object') {
        return
    }
    this.propertyName = data.propertyName;
    this.title = data.title;
    this.values = data.values;
};

window.DynamicForm.FormRadioPlugin.prototype.collectionValue = function () {
    var outer = this.element;
    var selected = $(outer).find('input[type=radio]:checked');
    if (selected && selected.length > 0) {
        return selected[0].getAttribute('item-name');
    } else {
        return ""
    }
}


