window.DynamicForm.FormSelectPlugin = function (dynamicForm, type, isMul) {
    this.values = [{name: "选项1", value: ''}];
    this.isMul = isMul;
    this.selectedValues = [];
    DynamicForm.FormInputPlugin.call(this, dynamicForm, type);
}
window.DynamicForm.FormSelectPlugin.prototype = new DynamicForm.FormInputPlugin();

window.DynamicForm.FormSelectPlugin.prototype.init = function () {
    var _init = false;
    var id = DynamicForm.genId();
    this.element = this.makeFormEle(this.getType());
    this.label = this.makeFormEleLabel(this.title);
    this.valuesEle = $("<div class='col-sm-10'></div>")[0]
    this.pluginDom = this.makePluginDom();
    this.dynamicForm._formEle.appendChild(this.pluginDom);
    this.dialog = this.makeDialogEle();
    this.bindEventHandler();
    this.getId = function () {
        return id;
    }

    _init = true;
    this.isInit = function () {
        return _init;
    };
};

window.DynamicForm.FormSelectPlugin.prototype.makeFormEle = function () {
    return $("<button class='btn btn-info'>点击选择</button>")[0];
}

window.DynamicForm.FormSelectPlugin.prototype.makePluginDom = function () {

    var outer = $("<div class='form-group'></div>")[0];

    var formEleDiv = $("<div class=' col-sm-10'></div>")[0];
    // 选择按钮部分
    var left = $("<div class='col-sm-2'></div>")[0];
    left.appendChild(this.element);
    formEleDiv.appendChild(left);
    // 选项部分
    formEleDiv.appendChild(this.valuesEle);

    outer.appendChild(this.label);
    outer.appendChild(formEleDiv);
    return outer;
};

window.DynamicForm.FormSelectPlugin.prototype.makeDialogEle = function () {

    var outer = $("<div class='dnf modal fade' tabindex='-1' role='dialog'></div>")[0];
    var modalDocument = $("<div class='modal-dialog' role='document'></div>")[0];
    outer.appendChild(modalDocument);

    var modalContent = $('<div class="modal-content"></div>')[0];
    modalDocument.appendChild(modalContent);

    var modalHeader = $("<div class='modal-header'></div>")[0];
    modalContent.appendChild(modalHeader);

    // 标题
    var modalHeaderText = $("<h4 class='modal-title'></h4>")[0];
    modalHeaderText.innerHTML = this.title;
    modalHeader.appendChild(modalHeaderText);

    // 候选主体
    var modalBody = $("<div class='modal-body'></div>")[0];
    modalContent.appendChild(modalBody);

    var modalFooter = $("<div class='modal-footer'></div>")[0];
    modalContent.appendChild(modalFooter);

    var modalCloseBtn = $("<button type='button' class='btn btn-default'>取消</button>")[0];
    var modalConfirmBtn = $("<button type='button' class='btn btn-primary'>保存</button>")[0];
    modalFooter.appendChild(modalCloseBtn);
    modalFooter.appendChild(modalConfirmBtn);

    $(outer).on('hidden.bs.modal', function () {
        outer.remove();
    })

    var that = this;

    function makeButton(value, name, propertyName) {
        var inputType = !!that.isMul ? 'checkbox' : 'radio';
        var btn = $("<label class='btn btn-info'> " + name + " </label>")[0];
        var shadowEle = $("<input item-name='" + name + "' name='" + propertyName + "' type='" + inputType + "' value='" + value + "' >")[0];

        if (that.selectedValues.length > 0) {

            for (var i = 0; i < that.selectedValues.length; i++) {
                if (that.selectedValues[i].value == value) {
                    shadowEle.setAttribute('checked', '')
                }
            }
        }

        btn.appendChild(shadowEle);

        shadowEle.addEventListener('change', function (event) {

            if (inputType === 'radio') {
                var selectedBtn = $(modalBody).find("label.btn.btn-success");
                selectedBtn.removeClass('btn-success');
                selectedBtn.addClass('btn-info');
                btn.classList.add('btn-success');
                btn.classList.remove('btn-info');
            } else {

                if (event.target.checked) {
                    btn.classList.add('btn-success');
                    btn.classList.remove('btn-info');
                } else {
                    btn.classList.add('btn-info');
                    btn.classList.remove('btn-success');
                }
            }
        })
        return btn;
    }


    var dialog = {
        outer: outer,
        title: modalHeaderText,
        body: modalBody,
        cancelBtn: modalCloseBtn,
        confirmBtn: modalConfirmBtn,
        show: function () {
            var values = that.values;
            $("body")[0].appendChild(outer);
            this.body.innerHTML = "";
            for (var i = 0; i < values.length; i++) {
                var btn = makeButton(values[i].value, values[i].name, that.propertyName);
                this.body.appendChild(btn);
            }
            $(outer).modal({
                backdrop: 'static',
                show: true
            })
        },
        hide: function () {
            $(outer).modal('hide');
        }
    };

    $(modalCloseBtn).on('click', function () {
        dialog.hide()
    })
    $(modalConfirmBtn).on('click', function () {
        var inputs = $(modalBody).find('label input:checked');
        var selectedValue = [];
        if (inputs.length > 0) {
            for (var i = 0; i < inputs.length; i++) {
                var input = inputs[i];
                var nm = input.getAttribute('item-name');
                var vl = input.value;
                selectedValue.push({name: nm, value: vl});
            }

        }
        that.selectedValues = selectedValue;
        that.refresh();
        dialog.hide();
    })

    return dialog;
};

/**
 * 显示弹出窗
 */
window.DynamicForm.FormSelectPlugin.prototype.showDialog = function () {
    this.dialog.show();
};

window.DynamicForm.FormSelectPlugin.prototype.refresh = function () {
    this.label.innerHTML = this.title;
    this.valuesEle.innerHTML = "";
    if (this.selectedValues.length > 0) {
        for (var i = 0; i < this.selectedValues.length; i++) {
            var sp = $("<span>" + this.selectedValues[i].name + "</span>");
            this.valuesEle.appendChild(sp[0]);
            if (i !== this.selectedValues.length - 1) {
                var d = $("<span>, </span>")[0];
                this.valuesEle.appendChild(d);
            }
        }
    } else {
        this.valuesEle.innerHTML = '';
    }
};

window.DynamicForm.FormSelectPlugin.prototype.configData = function () {
    var result = {};
    result.type = this.getType();
    result.title = this.title;
    result.propertyName = this.propertyName;
    result.values = this.values;
    result.isMul = this.isMul;
    return result;

};

window.DynamicForm.FormSelectPlugin.prototype.initConfig = function (data) {

    if (typeof data !== 'object') {
        return
    }
    this.propertyName = data.propertyName;
    this.title = data.title;
    this.values = data.values;
    this.isMul = data.isMul;
    var _this = this;
    $(this.element).click(function () {
        _this.showDialog();
    })
};

window.DynamicForm.FormSelectPlugin.prototype.collectionValue = function () {
    if (this.selectedValues.length > 0) {

        var resultStr = '';
        for (var i = 0; i < this.selectedValues.length; i++) {
            resultStr += this.selectedValues[i].name;
            if (i !== this.selectedValues.length - 1) {
                resultStr += ",";
            }
        }
        return resultStr;
    }
    return "";

};

