window.DynamicForm.FormTextareaPlugin = function (dynamicForm, type) {
    DynamicForm.FormInputPlugin.call(this, dynamicForm, type);
}
window.DynamicForm.FormTextareaPlugin.prototype = new DynamicForm.FormInputPlugin();

window.DynamicForm.FormTextareaPlugin.prototype.makeFormEle = function () {
    return $("<textarea class='form-control' rows='3'></textarea>")[0];
}

