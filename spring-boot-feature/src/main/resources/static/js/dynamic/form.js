$(function () {

    var df = new DynamicForm("container", "测试动态表单");

    $("#input-add").click(function () {
        var p = df.addPlugin(DynamicForm.Type.INPUT)
        p.addEventHandler('click', showInputConfigFn(p));
    });
    $("#radio-add").click(function () {
        var p = df.addPlugin(DynamicForm.Type.RADIO)
        p.addEventHandler('click', showRadioConfigFn(p));
    });
    $("#checkbox-add").click(function () {
        var p = df.addPlugin(DynamicForm.Type.CHECKBOX)
        p.addEventHandler('click', showRadioConfigFn(p));
    });
    $("#dialog-select-add").click(function () {
        var p = df.addPlugin(DynamicForm.Type.SELECT);
        p.addEventHandler('click', showSelectConfigFn(p));
    });
    $("#textarea-add").click(function () {
        var p = df.addPlugin(DynamicForm.Type.TEXTAREA);
        p.addEventHandler('click', showInputConfigFn(p));
    });

    $("#test-btn").click(function () {
        console.log(df.configData());
    });
    $("#test-btn-2").click(function () {
        console.log(df.collectData())
    });
    $("#test-btn-3").click(function () {
        df.configFormEle(DATA)
    });

    function showInputConfigFn(plugin) {

        return function () {
            var controllerPanel = $("div[config-type=input]");
            var this_id = plugin.getId();
            if (controllerPanel.attr('shown') === 'true' && this_id == controllerPanel.attr("plugin-id")) {
                return
            }

            // 获取各个控件对象
            var name = controllerPanel.find("input[ele-property=name]");
            var propertyName = controllerPanel.find("input[ele-property=propertyName]");
            var defaultValue = controllerPanel.find("input[ele-property=defaultValue]");
            propertyName.css('background-color', '');
            // 给控制面板赋值
            name.val(plugin.title);
            propertyName.val(plugin.propertyName);
            defaultValue.val(plugin.defaultValue);

            // 保存
            var saveBtn = controllerPanel.find("button[config-action=save]");
            saveBtn.off('click');
            saveBtn.on('click', function () {

                if (!propertyName.val() || propertyName.val().length === 0) {
                    propertyName.css('background-color', '#e4a2a2')
                    return;
                }
                propertyName.css('background-color', '');

                // 将控制面板的值保存
                plugin.title = name.val();
                plugin.propertyName = propertyName.val();
                plugin.defaultValue = defaultValue.val();
                plugin.refresh();
                controllerPanel.hide();
                controllerPanel.attr("shown", false)
            })

            // 删除
            var deleteBtn = controllerPanel.find("button[config-action=delete]");
            deleteBtn.off('click');
            deleteBtn.on("click", function () {
                plugin.remove();
                controllerPanel.hide();
                controllerPanel.attr("shown", "")
            });

            var controllers = $("div[form-ele-controller]");
            controllers.hide();
            controllers.attr('shown', false);
            controllerPanel.show();
            controllerPanel.attr("shown", "true");
            controllerPanel.attr("plugin-id", plugin.getId());
        }
    }

    function showRadioConfigFn(plugin) {

        return function () {

            var controllerPanel = $("div[config-type=radio]");
            var this_id = plugin.getId();
            if (controllerPanel.attr('shown') === true && this_id == controllerPanel.attr("plugin-id")) {
                return
            }


            // 获取各个控件对象
            var name = controllerPanel.find("input[ele-property=name]");
            var propertyName = controllerPanel.find("input[ele-property=propertyName]");
            var tbody = controllerPanel.find("table[config-item=item-table] tbody");
            // 给控制面板赋值
            name.val(plugin.title);
            propertyName.val(plugin.propertyName);
            propertyName.css('background-color', '');
            //生成候选项列表
            var values = plugin.values;
            tbody.html("");
            for (var i = 0; i < values.length; i++) {
                var tr = makeTr(values[i].value, values[i].name);
                tbody.append(tr)
            }

            // 添加候选项按键
            var addItemBtn = controllerPanel.find("button[config-action=add-item]");
            addItemBtn.off('click');
            addItemBtn.on('click', function () {
                tbody.append(makeTr('', ''))
            })

            var controllers = $("div[form-ele-controller]");
            controllers.hide();
            controllers.attr('shown', false);
            controllerPanel.show();
            controllerPanel.attr("shown", "true");
            controllerPanel.attr("plugin-id", plugin.getId());

            // 保存
            var saveBtn = controllerPanel.find("button[config-action=save]");
            saveBtn.off('click');
            saveBtn.on('click', function () {

                // 收集候选项

                if (!propertyName.val() || propertyName.val().length === 0) {
                    propertyName.css('background-color', '#e4a2a2')
                    return;
                }
                propertyName.css('background-color', '');

                var trArr = tbody.find("tr");
                trArr.css('background-color', '');
                var valueArr = [];
                if (trArr && trArr.length) {
                    for (var i = 0; i < trArr.length; i++) {
                        var _tr = $(trArr[i]);
                        var item = {};
                        var nameInput = _tr.find('input[item-name]')[0];
                        item.name = nameInput.value;
                        var valueInput = _tr.find('input[item-value]')[0];
                        item.value = valueInput.value;

                        for (var j = 0; j < valueArr.length; j++) {
                            if (valueArr[j].value === item.value) {
                                // 这里还要给提示
                                $(trArr[j]).css("background-color", '#e4a2a2');
                                $(trArr[i]).css("background-color", '#e4a2a2');
                                return
                            }

                            if (valueArr[j].name === item.name) {
                                $(trArr[j]).css("background-color", '#e4a2a2');
                                $(trArr[i]).css("background-color", '#e4a2a2');
                                // 这里还要给提示
                                return
                            }
                        }
                        valueArr.push(item);
                    }
                }
                plugin.values = valueArr

                // 将控制面板的值保存
                plugin.title = name.val();
                plugin.propertyName = propertyName.val();
                plugin.refresh();
                controllerPanel.hide();
                controllerPanel.attr("shown", "")
            })

            // 删除
            var deleteBtn = controllerPanel.find("button[config-action=delete]");
            deleteBtn.off('click');
            deleteBtn.on("click", function () {
                plugin.remove();
                controllerPanel.hide();
                controllerPanel.attr("shown", "")
            });

        }

    }

    function showSelectConfigFn(plugin) {

        return function () {

            var controllerPanel = $("div[config-type=select]");
            var this_id = plugin.getId();
            if (controllerPanel.attr('shown') === true && this_id == controllerPanel.attr("plugin-id")) {
                return
            }

            // 获取各个控件对象
            var name = controllerPanel.find("input[ele-property=name]");
            var propertyName = controllerPanel.find("input[ele-property=propertyName]");
            var isMul = controllerPanel.find("input[ele-property=isMul]")[0];
            var tbody = controllerPanel.find("table[config-item=item-table] tbody");
            // 给控制面板赋值
            name.val(plugin.title);
            propertyName.val(plugin.propertyName);
            if (plugin.isMul) {
                isMul.setAttribute('checked', '');
                isMul.checked = true;
            } else {
                isMul.removeAttribute('checked');
                isMul.checked = false;
            }
            propertyName.css('background-color', '');
            //生成候选项列表
            var values = plugin.values;
            tbody.html("");
            for (var i = 0; i < values.length; i++) {
                var tr = makeTr(values[i].value, values[i].name);
                tbody.append(tr)
            }

            // 添加候选项按键
            var addItemBtn = controllerPanel.find("button[config-action=add-item]");
            addItemBtn.off('click');
            addItemBtn.on('click', function () {
                tbody.append(makeTr('', ''))
            })

            var controllers = $("div[form-ele-controller]");
            controllers.hide();
            controllers.attr('shown', false);
            controllerPanel.show();
            controllerPanel.attr("shown", "true");
            controllerPanel.attr("plugin-id", plugin.getId());

            // 保存
            var saveBtn = controllerPanel.find("button[config-action=save]");
            saveBtn.off('click');
            saveBtn.on('click', function () {

                if (!propertyName.val() || propertyName.val().length === 0) {
                    propertyName.css('background-color', '#e4a2a2')
                    return;
                }
                propertyName.css('background-color', '');

                // 收集候选项
                var trArr = tbody.find("tr");
                trArr.css('background-color', '');
                var valueArr = [];
                if (trArr && trArr.length) {
                    for (var i = 0; i < trArr.length; i++) {
                        var _tr = $(trArr[i]);
                        var item = {};
                        var nameInput = _tr.find('input[item-name]')[0];
                        item.name = nameInput.value;
                        var valueInput = _tr.find('input[item-value]')[0];
                        item.value = valueInput.value;

                        for (var j = 0; j < valueArr.length; j++) {
                            if (valueArr[j].value === item.value) {
                                // 这里还要给提示
                                $(trArr[j]).css("background-color", '#e4a2a2');
                                $(trArr[i]).css("background-color", '#e4a2a2');
                                return
                            }

                            if (valueArr[j].name === item.name) {
                                $(trArr[j]).css("background-color", '#e4a2a2');
                                $(trArr[i]).css("background-color", '#e4a2a2');
                                // 这里还要给提示
                                return
                            }
                        }
                        valueArr.push(item);
                    }
                }
                plugin.values = valueArr

                // 将控制面板的值保存
                plugin.title = name.val();
                plugin.propertyName = propertyName.val();
                plugin.isMul = isMul.checked;
                plugin.selectedValues = [];
                plugin.refresh();

            })

            // 删除
            var deleteBtn = controllerPanel.find("button[config-action=delete]");
            deleteBtn.off('click');
            deleteBtn.on("click", function () {
                plugin.remove();
                controllerPanel.hide();
                controllerPanel.attr("shown", "")
            });

            // 预览按键
            var preBtn = controllerPanel.find("button[config-action=preview]");
            preBtn.off('click');
            preBtn.on('click', function () {
                plugin.showDialog();
            })

        }

    }

    function makeTr(value, name) {
        var tr = $("<tr></tr>")[0];
        var valueTd = $("<td></td>");
        var valueInput = $("<input item-value class='form-control' />")
        valueInput.val(value);
        valueTd.append(valueInput);

        var nameTd = $("<td></td>");
        var nameInput = $("<input item-name class='form-control' />");
        nameInput.val(name);
        nameTd.append(nameInput);

        tr.appendChild(nameTd[0]);
        tr.appendChild(valueTd[0]);

        var last = $("<td></td>")[0];
        var removeBtn = $("<button class='btn btn-info btn-sm'>删除</button>")[0];
        removeBtn.addEventListener('click', function () {
            tr.remove();
        });
        last.appendChild(removeBtn);
        tr.appendChild(last);
        return tr;
    }

})

