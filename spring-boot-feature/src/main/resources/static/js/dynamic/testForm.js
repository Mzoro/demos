var DATA = [{"type": "INPUT", "title": "标题1", "defaultValue": "1111", "propertyName": "t1"}, {
    "type": "RADIO",
    "title": "标题2",
    "propertyName": "t2",
    "values": [{"name": "选项1", "value": "1"}, {"name": "2", "value": "2"}, {"name": "3", "value": "3"}]
}, {
    "type": "CHECKBOX",
    "title": "标题3",
    "propertyName": "t3",
    "values": [{"name": "选项1", "value": "1"}, {"name": "2", "value": "2"}, {"name": "3", "value": "3"}, {
        "name": "4",
        "value": "4"
    }]
}, {
    "type": "SELECT",
    "title": "标题4",
    "propertyName": "t4",
    "values": [{"name": "选项1", "value": "1"}, {"name": "2", "value": "2"}, {"name": "3", "value": "3"}],
    "isMul": false
}, {"type": "TEXTAREA", "title": "标题5", "defaultValue": "tttt", "propertyName": "t5"}, {
    "type": "SELECT",
    "title": "标题6",
    "propertyName": "t6",
    "values": [{"name": "选项1", "value": "1"}, {"name": "2", "value": "2"}, {"name": "3", "value": "3"}, {
        "name": "4",
        "value": "4"
    }, {"name": "5", "value": "5"}],
    "isMul": true
}, {
    "type": "RADIO",
    "title": "标题7",
    "propertyName": "t7",
    "values": [{"name": "选项1", "value": ""}]
}, {"type": "CHECKBOX", "title": "标题8", "propertyName": "t8", "values": [{"name": "选项1", "value": ""}]}];

