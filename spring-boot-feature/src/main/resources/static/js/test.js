var app = angular.module("test", []);
app.controller('testCtrl', function ($scope, $interval, $timeout) {
    $scope.startTime = ""
    $scope.endTime = ""
    $scope.spliter = ""

    $scope.testTTS = function () {
        $scope.startTime = ""
        $scope.endTime = ""
        $scope.spliter = ""
        var utterThis = new window.SpeechSynthesisUtterance('你好，世界！');
        utterThis.lang = "zh-CN";
        utterThis.onend = function () {

            $scope.endTime = $scope.now();
            $scope.$apply()
        };
        utterThis.onstart = function () {
            var now = new Date();
            $scope.startTime = $scope.now();
            $scope.spliter = "---"
            $scope.$apply()

        };
        window.speechSynthesis.speak(utterThis)
    };

    $scope.now = function () {
        var now = new Date();
        return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDay() + "  " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    }

    $scope.deviceWidth = function () {
        return document.body.clientWidth
    };

    $scope.deviceHeight = function () {
        return document.body.clientHeight
    };
    $scope.browserInfo = function () {
        console.log(navigator)
        var result = [];
        result.push(navigator.appVersion);
        result.push(navigator.language)
        result.push(navigator.platform)
        result.push(navigator.userAgent)
        result.push('scale:' + window.devicePixelRatio);
        result.push('dpi:' + $scope.getAndroidDpi());
        return result
    };
    $scope.extendClick = function (flag) {
        var cs = $("#accordion .collapse");
        for (var i = 0; i < cs.length; i++) {
            if (flag === 1) {
                $(cs[i]).collapse('show');
            } else {
                $(cs[i]).collapse('hide');
            }
        }
    };

    $scope.extendOne = function (id) {
        $("#" + id).collapse("toggle")
    };

    $scope.fullScreen = function () {
        if (typeof ElectronApi !== 'undefined' && ElectronApi && ElectronApi.ScreenLocalApi && typeof ElectronApi.ScreenLocalApi.fullScreen === 'function') {
            console.log(ElectronApi)
            ElectronApi.ScreenLocalApi.fullScreen()
            return
        }

        if (typeof Android !== 'undefined' && Android && typeof Android.fullWindow === "function") {
            Android.fullWindow()
            return
        }
        $("#test-result").text("Android 对象不存在");
    };
    $scope.quitFullScreen = function () {

        if (typeof Android === 'undefined') {
            $("#test-result").text("Android 对象不存在")
        } else {
            Android.quitFullWindow()
        }

    };
    $scope.getAndroidDpi = function () {
        if (typeof Android === 'undefined') {
            return -1
        }
        if (!!Android) {
            return Android.getDpi();
        } else {
            return -1;
        }
    };

    $scope.innerFiles = function () {
        if (typeof Android === 'undefined') {
            return ''
        } else {
            var files = Android.innerFiles();
            $scope.alert(files)
            return files
        }
    };
    $scope.outerFiles = function () {
        if (typeof Android === 'undefined') {
            return ''
        } else {
            var files = Android.outerFiles();
            $scope.alert(files)
            return files
        }
    };

    $scope.readFile = function () {
        if (typeof Android === 'undefined') {
            return ''
        } else {
            var files = Android.readFile();
            $scope.alert(files)
            return files
        }
    };

    $scope.alert = function (text) {
        $("#dialog-body").html(text);
        $("#dialog").modal('show')
    };

    $scope.speak = function () {
        if (typeof Android === 'undefined') {
            $scope.alert("Android 方法库加载失败")
        } else {
            Android.speak("s", "我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到？呵呵")
        }
    };
    $scope.xspeak = function (type) {
        if (typeof Android === 'undefined') {
            $scope.alert("Android 方法库加载失败")
        } else {
            var start = new Date().getTime();
            if (type == 'xiaofeng') {
                Android.xspeak(type, "您听到了？哈哈");
            } else {
                Android.xspeak(type, "我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都没听到我什么都？哈哈")
            }
            console.log("调用结束 ————————————————————————————————————》 ")
            console.log(new Date().getTime() - start)
        }
    };
})
