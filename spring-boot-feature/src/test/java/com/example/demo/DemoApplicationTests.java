package com.example.demo;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.support.ConnectionPoolSupport;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Disabled
class DemoApplicationTests {

    private final GenericObjectPool<StatefulRedisConnection<String, String>> pool;

    public DemoApplicationTests() {
        GenericObjectPoolConfig<StatefulRedisConnection<String, String>> config = new GenericObjectPoolConfig<>();
        config.setMaxIdle(10);
        config.setMaxTotal(20);
        config.setMaxWaitMillis(-1);
        RedisClient client =
                RedisClient.create(RedisURI.builder().withHost("localhost").withPassword("123456").withDatabase(1).build());
        pool = ConnectionPoolSupport
                .createGenericObjectPool(client::connect, new GenericObjectPoolConfig<>());
    }

    @Test
    @Ignore
    void contextLoads() throws Exception {

        for (int i = 0; i < 50; i++) {

            try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
                RedisCommands<String, String> command = connection.sync();
                System.err.println("borrow connection");
                command.multi();

                System.err.println("create test-" + i);
                command.set("test-" + i, i + "");
                command.expire("test-" + i, i * 100);
                command.exec();
            }
        }


    }

    @Test
    @Ignore
    void testString() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String d = "2020-11-13 15:02:00";
        Date date = sdf.parse(d);

        System.out.println(date.getTime());
    }

    @Test
    @Ignore
    void testString2() {
        String JSON_ARR =
                "[{\"patientId\":\"p1111\",\"regId\":\"101\",\"patientName\":\"预约1\",\"patientno\":\"00002\"," +
                        "\"sex\":\"1\",\"serialno\":\"1\",\"doctorCode\":\"006789\",\"doctorName\":\"H吴用\"," +
                        "\"orgid\":\"73736916-0\",\"deptid\":\"0001\",\"bespokeStart\":1605249900000}," +
                        "{\"patientId\":\"p1112\",\"regId\":\"112\",\"patientName\":\"预约2\",\"patientno\":\"00002\"," +
                        "\"sex\":\"1\",\"serialno\":\"1\",\"doctorCode\":\"006789\",\"doctorName\":\"H吴用\"," +
                        "\"orgid\":\"73736916-0\",\"deptid\":\"0001\",\"bespokeStart\":1605250200000}," +
                        "{\"patientId\":\"p1113\",\"regId\":\"113\",\"patientName\":\"预约3\",\"patientno\":\"00002\"," +
                        "\"sex\":\"1\",\"serialno\":\"1\",\"doctorCode\":\"006789\",\"doctorName\":\"H吴用\"," +
                        "\"orgid\":\"73736916-0\",\"deptid\":\"0001\",\"bespokeStart\":1605250500000}," +
                        "{\"patientId\":\"p1114\",\"regId\":\"114\",\"patientName\":\"预约4\",\"patientno\":\"00002\"," +
                        "\"sex\":\"1\",\"serialno\":\"1\",\"doctorCode\":\"006789\",\"doctorName\":\"H吴用\"," +
                        "\"orgid\":\"73736916-0\",\"deptid\":\"0001\",\"bespokeStart\":1605250800000}," +
                        "{\"patientId\":\"p1115\",\"regId\":\"115\",\"patientName\":\"预约5\",\"patientno\":\"00002\"," +
                        "\"sex\":\"1\",\"serialno\":\"1\",\"doctorCode\":\"006789\",\"doctorName\":\"H吴用\"," +
                        "\"orgid\":\"73736916-0\",\"deptid\":\"0001\",\"bespokeStart\":1605250920000}]";

        System.out.println(JSON_ARR);
    }

}
