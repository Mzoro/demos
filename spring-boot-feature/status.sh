#!/bin/bash

# 用于将 git status -s 的文件复制到一个目录里

set -e

TO=$@
PWD=$(pwd)

if [ -z "$TO" ]; then
    echo "target directory should be point out"
    exit 1
fi

if [ ! -d $TO ]; then
    echo "target is not exist or not a directory"
    exit 1
fi

files=$(git status -s);
# 将字符串按空格和换行分割成数组
fileArr=($files)
i=0
len=${#fileArr[*]}

echo "target $TO"
while [ $i -lt $len ]
do
   if [ -f ${fileArr[$i]} ] || [ -d ${fileArr[$i]} ]; then
       echo "cp ${fileArr[$i]}"
       cp --parents -r ${fileArr[$i]} $TO
   fi
   i=$(($i+1))
done
