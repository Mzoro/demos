package com.zoro.springjpapractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SpringJpaPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJpaPracticeApplication.class, args);
    }

}
