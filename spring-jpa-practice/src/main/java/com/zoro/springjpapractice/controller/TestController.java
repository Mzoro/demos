package com.zoro.springjpapractice.controller;

import com.zoro.springjpapractice.entity.ProcedureEntity;
import com.zoro.springjpapractice.entity.SubProperty;
import com.zoro.springjpapractice.entity.SubPropertyClass;
import com.zoro.springjpapractice.entity.User;
import com.zoro.springjpapractice.repositories.ProcedureRepository;
import com.zoro.springjpapractice.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhaoxingwu
 */
@RestController
public class TestController {


    private final UserRepository userRepository;
    private final ProcedureRepository procedureRepository;

    public TestController(UserRepository userRepository, ProcedureRepository procedureRepository) {
        this.userRepository = userRepository;
        this.procedureRepository = procedureRepository;
    }

    @RequestMapping("/user/{id}")
    public User user(@PathVariable("id") Long id) {
        //  TODO 1.多表关联查询且一次性查询 使用NamedEntityGraphs   [OK]
        //       2.动态查询条件
        //       3.自动分页
        //       4.排序与分页
        return userRepository.queryById(id);
    }

    @RequestMapping("/user2/{id}")
    public User user2(@PathVariable("id") Long id) {
        return userRepository.queryById2(id);
    }

    @RequestMapping("/save")
    public User save(@RequestBody User user) {
        user.setId(System.currentTimeMillis());
        return userRepository.save(user);
    }

    @RequestMapping("/getName/{id}")
    public SubProperty getName(@PathVariable("id") Long id) {
        System.out.println(id);
        SubProperty sb = userRepository.getName(id);
        System.out.println(sb);
        return sb;
    }

    @RequestMapping("/getName2/{id}")
    public SubPropertyClass getName2(@PathVariable("id") Long id) {
        System.out.println(id);
        SubPropertyClass sb = userRepository.getById(id);
        System.out.println(sb);
        return sb;
    }


    @RequestMapping("/procedure/{arg}")
    public void procedure(@PathVariable("arg") int arg) {
        Integer result = userRepository.pl(arg);
        System.out.println(String.format("parameter: %s, result: %s", arg, result));
    }

    @RequestMapping("/procedure2/{arg}")
    public void procedure2(@PathVariable("arg") int arg) {
        ProcedureEntity entity = this.procedureRepository.p2(Long.parseLong(arg + ""));
        System.out.println(entity);
    }

    @RequestMapping("/leftJoin/{sbb}")
    public UsetTemp leftJoin(@PathVariable("sbb") Long sbb) {
        User user = this.userRepository.justQuery(sbb, "猫");
        UsetTemp temp = new UsetTemp();
        temp.id = user.getId();
        temp.name= user.getName();

        return temp;
    }
    @RequestMapping("/leftJoin2/{sbb}")
    public UsetTemp leftJoin2(@PathVariable("sbb") Long sbb) {
        User user = this.userRepository.justQuery2( "猫");
        UsetTemp temp = new UsetTemp();
        temp.id = user.getId();
        temp.name= user.getName();

        return temp;
    }

    public static class UsetTemp{
        private Long id;
        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
