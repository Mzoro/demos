package com.zoro.springjpapractice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * @author zhaoxingwu
 */
@Entity
public class Address {

    @Id
    private Long id;
    private String addressName;

    @ManyToMany(mappedBy = "addresses")
//    @JoinTable(name = "user_address", joinColumns = @JoinColumn(name = "addr_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
    @JsonIgnore
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", addressName='" + addressName + '\'' +
                ", users=" + users +
                '}';
    }
}
