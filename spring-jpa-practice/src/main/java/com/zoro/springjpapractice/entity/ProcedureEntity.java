package com.zoro.springjpapractice.entity;

/**
 * @author zhaoxingwu
 */
public class ProcedureEntity {

    private long res;
    private long res2;

    public long getRes() {
        return res;
    }

    public void setRes(long res) {
        this.res = res;
    }

    public long getRes2() {
        return res2;
    }

    public void setRes2(long res2) {
        this.res2 = res2;
    }

    @Override
    public String toString() {
        return "ProcedureEntity{" +
                "res=" + res +
                ", res2=" + res2 +
                '}';
    }
}
