package com.zoro.springjpapractice.entity;

/**
 * @author zhaoxingwu
 */
public interface SubProperty {
    String getName();
}
