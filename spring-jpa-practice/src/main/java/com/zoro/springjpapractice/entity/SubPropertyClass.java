package com.zoro.springjpapractice.entity;

import java.util.Objects;

/**
 * @author zhaoxingwu
 */
public class SubPropertyClass {

//    public SubPropertyClass(String name, Long id) {
//        this.name = name;
//        this.id = id;
//    }

    public SubPropertyClass(String name) {
        this.name = name;
    }
    private String name;
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubPropertyClass that = (SubPropertyClass) o;

        if (!Objects.equals(name, that.name)) return false;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
