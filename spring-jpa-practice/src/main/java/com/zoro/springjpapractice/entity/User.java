package com.zoro.springjpapractice.entity;


import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author zhaoxingwu
 */
@Entity
@NamedEntityGraphs(@NamedEntityGraph(name = "user.address", attributeNodes = @NamedAttributeNode(value = "addresses")))
public class User {

    @Id
    private Long id;

    private String name;


    @ManyToMany
    @JoinTable(name = "user_address", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = @JoinColumn(name = "addr_id"))
    private Set<Address> addresses;

    @OneToMany
    @JoinColumn(name = "USER_ID")
    private Set<PET> pets;

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<PET> getPets() {
        return pets;
    }

    public void setPets(Set<PET> pets) {
        this.pets = pets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
