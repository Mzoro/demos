package com.zoro.springjpapractice.repositories;

import com.zoro.springjpapractice.entity.ProcedureEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 * @author zhaoxingwu
 */
@Repository
public class ProcedureRepositoryImpl implements ProcedureRepository, org.springframework.data.repository.Repository<ProcedureEntity, Long> {

    private final EntityManager entityManager;

    public ProcedureRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public ProcedureEntity p2(Long in) {

        final String inName = "arg";
        final String outName = "res";
        final String outName2 = "res2";

        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("plus1inout2");
        query.registerStoredProcedureParameter(inName, Long.class, ParameterMode.IN);
        query.setParameter(inName, in);

        query.registerStoredProcedureParameter(outName, Long.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(outName2, Long.class, ParameterMode.OUT);

        query.execute();
        Long out1 = (Long) query.getOutputParameterValue(outName);
        Long out2 = (Long) query.getOutputParameterValue(outName2);

        ProcedureEntity entity = new ProcedureEntity();
        entity.setRes(out1);
        entity.setRes2(out2);


        return entity;
    }
}
