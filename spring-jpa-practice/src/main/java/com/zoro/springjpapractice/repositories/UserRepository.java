package com.zoro.springjpapractice.repositories;

import com.zoro.springjpapractice.entity.SubProperty;
import com.zoro.springjpapractice.entity.SubPropertyClass;
import com.zoro.springjpapractice.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;

/**
 * @author zhaoxingwu
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from #{#entityName} u where u.id = ?1")
    @EntityGraph(value = "user.address", type = EntityGraph.EntityGraphType.LOAD)
    User queryById(Long id);

    @Query("select u from #{#entityName} u where u.id = ?1")
    @EntityGraph(attributePaths = {"addresses", "pets"})
    User queryById2(Long id);

    @Query("select u.name as name from #{#entityName} u where u.id = ?1")
    SubProperty getName(Long id);

    SubPropertyClass getById(Long id);

    @Procedure("plus1inout")
    Integer pl(Integer arg);

    @Query("select u,a from User u left join PET a on u.id = a.userId where u.id = ?1 and a.name like %?2%")
    User justQuery(Long id ,String name);

    @Query("select u,a from User u left join PET a on u.id = a.userId where a.name like %?1%")
    User justQuery2(String name);
}
