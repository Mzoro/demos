package com.zoro.test.http.jobs;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zoro.test.http.response.RequestHandler;
import com.zoro.test.http.util.http.CustomRequest;
import com.zoro.test.http.util.http.SendUtil;
import com.zoro.test.http.util.json.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class EventListener implements Runnable {


    private static final String JAR_SUFFIX = ".jar";
    private static final String CLASS_SUFFIX = ".class";

    private static final Pattern WINDOWS_ROOT_PREFIX = Pattern.compile("^\\w+:.+");

    private static final Logger LOGGER = LoggerFactory.getLogger(EventListener.class);
    private static final ConcurrentHashMap<String, RequestHandler<?>> HANDLER_MAP;

    static {
        HANDLER_MAP = collectionHandler0();
    }

    private final ServerSocket ss;

    EventListener(int port) throws IOException {
        ss = new ServerSocket(port);
    }

    @Override
    public void run() {
        while (true) {
            try (
                    Socket s = ss.accept();
                    InputStream is = s.getInputStream();
                    OutputStream os = s.getOutputStream()
            ) {

                CustomRequest request = SendUtil.resolveRequest(is);
                LOGGER.debug("收到数据：{}", JsonUtil.toString(request));

                StringBuilder response = new StringBuilder();

                RequestHandler<?> handler = HANDLER_MAP.get(request.getUri());

                if (handler == null) {
                    response.append("HTTP/1.1 404 OK\r\n");
                    response.append("Content-Type: application/json;\r\n");
                    response.append("\r\n");
                    ObjectNode responseBody = JsonUtil.makeJsonObject()
                            .put("msg", "path:" + request.getUri() + " not found!")
                            .put("timestamp", System.currentTimeMillis());
                    response.append(JsonUtil.toString(responseBody));

                } else {
                    response.append(handler.createResponse(request));
                }

                LOGGER.debug("返回数据：{}", response);
                os.write(response.toString().getBytes());
                os.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static EventListener create(int port) {
        try {

            if (port <= 0) {
                return new EventListener(8087);
            } else {
                return new EventListener(port);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public static ConcurrentHashMap<String, RequestHandler<?>> collectionHandler0() {
        try {
            return collectionHandler();
        } catch (Exception e) {
            LOGGER.error("init error ", e);
            return new ConcurrentHashMap<>();
        }
    }

    public static ConcurrentHashMap<String, RequestHandler<?>> collectionHandler() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        boolean isDebug = LOGGER.isDebugEnabled();
        boolean isInfo = LOGGER.isInfoEnabled();

        String classPath = System.getProperty("java.class.path");
        String userDir = System.getProperty("user.dir");

        if (isInfo) {
            LOGGER.info("class path: {}", classPath);
            LOGGER.info("user dir  : {}", userDir);
        }

        String[] classPaths = classPath.split(":");
        String[] filePaths = new String[classPaths.length];

        for (int i = 0; i < classPaths.length; i++) {
            if (!classPaths[i].startsWith("/") && !WINDOWS_ROOT_PREFIX.matcher(classPaths[i]).find()) {
                filePaths[i] = userDir + "/" + classPaths[i];
                // 这里没有 处理 window 系统情况
            } else {
                filePaths[i] = classPaths[i];
            }
        }

        if (isDebug) {
            LOGGER.debug("class path array: \n {}", (Object) filePaths);
        }

        // 在所有的classpath中查找 class 文件 ，classpath 可能是 jar ,也可能是一个目录，也可能是一个class文件
        Set<Class<?>> result = new HashSet<>();
        for (String path : filePaths) {
            File file = Paths.get(path).toFile();
            if (file.exists()) {
                Set<Class<?>> temp = findClassFile(file, file.getAbsolutePath());
                if (temp != null && temp.size() > 0) {
                    result.addAll(temp);
                }
            }
        }
        // 这个结果会非常多，可以优化一下，只load 几个包下的class 文件
        if (isInfo) {
            LOGGER.info("得到class {} 个", result.size());
        }

        ConcurrentHashMap<String, RequestHandler<?>> resultMap = new ConcurrentHashMap<>(1024);
        for (Class<?> c : result) {

            if (RequestHandler.class.isAssignableFrom(c) && !Modifier.isInterface(c.getModifiers()) && !Modifier.isAbstract(c.getModifiers())) {

                if (isDebug) {
                    LOGGER.debug("found request handler : {}",c.getName());
                }

                Method method = c.getMethod("createHandler");
                Constructor<?> constructor = c.getDeclaredConstructor();
                Object o = constructor.newInstance();
                Object temp = method.invoke(o);
                if (temp instanceof RequestHandler) {
                    RequestHandler<?> handler = (RequestHandler<?>) temp;
                    String[] pathArray = handler.handlerPath();
                    if (pathArray != null) {
                        for (String p : pathArray) {
                            if (p == null) {
                                continue;
                            }

                            if (!p.startsWith("/")) {
                                p = "/" + p;
                            }
                            RequestHandler<?> h;
                            if ((h = resultMap.get(p)) != null) {
                                LOGGER.warn("path {} is already exist , Request handler is {}", p, h.getClass().getName());
                            } else {
                                resultMap.put(p, handler);
                            }
                        }
                    }
                }
            }
        }

        return resultMap;
    }

    private static Set<Class<?>> findClassFile(File packageDir, String classPath) {

        boolean isDebug = LOGGER.isDebugEnabled();

        if (!packageDir.exists()) {
            return null;
        }

        if (packageDir.isFile()) {
            if (packageDir.getName().endsWith(CLASS_SUFFIX)) {
                return null;
            }
        }
        String packageName = makePackageName(packageDir, classPath);

        File[] children;
        Set<Class<?>> classSet = new HashSet<>();
        if (packageDir.isDirectory()) {
            // 不是jar文件，递归，处理这个文件夹中的目录或文件
            children = packageDir.listFiles();

            if (children == null || children.length == 0) {
                return classSet;
            }
            for (File file : children) {
                if (file.exists()) {
                    if (file.isFile()) {
                        String fileName = file.getName();

                        if (fileName.endsWith(CLASS_SUFFIX)) {
                            // 如果是class 那么就解析一下
                            Class<?> clazz = loadClass(packageName, fileName);
                            if (clazz != null) {
                                classSet.add(clazz);
                            }
                        }
                        // 其他类型文件不处理
                    } else {
                        //loop here
                        Set<Class<?>> result = findClassFile(file, classPath);
                        if (result != null && result.size() > 0) {
                            classSet.addAll(result);
                        }
                    }
                }
            }

        } else if (packageDir.getName().endsWith(JAR_SUFFIX)) {
            try {
                // jar 文件 ， 这里就不处理 嵌套jar 了，太难了
                JarFile jarFile = new JarFile(packageDir.getAbsoluteFile());
                Enumeration<JarEntry> entries = jarFile.entries();
                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    String name = entry.getName();
                    if (isDebug) {
                        LOGGER.debug("jar entry name --> " + name);
                    }

                    if (name.endsWith(CLASS_SUFFIX)) {
                        // 只处理class文件
                        Class<?> clazz = loadClass(name);
                        if (clazz != null) {
                            classSet.add(clazz);
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.error("读取jar 包文件出错");
            }
        }

        return classSet;
    }

    /**
     * 构建 包名
     * {@param packageDir} 是包名的绝对路径，可能是以下的情况
     * - /root/test/class/com/test
     * - /root/test/class/lib/spring-boot.jar/class/com/test
     * {@param classPath} classPath的绝对路径，可能是以下情况
     * - /root/test/class/
     * - /home/user/test.jar/
     *
     * @param packageDir 包路径
     * @param classPath  classpath
     * @return 包名
     */
    private static String makePackageName(File packageDir, String classPath) {

        String sp = ".";

        String packagePath = packageDir.getAbsolutePath();
        packagePath = packagePath.substring(classPath.length());
        String packageName = packagePath.replaceAll("/", ".");
        if (packageName.startsWith(sp)) {
            packageName = packageName.substring(1);
        }
        if (packageName.endsWith(sp)) {
            packageName = packageName.substring(0, packageName.length() - 1);
        }

        return packageName;
    }

    /**
     * 获取类对象
     *
     * @param packageName   包名
     * @param classFileName class 文件名; .class 结尾
     * @return class对象
     */
    private static Class<?> loadClass(String packageName, String classFileName) {

        String classFullName = classFileName.substring(0, classFileName.length() - 6);

        if (packageName.length() != 0) {
            classFullName = packageName + "." + classFullName;
        }
        return load(classFullName);
    }

    /**
     * 获取类对象
     *
     * @param jarEntryName 例： com/test/Main.class
     * @return class对象
     */
    private static Class<?> loadClass(String jarEntryName) {
        String classNameTemp = jarEntryName.replace(CLASS_SUFFIX, "");
        String fullClassName = classNameTemp.replaceAll("/", ".");
        return load(fullClassName);
    }

    private static Class<?> load(String fullClassName) {
        Class<?> clazz = null;
        try {
            clazz = Thread.currentThread().getContextClassLoader().loadClass(fullClassName);
        } catch (ClassNotFoundException e) {
            LOGGER.error("class: {} not found", fullClassName);
        } catch (NoClassDefFoundError error) {
            LOGGER.error("class: {} ,NoClassDefFoundError", fullClassName);
        }
        return clazz;
    }
}