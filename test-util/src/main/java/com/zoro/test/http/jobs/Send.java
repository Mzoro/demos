package com.zoro.test.http.jobs;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zoro.test.http.util.http.SendUtil;
import com.zoro.test.http.util.json.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class Send implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Send.class);

    @Override
    public void run() {
        synchronized (this) {
            while (true) {

                try {
                    this.wait(5000);
                    String workPath = System.getProperty("user.dir");
                    Path classPath = Paths.get(workPath);

                    String paramFilePath = classPath.toString() + "/param.json";

                    LOGGER.debug("当前配置文件：{}", paramFilePath);
                    File file = new File(paramFilePath);
                    if (!file.exists() || !file.isFile()) {
                        LOGGER.error("param.json 不存在");
                        continue;
                    }

                    if (!file.canRead()) {
                        LOGGER.error("param.json can not read");
                        continue;
                    }

                    ObjectNode fileParam = JsonUtil.readJson(file);
                    String url = JsonUtil.getString(fileParam, "_url");

                    if (StringUtils.isBlank(url)) {
                        LOGGER.error("json中缺少参数 _url");
                        continue;
                    }

                    ObjectNode param = JsonUtil.getJson(fileParam, "_param");

                    if (param == null) {
                        LOGGER.error("json中缺少参数 _param,或者_param 不是一个json参数");
                        continue;
                    }


                    SendUtil.send(param, url);
                } catch (Exception e) {
                    LOGGER.error("", e);
                }
            }
        }
    }
}

