package com.zoro.test.http.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zoro.test.http.util.http.CustomRequest;
import com.zoro.test.http.util.json.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class CustomRequestHandler implements RequestHandler<CustomRequestHandler> {

    public static final Logger LOGGER = LoggerFactory.getLogger(CustomRequestHandler.class);

    private static final String[] PATH_ARRAY;

    static {

        PATH_ARRAY = getPath();

    }

    @Override
    public String createResponse(Object request) {
        StringBuilder response = new StringBuilder();
        try {
            if (request instanceof CustomRequest) {

                Map<String, String> paramMap = ((CustomRequest) request).getUrlParamMap();
                Integer result;
                String regIdStr = paramMap.get("regid");
                try {
                    int reg = Integer.parseInt(regIdStr);
                    result = reg % 2;
                } catch (Exception e) {
                    LOGGER.error("regid is not a number " + regIdStr);
                    result = null;
                }

                response.append("HTTP/1.1 200 OK\r\n");
                response.append("Content-Type: application/json;\r\n");
                response.append("\r\n");
                ObjectNode responseBody = JsonUtil.makeJsonObject()
                        .put("diagflag", result)
                        .put("timestamp", System.currentTimeMillis());

                response.append(JsonUtil.toString(responseBody));
            } else {
                response.append("HTTP/1.1 400 OK\r\n");
                response.append("Content-Type: application/json;\r\n");
                response.append("\r\n");
                ObjectNode responseBody = JsonUtil.makeJsonObject()
                        .put("msg", "参数不正确")
                        .put("timestamp", System.currentTimeMillis());

                response.append(JsonUtil.toString(responseBody));
            }

        } catch (JsonProcessingException e) {
            LOGGER.error("", e);
        }
        return response.toString();
    }

    @Override
    public CustomRequestHandler createHandler() {
        return new CustomRequestHandler();
    }

    @Override
    public String[] handlerPath() {
        return PATH_ARRAY;
    }

    private static String[] getPath() {
        String workPath = System.getProperty("user.dir");
        Path classPath = Paths.get(workPath);

        String paramFilePath = classPath.toString() + "/param.json";

        File param = new File(paramFilePath);

        ObjectNode temp;
        if (!param.exists()) {
            temp = JsonUtil.makeJsonObject();
        } else {
            try {
                temp = JsonUtil.readJson(param);
            } catch (Exception e) {
                temp = JsonUtil.makeJsonObject();
            }
        }
        ObjectNode json = temp == null ? JsonUtil.makeJsonObject() : temp;

        JsonNode node = json.get("customRequestHandlerParam");
        JsonNode path = null;
        if (node != null) {
            path = node.get("_url");
        }

        if (path != null && path.isArray() && !path.isEmpty()) {
            ArrayNode arr = (ArrayNode) path;
            String[] pathArr = new String[arr.size()];
            for (int i = 0; i < arr.size(); i++) {
                pathArr[i] = arr.get(i).asText();
            }
            LOGGER.debug("urls:{}", Arrays.toString(pathArr));
            return pathArr;

        } else {
            LOGGER.error("no path found");
            return new String[]{};
        }
    }
}
