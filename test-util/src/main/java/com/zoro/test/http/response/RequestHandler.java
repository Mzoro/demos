package com.zoro.test.http.response;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public interface RequestHandler<T extends RequestHandler<T>> {

    /**
     * 构建返回数据，包括头与体
     *
     * @param request 请求数据
     * @return 数据
     */
    String createResponse(Object request);

    /**
     * 构建 处理器实例
     *
     * @return handler
     */
    T createHandler();

    /**
     * 处理器可惜处理的请求
     *
     * @return path
     */
    String[] handlerPath();
}
