package com.zoro.test.http.tests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zoro.test.http.util.json.JsonUtil;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;

/**
 * @author zhaoxingwu
 */
public class EmrIeUrlMaker {

    public static void main(String[] args) throws IOException {
        String userDir = System.getProperty("user.dir");
        String file = "\\src\\main\\resources\\emr.json";
        ObjectNode emrJson = JsonUtil.readJson(Paths.get(userDir + file).toFile());
        Iterator<Map.Entry<String, JsonNode>> fields = emrJson.fields();
        StringBuilder urlQuery = new StringBuilder();
        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> nodeEntry = fields.next();
            JsonNode node = nodeEntry.getValue();

            if ("startMode".equals(nodeEntry.getKey())) {
                urlQuery.append(nodeEntry.getKey()).append("=ie").append("&");
                continue;
            }

            if (node.isNull()) {
                urlQuery.append(nodeEntry.getKey()).append("=").append("&");
            } else if (node.isTextual() || node.isNumber()) {
                urlQuery.append(nodeEntry.getKey()).append("=").append(node.textValue()).append("&");
            } else {
                System.err.println(nodeEntry.getKey() + ": 不能解析");
            }
        }
        String query = urlQuery.toString();
        if (query.length() > 0) {
            query = "?" + query;
        }
        System.out.println(query);
    }
}
