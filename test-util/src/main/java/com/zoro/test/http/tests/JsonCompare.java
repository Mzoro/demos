package com.zoro.test.http.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zoro.test.http.util.json.JsonUtil;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;

/**
 * @author zhaoxingwu
 */
public class JsonCompare {

    public static void main(String[] args) throws IOException {
        String userDir = System.getProperty("user.dir");
        String dir = "\\src\\main\\resources\\json\\";
        ObjectNode a = JsonUtil.readJson(Paths.get(userDir + dir + "a.json").toFile());
        ObjectNode b = JsonUtil.readJson(Paths.get(userDir + dir + "b.json").toFile());
        Iterator<Map.Entry<String, JsonNode>> aFields = a.fields();
        Iterator<Map.Entry<String, JsonNode>> bFields = b.fields();
        while (aFields.hasNext()) {
            Map.Entry<String, JsonNode> entry = aFields.next();
            if (!b.has(entry.getKey())) {
                System.out.printf("field %s not exists in b. \n", entry.getKey());
            } else if (! entry.getValue().equals(b.get(entry.getKey()))) {
                System.out.printf("field %s exists in b , but not equal, a: %s, b: %s. \n",entry.getKey(), entry.getValue(), b.get(entry.getKey()));
            }
        }
        for (int i = 0; i < 4; i++) {
            System.out.println();
        }
        while (bFields.hasNext()) {
            Map.Entry<String, JsonNode> entry = bFields.next();
            if (!a.has(entry.getKey())) {
                System.out.printf("field %s not exists in a. \n", entry.getKey());
            } else if (! entry.getValue().equals(a.get(entry.getKey()))) {
                System.out.printf("field %s exists in a , but not equal, b: %s, a: %s. \n",entry.getKey(), entry.getValue(), a.get(entry.getKey()));
            }
        }
    }
}
