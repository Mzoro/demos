package com.zoro.test.http.tests;

import com.zoro.test.http.jobs.EventListener;
import com.zoro.test.http.jobs.Send;
import com.zoro.test.http.util.thread.ThreadFactoryBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class TestHttp {

    private static final ExecutorService EXECUTORS_SERVICE = new ThreadPoolExecutor(2, 2, 0L,
            TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(),
            ThreadFactoryBuilder.create().defaultFactory());

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("请使用参数(不区分大小写)：1. send; 2. listen; 3. both");
            System.exit(0);
        }
        String cmd = args[0];

        if (StringUtils.equalsIgnoreCase(cmd, "send")) {

            EXECUTORS_SERVICE.execute(new Send());
        } else if (StringUtils.equalsIgnoreCase(cmd, "listen")) {

            EXECUTORS_SERVICE.execute(EventListener.create(8087));
        } else if (StringUtils.equalsIgnoreCase("both", cmd)) {
            EXECUTORS_SERVICE.execute(new Send());
            EXECUTORS_SERVICE.execute(EventListener.create(8087));
        } else {
            System.out.println("请使用参数(不区分大小写)：1. send; 2. listen; 3. both");
            System.exit(0);
        }

    }
}
