package com.zoro.test.http.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class TestLogger {

    public static final Logger LOGGER = LoggerFactory.getLogger(TestLogger.class);

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            LOGGER.info("info---------------------");
            LOGGER.debug("debug---------------------");
            LOGGER.error("error---------------------");
        }
    }
}
