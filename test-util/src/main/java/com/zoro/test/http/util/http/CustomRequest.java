package com.zoro.test.http.util.http;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class CustomRequest {

    String uri;
    String method;
    String status;
    String urlParam;
    Map<String, String> headers = new HashMap<>();
    String body;
    Map<String, String> urlParamMap = new HashMap<>();

    public Map<String, String> getUrlParamMap() {
        return urlParamMap;
    }

    public void setUrlParamMap(Map<String, String> urlParamMap) {
        this.urlParamMap = urlParamMap;
    }

    public String getUrlParam() {
        return urlParam;
    }

    public void setUrlParam(String urlParam) {
        this.urlParam = urlParam;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getHeader(String key) {
        return this.headers.get(key);
    }

    public void setHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public enum RequestMethod {
        /**
         * GET方法
         */
        GET,
        /**
         * POST 方法
         */
        POST
    }
}
