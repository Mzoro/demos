package com.zoro.test.http.util.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 发送http 的工具类
 *
 * @author zhaoxingwu
 */
public class HttpUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class);

    private static final OkHttpClient HTTP_CLIENT = new OkHttpClient.Builder()
            .callTimeout(Duration.ofSeconds(50))
            .connectTimeout(Duration.ofSeconds(30))
            .readTimeout(Duration.ofSeconds(30))
            .build();

    private static final ThreadFactory DEFAULT_THREAD_FACTORY = Executors.defaultThreadFactory();

    private static final AtomicInteger THREAD_ID = new AtomicInteger(1);

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


    private static final ExecutorService executorService = new ThreadPoolExecutor(10, 10,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(),
            (r) -> {
                Thread thread = DEFAULT_THREAD_FACTORY.newThread(r);
                thread.setDaemon(true);
                thread.setName("HTTP-UTIL-EXEC-" + THREAD_ID.getAndIncrement());
                return thread;
            });


    /**
     * 发送post 请求
     *
     * @return 响应数据
     */
    public static Future<String> post(final String url, final Object data, final Map<String, String> headers) {

        return executorService.submit(() -> {

            long start = 0;
            boolean isDebug = LOGGER.isDebugEnabled();
            String result;
            if (isDebug) {
                LOGGER.debug("Request Url    : {}", url);
                LOGGER.debug("Request Method : {}", "POST");
                LOGGER.debug("request headers: {}", headers);
                start = System.currentTimeMillis();
            }

            RequestBody requestBody = RequestBody.create(resolveRequestBody(data));

            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(url)
                    .headers(headers(headers))
                    .build();

            Response response = HTTP_CLIENT.newCall(request).execute();
            result = resolveResponseBody(response);
            if (isDebug) {
                long use = System.currentTimeMillis() - start;
                LOGGER.debug("Use            : {} ms", use);
                LOGGER.debug("Response       : {}", result);
            }
            return result;

        });
    }

    /**
     * 没有自定义请求头的 post请求
     *
     * @param url url
     * @return 响应体
     */
    public static Future<String> post(final String url, final Object data) {

        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");
        return post(url, data, headers);
    }

    /**
     * 发送get 请求
     *
     * @param url     url
     * @param headers 请求头
     * @return 响应数据
     */
    public static Future<String> get(final String url, final Map<String, String> headers) {
        return executorService.submit(() -> {

            long start = 0;
            boolean isDebug = LOGGER.isDebugEnabled();
            String result;
            if (isDebug) {
                LOGGER.debug("Request Url    : {}", url);
                LOGGER.debug("Request Method : {}", "GET");
                LOGGER.debug("request headers: {}", headers);
                start = System.currentTimeMillis();
            }

            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .headers(headers(headers))
                    .build();

            Response response = HTTP_CLIENT.newCall(request).execute();
            result = resolveResponseBody(response);
            if (isDebug) {
                long use = System.currentTimeMillis() - start;
                LOGGER.debug("Use            : {} ms", use);
                LOGGER.debug("Response       : {}", result);
            }
            return result;
        });
    }

    /**
     * 没有自定义请求头的 get请求
     *
     * @param url url
     * @return 响应体
     */
    public static Future<String> get(final String url) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        return get(url, headers);
    }

    private static Headers headers(Map<String, String> headers) {
        Headers.Builder builder = new Headers.Builder();
        if (headers != null && headers.size() > 0) {
            Set<Map.Entry<String, String>> entries = headers.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }

    /**
     * 将对象转为byte[]
     *
     * @param data 数据
     * @return 数据
     */
    private static byte[] resolveRequestBody(Object data) {
        if (data == null) {
            return null;
        }

        if (data instanceof String) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("request string data   : {}", data);
            }
            return ((String) data).getBytes();
        }

        if (data.getClass().isPrimitive()) {
            // 这个分支应该不会存在,因为会自动装箱
            String v = String.valueOf(data);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("request primitive data   : {}", data);
            }
            return v.getBytes();
        }

        try {
            String v = OBJECT_MAPPER.writeValueAsString(data);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("request object data   : {}", v);
            }
            return v.getBytes();
        } catch (JsonProcessingException e) {
            throw new RuntimeException("occurred a error when transform Object to byte array");
        }
    }

    /**
     * 从Response 中获取 String
     *
     * @param response response
     * @return string
     */
    private static String resolveResponseBody(Response response) {
        String result;
        try {

            if (response == null) {
                return null;
            }
            ResponseBody responseBody = response.body();
            if (responseBody == null) {
                return null;
            }
            result = responseBody.string();

        } catch (IOException e) {
            throw new RuntimeException("occurred a error when transform Response to String");
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return result;
    }
}
