package com.zoro.test.http.util.http;

import com.zoro.test.http.util.json.JsonUtil;
import com.zoro.test.http.util.thread.ThreadFactoryBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class SendUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendUtil.class);
    private static final Logger LOGGER_DATA = LoggerFactory.getLogger(SendUtil.class.getName() + "_LOG_DATA");


    private static ExecutorService executorService = new ThreadPoolExecutor(10, 10,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(),
            ThreadFactoryBuilder.create().prefixName("SEND-UTIL").build());

    private static final CloseableHttpClient HTTP_CLIENT = HttpClientBuilder.create()
            .setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(2000).setSoLinger(2000).build())
            .build();

    public static void send(Object reg, String urlStr) {

        executorService.execute(() -> {
            CloseableHttpResponse response = null;
            try {
                BasicHttpEntity entity = new BasicHttpEntity();
                entity.setContentType("application/json");
//                String content = JsonUtil.toString(reg);
                byte[] data = JsonUtil.toBytes(reg);
                LOGGER_DATA.debug("将要发送数据：{}", new String(data));
                entity.setContent(new ByteArrayInputStream(data));
                entity.setContentLength(data.length);
                HttpUriRequest request = RequestBuilder
                        .create(HttpPost.METHOD_NAME)
                        .setEntity(entity)
                        .setUri(urlStr)
                        .setHeader("Accept", "application/json")
                        .build();

                response = HTTP_CLIENT.execute(request);
                HttpEntity responseEntity = response.getEntity();
                InputStream is = responseEntity.getContent();
                String result = resolveInputStream(is);
                LOGGER_DATA.debug("返回数据：{}", result);

            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                if (response != null) {
                    try {
                        response.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    public static String resolveInputStream(InputStream is) {
        BufferedReader br = null;
        InputStreamReader isr = null;
        StringBuilder sb = new StringBuilder();
        try {

            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                LOGGER.debug("当前读取的数据：{}", sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(br, isr, is);
        }

        return sb.toString();
    }

    public static void close(Closeable cloneable) {
        if (cloneable != null) {
            try {
                cloneable.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void close(Closeable... closeableArr) {
        if (closeableArr == null) {
            return;
        }

        for (Closeable closeable : closeableArr) {
            close(closeable);
        }

    }

    private static String readLine(InputStream is) throws IOException {

        int lineFeedByte = 10;

        if (is == null) {
            return null;
        }

        List<Byte> bytes = new ArrayList<>();
        byte b;
        while ((b = (byte) is.read()) != lineFeedByte && b != -1) {
            bytes.add(b);
        }

        if (bytes.size() == 0) {
            return null;
        }
        byte[] bArr = new byte[bytes.size()];

        for (int i = 0; i < bytes.size(); i++) {
            bArr[i] = bytes.get(i);
        }

        return new String(bArr);

    }

    private static String readBody(InputStream is, int bodyLength) throws IOException {

        if (bodyLength <= 0) {
            return null;
        }
        ArrayList<Byte> bArray = new ArrayList<>();
        while (bArray.size() < bodyLength) {
            bArray.add((byte) is.read());
        }
        byte[] bArr = new byte[bArray.size()];

        for (int i = 0; i < bArray.size(); i++) {
            bArr[i] = bArray.get(i);
        }
        return new String(bArr);


    }

    public static CustomRequest resolveRequest(InputStream is) throws IOException {

        String contentLengthKey = "Content-Length";
        CustomRequest result = new CustomRequest();

        String first = readLine(is);

        if (StringUtils.isBlank(first)) {
            return null;
        }

        String[] tmps = first.split("\\s");
        result.setMethod(tmps[0]);
        // todo 这里需要处理url参数
        if (!tmps[1].contains("?")) {
            result.setUri(tmps[1]);
        } else {
            String[] temp = tmps[1].split("\\?");
            result.setUri(temp[0]);
            result.setUrlParam(temp[1].replaceFirst("\\?", ""));
            result.setUrlParamMap(resolePathParam(result.getUrlParam()));
        }
        result.setStatus(tmps[2]);

        String line;

        // 处理请求头
        while (StringUtils.isNotBlank((line = readLine(is)))) {

            int index = line.indexOf(":");
            String k = line.substring(0, index);
            String v = line.substring(index + 1).replaceAll("\\s", "");

            result.setHeader(k, v);
        }

        // 处理body
        if (!StringUtils.equals(result.method, CustomRequest.RequestMethod.GET.name())) {
            int lenth = 0;
            if (result.getHeader(contentLengthKey) != null) {
                try {
                    lenth = Integer.parseInt(result.getHeader(contentLengthKey));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (lenth >= 0) {
                result.setBody(readBody(is, lenth));
            }
        }
        return result;
    }

    /**
     * 将url 参数 解析成Map形式， url参数需要是没有开头的？号的形式
     *
     * @param search url参数
     * @return paramMap
     */
    private static Map<String, String> resolePathParam(String search) {
        Map<String, String> paramMap = new HashMap<>(16);
        if (!StringUtils.isBlank(search)) {
            String[] temp = search.split("&");
            if (temp.length > 0) {
                for (String s : temp) {

                    String[] pArr = s.split("=");
                    if (pArr.length > 1) {
                        paramMap.put(pArr[0], pArr[1]);
                    } else if (pArr.length == 1) {
                        paramMap.put(pArr[0], null);
                    } else {
                        // do nothing ,pass
                    }
                }
            }
        }

        return paramMap;
    }
}

