package com.zoro.test.http.util.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
public class JsonUtil {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS,false)
            .enable(SerializationFeature.INDENT_OUTPUT)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    /**
     * 对象转json string
     *
     * @param data 需要转换的对象
     * @return json string
     * @throws JsonProcessingException e
     */
    public static String toString(Object data) throws JsonProcessingException {
        if (data == null) {
            return null;
        }
        if (data instanceof String) {
            return (String) data;
        }
        return OBJECT_MAPPER.writeValueAsString(data);
    }

    /**
     * 转为json 数据对应的 byte数组
     *
     * @param data 需要转换的数组
     * @return bytes
     * @throws JsonProcessingException e
     */
    public static byte[] toBytes(Object data) throws JsonProcessingException {
        String result = toString(data);
        if (result == null) {
            return new byte[0];
        }
        return result.getBytes();
    }

    /**
     * json 字符串转为Json
     *
     * @param json  json串
     * @param clazz Class类
     * @param <T>   要转为哪种类的实例
     * @return 对象
     */
    public static <T> T toJavaType(String json, Class<T> clazz) throws JsonProcessingException {
        if (json == null) {
            return null;
        }
        if (json.length() < 2) {
            return null;
        }
        if (clazz == String.class) {
            return (T)json;
        }
        return OBJECT_MAPPER.readValue(json, clazz);
    }

    /**
     * 将 json array 字符串转为 list 并指定泛型
     * @param json json 串
     * @param tClass List 泛型class
     * @param <T>  泛型
     * @return e
     */
    public static <T> List<T> toJavaList(String json, Class<T> tClass) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(json, OBJECT_MAPPER.getTypeFactory().constructParametricType(List.class,
                tClass));
    }


    /**
     * 创建一个json对象
     *
     * @return json
     */
    public static ObjectNode makeJsonObject() {
        return OBJECT_MAPPER.createObjectNode();
    }

    /**
     * 创建一个json数组对象
     *
     * @return json array
     */
    public static ArrayNode makeJsonArray() {
        return OBJECT_MAPPER.createArrayNode();
    }

    /**
     * 从字符串读取json
     *
     * @param jsonStr json 字符串
     * @return Json
     * @throws JsonProcessingException e
     */
    public static ObjectNode readJson(String jsonStr) throws JsonProcessingException {
        JsonNode node = OBJECT_MAPPER.readTree(jsonStr);

        if (node instanceof ObjectNode) {
            return (ObjectNode) node;
        } else {
            throw new RuntimeException("参数不是一个json 字符串！！");
        }
    }

    public static ObjectNode readJson(File file) throws IOException {
        if (file == null || !file.exists() || !file.canRead()) {
            return null;
        }

        JsonNode node = OBJECT_MAPPER.readTree(file);
        if (node instanceof ObjectNode) {
            return (ObjectNode) node;
        } else {
            throw new RuntimeException("参数不是一个json 字符串！！");
        }
    }
    public static ArrayNode readJsonArray(File file) throws IOException {
        if (file == null || !file.exists() || !file.canRead()) {
            return null;
        }

        JsonNode node = OBJECT_MAPPER.readTree(file);
        if (node instanceof ArrayNode) {
            return (ArrayNode) node;
        } else {
            throw new RuntimeException("参数不是一个json array 字符串！！");
        }
    }

    /**
     * 从字符串读取jsonArray
     *
     * @param jsonStr jsonArray 字符串
     * @return JsonArray
     * @throws JsonProcessingException e
     */
    public static ArrayNode readJsonArray(String jsonStr) throws JsonProcessingException {
        JsonNode node = OBJECT_MAPPER.readTree(jsonStr);

        if (node instanceof ArrayNode) {
            return (ArrayNode) node;
        } else {
            throw new RuntimeException("参数不是一个json 数组形式的 字符串！！");
        }
    }

    /**
     * 获取 String 值，但凡没有这个key,或者 key对应的值不是一个 基本类型节点，都返回 null
     *
     * @param json json
     * @param key  key
     * @return String value
     */
    public static String getString(ObjectNode json, String key) {
        if (json == null || key == null) {
            return null;
        }

        if (json.has(key)) {
            JsonNode node = json.get(key);
            if (node instanceof NumericNode || node instanceof BooleanNode || node instanceof TextNode) {
                return node.asText();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 在 json 中没有 指定属性时返回自定义的默认值 {@param defaultValue}
     *
     * @param json         json
     * @param key          key
     * @param defaultValue 默认值
     * @return value
     */
    public static String getString(ObjectNode json, String key, String defaultValue) {
        String value = getString(json, key);
        return value == null ? defaultValue : value;
    }

    /**
     * 获取json 中嵌套的 json对象
     *
     * @param json json
     * @param key  key
     * @return e
     */
    public static ObjectNode getJson(ObjectNode json, String key) {

        if (json == null || key == null) {
            return null;
        }

        if (json.has(key)) {
            JsonNode node = json.get(key);
            return node.isObject() ? (ObjectNode) node : null;
        } else {
            return null;
        }
    }


    public static TreeNode getTreeNode(ObjectNode json, String key) {
        if (json == null || key == null) {
            return null;
        }

        if (json.has(key)) {
            return json.get(key);
        } else {
            return null;
        }
    }
}

