package com.zoro.test.http.util.thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class ThreadFactoryBuilder {

    private String pre;

    private ThreadFactoryBuilder() {

    }

    public static ThreadFactoryBuilder create() {
        return new ThreadFactoryBuilder();
    }

    public java.util.concurrent.ThreadFactory defaultFactory() {
        return new ThreadFactory();
    }

    public ThreadFactoryBuilder prefixName(String name) {
        this.pre = name;
        return this;
    }

    public java.util.concurrent.ThreadFactory build() {
        if (this.pre == null || this.pre.length() == 0) {
            return new ThreadFactory();
        } else {
            String preTemp = this.pre.replaceAll("\\s", "");
            if (preTemp.length() == 0) {
                return new ThreadFactory();
            } else {
                return new ThreadFactory(this.pre);
            }
        }
    }


    private static class ThreadFactory implements java.util.concurrent.ThreadFactory {
        private static final AtomicInteger POOL_NUMBER = new AtomicInteger(1);
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        public ThreadFactory(String prefix) {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() :
                    Thread.currentThread().getThreadGroup();
            namePrefix = prefix + "-" + POOL_NUMBER.getAndIncrement() + "-";
        }

        public ThreadFactory() {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() :
                    Thread.currentThread().getThreadGroup();
            String prefix = Thread.currentThread().getName();
            namePrefix = prefix + "-" + POOL_NUMBER.getAndIncrement() + "-";
        }


        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r,
                    namePrefix + threadNumber.getAndIncrement(),
                    0);
            if (t.isDaemon()) {

                t.setDaemon(false);
            }
            if (t.getPriority() != Thread.NORM_PRIORITY) {

                t.setPriority(Thread.NORM_PRIORITY);
            }
            return t;
        }
    }
}
