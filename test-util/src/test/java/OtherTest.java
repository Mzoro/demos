import com.fasterxml.jackson.core.JsonProcessingException;
import com.zoro.test.http.jobs.EventListener;
import com.zoro.test.http.util.json.JsonUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

/**
 * @author zhaoxingwu
 * @version 1.0
 */
public class OtherTest {

    private final Logger logger = LoggerFactory.getLogger(OtherTest.class);

    @Test
    @Ignore
    public void testByte() {
        System.out.println(Arrays.toString("测试数据".getBytes()));
        byte[] b = new byte[]{-26, -75, -117, -24, -81, -107, -26, -107, -80, -26, -115, -82};

        System.out.println(new String(b));
    }

    @Test
    @Ignore
    public void testPath() {
        EventListener.collectionHandler0();
    }


    @Test
    @Ignore
    public void testJar() throws IOException {
        JarFile jarFile = new JarFile("C:\\Users\\Mzoro\\Desktop\\operation-1.1.jar");
//        System.out.println("0---->" + jarFile.getName());
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String name = entry.getName();


//            System.out.println("1---->" + entry.getAttributes());
//            System.out.println("2---->" + name);
//            System.out.println("5---->" + entry.getComment());
            if (name.endsWith(".jar")) {
                URL url = new URL("jar:file:/" + jarFile.getName() + "!/" + name + "");
                logger.error(url.toString());
                URLConnection connection = url.openConnection();
                if (connection instanceof JarURLConnection) {
                    JarURLConnection jarURLConnection = (JarURLConnection) connection;
                    JarFile jarFile1 = jarURLConnection.getJarFile();
                    Enumeration<JarEntry> entryEnumeration = jarFile1.entries();
                    while (entryEnumeration.hasMoreElements()) {
                        JarEntry entry1 = entryEnumeration.nextElement();
                        logger.error("3---->" + entry1.getName());
                    }
                }
            }
        }

    }

    @Test
    @Ignore
    public void testWindowPath() {
        File file = new File("C:\\Users\\Mzoro\\Desktop\\operation-1.1.jar");
        System.out.println(Pattern.compile("^\\w+:.+").matcher(file.getAbsolutePath()).find());
    }

    @Test
    @Ignore
    public void testTmp() throws URISyntaxException {
        System.out.println(getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getSchemeSpecificPart());
    }


    @Test
    @Ignore
    public void testType() throws JsonProcessingException {
        String json = "[{\"name\":\"11\"},{\"name\":\"12\"}]";
        List<JsonEntity> result = JsonUtil.toJavaList(json, JsonEntity.class);
        List<JsonEntity> result2 = JsonUtil.toJavaList(json, JsonEntity.class);
        System.out.println(result);
    }

    public static class JsonEntity {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "JsonEntity{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    @Test
    public void md5() throws NoSuchAlgorithmException {
        String s = "李兆廷";
        MessageDigest md = MessageDigest.getInstance("MD5");

        char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        for (int i = 0; i < 100000000; i++) {
            byte[] sBytes = s.getBytes();
            byte[] digest = md.digest(sBytes);

            StringBuffer sb = new StringBuffer();
            for (byte bb : digest) {
                sb.append(chars[(bb >> 4) & 15]);
                sb.append(chars[bb & 15]);
            }
            s = sb.toString();
            if (i % 100000000 == 0) {
                System.out.println(s);
            }
        }
        System.out.println(s);
    }
    @Test
    public void md52() throws NoSuchAlgorithmException {
        String s = "李兆廷";
        MessageDigest md = MessageDigest.getInstance("MD4");


        System.out.println(Arrays.toString(md.digest(s.getBytes())));
    }


}
