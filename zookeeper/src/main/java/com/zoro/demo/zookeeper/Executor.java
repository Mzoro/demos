package com.zoro.demo.zookeeper;



import org.apache.zookeeper.KeeperException;

import java.io.IOException;

/**
 * @author zhaoxw
 * Create at 2018/8/14 16:31
 * @version 1.0
 */
public class Executor {

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {

        MyWatcher watcher = new MyWatcher();
        new Thread(watcher).start();
    }

}
