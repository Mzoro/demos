package com.zoro.demo.zookeeper;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.List;

/**
 * ZooKeeper {@link ZooKeeper} 这个类提供了对 zookeeper 服务器的操作主要下面几个方法
 * 1. {@link ZooKeeper#exists(String, Watcher)},他主要作用是查看 path 是否存在,并 watch这个 path 的delete/create/setData
 * 但是它监测不到他的子节点的这些变化
 * 2. {@link ZooKeeper#getChildren(String, Watcher)} 他主要的作用是 获取某个节点下的子节点,并 watch这个节点的子节点的增加与减少,
 * 但是不会监测子节点的data变化,所有需要我们自己实现监测所有子节点变化的功能,但是这可能需要递归
 * 3. 更多方法 .....{@link ZooKeeper}
 *
 * @author zhaoxw
 * Create at 2018/8/15 11:13
 * @version 1.0
 * @see ZooKeeper
 */
public class MyWatcher implements Watcher, Runnable {

    private ZooKeeper zooKeeper;

    private List<String> childrens;

    public MyWatcher() throws IOException, KeeperException, InterruptedException {
        this.zooKeeper = new ZooKeeper("localhost:2181", 3000000, this);
        this.zooKeeper.exists("/haha", this);
        childrens = this.zooKeeper.getChildren("/haha", this);
    }

    @Override
    public void process(WatchedEvent event) {
        System.err.println(event.getPath());
        switch (event.getType()) {
            case NodeCreated:
                System.err.println("create node:" + event.getPath());
                try {
                    zooKeeper.getData(event.getPath(), this, null);
                } catch (KeeperException | InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case NodeDeleted:
                System.err.println("delete node" + event.getPath());
                break;
            case NodeDataChanged:
                try {
                    System.err.println("nodeDataChange:" + new String(zooKeeper.getData(event.getPath(), true,
                            null)));
                } catch (KeeperException | InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case NodeChildrenChanged:
                System.err.println("NodeChildrenChanged:");
                // TODO 这里可能就需要递归了,如果这里不做,那么就要在程序启动时设置监测所有节点;每次对节点操作都要重新设置
                // TODO 如何知道增加的是哪个节点呢?如果得不到,就得遍历
                break;
            default:
                System.err.println("default");

        }
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                while (true) {
                    wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}
